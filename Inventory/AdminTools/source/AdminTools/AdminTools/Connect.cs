using System;
using Extensibility;
using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio.CommandBars;
using System.Resources;
using System.Reflection;
using System.Globalization;

namespace AdminTools
{
	/// <summary>The object for implementing an Add-in.</summary>
	/// <seealso class='IDTExtensibility2' />
	public partial class Connect : IDTExtensibility2, IDTCommandTarget
	{
        private DTE2 _applicationObject;
        private AddIn _addInInstance;

		/// <summary>Implements the constructor for the Add-in object. Place your initialization code within this method.</summary>
		public Connect()
		{
		}

		/// <summary>Implements the OnConnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being loaded.</summary>
		/// <param term='application'>Root object of the host application.</param>
		/// <param term='connectMode'>Describes how the Add-in is being loaded.</param>
		/// <param term='addInInst'>Object representing this Add-in.</param>
		/// <seealso class='IDTExtensibility2' />
		public void OnConnection(object application, ext_ConnectMode connectMode, object addInInst, ref Array custom)
		{
                try
                {
                    _applicationObject = (DTE2)application;
                    _addInInstance = (AddIn)addInInst;

                    switch (connectMode)
                    {
                        //Loaded for user interface setup
                        case ext_ConnectMode.ext_cm_UISetup:

                            // Initialize the UI of the add-in
                            InitializeUISettings();
                            AddAdminToolsMenu();
                            break;

                        //Loaded when Visual Studio .NET started
                        case ext_ConnectMode.ext_cm_Startup:

                            // The add-in was marked to load on startup
                            // Do nothing at this point because the IDE may not be fully initialized
                            // Visual Studio will call OnStartupComplete when fully initialized
                            break;

                        //Loaded after Visual Studio .NET started.
                        case ext_ConnectMode.ext_cm_AfterStartup:
                            // The add-in was loaded by hand after startup using the Add-In Manager
                            // Initialize it in the same way that when is loaded on startup
                            InitializeAddIn();
                            break;
                    }
                }
                catch (System.Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
		}

		/// <summary>Implements the OnDisconnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being unloaded.</summary>
		/// <param term='disconnectMode'>Describes how the Add-in is being unloaded.</param>
		/// <param term='custom'>Array of parameters that are host application specific.</param>
		/// <seealso class='IDTExtensibility2' />
		public void OnDisconnection(ext_DisconnectMode disconnectMode, ref Array custom)
		{
		}

		/// <summary>Implements the OnAddInsUpdate method of the IDTExtensibility2 interface. Receives notification when the collection of Add-ins has changed.</summary>
		/// <param term='custom'>Array of parameters that are host application specific.</param>
		/// <seealso class='IDTExtensibility2' />		
		public void OnAddInsUpdate(ref Array custom)
		{
		}

		/// <summary>Implements the OnStartupComplete method of the IDTExtensibility2 interface. Receives notification that the host application has completed loading.</summary>
		/// <param term='custom'>Array of parameters that are host application specific.</param>
		/// <seealso class='IDTExtensibility2' />
		public void OnStartupComplete(ref Array custom)
		{
		}

		/// <summary>Implements the OnBeginShutdown method of the IDTExtensibility2 interface. Receives notification that the host application is being unloaded.</summary>
		/// <param term='custom'>Array of parameters that are host application specific.</param>
		/// <seealso class='IDTExtensibility2' />
		public void OnBeginShutdown(ref Array custom)
		{
            //_applicationObject.Commands.Item("AdminTools.Connect.AdminTools").Delete();
            //AddinName.Connect.CommandName
		}
		

	}
}