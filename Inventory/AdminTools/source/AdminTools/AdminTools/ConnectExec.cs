﻿using System;
using Extensibility;
using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio.CommandBars;
using System.Resources;
using System.Reflection;
using System.Globalization;

// debugging
using System.Diagnostics;

namespace AdminTools
{
    public partial class Connect : IDTExtensibility2, IDTCommandTarget
    {
        /// <summary>Implements the Exec method of the IDTCommandTarget interface. This is called when the command is invoked.</summary>
        /// <param term='commandName'>The name of the command to execute.</param>
        /// <param term='executeOption'>Describes how the command should be run.</param>
        /// <param term='varIn'>Parameters passed from the caller to the command handler.</param>
        /// <param term='varOut'>Parameters passed from the command handler to the caller.</param>
        /// <param term='handled'>Informs the caller if the command was handled or not.</param>
        /// <seealso class='Exec' />
        public void Exec(string commandName, vsCommandExecOption executeOption, ref object varIn, ref object varOut, ref bool handled)
        {
            handled = false;

            if ((executeOption == vsCommandExecOption.vsCommandExecOptionDoDefault))
            {
                //if (commandName == _addInInstance.ProgID + "." + "Inventory_ViewServers_CommandName")
                //{

                //    handled = true;

                //}
                //if (commandName == _addInInstance.ProgID + "." + "Inventory_AddServer_CommandName")
                //{

                //    handled = true;

                //}

                //if (commandName == _addInInstance.ProgID + "." + "Inventory_RemoveServer_CommandName")
                //{


                //    handled = true;

                //}


                if (commandName == _addInInstance.ProgID + "." + "AdminTools_ServerInventory_CommandName")
                {
                    ShowServerInventory();

                    handled = true;
                }
            }
        }
    }
}
