﻿using System;
using Extensibility;
using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio.CommandBars;
using System.Resources;
using System.Reflection;
using System.Globalization;



namespace AdminTools
{
    public partial class Connect : IDTExtensibility2, IDTCommandTarget
    {
        /// <summary>Implements the QueryStatus method of the IDTCommandTarget interface. This is called when the command's availability is updated</summary>
        /// <param term='commandName'>The name of the command to determine state for.</param>
        /// <param term='neededText'>Text that is needed for the command.</param>
        /// <param term='status'>The state of the command in the user interface.</param>
        /// <param term='commandText'>Text requested by the neededText parameter.</param>
        /// <seealso class='Exec' />
        public void QueryStatus(string commandName, vsCommandStatusTextWanted neededText, ref vsCommandStatus status, ref object commandText)
        {
            //if(neededText == vsCommandStatusTextWanted.vsCommandStatusTextWantedNone)
            //{
            //    if(commandName == "AdminTools.Connect.AdminTools")
            //    {
            //        status = (vsCommandStatus)vsCommandStatus.vsCommandStatusSupported|vsCommandStatus.vsCommandStatusEnabled;
            //        return;
            //    }
            //}


            if (neededText == vsCommandStatusTextWanted.vsCommandStatusTextWantedNone)
            {
                //if (commandName == _addInInstance.ProgID + "." + "Inventory_ViewServers_CommandName")
                //{
                //    status = vsCommandStatus.vsCommandStatusSupported | vsCommandStatus.vsCommandStatusEnabled;
                //}
                //else
                //{
                //    status = vsCommandStatus.vsCommandStatusUnsupported;
                //}

                //if (commandName == _addInInstance.ProgID + "." + "Inventory_AddServer_CommandName")
                //{
                //    status = vsCommandStatus.vsCommandStatusSupported | vsCommandStatus.vsCommandStatusEnabled;
                //}
                //else
                //{
                //    status = vsCommandStatus.vsCommandStatusUnsupported;
                //}

                //if (commandName == _addInInstance.ProgID + "." + "Inventory_RemoveServer_CommandName")
                //{
                //    status = vsCommandStatus.vsCommandStatusSupported | vsCommandStatus.vsCommandStatusEnabled;
                //}
                //else
                //{
                //    status = vsCommandStatus.vsCommandStatusUnsupported;
                //}

                if (commandName == _addInInstance.ProgID + "." + "AdminTools_ServerInventory_CommandName")
                {
                    status = vsCommandStatus.vsCommandStatusSupported | vsCommandStatus.vsCommandStatusEnabled;
                }
                else
                {
                    status = vsCommandStatus.vsCommandStatusUnsupported;
                }
            }
        }
    }
}
