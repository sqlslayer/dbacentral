﻿using System;
using Extensibility;
using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio.CommandBars;
using System.Resources;
using System.Reflection;
using System.Globalization;

// debugging
using System.Diagnostics;

// config
using System.Configuration;

// registry
using Microsoft.Win32;

// settings
using AdminTools.Properties;

// WPF user controls
using AdminToolsUI;

namespace AdminTools
{
    public partial class Connect : IDTExtensibility2, IDTCommandTarget
    {
        private EnvDTE.Window myToolWindow;


        private void InitializeUISettings()
        {
        }

        private void AddAdminToolsMenu()
        {
            // get all commands
            Commands2 commands = (Commands2)_applicationObject.Commands;

            // CommandBars that will be created by the add-in
            CommandBar adminToolsCommandBar = null;
            CommandBarPopup adminToolsCommandBarPopup = null;

            // Commands that will be created
            Command cmdInventoryView = null;
            Command cmdInventoryAddServer = null;
            Command cmdInventoryRemoveServer = null;
            Command cmdAdminLaunchPad = null;

            // Built-in commandbars of Visual Studio
            CommandBar menuCommandBar = null;
            CommandBar toolsCommandBar = null;

            // The collection of Visual Studio commandbars
            CommandBars commandBars = null;

            // Other variables
            object[] contextUIGuids = new object[] { };


            // delete commands that were created the last time it was run ( DEBUGGING ) 
            try
            {

                foreach (Command cmd in _applicationObject.Commands)
                {
                    if (cmd.Name.StartsWith(_addInInstance.ProgID.ToString()))
                    {
                        cmd.Delete();
                    }
                }

            }
            catch { }


            try
            {
                // Try to retrieve the commands, just in case they're already created, ignoring the 
                // exception that would happen if the commands were not created yet.
                try { cmdInventoryView = _applicationObject.Commands.Item(_addInInstance.ProgID + "." + "Inventory_ViewServers_CommandName"); } catch { }
                try { cmdInventoryAddServer = _applicationObject.Commands.Item(_addInInstance.ProgID + "." + "Inventory_AddServer_CommandName"); } catch { }
                try { cmdInventoryRemoveServer = _applicationObject.Commands.Item(_addInInstance.ProgID + "." + "Inventory_RemoveServer_CommandName"); } catch { }
                try { cmdAdminLaunchPad = _applicationObject.Commands.Item(_addInInstance.ProgID + "." + "AdminTools_ServerInventory_CommandName"); } catch { }


                // Add the commands if they don't exist
                //if (cmdInventoryView == null)
                //{
                //    cmdInventoryView = commands.AddNamedCommand2(_addInInstance
                //                                                ,"Inventory_ViewServers_CommandName"
                //                                                ,"Inventory_ViewServers_CommandCaption"
                //                                                ,"Inventory_ViewServers_CommandTooltip"
                //                                                ,true
                //                                                ,59
                //                                                ,ref contextUIGuids
                //                                                ,(int)(vsCommandStatus.vsCommandStatusSupported | vsCommandStatus.vsCommandStatusEnabled));
                //}
                //if (cmdInventoryAddServer == null)
                //{
                //    cmdInventoryAddServer = commands.AddNamedCommand2(_addInInstance
                //                                                     ,"Inventory_AddServer_CommandName"
                //                                                     ,"Inventory_AddServer_CommandCaption"
                //                                                     ,"Inventory_AddServer_CommandTooltip"
                //                                                     ,true
                //                                                     ,59
                //                                                     ,ref contextUIGuids
                //                                                     ,(int)(vsCommandStatus.vsCommandStatusSupported | vsCommandStatus.vsCommandStatusEnabled));
                //}
                //if (cmdInventoryRemoveServer == null)
                //{
                //    cmdInventoryRemoveServer = commands.AddNamedCommand2(_addInInstance
                //                                                        ,"Inventory_RemoveServer_CommandName"
                //                                                        ,"Inventory_RemoveServer_CommandCaption"
                //                                                        ,"Inventory_RemoveServer_CommandTooltip"
                //                                                        ,true
                //                                                        ,59
                //                                                        ,ref contextUIGuids
                //                                                        ,(int)(vsCommandStatus.vsCommandStatusSupported | vsCommandStatus.vsCommandStatusEnabled));
                //}
                if (cmdAdminLaunchPad == null)
                {
                    cmdAdminLaunchPad = commands.AddNamedCommand2(_addInInstance
                                                                        ,"AdminTools_ServerInventory_CommandName"
                                                                        ,"AdminTools_ServerInventory_CommandCaption"
                                                                        ,"AdminTools_ServerInventory_CommandTooltip"
                                                                        ,false
                                                                        ,1
                                                                        ,ref contextUIGuids
                                                                        ,(int)(vsCommandStatus.vsCommandStatusSupported | vsCommandStatus.vsCommandStatusEnabled));
                }


                // ------------------------------------------------------------------------------------
                // New AdminTools Menu
                // ------------------------------------------------------------------------------------

                // Retrieve the collection of commandbars
                // Note:
                // - In VS 2005 or higher (which uses the new Microsoft.VisualStudio.CommandBars.dll reference) 
                //   DTE.CommandBars returns an Object type, so we do need a cast to CommandBars
                commandBars = (CommandBars)_applicationObject.CommandBars;


                // Retrieve some built-in commandbars
                menuCommandBar = commandBars["MenuBar"];
                toolsCommandBar = GetCommandBarPopup(menuCommandBar, "Tools");

                // Set up AdminTools menu if it's not created
                try { adminToolsCommandBar = GetCommandBarPopup(menuCommandBar, "AdminTools"); } catch { }
                if (adminToolsCommandBar == null)
                {
                    // Add a new commandbar popup 
                    adminToolsCommandBar = (CommandBar)_applicationObject.Commands.AddCommandBar("AdminTools", vsCommandBarType.vsCommandBarTypeMenu, menuCommandBar, ((CommandBarControl)toolsCommandBar.Parent).Index + 1);

                    // Get the actual commandbar popup
                    adminToolsCommandBarPopup = (CommandBarPopup)adminToolsCommandBar.Parent;

                    // Change some commandbar popup properties
                    adminToolsCommandBarPopup.Caption = "AdminTools";


                    // add ServerInventory button
                    CommandBarButton adminLaunchPadCmdBarButton = (CommandBarButton)cmdAdminLaunchPad.AddControl(adminToolsCommandBar, adminToolsCommandBar.Controls.Count + 1);
                    adminLaunchPadCmdBarButton.Caption = "Server Inventory";


                    //// CommandBar -> ServerInventory
                    //CommandBar serverInventoryCommandBar = (CommandBar)_applicationObject.Commands.AddCommandBar("ServerInventory", vsCommandBarType.vsCommandBarTypeMenu, adminToolsCommandBar, adminToolsCommandBar.Controls.Count + 1);
                    //CommandBarPopup serverInventoryCommandBarPopup = (CommandBarPopup)serverInventoryCommandBar.Parent;
                    //serverInventoryCommandBarPopup.Caption = "Server Inventory";
                    //serverInventoryCommandBarPopup.Enabled = true;

                    //// add view button
                    //CommandBarButton inventoryViewServersCmdBarButton = (CommandBarButton)cmdInventoryView.AddControl(serverInventoryCommandBar, serverInventoryCommandBar.Controls.Count + 1);
                    //inventoryViewServersCmdBarButton.Caption = "View Servers";
                    //inventoryViewServersCmdBarButton.Enabled = true;
                    //inventoryViewServersCmdBarButton.Visible = true;


                    //// add remove button
                    //CommandBarButton inventoryRemoveServerCmdBarPopupButton = (CommandBarButton)cmdInventoryRemoveServer.AddControl(serverInventoryCommandBar, serverInventoryCommandBar.Controls.Count + 1);
                    //inventoryRemoveServerCmdBarPopupButton.Caption = "Remove Server";
                    //inventoryRemoveServerCmdBarPopupButton.Enabled = true;
                    //inventoryRemoveServerCmdBarPopupButton.Visible = true;

                    //// add new button
                    //CommandBarButton inventoryNewServerCmdBarPopupButton = (CommandBarButton)cmdInventoryAddServer.AddControl(serverInventoryCommandBar, serverInventoryCommandBar.Controls.Count + 1);
                    //inventoryNewServerCmdBarPopupButton.Caption = "Add Server";
                    //inventoryNewServerCmdBarPopupButton.Enabled = true;
                    //inventoryNewServerCmdBarPopupButton.Visible = true;
                }


                //// Buttons that will be created on a toolbars/commandbar popups created by the add-in
                //// We don't need to keep them at class level to remove them when the add-in is unloaded 
                //// because we will remove the whole toolbars/commandbar popups
                //CommandBarButton inventoryViewServersCmdBarPopupButton = null;
                //CommandBarButton inventoryAddServerCmdBarPopupButton = null;
                //CommandBarButton inventoryRemoveServerCmdBarPopupButton = null;

            }
            catch (System.Exception e)
            {
                Console.WriteLine(e.ToString());
            }

        }



        private void ShowServerInventory()
        {
            try
            {
                Window myToolWindow;
                object myUserControlObject = null;  // Placeholder object; will eventually refer to the user control hosted by the user control???
                //LaunchPad myUserControl = new LaunchPad();
                Windows2 allToolWindows = (Windows2)_applicationObject.Windows;


                Assembly asm = Assembly.GetAssembly(typeof(Inventory));
                string assemblyPath = asm.Location;
                string className = typeof(ctlInventory).Namespace + "." + typeof(Inventory).Name;
                string guid = "{C0C40530-6A7F-40C8-A55A-BA5185CF62D0}";
                string caption = "Server Inventory";



                myToolWindow = allToolWindows.CreateToolWindow2(_addInInstance, assemblyPath, className, caption, guid, myUserControlObject);
                if (myToolWindow != null)
                {
                    myToolWindow.Visible = true;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }



        private CommandBar GetCommandBarPopup(CommandBar parentCommandBar, string commandBarPopupName)
        {
            CommandBar commandBar = null;
            CommandBarPopup commandBarPopup;

            foreach (CommandBarControl commandBarControl in parentCommandBar.Controls)
            {
                if (commandBarControl.Type == MsoControlType.msoControlPopup)
                {
                    commandBarPopup = (CommandBarPopup)commandBarControl;

                    if (commandBarPopup.CommandBar.Name == commandBarPopupName)
                    {
                        commandBar = commandBarPopup.CommandBar;
                        break;
                    }
                }
            }
            return commandBar;
        }

        private void InitializeAddIn()
        {
            // Initialize non-UI add-in
        }




    }
}
