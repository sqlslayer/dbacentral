﻿using System;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

using AdminToolsUI.Helpers;
using AdminToolsUI.ViewModels;
using DBACentral.Data;

namespace AdminToolsUI.Controls
{
    /// <summary>
    /// Main control for inventory.
    /// </summary>
    public partial class Inventory : UserControl
    {
        #region Members
        private InventoryViewModel _viewModel;
        public static RoutedCommand GlobalSearchShortcutCommand = new RoutedCommand();
        #endregion
        

        #region Constructors
        public Inventory()
        {
                InitializeComponent();

                if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                {
                    BackgroundWorker loadListWorker = new BackgroundWorker();

                    loadListWorker.DoWork += (o, ea) =>
                    {
                        Dispatcher.Invoke((Action)(() =>
                        {
                            try
                            {
                                _viewModel = new InventoryViewModel();
                                this.DataContext = _viewModel;

                                RefreshMachineList();
                                RefreshClusterList();
                                RefreshInstanceList();
                                RefreshEnvironmentList();
                                RefreshLocationList();
                            }
                            catch (SqlException e)
                            {
                                MessageBoxResult result = new MessageBoxResult();

                                bsyLoadingMachineList.IsBusy = false;
                                bsyLoadingClusterList.IsBusy = false;
                                bsyLoadingInstanceList.IsBusy = false;
                                bsyLoadingEnvironmentList.IsBusy = false;
                                bsyLoadingLocationList.IsBusy = false;
                                if (e.InnerException != null)
                                {
                                    result = MessageBox.Show(string.Format("An error has occurred while attemping a connection to the datbase: {0} Check the Application Event Log for more information.", e.InnerException.Message)
                                                             , "Inventory: Error", MessageBoxButton.OK, MessageBoxImage.Error);
                                    LogHelper.WriteToLog("An error has occurred while attemping a connection to the datbase: " + e.InnerException.ToString(), EventLogEntryType.Error);
                                }
                                else
                                {
                                    result = MessageBox.Show(string.Format("An error has occurred while attemping a connection to the datbase: {0} Check the Application Event Log for more information.", e.Message)
                                                            , "Inventory: Error", MessageBoxButton.OK, MessageBoxImage.Error);
                                    LogHelper.WriteToLog(string.Format("An error has occurred while attemping a connection to the datbase.  Stack dump: {0}", e.ToString()), EventLogEntryType.Error);
                                }
                            }
                            catch (Exception e)
                            {
                                MessageBoxResult result = new MessageBoxResult();

                                bsyLoadingMachineList.IsBusy = false;
                                bsyLoadingClusterList.IsBusy = false;
                                bsyLoadingInstanceList.IsBusy = false;
                                bsyLoadingEnvironmentList.IsBusy = false;
                                bsyLoadingLocationList.IsBusy = false;
                                if (e.InnerException != null)
                                {
                                    result = MessageBox.Show(string.Format("An error has occurred: {0} Check the Application Event Log for more information.", e.InnerException.Message)
                                                            , "Inventory: Error", MessageBoxButton.OK, MessageBoxImage.Error);
                                    LogHelper.WriteToLog(string.Format("An error occurred during application startup.  Stack dump: {0}", e.InnerException.ToString()), EventLogEntryType.Error);
                                }
                                else
                                {
                                    result = MessageBox.Show(string.Format("An error has occurred: {0} Check the Application Event Log for more information.", e.Message)
                                                            , "Inventory: Error", MessageBoxButton.OK, MessageBoxImage.Error);
                                    LogHelper.WriteToLog(string.Format("An error occurred during application startup.  Stack dump: {0}", e.ToString()), EventLogEntryType.Error);
                                }

                                if (result == MessageBoxResult.OK)
                                {
                                    System.Environment.Exit(0);
                                }
                            }

                            GlobalSearchShortcutCommand.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control));
                        }));
                    };

                    loadListWorker.RunWorkerCompleted += (o, ea) =>
                    {
                        bsyLoadingMachineList.IsBusy = false;
                        bsyLoadingClusterList.IsBusy = false;
                        bsyLoadingInstanceList.IsBusy = false;
                        bsyLoadingEnvironmentList.IsBusy = false;
                        bsyLoadingLocationList.IsBusy = false;
                    };

                    bsyLoadingMachineList.IsBusy = true;
                    bsyLoadingClusterList.IsBusy = true;
                    bsyLoadingInstanceList.IsBusy = true;
                    bsyLoadingEnvironmentList.IsBusy = true;
                    bsyLoadingLocationList.IsBusy = true;

                    loadListWorker.RunWorkerAsync();
                }
        }
        #endregion

        #region Methods
        private void MainTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source == MainTabControl && tabGlobalSearch.Visibility == Visibility.Visible && MainTabControl.SelectedItem != tabGlobalSearch)
            {
                tabGlobalSearch.Visibility  = Visibility.Collapsed;
            }
        }

        private void RefreshExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            txtGlobalSearch.Focus();
            e.Handled = true;
        }

        private void CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }

        private void txtGlobalSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            Search();
        }

        private void txtGlobalSearch_GotFocus(object sender, RoutedEventArgs e)
        {
            Search();
        }

        private void Search()
        {
            if (txtGlobalSearch.Text.Length > 0)
            {
                BackgroundWorker searchWorker = new BackgroundWorker();

                tabGlobalSearch.Visibility = Visibility.Visible;
                tabGlobalSearch.IsSelected = true;

                searchWorker.DoWork += (o, ea) =>
                {
                    Dispatcher.Invoke((Action)(() =>
                    {
                        _viewModel.Search(txtGlobalSearch.Text.Trim());
                    }));
                };

                searchWorker.RunWorkerCompleted += (o, ea) =>
                {
                    bsyLoadingSearchResults.IsBusy = false;
                };

                bsyLoadingSearchResults.IsBusy = true;

                searchWorker.RunWorkerAsync();
            }
        }

        private void mnuCopySearchItemName_Click(object sender, RoutedEventArgs e)
        {
            if (lbxMachines.SelectedIndex > -1 && _viewModel.SelectedMachine != null)
            {
                if (lbxSearchResults.SelectedItem.GetType() == typeof(Machine))
                {
                    Clipboard.SetDataObject(((Machine)lbxSearchResults.SelectedItem).MachineName, true);
                }
                else if (lbxSearchResults.SelectedItem.GetType() == typeof(Cluster))
                {
                    Clipboard.SetDataObject(((Cluster)lbxSearchResults.SelectedItem).ClusterName, true);
                }
                else if (lbxSearchResults.SelectedItem.GetType() == typeof(SQLServer))
                {
                    Clipboard.SetDataObject(((SQLServer)lbxSearchResults.SelectedItem).FullName, true);
                }
                else if (lbxSearchResults.SelectedItem.GetType() == typeof(Person))
                {
                    Clipboard.SetDataObject(((Person)lbxSearchResults.SelectedItem).FullName, true);
                }
                else if (lbxSearchResults.SelectedItem.GetType() == typeof(DBACentral.Data.Environment))
                {
                    Clipboard.SetDataObject(((DBACentral.Data.Environment)lbxSearchResults.SelectedItem).EnvironmentName, true);
                }
                else if (lbxSearchResults.SelectedItem.GetType() == typeof(Location))
                {
                    Clipboard.SetDataObject(((Location)lbxSearchResults.SelectedItem).LocationName, true);
                }
            }
        }
        #endregion
    }
}