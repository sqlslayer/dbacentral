﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

using AdminToolsUI.Views;
using DBACentral.Data;

namespace AdminToolsUI.Controls
{
    public partial class Inventory : UserControl
    {
        #region Left Side
        private void DecommissionClusterDialog()
        {
            if (lbxClusters.SelectedIndex > -1)
            {
                if (_viewModel.SelectedCluster.IsActive)
                {
                    winDecommission decommissionWindow = new winDecommission(_viewModel.SelectedCluster.ClusterName, _viewModel.SelectedCluster.Notes);
                    decommissionWindow.ShowDialog();

                    if (decommissionWindow.DialogResult == true)
                    {
                        MessageBoxResult result = MessageBox.Show("Are you sure you want to decommission " + _viewModel.SelectedCluster.ClusterName + "?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                        if (result == MessageBoxResult.No)
                        {
                            return;
                        }

                        _viewModel.SelectedCluster.Notes += " --Decommissioned " + DateTime.Now + ": " + decommissionWindow.txtNotes.Text;
                        _viewModel.SelectedCluster.IsActive = false;
                        _viewModel.Save();

                        BackgroundWorker worker = new BackgroundWorker();
                        worker.DoWork += (o, ea) =>
                        {
                            Dispatcher.Invoke((Action)(() =>
                            {
                                RefreshClusterList();
                            }));
                        };
                        worker.RunWorkerCompleted += (o, ea) =>
                        {
                            bsyLoadingClusterList.IsBusy = false;
                        };

                        bsyLoadingClusterList.IsBusy = true;
                        worker.RunWorkerAsync();
                    }
                }
            }
        }
        private void EditClusterDialog()
        {
            bool isListDirty = false;

            if (lbxClusters.SelectedIndex > -1)
            {
                winEditCluster editClusterWindow = new winEditCluster(_viewModel);
                _viewModel.PossibleEnvironmentsRefresh(new List<DBACentral.Data.Environment>()); // get of list all environments
                editClusterWindow.ShowDialog();

                if (editClusterWindow.DialogResult == true)
                {
                    if (editClusterWindow.cbxDecommissioned.IsChecked == true && _viewModel.SelectedCluster.IsActive)
                    {
                        DecommissionClusterDialog();

                        // check that the user actually decided to decommission the cluster
                        if (!_viewModel.SelectedCluster.IsActive)
                        {
                            isListDirty = true;
                        }
                    }
                    else if (editClusterWindow.cbxDecommissioned.IsChecked == false && !_viewModel.SelectedCluster.IsActive)
                    {
                        _viewModel.SelectedCluster.IsActive = true;
                        _viewModel.SelectedCluster.Notes += " --ReEnabled " + DateTime.Now;
                        isListDirty = true;
                    }

                    _viewModel.Save();
                    _viewModel.SelectedClusterRefresh();
                    editClusterWindow.DataContext = null;

                    // refresh list if we decomissioned or enabled a cluster
                    if (isListDirty)
                    {
                        RefreshClusterList();
                    }
                }
                else
                {
                    editClusterWindow.DataContext = null;
                }
            }
        }
        private void RefreshClusterList()
        {
            _viewModel.ClusterListRefresh(cbxShowDecomClusters.IsChecked.Value);
        }

        #region Checkbox Events
        private void cbxShowDecomClusters_Checked(object sender, RoutedEventArgs e)
        {
            RefreshClusterList();
        }
        private void cbxShowDecomClusters_Unchecked(object sender, RoutedEventArgs e)
        {
            RefreshClusterList();
        }
        #endregion

        #region Listbox Events
        private void lbxClusters_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            EditClusterDialog();
        }
        private void lbxClusters_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbxClusters.SelectedIndex > -1)
            {
                _viewModel.SelectedCluster = lbxClusters.SelectedItem as Cluster;
                _viewModel.SelectedClusterRefresh();
                _viewModel.SelectedClusterContactListRefresh();
            }
        }
        #endregion

        #region Menu Events
        private void mnuCopyClusterName_Click(object sender, RoutedEventArgs e)
        {
            if (lbxClusters.SelectedIndex > -1 && _viewModel.SelectedCluster != null)
            {
                Clipboard.SetDataObject(_viewModel.SelectedCluster.ClusterName, true);
            }
        }
        private void mnuCopyClusterDetails_Click(object sender, RoutedEventArgs e)
        {
            if (lbxClusters.SelectedIndex > -1)
            {
                StringBuilder sb = new StringBuilder();


                sb.Append(System.Environment.NewLine).Append("-- CLUSTER --").Append(System.Environment.NewLine);
                sb.Append("Name: ").Append(_viewModel.SelectedCluster.ClusterName).Append(System.Environment.NewLine);
                sb.Append("IP: ").Append(_viewModel.SelectedCluster.IP).Append(System.Environment.NewLine);
                sb.Append("Quorum Type: ").Append(_viewModel.SelectedCluster.QuorumType).Append(System.Environment.NewLine);
                sb.Append("Quorum Path: ").Append(_viewModel.SelectedCluster.QuorumPath).Append(System.Environment.NewLine);
                sb.Append("Description: ").Append(_viewModel.SelectedCluster.Description).Append(System.Environment.NewLine);
                sb.Append("Notes: " + _viewModel.SelectedCluster.Notes).Append(System.Environment.NewLine);

                sb.Append(System.Environment.NewLine).Append("-- NODES --").Append(System.Environment.NewLine);
                sb.Append(string.Join(System.Environment.NewLine, _viewModel.SelectedCluster.Machines.Where(p => p.IsActive).Select(p => p.MachineName)));

                sb.Append(System.Environment.NewLine).Append(System.Environment.NewLine).Append("-- INSTANCES --").Append(System.Environment.NewLine);
                sb.Append(string.Join(System.Environment.NewLine, _viewModel.SelectedCluster.DBMSInstances.OfType<SQLServer>().Where(p => p.IsActive).Select(p => p.FullName)));

                Clipboard.SetDataObject(sb.ToString(), true);
                MessageBox.Show("Copied to clipboard.", _viewModel.SelectedCluster.ClusterName, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        private void mnuDecommissionCluster_Click(object sender, RoutedEventArgs e)
        {
            DecommissionClusterDialog();
        }
        private void mnuEditClusterName_Click(object sender, RoutedEventArgs e)
        {
            EditClusterDialog();
        }
        #endregion
        #endregion

        #region Right Side
        #region Button Events
        private void btnClusterAddContact_Click(object sender, RoutedEventArgs e)
        {
            if (lbxClusters.SelectedIndex > -1)
            {
                int itemTypeID = _viewModel.Context.ItemTypes.Where(p => p.ItemTypeName == "Cluster").FirstOrDefault().ItemTypeID;

                winAddContact addContactWindow = new winAddContact(_viewModel, _viewModel.SelectedCluster.ClusterName);
                addContactWindow.ShowDialog();

                if (addContactWindow.DialogResult.Value == true)
                {
                    var newContact = new Contact();
                    newContact.Person = addContactWindow.cbxContact.SelectedItem as Person;
                    newContact.ContactType = addContactWindow.cbxContactType.SelectedItem as ContactType;
                    newContact.ItemType = _viewModel.Context.ItemTypes.Where(p => p.ItemTypeName == "Cluster").FirstOrDefault();
                    newContact.ItemID = _viewModel.SelectedCluster.ClusterID;
                    newContact.Notes = addContactWindow.txtNotes.Text;
                    _viewModel.Context.Contacts.AddObject(newContact);
                    _viewModel.Save();

                    _viewModel.SelectedClusterContactListRefresh();
                }
            }
        }
        #endregion

        #region Listbox Events
        private void lbxClusterContacts_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            // TODO
        }
        private void lbxClusterInstances_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lbxClusterInstances.SelectedIndex > -1)
            {
                if (!(lbxClusterInstances.SelectedItem as SQLServer).IsActive && cbxShowDecomInstances.IsChecked == false)
                {
                    cbxShowDecomInstances.IsChecked = true;
                    RefreshInstanceList();
                }

                MainTabControl.SelectedItem = MainTabControl.FindName("tabInstances");
                lbxInstances.SelectedItem = lbxClusterInstances.SelectedItem;
                lbxClusterInstances.SelectedIndex = -1;
            }
        }
        private void lbxClusterMachines_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lbxClusterMachines.SelectedIndex > -1)
            {
                if (!(lbxClusterMachines.SelectedItem as Machine).IsActive && cbxShowDecomMachines.IsChecked == false)
                {
                    cbxShowDecomMachines.IsChecked = true;
                    RefreshMachineList();
                }

                MainTabControl.SelectedItem = MainTabControl.FindName("tabMachines");
                lbxMachines.SelectedItem = lbxClusterMachines.SelectedItem;
                lbxClusterMachines.SelectedIndex = -1;
            }
        }
        #endregion

        #region Menu Events
        private void mnuEditClusterContact_Click(object sender, RoutedEventArgs e)
        {
            if (lbxClusterContacts.SelectedIndex > -1)
            {
                _viewModel.EditableContact = lbxClusterContacts.SelectedItem as Contact;
                winEditContact editContactWindow = new winEditContact(_viewModel);
                editContactWindow.ShowDialog();

                if (editContactWindow.DialogResult == true)
                {
                    (lbxClusterContacts.SelectedItem as Contact).ContactType = editContactWindow.cbxContactType.SelectedItem as ContactType;
                    _viewModel.Save();
                    _viewModel.SelectedClusterContactListRefresh();
                }
            }
        }
        private void mnuRemoveClusterContact_Click(object sender, RoutedEventArgs e)
        {
            if (lbxClusterContacts.SelectedIndex > -1)
            {
                MessageBoxResult result = MessageBox.Show("Are you sure you want to remove " + (lbxClusterContacts.SelectedItem as Contact).Person.FullName + "?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                if (result == MessageBoxResult.No)
                {
                    return;
                }

                _viewModel.Context.Contacts.DeleteObject(lbxClusterContacts.SelectedItem as Contact);
                _viewModel.Save();
                _viewModel.SelectedClusterContactListRefresh();
            }
        }
        #endregion
        #endregion
    }
}