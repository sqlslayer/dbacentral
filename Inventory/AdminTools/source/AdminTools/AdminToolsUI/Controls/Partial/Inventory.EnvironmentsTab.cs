﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

using AdminToolsUI.Views;
using DBACentral.Data;

namespace AdminToolsUI.Controls
{
    public partial class Inventory : UserControl
    {
        #region Left Side
        private void DecommissionEnvironmentDialog()
        {
            if (lbxEnvironments.SelectedIndex > -1)
            {
                if (_viewModel.SelectedEnvironment.IsActive)
                {
                    winDecommission decommissionWindow = new winDecommission(_viewModel.SelectedEnvironment.EnvironmentName, (_viewModel.SelectedEnvironment.Notes));
                    decommissionWindow.ShowDialog();

                    if (decommissionWindow.DialogResult == true)
                    {
                        MessageBoxResult result = MessageBox.Show("Are you sure you want to decommission " + _viewModel.SelectedEnvironment.EnvironmentName + "?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                        if (result == MessageBoxResult.No)
                        {
                            return;
                        }

                        _viewModel.SelectedEnvironment.Notes += " --Decommissioned " + DateTime.Now + ": " + decommissionWindow.txtNotes.Text;
                        _viewModel.SelectedEnvironment.IsActive = false;
                        _viewModel.Save();


                        BackgroundWorker worker = new BackgroundWorker();

                        worker.DoWork += (o, ea) =>
                        {
                            Dispatcher.Invoke((Action)(() =>
                            {
                                RefreshEnvironmentList();
                            }));
                        };
                        worker.RunWorkerCompleted += (o, ea) =>
                        {
                            bsyLoadingEnvironmentList.IsBusy = false;
                        };

                        bsyLoadingEnvironmentList.IsBusy = true;
                        worker.RunWorkerAsync();
                    }
                }
                else
                {
                    MessageBox.Show(_viewModel.SelectedEnvironment.EnvironmentName + " is already decommissioned.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        private void EditEnvironmentDialog()
        {
            if (lbxEnvironments.SelectedIndex > -1)
            {
                winEditEnvironment editEnvDialog = new winEditEnvironment(_viewModel);
                bool isListDirty = false;
                editEnvDialog.ShowDialog();

                if (editEnvDialog.DialogResult == true)
                {
                    if (editEnvDialog.cbxDecommissioned.IsChecked == true && _viewModel.SelectedEnvironment.IsActive)
                    {
                        DecommissionEnvironmentDialog();

                        // check that the user really decommissioned the environment
                        if (!_viewModel.SelectedEnvironment.IsActive)
                        {
                            isListDirty = true;
                        }
                    }
                    else if (editEnvDialog.cbxDecommissioned.IsChecked == false && !_viewModel.SelectedEnvironment.IsActive)
                    {
                        _viewModel.SelectedEnvironment.IsActive = true;
                        _viewModel.SelectedEnvironment.Notes += " --ReEnabled " + DateTime.Now;
                        isListDirty = true;
                    }

                    _viewModel.Save();
                    _viewModel.SelectedEnvironmentRefresh();

                    if (isListDirty)
                    {
                        BackgroundWorker worker = new BackgroundWorker();

                        worker.DoWork += (o, ea) =>
                        {
                            Dispatcher.Invoke((Action)(() =>
                            {
                                RefreshEnvironmentList();
                            }));
                        };
                        worker.RunWorkerCompleted += (o, ea) =>
                        {
                            bsyLoadingEnvironmentList.IsBusy = false;
                        };

                        bsyLoadingEnvironmentList.IsBusy = true;
                        worker.RunWorkerAsync();
                    }
                }
            }
        }
        private void RefreshEnvironmentList()
        {
            _viewModel.EnvironmentListRefresh(cbxShowDecomEnvs.IsChecked.Value);
        }

        #region Button Events
        private void btnNewEnvironment_Click(object sender, RoutedEventArgs e)
        {
            winNewEnvironment newEnvWindow = new winNewEnvironment(_viewModel);
            newEnvWindow.ShowDialog();

            if (newEnvWindow.DialogResult == true)
            {
                RefreshEnvironmentList();
            }
        }
        #endregion

        #region Checkbox Events
        private void cbxShowDecomEnvs_Checked(object sender, RoutedEventArgs e)
        {
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += (o, ea) =>
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    RefreshEnvironmentList();
                }));
            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                bsyLoadingEnvironmentList.IsBusy = false;
            };

            bsyLoadingEnvironmentList.IsBusy = true;
            worker.RunWorkerAsync();
        }
        private void cbxShowDecomEnvs_Unchecked(object sender, RoutedEventArgs e)
        {
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += (o, ea) =>
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    RefreshEnvironmentList();
                }));
            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                bsyLoadingEnvironmentList.IsBusy = false;
            };

            bsyLoadingEnvironmentList.IsBusy = true;
            worker.RunWorkerAsync();
        }
        #endregion

        #region Menu Events
        private void mnuCopyEnvironmentName_Click(object sender, RoutedEventArgs e)
        {
            if (lbxEnvironments.SelectedIndex > -1 && _viewModel.SelectedEnvironment != null)
            {
                Clipboard.SetDataObject(_viewModel.SelectedEnvironment.EnvironmentName, true);
            }
        }
        private void mnuCopyEnvironmentDetails_Click(object sender, RoutedEventArgs e)
        {
            if (lbxEnvironments.SelectedIndex > -1)
            {
                StringBuilder sb = new StringBuilder();

                sb.Append(System.Environment.NewLine).Append("-- ENVIRONMENT --").Append(System.Environment.NewLine);
                sb.Append("Name: ").Append(_viewModel.SelectedEnvironment.EnvironmentName).Append(System.Environment.NewLine);
                sb.Append("Description: ").Append(_viewModel.SelectedEnvironment.Description).Append(System.Environment.NewLine);
                sb.Append("Notes: " + _viewModel.SelectedEnvironment.Notes).Append(System.Environment.NewLine);

                sb.Append(System.Environment.NewLine).Append("-- MACHINES --").Append(System.Environment.NewLine);
                sb.Append(string.Join(System.Environment.NewLine, _viewModel.SelectedEnvironment.Machines.Where(p => p.IsActive).Select(p => p.MachineName)));

                sb.Append(System.Environment.NewLine).Append(System.Environment.NewLine).Append("-- CLUSTERS --").Append(System.Environment.NewLine);
                sb.Append(string.Join(System.Environment.NewLine, _viewModel.SelectedEnvironment.Clusters.Where(p => p.IsActive).Select(p => p.ClusterName)));

                sb.Append(System.Environment.NewLine).Append(System.Environment.NewLine).Append("-- INSTANCES --").Append(System.Environment.NewLine);
                sb.Append(string.Join(System.Environment.NewLine, _viewModel.SelectedEnvironment.DBMSInstances.OfType<SQLServer>().Where(p => p.IsActive).OfType<SQLServer>().Select(p => p.FullName)));

                Clipboard.SetDataObject(sb.ToString(), true);
                MessageBox.Show("Copied to clipboard.", _viewModel.SelectedEnvironment.EnvironmentName, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        private void mnuDecommissionEnvironment_Click(object sender, RoutedEventArgs e)
        {
            DecommissionEnvironmentDialog();
        }
        private void mnuEditEnvironment_Click(object sender, RoutedEventArgs e)
        {
            EditEnvironmentDialog();
        }
        #endregion

        #region Listbox Events
        private void lbxEnvironments_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            EditEnvironmentDialog();
        }
        private void lbxEnvironments_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbxEnvironments.SelectedIndex > -1)
            {
                _viewModel.SelectedEnvironment = lbxEnvironments.SelectedItem as DBACentral.Data.Environment;
            }
        }
        #endregion
        #endregion

        #region Right Side
        #region Listbox Events
        #endregion
        private void lbxEnvMachines_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lbxEnvMachines.SelectedIndex > -1)
            {
                if (!((Machine)lbxEnvMachines.SelectedItem).IsActive && cbxShowDecomMachines.IsChecked == false)
                {
                    cbxShowDecomMachines.IsChecked = true;
                    RefreshMachineList();
                }

                MainTabControl.SelectedItem = MainTabControl.FindName("tabMachines");
                lbxMachines.SelectedItem = lbxEnvMachines.SelectedItem;
                lbxEnvMachines.SelectedIndex = -1;
            }
        }
        private void lbxEnvClusters_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lbxEnvClusters.SelectedIndex > -1)
            {
                if (!(lbxEnvClusters.SelectedItem as Cluster).IsActive && cbxShowDecomClusters.IsChecked == false)
                {
                    cbxShowDecomClusters.IsChecked = true;
                    RefreshClusterList();
                }

                MainTabControl.SelectedItem = MainTabControl.FindName("tabClusters");
                lbxClusters.SelectedItem = lbxEnvClusters.SelectedItem;
                lbxEnvClusters.SelectedIndex = -1;
            }
        }
        private void lbxEnvInstances_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lbxEnvInstances.SelectedIndex > -1)
            {
                if (!(lbxEnvInstances.SelectedItem as SQLServer).IsActive && cbxShowDecomInstances.IsChecked == false)
                {
                    cbxShowDecomInstances.IsChecked = true;
                    RefreshInstanceList();
                }

                MainTabControl.SelectedItem = MainTabControl.FindName("tabInstances");
                lbxEnvInstances.SelectedItem = lbxEnvInstances.SelectedItem;
                lbxEnvInstances.SelectedIndex = -1;
            }
        }
        #endregion
    }
}