﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Microsoft.SqlServer.Management.Common;

using AdminToolsUI.Helpers;
using AdminToolsUI.Views;
using Collector;
using DBACentral.Data;

namespace AdminToolsUI.Controls
{
    public partial class Inventory : UserControl
    {
        #region Left Side
        private void DecommissionInstanceDialog()
        {
            if (lbxInstances.SelectedIndex > -1)
            {
                if (_viewModel.SelectedInstance.IsActive)
                {
                    winDecommission decommissionWindow = new winDecommission(_viewModel.SelectedInstance.SQLServerDetail.FullName, _viewModel.SelectedInstance.Notes);
                    decommissionWindow.ShowDialog();

                    if (decommissionWindow.DialogResult == true)
                    {
                        MessageBoxResult result = MessageBox.Show("Are you sure you want to decommission " + _viewModel.SelectedInstance.SQLServerDetail.FullName + "?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                        if (result == MessageBoxResult.No)
                        {
                            return;
                        }

                        // TODO: how to handle decoms
                        // TODO: should we maybe bind the selected Machine in the decom window?
                        _viewModel.SelectedInstance.Notes += " --Decommissioned " + DateTime.Now + ": " + decommissionWindow.txtNotes.Text;
                        _viewModel.SelectedInstance.IsActive = false;
                        _viewModel.Save();

                        BackgroundWorker worker = new BackgroundWorker();

                        worker.DoWork += (o, ea) =>
                        {
                            Dispatcher.Invoke((Action)(() =>
                            {
                                RefreshInstanceList();
                            }));
                        };
                        worker.RunWorkerCompleted += (o, ea) =>
                        {
                            bsyLoadingInstanceList.IsBusy = false;
                        };

                        bsyLoadingInstanceList.IsBusy = true;
                        worker.RunWorkerAsync();
                    }
                }
                else
                {
                    MessageBox.Show(_viewModel.SelectedInstance.SQLServerDetail.FullName + " is already decommissioned.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        private void EditInstanceDialog()
        {
            if (lbxInstances.SelectedIndex > -1)
            {
                winEditInstance editInstanceWindow = new winEditInstance(_viewModel);
                _viewModel.PossibleEnvironmentsRefresh(new List<DBACentral.Data.Environment>()); // get a list of all environments
                bool isListDirty = false;
                editInstanceWindow.ShowDialog();

                if (editInstanceWindow.DialogResult == true)
                {
                    if (editInstanceWindow.cbxDefaultInstance.IsChecked == true && _viewModel.SelectedInstance.DBMSInstanceName != null)
                    {
                        _viewModel.SelectedInstance.DBMSInstanceName = null;
                    }
                    // otherwise the binding will handle it

                    if (editInstanceWindow.cbxDefaultPortNumber.IsChecked == true && _viewModel.SelectedInstance.PortNumber != null)
                    {
                        _viewModel.SelectedInstance.PortNumber = null;
                    }
                    // otherwise the binding will handle it

                    if (editInstanceWindow.cbxDecommissioned.IsChecked == true && _viewModel.SelectedInstance.IsActive)
                    {
                        DecommissionInstanceDialog();

                        // check that the user really decommissioned the instance
                        if (!_viewModel.SelectedInstance.IsActive)
                        {
                            isListDirty = true;
                        }
                    }
                    else if (editInstanceWindow.cbxDecommissioned.IsChecked == false && !_viewModel.SelectedInstance.IsActive)
                    {
                        _viewModel.SelectedInstance.IsActive = true;
                        _viewModel.SelectedInstance.Notes += " --ReEnabled " + DateTime.Now;
                        isListDirty = true;
                    }

                    _viewModel.Save();
                    _viewModel.SelectedInstanceRefresh();
                    editInstanceWindow.DataContext = null;

                    if (isListDirty)
                    {
                        BackgroundWorker worker = new BackgroundWorker();

                        worker.DoWork += (o, ea) =>
                        {
                            Dispatcher.Invoke((Action)(() =>
                            {
                                RefreshInstanceList();
                            }));
                        };
                        worker.RunWorkerCompleted += (o, ea) =>
                        {
                            bsyLoadingInstanceList.IsBusy = false;
                        };

                        bsyLoadingInstanceList.IsBusy = true;
                        worker.RunWorkerAsync();
                    }
                }
                else
                {
                    editInstanceWindow.DataContext = null;
                }
            }
        }
        private void RefreshInstanceList()
        {
            _viewModel.InstanceListRefresh(cbxShowDecomInstances.IsChecked.Value);
        }

        #region Button Events
        private void btnNewInstance_Click(object sender, RoutedEventArgs e)
        {
            winNewInstance newInstanceWindow = new winNewInstance(_viewModel);
            _viewModel.PossiblePeopleRefresh(new List<Person>());  // pass in empty list to get all
            _viewModel.PossibleEnvironmentsRefresh(new List<DBACentral.Data.Environment>());
            newInstanceWindow.ShowDialog();

            if (newInstanceWindow.DialogResult == true)
            {
                BackgroundWorker worker = new BackgroundWorker();

                worker.DoWork += (o, ea) =>
                {
                    Dispatcher.Invoke((Action)(() =>
                    {
                        RefreshInstanceList();
                    }));
                };
                worker.RunWorkerCompleted += (o, ea) =>
                {
                    bsyLoadingInstanceList.IsBusy = false;
                };

                bsyLoadingInstanceList.IsBusy = true;
                worker.RunWorkerAsync();

                BackgroundWorker worker2 = new BackgroundWorker();

                worker2.DoWork += (o, ea) =>
                {
                    Dispatcher.Invoke((Action)(() =>
                    {
                        RefreshMachineList();
                    }));
                };
                worker2.RunWorkerCompleted += (o, ea) =>
                {
                    bsyLoadingMachineList.IsBusy = false;
                };

                bsyLoadingMachineList.IsBusy = true;
                worker2.RunWorkerAsync();
            }
        }
        #endregion

        #region Checkbox Events
        private void cbxShowDecomInstances_Checked(object sender, RoutedEventArgs e)
        {
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += (o, ea) =>
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    RefreshInstanceList();
                }));
            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                bsyLoadingInstanceList.IsBusy = false;
            };

            bsyLoadingInstanceList.IsBusy = true;
            worker.RunWorkerAsync();
        }
        private void cbxShowDecomInstances_Unchecked(object sender, RoutedEventArgs e)
        {
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += (o, ea) =>
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    RefreshInstanceList();
                }));
            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                bsyLoadingInstanceList.IsBusy = false;
            };

            bsyLoadingInstanceList.IsBusy = true;
            worker.RunWorkerAsync();
        }
        #endregion

        #region ListboxEvents
        private void lbxInstances_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            EditInstanceDialog();
        }
        private void lbxInstances_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (bsyInstanceInfo.IsBusy)
            {
                bsyInstanceInfo.IsBusy = false;
            }

            if (lbxInstances.SelectedIndex > -1)
            {
                _viewModel.SelectedInstance = lbxInstances.SelectedItem as SQLServer;
                _viewModel.SelectedInstanceRefresh();
                _viewModel.SelectedInstanceContactListRefresh();
            }
        }
        #endregion

        #region Menu Events
        private void mnuCopyInstanceName_Click(object sender, RoutedEventArgs e)
        {
            if (lbxInstances.SelectedIndex > -1 && _viewModel.SelectedInstance != null)
            {
                Clipboard.SetDataObject(_viewModel.SelectedInstance.SQLServerDetail.FullName, true);
            }
        }
        private void mnuCopyInstanceDetails_Click(object sender, RoutedEventArgs e)
        {
            if (lbxInstances.SelectedIndex > -1)
            {
                StringBuilder sb = new StringBuilder();


                sb.Append(System.Environment.NewLine).Append("-- INSTANCE --").Append(System.Environment.NewLine);
                sb.Append("Name: ");
                if (_viewModel.SelectedInstance.DBMSInstanceName == null)
                {
                    sb.Append("{DEFAULT}").Append(System.Environment.NewLine);
                }
                else
                {
                    sb.Append(_viewModel.SelectedInstance.DBMSInstanceName).Append(System.Environment.NewLine);
                }
                sb.Append("Environment: ").Append(_viewModel.SelectedInstance.Environment.EnvironmentName).Append(System.Environment.NewLine);
                sb.Append("IP: ").Append(_viewModel.SelectedInstance.IP).Append(System.Environment.NewLine);
                sb.Append("Description: " + _viewModel.SelectedInstance.Description).Append(System.Environment.NewLine);
                sb.Append("Install Date: " + _viewModel.SelectedInstance.InstallDate).Append(System.Environment.NewLine);
                sb.Append("Last Updated: " + _viewModel.SelectedInstance.LastUpdated).Append(System.Environment.NewLine);

                sb.Append(System.Environment.NewLine).Append("-- ENGINE --").Append(System.Environment.NewLine);
                sb.Append("Database Count: " + _viewModel.SelectedInstance.SQLServerDetail.NumberOfDatabases).Append(System.Environment.NewLine);
                sb.Append("Edition: " + _viewModel.SelectedInstance.SQLServerDetail.Edition).Append(System.Environment.NewLine);
                sb.Append("Version: " + _viewModel.SelectedInstance.SQLServerDetail.Version).Append(System.Environment.NewLine);
                sb.Append("Product Level: " + _viewModel.SelectedInstance.SQLServerDetail.ProductLevel).Append(System.Environment.NewLine);
                sb.Append("Build: " + _viewModel.SelectedInstance.SQLServerDetail.Build).Append(System.Environment.NewLine);
                sb.Append("Architecture: " + _viewModel.SelectedInstance.SQLServerDetail.Architecture).Append(System.Environment.NewLine);
                sb.Append("Platform: " + _viewModel.SelectedInstance.SQLServerDetail.Platform).Append(System.Environment.NewLine);
                sb.Append("Engine Service Start Mode: " + _viewModel.SelectedInstance.SQLServerDetail.EngineServiceStartMode).Append(System.Environment.NewLine);
                sb.Append("Min ServerMemory: " + _viewModel.SelectedInstance.SQLServerDetail.MinServerMemory).Append(System.Environment.NewLine);
                sb.Append("Max ServerMemory: " + _viewModel.SelectedInstance.SQLServerDetail.MaxServerMemory).Append(System.Environment.NewLine);
                sb.Append("Max DOP: " + _viewModel.SelectedInstance.SQLServerDetail.MaxDegreeOfParallelism).Append(System.Environment.NewLine);
                sb.Append("Language: " + _viewModel.SelectedInstance.SQLServerDetail.Language).Append(System.Environment.NewLine);
                sb.Append("Error Log File Count: " + _viewModel.SelectedInstance.SQLServerDetail.NumberOfLogFiles).Append(System.Environment.NewLine);
                sb.Append("Collation: " + _viewModel.SelectedInstance.SQLServerDetail.Collation).Append(System.Environment.NewLine);
                sb.Append("Sort Order Name: " + _viewModel.SelectedInstance.SQLServerDetail.SqlSortOrderName).Append(System.Environment.NewLine);
                sb.Append("Trace Flags: " + _viewModel.SelectedInstance.SQLServerDetail.GlobalTraceFlags).Append(System.Environment.NewLine);
                sb.Append("Linked Servers: " + _viewModel.SelectedInstance.SQLServerDetail.LinkedServers).Append(System.Environment.NewLine);
                sb.Append("Mail Profiles: " + _viewModel.SelectedInstance.SQLServerDetail.MailProfiles).Append(System.Environment.NewLine);
                sb.Append("Availability Groups: " + _viewModel.SelectedInstance.SQLServerDetail.AvailabilityGroups).Append(System.Environment.NewLine);
                sb.Append("HADR Enabled: " + _viewModel.SelectedInstance.SQLServerDetail.IsHADREnabled).Append(System.Environment.NewLine);
                sb.Append("TCP Enabled: " + _viewModel.SelectedInstance.SQLServerDetail.IsTcpEnabled).Append(System.Environment.NewLine);
                sb.Append("FullText Installed: " + _viewModel.SelectedInstance.SQLServerDetail.IsFullTextInstalled).Append(System.Environment.NewLine);


                sb.Append(System.Environment.NewLine).Append("-- AGENT --").Append(System.Environment.NewLine);
                sb.Append("Job Count: " + _viewModel.SelectedInstance.SQLServerDetail.AgentJobCount).Append(System.Environment.NewLine);
                sb.Append("Alerts: " + _viewModel.SelectedInstance.SQLServerDetail.AgentAlerts).Append(System.Environment.NewLine);
                sb.Append("Operators: " + _viewModel.SelectedInstance.SQLServerDetail.AgentOperators).Append(System.Environment.NewLine);

                sb.Append(System.Environment.NewLine).Append("-- SERVICE ACCOUNTS --").Append(System.Environment.NewLine);
                sb.Append("Engine: " + _viewModel.SelectedInstance.SQLServerDetail.EngineServiceAccount).Append(System.Environment.NewLine);
                sb.Append("Agent: " + _viewModel.SelectedInstance.SQLServerDetail.AgentServiceAccount).Append(System.Environment.NewLine);
                sb.Append("Browser: " + _viewModel.SelectedInstance.SQLServerDetail.BrowserServiceAccount).Append(System.Environment.NewLine);

                sb.Append(System.Environment.NewLine).Append("-- DIRECTORIES --").Append(System.Environment.NewLine);
                sb.Append("Default Data: " + _viewModel.SelectedInstance.SQLServerDetail.DefaultFile).Append(System.Environment.NewLine);
                sb.Append("Default Log: " + _viewModel.SelectedInstance.SQLServerDetail.DefaultLog).Append(System.Environment.NewLine);
                sb.Append("Default Backup: " + _viewModel.SelectedInstance.SQLServerDetail.BackupDirectory).Append(System.Environment.NewLine);
                sb.Append("Engine Error Log: " + _viewModel.SelectedInstance.SQLServerDetail.ErrorLogPath).Append(System.Environment.NewLine);
                sb.Append("Agent Error Log: " + _viewModel.SelectedInstance.SQLServerDetail.AgentErrorLogFile).Append(System.Environment.NewLine);
                sb.Append("Root: " + _viewModel.SelectedInstance.SQLServerDetail.RootDirectory).Append(System.Environment.NewLine);
                sb.Append("Root: " + _viewModel.SelectedInstance.SQLServerDetail.RootDirectory).Append(System.Environment.NewLine);
                sb.Append("Install Data: " + _viewModel.SelectedInstance.SQLServerDetail.InstallDataDirectory).Append(System.Environment.NewLine);
                sb.Append("Install Shared: " + _viewModel.SelectedInstance.SQLServerDetail.InstallSharedDirectory).Append(System.Environment.NewLine);

                Clipboard.SetDataObject(sb.ToString(), true);

                MessageBox.Show("Copied to clipboard.", _viewModel.SelectedInstance.SQLServerDetail.FullName, MessageBoxButton.OK, MessageBoxImage.Information);

            }
        }
        private void mnuDecommissionInstance_Click(object sender, RoutedEventArgs e)
        {
            DecommissionInstanceDialog();
        }
        private void mnuEditInstance_Click(object sender, RoutedEventArgs e)
        {
            EditInstanceDialog();
        }
        #endregion
        #endregion

        #region Right Side
        #region Button Events
        private void btnCollectInstanceInfo_Click(object sender, RoutedEventArgs rea)
        {
            if (lbxInstances.SelectedIndex > -1)
            {
                BackgroundWorker worker = new BackgroundWorker();

                worker.DoWork += (o, ea) =>
                {
                    try
                    {
                        Collect.SQLServer(_viewModel.SelectedInstance.SQLServerDetail, _viewModel.SelectedInstance.SQLServerDetail.FullName);

                        if (_viewModel.SelectedInstance.SQLServerDetail.IsClustered)
                        {
                            string clusterName = Collect.GetClusterName(_viewModel.SelectedInstance.SQLServerDetail.VirtualServerName);
                            _viewModel.SelectedInstance.IP = Collect.GetIPv4(_viewModel.SelectedInstance.SQLServerDetail.VirtualServerName);

                            // create cluster if it doesn't exist
                            if (!_viewModel.Context.Clusters.Where(p => p.IsActive == true && p.ClusterName == clusterName).Any())
                            {
                                var newCluster = new Cluster();
                                newCluster.IsActive = true;
                                Collect.Cluster(newCluster, _viewModel.SelectedInstance.SQLServerDetail.VirtualServerName);
                                newCluster.Notes = string.Format("Added to inventory while collecting information about {0}.", _viewModel.SelectedInstance.SQLServerDetail.FullName);
                                _viewModel.Context.Clusters.AddObject(newCluster);
                                _viewModel.Save();
                            }

                            _viewModel.SelectedInstance.SQLServerDetail.Cluster = _viewModel.Context.Clusters.Where(p => p.IsActive == true && p.ClusterName == clusterName).FirstOrDefault();

                            Collect.Cluster(_viewModel.SelectedInstance.SQLServerDetail.Cluster, _viewModel.SelectedInstance.SQLServerDetail.VirtualServerName);
                        }
                        else
                        {
                            _viewModel.SelectedInstance.IP = Collect.GetIPv4(_viewModel.SelectedInstance.SQLServerDetail.Machine.MachineName);
                        }

                        _viewModel.Save();
                        _viewModel.SelectedInstanceRefresh();
                    }
                    catch (UnauthorizedAccessException)
                    {
                        MessageBox.Show(string.Format("Access denied to SQL instance {0}.", _viewModel.SelectedInstance.SQLServerDetail.FullName), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        LogHelper.WriteToLog(string.Format("Access denied to SQL instance {0}.", _viewModel.SelectedInstance.SQLServerDetail.FullName), EventLogEntryType.FailureAudit);
                    }
                    catch (ConnectionFailureException e)
                    {
                        if (e.InnerException.Message.Contains("Login failed for user"))
                        {
                            MessageBox.Show(string.Format("Login failure attempting to connect to SQL instance {0}.", _viewModel.SelectedInstance.SQLServerDetail.FullName), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                            LogHelper.WriteToLog(string.Format("Login failure attempting to connect to SQL instance {0}.", _viewModel.SelectedInstance.SQLServerDetail.FullName), EventLogEntryType.FailureAudit);
                        }
                        else if (e.InnerException.Message.Contains("Timeout Expired"))
                        {
                            MessageBox.Show(string.Format("A timeout occurred attempting to connect to the SQL instance {0}.", _viewModel.SelectedInstance.SQLServerDetail.FullName), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                            LogHelper.WriteToLog(string.Format("A timeout occurred attempting to connect to the SQL instance {0}.", _viewModel.SelectedInstance.SQLServerDetail.FullName), EventLogEntryType.Warning);
                        }
                        else if (e.InnerException.Message.Contains("A network-related or instance-specific error occurred "))
                        {
                            MessageBox.Show(string.Format("SQL instance {0} was not found or was not accessible. Verify that the instance name is correct and that SQL Machine is configured to allow remote connections.", _viewModel.SelectedInstance.SQLServerDetail.FullName), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                            LogHelper.WriteToLog(string.Format("SQL instance {0} was not found or was not accessible. Verify that the instance name is correct and that SQL Machine is configured to allow remote connections.", _viewModel.SelectedInstance.SQLServerDetail.FullName), EventLogEntryType.Warning);
                        }
                        else
                        {
                            MessageBox.Show(string.Format("Unable to connect to SQL instance {0} : {1}", _viewModel.SelectedInstance.SQLServerDetail.FullName, e.ToString()), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                            LogHelper.WriteToLog(string.Format("Unable to connect to SQL instance {0} : {1}", _viewModel.SelectedInstance.SQLServerDetail.FullName, e.ToString()), EventLogEntryType.Error);
                        }
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(string.Format("An error was encountered collecting information from the SQL instance {0} : {1}", _viewModel.SelectedInstance.SQLServerDetail.FullName, e.ToString()), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        LogHelper.WriteToLog(string.Format("An error was encountered collecting information from the SQL instance {0} : {1}", _viewModel.SelectedInstance.SQLServerDetail.FullName, e.ToString()), EventLogEntryType.Error);
                    }
                };

                worker.RunWorkerCompleted += (o, ea) =>
                {
                    bsyInstanceInfo.IsBusy = false;
                };

                bsyInstanceInfo.IsBusy = true;
                worker.RunWorkerAsync();
            }
        }
        private void btnInstanceAddContact_Click(object sender, RoutedEventArgs e)
        {
            if (lbxInstances.SelectedIndex > -1)
            {
                int itemTypeID = _viewModel.Context.ItemTypes.Where(p => p.ItemTypeName == "DBMSInstance").FirstOrDefault().ItemTypeID;

                winAddContact addContactWindow = new winAddContact(_viewModel, _viewModel.SelectedInstance.SQLServerDetail.FullName);
                addContactWindow.ShowDialog();

                if (addContactWindow.DialogResult.Value == true)
                {
                    var newContact = new Contact();
                    newContact.Person = addContactWindow.cbxContact.SelectedItem as Person;
                    newContact.ContactType = addContactWindow.cbxContactType.SelectedItem as ContactType;
                    newContact.ItemType = _viewModel.Context.ItemTypes.Where(p => p.ItemTypeName == "DBMSInstance").FirstOrDefault();
                    newContact.ItemID = _viewModel.SelectedInstance.DBMSInstanceID;
                    newContact.Notes = addContactWindow.txtNotes.Text;
                    _viewModel.Context.Contacts.AddObject(newContact);
                    _viewModel.Save();

                    _viewModel.SelectedInstanceContactListRefresh();
                }
            }
        }
        #endregion

        #region Listbox Events
        private void lbxInstanceContacts_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            // TODO
        }
        #endregion

        #region Menu Events
        private void mnuEditInstanceContact_Click(object sender, RoutedEventArgs e)
        {
            if (lbxInstanceContacts.SelectedIndex > -1)
            {
                _viewModel.EditableContact = lbxInstanceContacts.SelectedItem as Contact;
                winEditContact editContactWindow = new winEditContact(_viewModel);
                editContactWindow.ShowDialog();

                if (editContactWindow.DialogResult == true)
                {
                    (lbxInstanceContacts.SelectedItem as Contact).ContactType = editContactWindow.cbxContactType.SelectedItem as ContactType;
                    _viewModel.Save();
                    _viewModel.SelectedInstanceContactListRefresh();
                }
            }
        }
        private void mnuRemoveInstanceContact_Click(object sender, RoutedEventArgs e)
        {
            if (lbxInstances.SelectedIndex > -1)
            {
                MessageBoxResult result = MessageBox.Show("Are you sure you want to remove " + (lbxInstanceContacts.SelectedItem as Contact).Person.FullName + "?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                if (result == MessageBoxResult.No)
                {
                    return;
                }

                _viewModel.Context.Contacts.DeleteObject(lbxInstanceContacts.SelectedItem as Contact);
                _viewModel.Save();
                _viewModel.SelectedInstanceContactListRefresh();
            }
        }
        #endregion
        #endregion
    }
}



