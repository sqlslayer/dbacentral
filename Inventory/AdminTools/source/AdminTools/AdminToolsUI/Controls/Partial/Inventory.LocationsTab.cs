﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

using AdminToolsUI.Views;
using DBACentral.Data;

namespace AdminToolsUI.Controls
{
    public partial class Inventory : UserControl
    {
        #region Left Side
        private void DecommissionLocationDialog()
        {
            if (lbxLocations.SelectedIndex > -1)
            {
                if (_viewModel.SelectedLocation.IsActive)
                {
                    winDecommission decommissionWindow = new winDecommission(_viewModel.SelectedLocation.LocationName, (_viewModel.SelectedLocation.Notes));
                    decommissionWindow.ShowDialog();

                    if (decommissionWindow.DialogResult == true)
                    {
                        MessageBoxResult result = MessageBox.Show("Are you sure you want to decommission " + _viewModel.SelectedLocation.LocationName + "?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                        if (result == MessageBoxResult.No)
                        {
                            return;
                        }

                        _viewModel.SelectedLocation.Notes += " --Decommissioned " + DateTime.Now + ": " + decommissionWindow.txtNotes.Text;
                        _viewModel.SelectedLocation.IsActive = false;
                        _viewModel.Save();

                        BackgroundWorker worker = new BackgroundWorker();

                        worker.DoWork += (o, ea) =>
                        {
                            Dispatcher.Invoke((Action)(() =>
                            {
                                RefreshLocationList();
                            }));
                        };
                        worker.RunWorkerCompleted += (o, ea) =>
                        {
                            bsyLoadingLocationList.IsBusy = false;
                        };

                        bsyLoadingLocationList.IsBusy = true;
                        worker.RunWorkerAsync();
                    }
                }
            }
        }
        private void EditLocationDialog()
        {
            if (lbxLocations.SelectedIndex > -1)
            {
                winEditLocation editLocationDialog = new winEditLocation(_viewModel);
                bool isListDirty = false;
                editLocationDialog.ShowDialog();

                if (editLocationDialog.DialogResult == true)
                {
                    if (editLocationDialog.cbxDecommissioned.IsChecked == true && _viewModel.SelectedLocation.IsActive)
                    {
                        DecommissionLocationDialog();

                        // check that the user really decommissioned the location
                        if (!_viewModel.SelectedLocation.IsActive)
                        {
                            isListDirty = true;
                        }
                    }
                    else if (editLocationDialog.cbxDecommissioned.IsChecked == false && !_viewModel.SelectedLocation.IsActive)
                    {
                        _viewModel.SelectedLocation.IsActive = true;
                        _viewModel.SelectedLocation.Notes += " --ReEnabled " + DateTime.Now;
                        isListDirty = true;
                    }

                    _viewModel.Save();
                    _viewModel.SelectedLocationRefresh();

                    if (isListDirty)
                    {
                        BackgroundWorker worker = new BackgroundWorker();

                        worker.DoWork += (o, ea) =>
                        {
                            Dispatcher.Invoke((Action)(() =>
                            {
                                RefreshLocationList();
                            }));
                        };
                        worker.RunWorkerCompleted += (o, ea) =>
                        {
                            bsyLoadingLocationList.IsBusy = false;
                        };

                        bsyLoadingLocationList.IsBusy = true;
                        worker.RunWorkerAsync();
                    }
                }
            }
        }
        private void RefreshLocationList()
        {
            _viewModel.LocationListRefresh(cbxShowDecomLocs.IsChecked.Value);
        }

        #region Button Events
        private void btnNewLocation_Click(object sender, RoutedEventArgs e)
        {
            winNewLocation newLocWindow = new winNewLocation(_viewModel);
            newLocWindow.ShowDialog();

            if (newLocWindow.DialogResult == true)
            {
                RefreshLocationList();
            }
        }
        #endregion

        #region Checkbox Events
        private void cbxShowDecomLocs_Checked(object sender, RoutedEventArgs e)
        {
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += (o, ea) =>
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    RefreshLocationList();
                }));
            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                bsyLoadingLocationList.IsBusy = false;
            };

            bsyLoadingLocationList.IsBusy = true;
            worker.RunWorkerAsync();
        }
        private void cbxShowDecomLocs_Unchecked(object sender, RoutedEventArgs e)
        {
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += (o, ea) =>
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    RefreshLocationList();
                }));
            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                bsyLoadingLocationList.IsBusy = false;
            };

            bsyLoadingLocationList.IsBusy = true;
            worker.RunWorkerAsync();
        }
        #endregion

        #region Listbox Events
        private void lbxLocations_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            EditLocationDialog();
        }
        #endregion

        #region Menu Events
        #endregion
        private void mnuCopyLocationName_Click(object sender, RoutedEventArgs e)
        {
            if (lbxLocations.SelectedIndex > -1 && _viewModel.SelectedLocation != null)
            {
                Clipboard.SetDataObject(_viewModel.SelectedLocation.LocationName, true);
            }
        }
        private void mnuCopyLocationDetails_Click(object sender, RoutedEventArgs e)
        {
            if (lbxEnvironments.SelectedIndex > -1)
            {
                StringBuilder sb = new StringBuilder();

                sb.Append(System.Environment.NewLine).Append("-- LOCATION --").Append(System.Environment.NewLine);
                sb.Append("Name: ").Append(_viewModel.SelectedLocation.LocationName).Append(System.Environment.NewLine);
                sb.Append("Description: ").Append(_viewModel.SelectedLocation.Description).Append(System.Environment.NewLine);
                sb.Append("Notes: ").Append(_viewModel.SelectedLocation.Notes).Append(System.Environment.NewLine);

                sb.Append(System.Environment.NewLine).Append("-- MACHINES --").Append(System.Environment.NewLine);
                sb.Append(string.Join(System.Environment.NewLine, _viewModel.SelectedEnvironment.Machines.Where(p => p.IsActive).Select(p => p.MachineName)));

                Clipboard.SetDataObject(sb.ToString(), true);
                MessageBox.Show("Copied to clipboard.", _viewModel.SelectedEnvironment.EnvironmentName, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        private void mnuDecommissionLocation_Click(object sender, RoutedEventArgs e)
        {
            DecommissionLocationDialog();
        }
        private void mnuEditLocation_Click(object sender, RoutedEventArgs e)
        {
            EditLocationDialog();
        }
        #endregion

        #region Right Side
        #region Listbox Events
        private void lbxLocMachines_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lbxLocMachines.SelectedIndex > -1)
            {
                if (!(lbxLocMachines.SelectedItem as Machine).IsActive && cbxShowDecomMachines.IsChecked == false)
                {
                    cbxShowDecomMachines.IsChecked = true;
                    RefreshMachineList();
                }

                MainTabControl.SelectedItem = MainTabControl.FindName("tabMachines");
                lbxMachines.SelectedItem = lbxLocMachines.SelectedItem;
                lbxLocMachines.SelectedIndex = -1;
            }
        }
        private void lbxLocations_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbxLocations.SelectedIndex > -1)
            {
                _viewModel.SelectedLocation = lbxLocations.SelectedItem as Location;
            }
        }
        #endregion
        #endregion
    }
}