﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

using AdminToolsUI.Helpers;
using AdminToolsUI.Views;
using Collector;
using DBACentral.Data;

namespace AdminToolsUI.Controls
{
    public partial class Inventory : UserControl
    {
        #region Left Side
        private void DecommissionMachineDialog()
        {
            if (lbxMachines.SelectedIndex > -1)
            {
                if (_viewModel.SelectedMachine.IsActive)
                {
                    winDecommission decommissionWindow = new winDecommission(_viewModel.SelectedMachine.MachineName, _viewModel.SelectedMachine.Notes);
                    decommissionWindow.ShowDialog();

                    if (decommissionWindow.DialogResult == true)
                    {
                        // check for instances installed
                        if (_viewModel.SelectedMachine.DBMSInstances.OfType<SQLServer>().Where(p => p.IsActive == true).Count() > 0)
                        {
                            StringBuilder sb = new StringBuilder("The following SQL instances are currently associated with this Machine:");
                            sb.Append(System.Environment.NewLine).Append(System.Environment.NewLine);

                            foreach (SQLServer iSQLServer in _viewModel.SelectedMachine.DBMSInstances.OfType<SQLServer>())
                            {
                                if (iSQLServer.DBMSInstanceName == null)
                                {
                                    sb.Append("{DEFAULT}").Append(System.Environment.NewLine);
                                }
                                else
                                {
                                    sb.Append(iSQLServer.DBMSInstanceName).Append(System.Environment.NewLine);
                                }
                            }

                            sb.Append(System.Environment.NewLine).Append("These SQL instances will be decommissioned as well.");

                            MessageBoxResult result = MessageBox.Show(sb.ToString(), "Warning!", MessageBoxButton.OKCancel, MessageBoxImage.Exclamation);
                            if (result == MessageBoxResult.OK)
                            {
                                foreach (SQLServer iSQLServer in _viewModel.SelectedMachine.DBMSInstances.OfType<SQLServer>().Where(p => p.IsActive == true))
                                {
                                    iSQLServer.Notes += " --Decommissioned " + DateTime.Now + ": " + decommissionWindow.txtNotes.Text;
                                    iSQLServer.IsActive = false;
                                }
                                _viewModel.Save();

                                RefreshInstanceList();
                            }
                            else
                            {
                                return;
                            }
                        }

                        MessageBoxResult result2 = MessageBox.Show("Are you sure you want to decommission " + _viewModel.SelectedMachine.MachineName + "?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                        if (result2 == MessageBoxResult.Yes)
                        {
                            _viewModel.SelectedMachine.Notes += " --Decommissioned " + DateTime.Now + ": " + decommissionWindow.txtNotes.Text;
                            _viewModel.SelectedMachine.IsActive = false;
                            _viewModel.Save();

                            RefreshMachineList();
                        }
                    }
                }
                else
                {
                    MessageBox.Show((lbxMachines.SelectedItem as Machine).MachineName + " is already decommissioned.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        private void EditMachineDialog()
        {
            if (lbxMachines.SelectedIndex > -1)
            {
                _viewModel.PossibleEnvironmentsRefresh(new List<DBACentral.Data.Environment>());
                _viewModel.PossibleLocationsRefresh(new List<Location>());

                winEditMachine editMachineWindow = new winEditMachine(_viewModel);

                bool isListDirty = false;
                editMachineWindow.ShowDialog();

                if (editMachineWindow.DialogResult == true)
                {
                    if (editMachineWindow.cbxDecommissioned.IsChecked == true && _viewModel.SelectedMachine.IsActive)
                    {
                        DecommissionMachineDialog();

                        // check that the user actually decided to decommission the Machine
                        if (!_viewModel.SelectedMachine.IsActive)
                        {
                            isListDirty = true;
                        }
                    }
                    else if (editMachineWindow.cbxDecommissioned.IsChecked == false && !_viewModel.SelectedMachine.IsActive)
                    {
                        _viewModel.SelectedMachine.IsActive = true;
                        _viewModel.SelectedMachine.Notes += " --ReEnabled " + DateTime.Now;
                        isListDirty = true;
                    }
                    _viewModel.Save();
                    _viewModel.SelectedMachineRefresh();


                    // refresh list if we decomissioned or enabled a Machine
                    if (isListDirty)
                    {
                        RefreshMachineList();
                    }
                }
                else
                {
                    editMachineWindow.DataContext = null;
                }
            }
        }
        private void RefreshMachineList()
        {
            _viewModel.MachineListRefresh(cbxShowDecomMachines.IsChecked.Value);
        }

        #region Buttons Events
        private void btnCollectMachineInfo_Click(object sender, RoutedEventArgs rea)
        {
            if (lbxMachines.SelectedIndex > -1)
            {
                BackgroundWorker worker = new BackgroundWorker();

                // collect Machine info
                worker.DoWork += (o, ea) =>
                {
                    try
                    {
                        Collect.Machine(_viewModel.SelectedMachine);

                        string clusterName = Collect.GetClusterName(_viewModel.SelectedMachine.MachineName);

                        if (clusterName != null)
                        {
                            if (!_viewModel.Context.Clusters.Where(p => p.IsActive == true && p.ClusterName == clusterName).Any())
                            {
                                var newCluster = new Cluster();
                                newCluster.IsActive = true;
                                Collect.Cluster(newCluster, _viewModel.SelectedMachine.MachineName);
                                newCluster.Notes = string.Format("Added to inventory while collecting information about {0}.", _viewModel.SelectedMachine.MachineName);
                                _viewModel.Context.Clusters.AddObject(newCluster);
                                _viewModel.Save();
                                _viewModel.SelectedMachine.Cluster = _viewModel.Context.Clusters.Where(p => p.ClusterName == clusterName && p.IsActive == true).FirstOrDefault();
                            }
                            else
                            {
                                Collect.Cluster(_viewModel.SelectedMachine.Cluster, _viewModel.SelectedMachine.MachineName);
                            }
                        }

                        _viewModel.Save();
                        _viewModel.SelectedMachineRefresh();
                    }
                    catch (UnauthorizedAccessException)
                    {
                        MessageBox.Show(string.Format("Access denied to machine {0}.", _viewModel.SelectedMachine.MachineName), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        LogHelper.WriteToLog(string.Format("Access denied to machine {0}.", _viewModel.SelectedMachine.MachineName), EventLogEntryType.FailureAudit);
                    }
                    catch (COMException e)
                    {
                        MessageBox.Show(string.Format("Access denied to machine {0}.", _viewModel.SelectedMachine.MachineName), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        LogHelper.WriteToLog(string.Format("An error was encountered while attemping to collect information from the machine {0} : {1}", _viewModel.SelectedMachine.MachineName, e.Message), EventLogEntryType.Error);
                    }
                    catch (ManagementException e)
                    {
                        MessageBox.Show(string.Format("An error was encountered while attemping to collect information from the machine {0} : {1}", _viewModel.SelectedMachine.MachineName, e.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        LogHelper.WriteToLog(string.Format("An error was encountered while attemping to collect information from the machine {0} : {1}", _viewModel.SelectedMachine.MachineName, e.Message), EventLogEntryType.Error);
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(string.Format("An error was encountered while attemping to collect information from the machine {0} : {1}", _viewModel.SelectedMachine.MachineName, e.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        LogHelper.WriteToLog(string.Format("An error was encountered while attemping to collect information from the machine {0} : {1}", _viewModel.SelectedMachine.MachineName, e.Message), EventLogEntryType.Error);
                    }
                };
                worker.RunWorkerCompleted += (o, ea) =>
                {
                    bsyMachineInfo.IsBusy = false;
                };

                bsyMachineInfo.IsBusy = true;
                worker.RunWorkerAsync();
            }
        }
        private void btnNewMachine_Click(object sender, RoutedEventArgs e)
        {
            winNewMachine newMachineWindow = new winNewMachine(_viewModel);

            _viewModel.PossiblePeopleRefresh(new List<Person>());
            _viewModel.PossibleEnvironmentsRefresh(new List<DBACentral.Data.Environment>());
            _viewModel.PossibleLocationsRefresh(new List<Location>());

            newMachineWindow.ShowDialog();

            if (newMachineWindow.DialogResult == true)
            {
                RefreshMachineList();
            }
        }
        #endregion

        #region Menu Events
        private void mnuCopyMachineName_Click(object sender, RoutedEventArgs e)
        {
            if (lbxMachines.SelectedIndex > -1 && _viewModel.SelectedMachine != null)
            {
                Clipboard.SetDataObject(_viewModel.SelectedMachine.MachineName, true);
            }
        }
        private void mnuCopyMachineDetails_Click(object sender, RoutedEventArgs e)
        {
            if (lbxMachines.SelectedIndex > -1)
            {
                StringBuilder sb = new StringBuilder();

                sb.Append(System.Environment.NewLine).Append("-- Machine --").Append(System.Environment.NewLine);
                sb.Append("Name: " + _viewModel.SelectedMachine.MachineName).Append(System.Environment.NewLine);
                sb.Append("IP: " + _viewModel.SelectedMachine.IP).Append(System.Environment.NewLine);
                sb.Append("Description: " + _viewModel.SelectedMachine.Description).Append(System.Environment.NewLine);
                sb.Append("Install Date: " + _viewModel.SelectedMachine.InstallDate).Append(System.Environment.NewLine);
                sb.Append("Last Updated: " + _viewModel.SelectedMachine.LastUpdated).Append(System.Environment.NewLine);

                sb.Append(System.Environment.NewLine).Append("-- HARDWARE --").Append(System.Environment.NewLine);
                sb.Append("Manufacturer: " + _viewModel.SelectedMachine.Manufacturer).Append(System.Environment.NewLine);
                sb.Append("Model: " + _viewModel.SelectedMachine.Model).Append(System.Environment.NewLine);
                sb.Append("Virtual: " + _viewModel.SelectedMachine.IsVirtual).Append(System.Environment.NewLine);

                sb.Append(System.Environment.NewLine).Append("-- OPERATING SYSTEM --").Append(System.Environment.NewLine);
                sb.Append("OS: " + _viewModel.SelectedMachine.OS).Append(System.Environment.NewLine);
                sb.Append("OS Version: " + _viewModel.SelectedMachine.OSVersion).Append(System.Environment.NewLine);
                sb.Append("OS Architecture: " + _viewModel.SelectedMachine.OSArchitecture).Append(System.Environment.NewLine);
                sb.Append("OS Service Pack: " + _viewModel.SelectedMachine.OSServicePack).Append(System.Environment.NewLine);
                sb.Append("Power Plan: " + _viewModel.SelectedMachine.PowerPlan).Append(System.Environment.NewLine);

                sb.Append(System.Environment.NewLine).Append("-- PROCESSOR --").Append(System.Environment.NewLine);
                sb.Append("Processor(s): " + _viewModel.SelectedMachine.Processor).Append(System.Environment.NewLine);
                sb.Append("Processor Address Width: " + _viewModel.SelectedMachine.ProcessorAddressWidth).Append(System.Environment.NewLine);
                sb.Append("Processor Architecture: " + _viewModel.SelectedMachine.ProcessorArchitecture).Append(System.Environment.NewLine);
                sb.Append("# Physical Processors: " + _viewModel.SelectedMachine.NumberOfProcessors).Append(System.Environment.NewLine);
                sb.Append("# Logical Processors: " + _viewModel.SelectedMachine.NumberOfLogicalProcessors).Append(System.Environment.NewLine);
                sb.Append("HT Enabled: " + _viewModel.SelectedMachine.IsHTEnabled).Append(System.Environment.NewLine);

                sb.Append(System.Environment.NewLine).Append("-- MEMORY --").Append(System.Environment.NewLine);
                sb.Append("Total Memory (MB): " + _viewModel.SelectedMachine.PhysicalMemoryMB).Append(System.Environment.NewLine);

                sb.Append(System.Environment.NewLine).Append(System.Environment.NewLine).Append("-- INSTANCES --").Append(System.Environment.NewLine);
                sb.Append(string.Join(System.Environment.NewLine, _viewModel.SelectedMachine.DBMSInstances.OfType<SQLServer>().Select(p => p.FullName)));

                sb.Append(System.Environment.NewLine).Append(System.Environment.NewLine).Append("-- CONTACTS --").Append(System.Environment.NewLine);
                sb.Append(string.Join(System.Environment.NewLine, _viewModel.SelectedMachineContactList.Select(p =>
                                                               string.Format("{0} [{1}] {2}", p.Person.FullName, p.ContactType.ContactTypeName, p.Notes))));

                Clipboard.SetDataObject(sb.ToString(), true);

                MessageBox.Show("Copied to clipboard.", _viewModel.SelectedMachine.MachineName, MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                return;
            }
        }
        private void mnuDecommissionMachine_Click(object sender, RoutedEventArgs e)
        {
            DecommissionMachineDialog();
        }
        private void mnuEditMachine_Click(object sender, RoutedEventArgs e)
        {
            EditMachineDialog();
        }
        #endregion

        #region Listbox Events
        private void lbxMachines_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            EditMachineDialog();
        }
        private void lbxMachines_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (bsyMachineInfo.IsBusy)
            {
                bsyMachineInfo.IsBusy = false;
            }

            if (lbxMachines.SelectedIndex > -1)
            {
                _viewModel.SelectedMachine = lbxMachines.SelectedItem as Machine;
                _viewModel.SelectedMachineRefresh();
                _viewModel.SelectedMachineContactListRefresh();
            }
        }
        private void lbxMachInstances_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lbxMachInstances.SelectedIndex > -1)
            {
                if (!(lbxMachInstances.SelectedItem as SQLServer).IsActive && cbxShowDecomInstances.IsChecked == false)
                {
                    cbxShowDecomInstances.IsChecked = true;
                    RefreshInstanceList();
                }

                MainTabControl.SelectedItem = MainTabControl.FindName("tabInstances");
                lbxInstances.SelectedItem = lbxMachInstances.SelectedItem;
                lbxMachInstances.SelectedIndex = -1;
            }
        }
        #endregion

        #region Checkbox Events
        private void cbxShowDecomMachines_Checked(object sender, RoutedEventArgs e)
        {
            RefreshMachineList();
        }
        private void cbxShowDecomMachines_Unchecked(object sender, RoutedEventArgs e)
        {
            RefreshMachineList();
        }
        #endregion
        #endregion

        #region Right Side
        #region Buttons Events
        private void btnMachAddContact_Click(object sender, RoutedEventArgs e)
        {
            if (lbxMachines.SelectedIndex > -1)
            {
                int itemTypeID = _viewModel.Context.ItemTypes.Where(p => p.ItemTypeName == "Machine").FirstOrDefault().ItemTypeID;

                winAddContact addContactWindow = new winAddContact(_viewModel, _viewModel.SelectedMachine.MachineName);
                addContactWindow.ShowDialog();

                if (addContactWindow.DialogResult.Value == true)
                {
                    var newContact = new Contact();
                    newContact.Person = addContactWindow.cbxContact.SelectedItem as Person;
                    newContact.ContactType = addContactWindow.cbxContactType.SelectedItem as ContactType;
                    newContact.ItemType = _viewModel.Context.ItemTypes.Where(p => p.ItemTypeName == "Machine").FirstOrDefault();
                    newContact.ItemID = _viewModel.SelectedMachine.MachineID;
                    newContact.Notes = addContactWindow.txtNotes.Text;
                    _viewModel.Context.Contacts.AddObject(newContact);
                    _viewModel.Save();

                    _viewModel.SelectedMachineContactListRefresh();
                }
            }
        }
        #endregion

        #region Listbox Events
        private void lbxMachineContacts_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            // TODO
        }
        #endregion

        #region Menu Events
        private void mnuEditMachineContact_Click(object sender, RoutedEventArgs e)
        {
            if (lbxMachineContacts.SelectedIndex > -1)
            {
                _viewModel.EditableContact = lbxMachineContacts.SelectedItem as Contact;
                winEditContact editContactWindow = new winEditContact(_viewModel);
                editContactWindow.ShowDialog();

                if (editContactWindow.DialogResult == true)
                {
                    (lbxMachineContacts.SelectedItem as Contact).ContactType = editContactWindow.cbxContactType.SelectedItem as ContactType;
                    _viewModel.Save();
                    _viewModel.SelectedMachineContactListRefresh();
                }

            }
        }
        private void mnuRemoveMachineContact_Click(object sender, RoutedEventArgs e)
        {
            if (lbxMachineContacts.SelectedIndex > -1)
            {
                MessageBoxResult result = MessageBox.Show("Are you sure you want to remove " + (lbxMachineContacts.SelectedItem as Contact).Person.FullName + "?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                if (result == MessageBoxResult.No)
                {
                    return;
                }

                _viewModel.Context.Contacts.DeleteObject(lbxMachineContacts.SelectedItem as Contact);
                _viewModel.Save();
                _viewModel.SelectedMachineContactListRefresh();
            }
        }
        #endregion
        #endregion
    }
}