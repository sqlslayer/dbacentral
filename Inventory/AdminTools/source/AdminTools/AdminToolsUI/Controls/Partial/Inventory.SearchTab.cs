﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

using DBACentral.Data;

namespace AdminToolsUI.Controls
{
    public partial class Inventory : UserControl
    {
        private void lbxSearchResults_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lbxSearchResults.SelectedIndex > -1)
            {
                if (lbxSearchResults.SelectedItem.GetType() == typeof(Machine))
                {
                    MainTabControl.SelectedItem = MainTabControl.FindName("tabMachines");
                    lbxMachines.SelectedItem =  lbxSearchResults.SelectedItem;
                }
                else if (lbxSearchResults.SelectedItem.GetType() == typeof(Cluster))
                {
                    MainTabControl.SelectedItem = MainTabControl.FindName("tabClusters");
                    lbxClusters.SelectedItem = lbxSearchResults.SelectedItem as Cluster;
                }
                else if (lbxSearchResults.SelectedItem.GetType() == typeof(SQLServer))
                {
                    MainTabControl.SelectedItem = MainTabControl.FindName("tabInstances");
                    lbxInstances.SelectedItem = lbxSearchResults.SelectedItem;
                }
                else if (lbxSearchResults.SelectedItem.GetType() == typeof(DBACentral.Data.Environment))
                {
                    MainTabControl.SelectedItem = MainTabControl.FindName("tabEnvironments");
                    lbxEnvironments.SelectedItem = lbxSearchResults.SelectedItem;
                }
                else if (lbxSearchResults.SelectedItem.GetType() == typeof(Location))
                {
                    MainTabControl.SelectedItem = MainTabControl.FindName("tabLocations");
                    lbxLocations.SelectedItem = lbxSearchResults.SelectedItem;
                }
            }
        }
    }
}