﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

using System.Windows.Media.Imaging;
using AdminToolsUI;
using System.Diagnostics;

namespace AdminToolsUI.Converters
{
    class IsVirtualImageConverter : IValueConverter
    {
        public object Convert(
        object value,
        Type targetType,
        object parameter,
        CultureInfo culture)
        {
            if (value != null)
            {
                bool isVirtual = (bool)value;

                return isVirtual ?  new System.Windows.Media.Imaging.BitmapImage(new Uri("../Images/machine_virtual.png", UriKind.Relative))
                                  : new System.Windows.Media.Imaging.BitmapImage(new Uri("../Images/machine.png", UriKind.Relative));
            }

            return new System.Windows.Media.Imaging.BitmapImage(new Uri("../Images/machine.png", UriKind.Relative));
        }

        public object ConvertBack(
            object value,
            Type targetType,
            object parameter,
            CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
