﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Objects;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Threading;

using AdminToolsUI.Helpers;
using DBACentral.Data;

namespace AdminToolsUI.ViewModels
{
    public partial class InventoryViewModel: ViewModelBase
    {
        public void Save()
        {
            try
            {
                Context.SaveChanges();
            }
            catch (InvalidOperationException e)
            {
                MessageBox.Show(string.Format("An error has occurred while saving your changes to the database: {0} Check the Application Event Log for more information.", e.Message)
                                                            , "Inventory: Error", MessageBoxButton.OK, MessageBoxImage.Error);
                LogHelper.WriteToLog(string.Format("An error has occurred while saving changes to the database.  Stack dump: {0}", e.ToString()), EventLogEntryType.Error);
            }
            catch (EntityException e)
            {
                MessageBox.Show(string.Format("An error has occurred while saving your changes to the database: {0} Check the Application Event Log for more information.", e.Message)
                                                            , "Inventory: Error", MessageBoxButton.OK, MessageBoxImage.Error);
                LogHelper.WriteToLog(string.Format("An error has occurred while saving changes to the database: {0}", e.ToString()), EventLogEntryType.Error);
            }
            catch (Exception e)
            {
                if (e.InnerException != null && e.InnerException.Message.Contains("Cannot insert duplicate key"))
                {
                    MessageBox.Show("Cannot insert duplicate record.  Check the Application Event Log for more information.","Inventory: Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    LogHelper.WriteToLog(string.Format("An error has occurred while saving changes to the database.  Stack dump: {0}", e.ToString()), EventLogEntryType.Error);
                }
                else
                {
                    MessageBox.Show(string.Format("An error has occurred while saving your changes to the database: {0} Check the Application Event Log for more information.", e.Message)
                                                                , "Inventory: Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    LogHelper.WriteToLog(string.Format("An error has occurred while saving changes to the database.  Stack dump: {0}", e.ToString()), EventLogEntryType.Error);
                }
              
            }
        }

        public void RejectChanges(object entity)
        {
            try
            {
                if (Context.ObjectStateManager.GetObjectStateEntries(EntityState.Modified).Count<ObjectStateEntry>() > 0)
                {
                    Context.Refresh(RefreshMode.StoreWins, entity);
                }
            }
            catch (InvalidOperationException e)
            {
                MessageBoxResult result = new MessageBoxResult();
                result = MessageBox.Show(string.Format("An error has occurred while canceling your changes: {0} Check the Application Event Log for more information.", e.Message)
                                                            , "Inventory: Error", MessageBoxButton.OK, MessageBoxImage.Error);
                LogHelper.WriteToLog(string.Format("An error has occurred while rejecting changes.  Stack dump: {0}", e.ToString()), EventLogEntryType.Error);
            }
            catch (EntityException e)
            {
                MessageBoxResult result = new MessageBoxResult();
                result = MessageBox.Show(string.Format("An error has occurred while canceling your changes: {0} Check the Application Event Log for more information.", e.Message)
                                                            , "Inventory: Error", MessageBoxButton.OK, MessageBoxImage.Error);
                LogHelper.WriteToLog(string.Format("An error has occurred while rejecting changes.  Stack dump: {0}", e.ToString()), EventLogEntryType.Error);
            }
            catch (Exception e)
            {
                MessageBoxResult result = new MessageBoxResult();
                result = MessageBox.Show(string.Format("An error has occurred while canceling your changes: {0} Check the Application Event Log for more information.", e.Message)
                                                            , "Inventory: Error", MessageBoxButton.OK, MessageBoxImage.Error);
                LogHelper.WriteToLog(string.Format("An error has occurred while rejecting changes.  Stack dump: {0}", e.ToString()), EventLogEntryType.Error);
            }
        }

        public void PossibleMachinesRefresh(List<Machine> currentMachines)
        {
            IEnumerable<int> currentMachineIDs = currentMachines.Select(p => p.MachineID);

            PossibleMachines.Clear();
            Context.Machines
                   .Where(p => p.IsActive == true)
                   .Where(p => !currentMachineIDs.Contains(p.MachineID))
                   .OrderBy(p => p.MachineName).ToList()
                   .ForEach(m => PossibleMachines.Add(m));
        }
        public void PossibleInstancesRefresh(List<DBMSInstance> currentInstances)
        {
            IEnumerable<int> currentInstanceIDs = currentInstances.Select(p => p.DBMSInstanceID);

            PossibleInstances.Clear();
            Context.DBMSInstances.OfType<SQLServer>()
                   .Where(p => p.IsActive == true)
                   .Where(p => !currentInstanceIDs.Contains(p.DBMSInstanceID))
                   .OrderBy(p => p.FullName).ToList()
                   .ForEach(i => PossibleInstances.Add(i));
        }
        public void PossibleClustersRefresh(List<Cluster> currentClusters)
        {
            IEnumerable<int> currentClusterIDs = currentClusters.Select(p => p.ClusterID);

            PossibleClusters.Clear();
            Context.Clusters
                   .Where(p => p.IsActive == true)
                   .Where(p => !currentClusterIDs.Contains(p.ClusterID))
                   .OrderBy(p => p.ClusterName).ToList()
                   .ForEach(c => PossibleClusters.Add(c));
        }
        public void PossiblePeopleRefresh(List<Person> currentPeople)
        {
            IEnumerable<int> currentPersonIDs = currentPeople.Select(p => p.PersonID);

            PossiblePeople.Clear();
            Context.People
                   .Where(p => p.IsActive == true)
                   .Where(p => !currentPersonIDs.Contains(p.PersonID))
                   .OrderBy(p => p.FullName).ToList()
                   .ForEach(o => PossiblePeople.Add(o));
        }
        public void PossibleEnvironmentsRefresh(List<DBACentral.Data.Environment> currentEnvironments)
        {
            IEnumerable<int> currentEnvironmentsIDs = currentEnvironments.Select(p => p.EnvironmentID);

            PossibleEnvironments.Clear();
            Context.Environments
                   .Where(p => p.IsActive == true)
                   .Where(p => !currentEnvironmentsIDs.Contains(p.EnvironmentID))
                   .OrderBy(p => p.EnvironmentName).ToList()
                   .ForEach(e => PossibleEnvironments.Add(e));
        }
        public void PossibleLocationsRefresh(List<Location> currentLocations)
        {
            IEnumerable<int> currentLocationIDs = currentLocations.Select(p => p.LocationID);

            PossibleLocations.Clear();
            Context.Locations
                   .Where(p => p.IsActive == true)
                   .Where(p => !currentLocationIDs.Contains(p.LocationID))
                   .OrderBy(p => p.LocationName).ToList()
                   .ForEach(l => PossibleLocations.Add(l));
        }
    }
}
