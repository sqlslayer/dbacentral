﻿using System.Collections.ObjectModel;
using System.Linq;

using DBACentral.Data;

namespace AdminToolsUI.ViewModels
{
    public partial class InventoryViewModel : ViewModelBase
    {
        public void ContactListRefresh()
        {
            ContactList.Clear();
            Context.People
               .Where(p => p.IsActive)
               .OrderBy(p => p.FullName).ToList()
               .ForEach(p => ContactList.Add(p));

            NotifyPropertyChanged("ContactList");
        }

        public void ContactTypeListRefresh()
        {
            ContactTypeList.Clear();
            Context.ContactTypes
               .OrderBy(p => p.ContactTypeName).ToList()
               .ForEach(p=> ContactTypeList.Add(p));

            NotifyPropertyChanged("ContactTypeList");
        }
    }
}