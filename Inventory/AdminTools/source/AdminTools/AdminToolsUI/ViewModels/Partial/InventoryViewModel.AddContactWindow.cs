﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using DBACentral.Data;

namespace AdminToolsUI.ViewModels
{
    public partial class InventoryViewModel : ViewModelBase
    {
            private ObservableCollection<Person> _contactList;
            public ObservableCollection<Person> ContactList
            {
                get
                {
                    return _contactList ?? (_contactList = new ObservableCollection<Person>());
                }
                set
                {
                    _contactList = value;
                    NotifyPropertyChanged("ContactList");
                }
            }

            private ObservableCollection<ContactType> _contactTypeList;
            public ObservableCollection<ContactType> ContactTypeList
            {
                get
                {
                    return _contactTypeList ?? (_contactTypeList = new ObservableCollection<ContactType>());
                }
                set
                {
                    _contactTypeList = value;
                    NotifyPropertyChanged("ContactTypeList");
                }
            }
    }
}
