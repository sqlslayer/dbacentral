﻿using System.Linq;

namespace AdminToolsUI.ViewModels
{
    public partial class InventoryViewModel : ViewModelBase
    {
        public void ClusterListRefresh(bool showDecommissioned)
        {
            ClusterList.Clear();
            Context.Clusters
                   .Where(p => showDecommissioned || p.IsActive)
                   .OrderBy(p => p.ClusterName).ToList()
                   .ForEach(c => ClusterList.Add(c));
        }

        public void SelectedClusterRefresh()
        {
            NotifyPropertyChanged("SelectedCluster");
        }

        public void SelectedClusterContactListRefresh()
        {
            SelectedClusterContactList.Clear();
            Context.Contacts
                   .Where(p => p.ItemType.ItemTypeName == "Cluster")
                   .Where(p => p.ItemID == SelectedCluster.ClusterID)
                   .OrderBy(p => p.Person.FullName).ToList()
                   .ForEach(c => SelectedClusterContactList.Add(c));

            NotifyPropertyChanged("SelectedClusterContactList");
        }
    }
}