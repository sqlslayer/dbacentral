﻿using System.Collections.ObjectModel;

using DBACentral.Data;

namespace AdminToolsUI.ViewModels
{
    public partial class InventoryViewModel : ViewModelBase
    {
        private ObservableCollection<Cluster> _clusterList;
        public ObservableCollection<Cluster> ClusterList
        {
            get
            {
                return _clusterList ?? (_clusterList = new ObservableCollection<Cluster>());
            }
            set
            {
                _clusterList = value;
                NotifyPropertyChanged("ClusterList");
            }
        }

        private Cluster _selectedCluster;
        public Cluster SelectedCluster
        {
            get
            {
                return _selectedCluster;
            }
            set
            {
                _selectedCluster = value;
                NotifyPropertyChanged("SelectedCluster");
            }
        }

        private ObservableCollection<Contact> _selectedClusterContactList;
        public ObservableCollection<Contact> SelectedClusterContactList
        {
            get
            {
                return _selectedClusterContactList ?? (_selectedClusterContactList = new ObservableCollection<Contact>());
            }
            set
            {
                _selectedClusterContactList = value;
                NotifyPropertyChanged("SelectedClusterContactList");
            }
        }
    }
}