﻿
using DBACentral.Data;

namespace AdminToolsUI.ViewModels
{
    public partial class InventoryViewModel : ViewModelBase
    {
        private Contact _editableContact;
        public Contact EditableContact
        {
            get
            {
                if (_editableContact == null)
                {
                    _editableContact = new Contact();
                }

                return _editableContact;
            }
            set
            {
                _editableContact = value;
                NotifyPropertyChanged("EditableContact");
            }
        }
    }
}
