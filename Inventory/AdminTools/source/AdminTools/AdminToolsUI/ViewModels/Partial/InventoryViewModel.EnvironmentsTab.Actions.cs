﻿using System.Linq;

namespace AdminToolsUI.ViewModels
{
    public partial class InventoryViewModel : ViewModelBase
    {
        public void EnvironmentListRefresh(bool showDecommissioned)
        {
            EnvironmentList.Clear();
            Context.Environments
                   .Where(p => showDecommissioned || p.IsActive)
                   .OrderBy(p => p.EnvironmentName).ToList()
                   .ForEach(e => EnvironmentList.Add(e));
        }

        public void SelectedEnvironmentRefresh()
        {
            NotifyPropertyChanged("SelectedEnvironment");
        }
    }
}