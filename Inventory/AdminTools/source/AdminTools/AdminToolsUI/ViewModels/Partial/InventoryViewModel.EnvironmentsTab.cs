﻿using System.Collections.ObjectModel;

using DBACentral.Data;

namespace AdminToolsUI.ViewModels
{

    public partial class InventoryViewModel : ViewModelBase
    {
        private ObservableCollection<DBACentral.Data.Environment> _environmentList;
        public ObservableCollection<DBACentral.Data.Environment> EnvironmentList
        {
            get
            {
                return _environmentList ?? (_environmentList = new ObservableCollection<DBACentral.Data.Environment>());
            }
            set
            {
                _environmentList = value;
                NotifyPropertyChanged("EnvironmentList");
            }
        }

        private DBACentral.Data.Environment _selectedEnvironment;
        public DBACentral.Data.Environment SelectedEnvironment
        {
            get
            {
                return _selectedEnvironment;
            }
            set
            {
                _selectedEnvironment = value;
                NotifyPropertyChanged("SelectedEnvironment");
            }
        }
    }
}