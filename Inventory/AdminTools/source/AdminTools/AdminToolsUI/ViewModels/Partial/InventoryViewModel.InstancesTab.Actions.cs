﻿using System.Linq;

using DBACentral.Data;

namespace AdminToolsUI.ViewModels
{
    public partial class InventoryViewModel : ViewModelBase
    {
        public void InstanceListRefresh(bool showDecommissioned)
        {
            InstanceList.Clear();
            Context.DBMSInstances.OfType<SQLServer>()
                   .Where(p => showDecommissioned || p.IsActive)
                   .OrderBy(p => p.FullName).ToList()
                   .ForEach(i => InstanceList.Add(i));
        }

        public void SelectedInstanceRefresh()
        {
            NotifyPropertyChanged("SelectedInstance");
        }

        public void SelectedInstanceContactListRefresh()
        {
            SelectedInstanceContactList.Clear();
            Context.Contacts
                   .Where(p => p.ItemType.ItemTypeName == "DBMSInstance")
                   .Where(p => p.ItemID == SelectedInstance.DBMSInstanceID)
                   .OrderBy(p => p.Person.FullName).ToList()
                   .ForEach(c => SelectedInstanceContactList.Add(c));

            NotifyPropertyChanged("SelectedInstanceContactList");
        }
    }
}