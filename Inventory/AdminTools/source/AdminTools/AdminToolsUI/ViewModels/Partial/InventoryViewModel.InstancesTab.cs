﻿using System.Collections.ObjectModel;

using DBACentral.Data;

namespace AdminToolsUI.ViewModels
{

    public partial class InventoryViewModel : ViewModelBase
    {
        private ObservableCollection<DBMSInstance> _instanceList;
        public ObservableCollection<DBMSInstance> InstanceList
        {
            get
            {
                return _instanceList ?? (_instanceList = new ObservableCollection<DBMSInstance>());
            }
            set
            {
                _instanceList = value;
                NotifyPropertyChanged("InstanceList");
            }
        }

        private DBMSInstance _selectedInstance;
        public DBMSInstance SelectedInstance
        {
            get
            {
                return _selectedInstance;
            }
            set
            {
                _selectedInstance = value;
                NotifyPropertyChanged("SelectedInstance");
            }
        }

        private ObservableCollection<Contact> _selectedInstanceContactList;
        public ObservableCollection<Contact> SelectedInstanceContactList
        {
            get
            {
                return _selectedInstanceContactList ?? (_selectedInstanceContactList = new ObservableCollection<Contact>());
            }
            set
            {
                _selectedInstanceContactList = value;
                NotifyPropertyChanged("SelectedInstanceContactList");
            }
        }
    }
}