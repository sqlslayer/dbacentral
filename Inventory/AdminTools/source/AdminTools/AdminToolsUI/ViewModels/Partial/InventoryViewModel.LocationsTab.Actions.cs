﻿using System.Linq;

namespace AdminToolsUI.ViewModels
{
    public partial class InventoryViewModel : ViewModelBase
    {
        public void LocationListRefresh(bool showDecommissioned)
        {
            LocationList.Clear();
            Context.Locations
                   .Where(p => showDecommissioned || p.IsActive)
                   .OrderBy(p => p.LocationName).ToList()
                   .ForEach(l => LocationList.Add(l));
        }

        public void SelectedLocationRefresh()
        {
            NotifyPropertyChanged("SelectedLocation");
        }
    }
}