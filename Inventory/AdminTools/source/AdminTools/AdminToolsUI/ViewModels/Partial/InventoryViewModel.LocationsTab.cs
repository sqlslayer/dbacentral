﻿using System.Collections.ObjectModel;

using DBACentral.Data;

namespace AdminToolsUI.ViewModels
{
    public partial class InventoryViewModel : ViewModelBase
    {
        private ObservableCollection<Location> _locationList;
        public ObservableCollection<Location> LocationList
        {
            get
            {
                return _locationList ?? (_locationList = new ObservableCollection<Location>());
            }
            set
            {
                _locationList = value;
                NotifyPropertyChanged("LocationList");
            }
        }

        private Location _selectedLocation;
        public Location SelectedLocation
        {
            get
            {
                return _selectedLocation;
            }
            set
            {
                _selectedLocation = value;
                NotifyPropertyChanged("SelectedLocation");
            }
        }
    }
}