﻿using System.Collections.Generic;
using System.Linq;

using DBACentral.Data;

namespace AdminToolsUI.ViewModels
{
    public partial class InventoryViewModel : ViewModelBase
    {
        public void MachineListRefresh(bool showDecommissioned)
        {
            MachineList.Clear();
            Context.Machines
                    .Where(p => showDecommissioned || p.IsActive)
                    .OrderBy(p => p.MachineName).ToList()
                    .ForEach(m => MachineList.Add(m));
        }

        public void SelectedMachineRefresh()
        {
            NotifyPropertyChanged("SelectedMachine");
        }

        public void SelectedMachineContactListRefresh()
        {
            SelectedMachineContactList.Clear();
            Context.Contacts
                   .Where(p => p.ItemType.ItemTypeName == "Machine")
                   .Where(p => p.ItemID == SelectedMachine.MachineID)
                   .OrderBy(p => p.Person.FullName).ToList()
                   .ForEach(c => SelectedMachineContactList.Add(c));

            NotifyPropertyChanged("SelectedMachineContactList");
        }
    }
}