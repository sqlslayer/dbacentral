﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using DBACentral.Data;


namespace AdminToolsUI.ViewModels
{

    public partial class InventoryViewModel : ViewModelBase
    {
        private ObservableCollection<Machine> _machineList;
        public ObservableCollection<Machine> MachineList
        {
            get
            {
                if (_machineList == null)
                {
                    _machineList = new ObservableCollection<Machine>();
                }

                return _machineList;
            }
            set
            {
                _machineList = value;
                NotifyPropertyChanged("MachineList");
            }
        }

        private Machine _selectedMachine;
        public Machine SelectedMachine
        {
            get
            {
                return _selectedMachine;
            }
            set
            {
                _selectedMachine = value;
                NotifyPropertyChanged("SelectedMachine");
            }
        }

        private ObservableCollection<Contact> _selectedMachineContactList;
        public ObservableCollection<Contact> SelectedMachineContactList
        {
            get
            {
                return _selectedMachineContactList ?? (_selectedMachineContactList = new ObservableCollection<Contact>());
            }
            set
            {
                _selectedMachineContactList = value;
                NotifyPropertyChanged("SelectedMachineContactList");
            }
        }
    }
}