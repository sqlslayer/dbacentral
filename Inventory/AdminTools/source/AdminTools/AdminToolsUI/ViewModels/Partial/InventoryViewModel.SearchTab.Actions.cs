﻿using System.Linq;

using DBACentral.Data;

namespace AdminToolsUI.ViewModels
{
    public partial class InventoryViewModel : ViewModelBase
    {
        public void Search(string searchString)
        {
            SearchResults.Clear();

            Context.Machines
                   .Where(p => p.MachineName.Contains(searchString)
                            || p.Description.Contains(searchString)
                            || p.Notes.Contains(searchString)
                            || p.OS.Contains(searchString)
                            || p.OSVersion.Contains(searchString))
                   .OrderBy(p => p.MachineName).ToList()
                   .ForEach(m => SearchResults.Add(m));

            Context.Clusters
                   .Where(p => p.ClusterName.Contains(searchString)
                            || p.Description.Contains(searchString)
                            || p.Notes.Contains(searchString))
                   .OrderBy(p => p.ClusterName).ToList()
                   .ForEach(m => SearchResults.Add(m));

            Context.DBMSInstances.OfType<SQLServer>()
                   .Where(p => p.FullName.Contains(searchString)
                            || p.Description.Contains(searchString)
                            || p.Notes.Contains(searchString)
                            || p.Edition.Contains(searchString)
                            || p.Version.Contains(searchString))
                   .OrderBy(p => p.FullName).ToList()
                   .ForEach(m => SearchResults.Add(m));

            Context.Environments
                   .Where(p => p.EnvironmentName.Contains(searchString)
                            || p.Description.Contains(searchString)
                            || p.Notes.Contains(searchString))
                   .OrderBy(p => p.EnvironmentName).ToList()
                   .ForEach(m => SearchResults.Add(m));

            Context.Locations
                   .Where(p => p.LocationName.Contains(searchString)
                            || p.Description.Contains(searchString)
                            || p.Notes.Contains(searchString))
                   .OrderBy(p => p.LocationName).ToList()
                   .ForEach(m => SearchResults.Add(m));
        }
    }
}