﻿using System.Windows.Data;

namespace AdminToolsUI.ViewModels
{

    public partial class InventoryViewModel : ViewModelBase
    {
        private CompositeCollection _searchResults;
        public CompositeCollection SearchResults
        {
            get
            {
                return _searchResults ?? (_searchResults = new CompositeCollection());
            }
            set
            {
                _searchResults = value;
                NotifyPropertyChanged("SearchResults");
            }
        }
    }
}