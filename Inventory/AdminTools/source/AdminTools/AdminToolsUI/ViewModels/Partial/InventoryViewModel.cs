﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Data;

using DBACentral.Data;


namespace AdminToolsUI.ViewModels
{
    public partial class InventoryViewModel : ViewModelBase
    {
        public InventoryViewModel()
        {

        }

        private DBACentralEntities _context;
        public DBACentralEntities Context
        {
            get
            {
                return _context ?? (_context = new DBACentralEntities());
            }
            set
            {
                _context = value;
            }
        }
        
        private ObservableCollection<Machine> _possibleMachines;
        public ObservableCollection<Machine> PossibleMachines
        {
            get
            {
                return _possibleMachines ?? (_possibleMachines = new ObservableCollection<Machine>());
            }
            set
            {
                _possibleMachines = value;
                NotifyPropertyChanged("PossibleMachines");
            }
        }

        private ObservableCollection<Cluster> _possibleClusters;
        public ObservableCollection<Cluster> PossibleClusters
        {
            get
            {
                return _possibleClusters ?? (_possibleClusters = new ObservableCollection<Cluster>());
            }
            set
            {
                _possibleClusters = value;
                NotifyPropertyChanged("PossibleClusters");
            }
        }

        private ObservableCollection<Person> _possiblePeople;
        public ObservableCollection<Person> PossiblePeople
        {
            get
            {
                return _possiblePeople ?? (_possiblePeople = new ObservableCollection<Person>());
            }
            set
            {
                _possiblePeople = value;
                NotifyPropertyChanged("PossiblePeople");
            }
        }

        private ObservableCollection<SQLServer> _possibleInstances;
        public ObservableCollection<SQLServer> PossibleInstances
        {
            get
            {
                return _possibleInstances ?? (_possibleInstances = new ObservableCollection<SQLServer>());
            }
            set
            {
                _possibleInstances = value;
                NotifyPropertyChanged("PossibleInstances");
            }
        }

        private ObservableCollection<DBACentral.Data.Environment> _possibleEnvironments;
        public ObservableCollection<DBACentral.Data.Environment> PossibleEnvironments
        {
            get
            {
                return _possibleEnvironments ?? (_possibleEnvironments = new ObservableCollection<DBACentral.Data.Environment>());
            }
            set
            {
                _possibleEnvironments = value;
                NotifyPropertyChanged("PossibleInstances");
            }
        }


        private ObservableCollection<Location> _possibleLocations;
        public ObservableCollection<Location> PossibleLocations
        {
            get
            {
                return _possibleLocations ?? (_possibleLocations = new ObservableCollection<Location>());
            }
            set
            {
                _possibleLocations = value;
                NotifyPropertyChanged("PossibleLocations");
            }
        }
    }
}
