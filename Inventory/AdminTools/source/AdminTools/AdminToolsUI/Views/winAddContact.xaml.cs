﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using AdminToolsUI.ViewModels;
using DBACentral.Data;

namespace AdminToolsUI.Views
{
    /// <summary>
    /// Interaction logic for winAddContact.xaml
    /// </summary>
    public partial class winAddContact : Window
    {
        public winAddContact(InventoryViewModel viewModel, string obj)
        {
            InitializeComponent();

            _viewModel = viewModel;
            DataContext = _viewModel;

            gbxContainer.Header = obj;

            _viewModel.ContactListRefresh();
            _viewModel.ContactTypeListRefresh();
        }

        #region PROPERTIES
        public InventoryViewModel _viewModel
        {
            get;
            set;
        }
        #endregion

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            if (cbxContact.SelectedIndex == -1)
            {
                MessageBox.Show("Choose a contact.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (cbxContactType.SelectedIndex == -1)
            {
                MessageBox.Show("Choose a contact type.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            this.DialogResult = true;
        }

        private void btnNewContact_Click(object sender, RoutedEventArgs e)
        {
            winNewPerson newPersonWindow = new winNewPerson(_viewModel);
            newPersonWindow.ShowDialog();

            if (newPersonWindow.DialogResult == true)
            {
                _viewModel.ContactListRefresh();
            }
        }

        private void btnNewContactType_Click(object sender, RoutedEventArgs e)
        {
            winNewContactType newContactTypeWindow = new winNewContactType(_viewModel);
            newContactTypeWindow.ShowDialog();

            if (newContactTypeWindow.DialogResult == true)
            {
                _viewModel.ContactTypeListRefresh();
            }
        }
    }
}