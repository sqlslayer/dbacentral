﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using AdminToolsUI.ViewModels;

namespace AdminToolsUI.Views
{
    /// <summary>
    /// Interaction logic for winAddInstance.xaml
    /// </summary>
    public partial class winAddInstance : Window
    {
        public winAddInstance(string thingToAddTo, InventoryViewModel viewModel)
        {
            InitializeComponent();

            txtSelectInstancesCaption.Text = "Select instances for " + thingToAddTo + "...";
            _viewModel = viewModel;
            DataContext = _viewModel;
        }

        #region PROPERTIES
        public InventoryViewModel _viewModel
        {
            get;
            set;
        }
        #endregion

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
