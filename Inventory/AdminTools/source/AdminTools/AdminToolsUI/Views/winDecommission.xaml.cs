﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AdminToolsUI.Views
{
    /// <summary>
    /// Interaction logic for DecommissionMachine.xaml
    /// </summary>
    public partial class winDecommission : Window
    {
        public winDecommission(string objectName, string notes)
        {
            InitializeComponent();
            grpHeader.Header = objectName;
            txtNotes.Text = notes;
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
