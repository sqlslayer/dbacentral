﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using AdminToolsUI.ViewModels;

namespace AdminToolsUI.Views
{
    /// <summary>
    /// Interaction logic for winEditContact.xaml
    /// </summary>
    public partial class winEditContact : Window
    {
        public winEditContact(InventoryViewModel viewModel)
        {
            InitializeComponent();

            _viewModel = viewModel;
            DataContext = _viewModel;

            _viewModel.ContactTypeListRefresh();
        }

        #region PROPERTIES
        public InventoryViewModel _viewModel
        {
            get;
            set;
        }
        #endregion

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            if (cbxContactType.SelectedIndex == -1)
            {
                MessageBox.Show("Choose a contact type.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;

            }
            this.DialogResult = true;
        }

        private void btnNewContactType_Click(object sender, RoutedEventArgs e)
        {
            winNewContactType newContactTypeWindow = new winNewContactType(_viewModel);
            newContactTypeWindow.ShowDialog();

            if (newContactTypeWindow.DialogResult == true)
            {
                _viewModel.ContactTypeListRefresh();
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.RejectChanges(_viewModel.EditableContact);
        }
    }
}
