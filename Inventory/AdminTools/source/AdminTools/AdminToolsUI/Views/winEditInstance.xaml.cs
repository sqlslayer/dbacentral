﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using AdminToolsUI.ViewModels;
using DBACentral.Data;

namespace AdminToolsUI.Views
{
    /// <summary>
    /// Interaction logic for winEditInstance.xaml
    /// </summary>
    public partial class winEditInstance : Window
    {
        public winEditInstance(InventoryViewModel viewModel)
        {
            InitializeComponent();

            _viewModel = viewModel;
            DataContext = _viewModel;
        }

        #region PROPERTIES
        private InventoryViewModel _viewModel
        {
            get;
            set;
        }
        #endregion

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            int tryParseOut;

            if (string.IsNullOrWhiteSpace(txtInstanceName.Text) && !cbxDefaultInstance.IsChecked.Value)
            {
                MessageBox.Show("Enter an instance name or select default.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrWhiteSpace(txtPortNumber.Text) && !cbxDefaultPortNumber.IsChecked.Value)
            {
                MessageBox.Show("Enter a port number or select default.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (cbxDefaultPortNumber.IsChecked.Value == false && (string.IsNullOrWhiteSpace(txtPortNumber.Text) || !Int32.TryParse(txtPortNumber.Text, out tryParseOut)))
            {
                MessageBox.Show("Enter a valid port number or select default.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            txtInstanceName.Text = txtInstanceName.Text;

            // check for duplicate instances //TODO: not working!
            if (cbxDefaultInstance.IsChecked == true)
            {
                if (_viewModel.SelectedInstance.SQLServerDetail.IsClustered)
                {
                    if (_viewModel.SelectedInstance.SQLServerDetail.Cluster.DBMSInstances.OfType<SQLServer>().Where(p => p.DBMSInstanceName == null && p.DBMSInstanceID != _viewModel.SelectedInstance.DBMSInstanceID && p.IsActive == true).Any())
                    {
                        MessageBox.Show("There is already an instance with that name on the cluster " + _viewModel.SelectedInstance.SQLServerDetail.Cluster.ClusterName + ". Either decommission the other instance or choose another name.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                }
                else
                {
                    if (_viewModel.SelectedInstance.SQLServerDetail.Machine.DBMSInstances.OfType<SQLServer>().Where(p => p.DBMSInstanceName == null && p.DBMSInstanceID != _viewModel.SelectedInstance.DBMSInstanceID && p.IsActive == true).Any())
                    {
                        MessageBox.Show("There is already an instance with that name on the Machine " + _viewModel.SelectedInstance.SQLServerDetail.Machine.MachineName + ". Either decommission the other instance or choose another name.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                }
            }
            else
            {
                if (_viewModel.SelectedInstance.SQLServerDetail.IsClustered)
                {
                    if (_viewModel.SelectedInstance.SQLServerDetail.Cluster.DBMSInstances.OfType<SQLServer>().Where(p => p.DBMSInstanceName == txtInstanceName.Text && p.DBMSInstanceID != _viewModel.SelectedInstance.DBMSInstanceID && p.IsActive == true).Any())
                    {
                        MessageBox.Show("There is already an instance with that name on the cluster " + _viewModel.SelectedInstance.SQLServerDetail.Cluster.ClusterName + ". Either decommission the other instance or choose another name.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                }
                else
                {
                    if (_viewModel.SelectedInstance.SQLServerDetail.Machine.DBMSInstances.OfType<SQLServer>().Where(p => p.DBMSInstanceName == txtInstanceName.Text && p.DBMSInstanceID != _viewModel.SelectedInstance.DBMSInstanceID && p.IsActive == true).Any())
                    {
                        MessageBox.Show("There is already an instance with that name on the Machine " + _viewModel.SelectedInstance.SQLServerDetail.Machine.MachineName + ". Either decommission the other instance or choose another name.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                }
            }

            this.DialogResult = true;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.RejectChanges(_viewModel.SelectedInstance);
        }

        // only digits in the port
        private void txtPortNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex(@"[\d+]");
            return !regex.IsMatch(text);
        }
    }
}
