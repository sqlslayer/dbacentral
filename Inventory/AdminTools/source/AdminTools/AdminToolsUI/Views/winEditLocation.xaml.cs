﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using AdminToolsUI.ViewModels;

namespace AdminToolsUI.Views
{
    /// <summary>
    /// Interaction logic for winEditLocation.xaml
    /// </summary>
    public partial class winEditLocation : Window
    {
        public winEditLocation(InventoryViewModel viewModel)
        {
            InitializeComponent();

            _viewModel = viewModel;
            DataContext = _viewModel;
        }

        #region PROPERTIES
        public InventoryViewModel _viewModel
        {
            get;
            set;
        }
        #endregion

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtLocationName.Text))
            {
                MessageBox.Show("Enter a location name.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            this.DialogResult = true;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.RejectChanges(_viewModel.SelectedLocation);
        }
    }
}
