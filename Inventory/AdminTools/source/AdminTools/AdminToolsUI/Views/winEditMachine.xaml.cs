﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows;

using AdminToolsUI.ViewModels;

namespace AdminToolsUI.Views
{
    /// <summary>
    /// Interaction logic for winRename.xaml
    /// </summary>
    public partial class winEditMachine : Window
    {
        public winEditMachine(InventoryViewModel viewModel)
        {
            InitializeComponent();

            _viewModel = viewModel;
            DataContext = _viewModel;
        }

        #region PROPERTIES
        public InventoryViewModel _viewModel
        {
            get;
            set;
        }
        #endregion

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtMachineName.Text))
            {
                MessageBox.Show("Enter a Machine name.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (_viewModel.Context.Machines.Where(p => p.IsActive == true && p.MachineName == txtMachineName.Text && p.MachineID != _viewModel.SelectedMachine.MachineID).Any())
            {
                MessageBox.Show("A Machine with that name already exists.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            
            txtMachineName.Text = txtMachineName.Text;

            this.DialogResult = true;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.RejectChanges(_viewModel.SelectedMachine);
        }
    }
}
