﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using AdminToolsUI.ViewModels;
using DBACentral.Data;

namespace AdminToolsUI.Views
{
    /// <summary>
    /// Interaction logic for winNewContactType.xaml
    /// </summary>
    public partial class winNewContactType : Window
    {
        public winNewContactType(InventoryViewModel viewModel)
        {
            InitializeComponent();

            _viewModel = viewModel;
            DataContext = _viewModel;
        }

        #region PROPERTIES
        public InventoryViewModel _viewModel
        {
            get;
            set;
        }
        #endregion

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNewContactTypeName.Text))
            {
                MessageBox.Show("Enter a new contact type name.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (_viewModel.Context.ContactTypes.Any(p => p.ContactTypeName == txtNewContactTypeName.Text))
            {
                MessageBox.Show(" The contact type name already exists.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            var newContactType = new ContactType();
            newContactType.ContactTypeName = txtNewContactTypeName.Text;
            _viewModel.Context.ContactTypes.AddObject(newContactType);
            _viewModel.Save();

            this.DialogResult = true;
        }
    }
}
