﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using AdminToolsUI.ViewModels;

namespace AdminToolsUI.Views
{
    /// <summary>
    /// Interaction logic for winNewEnvironment.xaml
    /// </summary>
    public partial class winNewEnvironment : Window
    {
        public winNewEnvironment(InventoryViewModel viewModel)
        {
            InitializeComponent();

            _viewModel = viewModel;
            DataContext = _viewModel;
        }

        #region PROPERTIES
        public InventoryViewModel _viewModel
        {
            get;
            set;
        }
        #endregion

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtEnvironmentName.Text))
            {
                MessageBox.Show("Enter an environment name.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            var newEnv = new DBACentral.Data.Environment();
            newEnv.IsActive = true;
            newEnv.EnvironmentName = txtEnvironmentName.Text;
            if (!string.IsNullOrWhiteSpace(txtDescription.Text))
            {
                newEnv.Description = txtDescription.Text;
            }
            if (!string.IsNullOrWhiteSpace(txtNotes.Text))
            {
                newEnv.Notes = txtNotes.Text;
            }

            _viewModel.Context.Environments.AddObject(newEnv);
            _viewModel.Save();

            this.DialogResult = true;
        }
    }
}
