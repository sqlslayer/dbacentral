﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.SqlServer.Management.Common;

using AdminToolsUI.Helpers;
using AdminToolsUI.ViewModels;
using Collector;
using DBACentral.Data;

namespace AdminToolsUI.Views
{
    /// <summary>
    /// This window is for creating a new SQL instance.
    /// </summary>
    public partial class winNewInstance : Window
    {
        private SQLServer _newSQLServer;
        private string _fullName;

        public winNewInstance(InventoryViewModel viewModel)
        {
            InitializeComponent();

            _newSQLServer = new SQLServer();
            _fullName = null;

            _viewModel = viewModel;
            DataContext = _viewModel;
        }

        #region PROPERTIES
        public InventoryViewModel _viewModel
        {
            get;
            set;
        }
        #endregion

        private void btnOK_Click(object sender, RoutedEventArgs rea)
        {
            BackgroundWorker worker = new BackgroundWorker();
            _fullName = BuildFullName();

            // validate input
            int tryParseOut;
            if (string.IsNullOrWhiteSpace(txtMachineName.Text))
            {
                System.Windows.MessageBox.Show("Enter a Machine name.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrWhiteSpace(txtInstanceName.Text) && cbxDefaultInstance.IsChecked == false)
            {
                System.Windows.MessageBox.Show("Enter an instance name or select default", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if ((string.IsNullOrWhiteSpace(txtPortNumber.Text) || !Int32.TryParse(txtPortNumber.Text, out tryParseOut)) && cbxDefaultPortNumber.IsChecked == false)
            {
                System.Windows.MessageBox.Show("Enter a valid port number or select default.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (_viewModel.Context.DBMSInstances.OfType<SQLServer>().Any(p => p.FullName == _fullName && p.IsActive))
            {
                MessageBox.Show("The instance already exists and cannot not be added.", txtMachineName.Text, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            BuildNewSQLServer();


            // collect instance information
            worker.DoWork += (o, ea) =>
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    try
                    {
                        if (cbxCollect.IsChecked == true)
                        {
                            CollectSQLServerInformation();
                        }

                        _viewModel.Save();
                        MessageBox.Show("The instance has been added.", _newSQLServer.FullName, MessageBoxButton.OK, MessageBoxImage.Information);
                        this.DialogResult = true;
                    }
                    catch (UnauthorizedAccessException)
                    {
                        MessageBox.Show(string.Format("Access denied to SQL instance {0}.", _newSQLServer.FullName, "Error", MessageBoxButton.OK, MessageBoxImage.Error));
                        LogHelper.WriteToLog(string.Format("Access denied to SQL instance {0}.", _newSQLServer.FullName), EventLogEntryType.FailureAudit);
                        // TODO: try again option or maybe don't collect option? same for addmachine?

                        return;
                    }
                    catch (ConnectionFailureException e)
                    {
                        if (e.InnerException.Message.Contains("Login failed for user"))
                        {
                            MessageBox.Show(string.Format("Login failure attempting to connect to SQL instance {0}.", _newSQLServer.FullName), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                            LogHelper.WriteToLog(string.Format("Login failure attempting to connect to SQL instance {0}.", _newSQLServer.FullName), EventLogEntryType.FailureAudit);

                            return;
                        }
                        else if (e.InnerException.Message.Contains("Timeout Expired"))
                        {
                            MessageBox.Show(string.Format("A timeout occurred attempting to connect to the SQL instance {0}.", _newSQLServer.FullName), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                            LogHelper.WriteToLog(string.Format("A timeout occurred attempting to connect to the SQL instance {0}.", _newSQLServer.FullName), EventLogEntryType.Warning);

                            return;
                        }
                        else if (e.InnerException.Message.Contains("A network-related or instance-specific error occurred "))
                        {
                            MessageBox.Show(string.Format("SQL instance {0} was not found or was not accessible. Verify that the instance name is correct and that SQL Machine is configured to allow remote connections.", _newSQLServer.FullName), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                            LogHelper.WriteToLog(string.Format("SQL instance {0} was not found or was not accessible. Verify that the instance name is correct and that SQL Machine is configured to allow remote connections.", _newSQLServer.FullName), EventLogEntryType.Warning);

                            return;
                        }
                        else
                        {
                            MessageBox.Show(string.Format("Unable to connect to SQL instance {0} : {1}", _newSQLServer.FullName, e.ToString()), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                            LogHelper.WriteToLog(string.Format("Unable to connect to SQL instance {0} : {1}", _newSQLServer.FullName, e.ToString()), EventLogEntryType.Error);

                            return;
                        }
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(string.Format("An error was encountered collecting information from the SQL instance {0} : {1}", _newSQLServer.FullName, e.ToString()), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        LogHelper.WriteToLog(string.Format("An error was encountered collecting information from the SQL instance {0} : {1}", _newSQLServer.FullName, e.ToString()), EventLogEntryType.Error);

                        return;
                    }
                }));
            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                bsyCollecting.IsBusy = false;
            };

            bsyCollecting.IsBusy = true;
            worker.RunWorkerAsync();
        }

        private string BuildFullName()
        {
            // build full name for collector (the fullname is a calculated field in the DB so it's not created until we actually add the new instance to the database
            StringBuilder fullNameStringBuilder = new StringBuilder().Append(txtMachineName.Text);

            if (cbxDefaultInstance.IsChecked == true)
            {
                _newSQLServer.DBMSInstanceName = null;
            }
            else
            {
                fullNameStringBuilder.Append(string.Join("\\", txtInstanceName.Text));
            }

            if (cbxDefaultPortNumber.IsChecked == true)
            {
                _newSQLServer.PortNumber = null;
            }
            else
            {
                fullNameStringBuilder.Append(string.Join(",", txtPortNumber.Text));
            }

            return fullNameStringBuilder.ToString();
        }

        private void BuildNewSQLServer()
        {
            // create bones of the instance and build connection string
            _newSQLServer = new SQLServer();
            _newSQLServer.IsActive = true;
            _newSQLServer.IP = Collect.GetIPv4(txtMachineName.Text);
            _newSQLServer.Description = txtDescription.Text;
            _newSQLServer.Notes = txtNotes.Text;
        }
        private void CollectSQLServerInformation()
        {
            Collect.SQLServer(_newSQLServer, _fullName);

            // collect cluster information
            if (_newSQLServer.IsClustered)
            {
                _newSQLServer.VirtualServerName = txtMachineName.Text;
                string clusterName = Collect.GetClusterName(_newSQLServer.VirtualServerName);

                // create cluster if it doesn't exist
                if (_viewModel.Context.Clusters.Where(p => p.IsActive == true && p.ClusterName == clusterName).Any())
                {
                    var newCluster = new Cluster();
                    newCluster.IsActive = true;
                    Collect.Cluster(newCluster,_newSQLServer.VirtualServerName);
                    newCluster.Notes = string.Format("Added to inventory while collecting information about {0}.", _newSQLServer.FullName);
                    _viewModel.Context.Clusters.AddObject(newCluster);
                    _viewModel.Save();
                }

                _newSQLServer.Cluster = _viewModel.Context.Clusters.Where(p => p.IsActive == true && p.ClusterName == clusterName).FirstOrDefault();

                Collect.Cluster(_newSQLServer.Cluster, _newSQLServer.VirtualServerName);
            }
            // not part of a cluster so we need to create the machine if it doesn't exist and assign the machine to the instance
            else
            {
                if (_viewModel.Context.Machines.Where(p => p.MachineName == txtMachineName.Text && p.IsActive == true).Any())
                {
                    // an active machine already exists so we can associate it with the instance
                    _viewModel.Context.Machines.Where(p => p.MachineName == txtMachineName.Text).FirstOrDefault().DBMSInstances.Add(_newSQLServer);
                }
                else
                {
                    // the machine has not been created so we have to ask the user to create it then we associtate it with the instance and save
                    Dispatcher.Invoke((Action)(() =>
                    {
                        winNewMachine newMachineWindow = new winNewMachine(_viewModel);

                        newMachineWindow.txtMachineName.Text = _newSQLServer.Machine.MachineName;
                        newMachineWindow.txtDescription.Text = _newSQLServer.Description;
                        newMachineWindow.txtNotes.Text = _newSQLServer.Notes;
                        newMachineWindow.ShowDialog();

                        if (newMachineWindow.DialogResult == true)
                        {
                            _viewModel.Context.Machines.Where(p => p.MachineName == _newSQLServer.Machine.MachineName && p.IsActive == true).FirstOrDefault().DBMSInstances.Add(_newSQLServer);
                        }
                        else
                        {
                            newMachineWindow.Close();
                        }
                    }));
                }
            }


        }

        // only allow digits in the port
        private void txtPortNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex(@"[\d+]");
            return !regex.IsMatch(text);
        }

    }
}
