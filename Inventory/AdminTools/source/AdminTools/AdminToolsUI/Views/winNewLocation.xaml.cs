﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using AdminToolsUI.ViewModels;
using DBACentral.Data;

namespace AdminToolsUI.Views
{
    /// <summary>
    /// Interaction logic for winNewLocation.xaml
    /// </summary>
    public partial class winNewLocation : Window
    {
        public winNewLocation(InventoryViewModel viewModel)
        {
            InitializeComponent();

            _viewModel = viewModel;
            DataContext = _viewModel;
        }

        #region PROPERTIES
        public InventoryViewModel _viewModel
        {
            get;
            set;
        }
        #endregion

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtLocationName.Text))
            {
                System.Windows.MessageBox.Show("Enter a location name.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            var newLocation = new Location();
            newLocation.IsActive = true;
            newLocation.LocationName = txtLocationName.Text;
            if (!string.IsNullOrWhiteSpace(txtDescription.Text))
            {
                newLocation.Description = txtDescription.Text;
            }
            if (!string.IsNullOrWhiteSpace(txtNotes.Text))
            {
                newLocation.Notes = txtNotes.Text;
            }

            _viewModel.Context.Locations.AddObject(newLocation);
            _viewModel.Save();

            this.DialogResult = true;
        }
    }
}
