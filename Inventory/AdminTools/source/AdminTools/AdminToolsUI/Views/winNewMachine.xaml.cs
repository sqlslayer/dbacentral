﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Data.Objects;
using DBACentral.Data;

using System.ComponentModel;
using Collector;
using AdminToolsUI.ViewModels;
using System.Diagnostics;
using AdminToolsUI.Helpers;
using System.Runtime.InteropServices;

namespace AdminToolsUI.Views
{   
    /// <summary>
    /// Interaction logic for winNewMachine.xaml
    /// </summary>
    public partial class winNewMachine : Window
    {
        private Machine _newMachine;

        public winNewMachine(InventoryViewModel viewModel)
        {
            InitializeComponent();

            _newMachine = null;
            _viewModel = viewModel;
            DataContext = _viewModel;
        }

        #region PROPERTIES
        private InventoryViewModel _viewModel
        {
            get;
            set;
        }
        #endregion

        private void btnOK_Click(object sender, RoutedEventArgs rea)
        {
            BackgroundWorker worker = new BackgroundWorker();

            // validate input
            if (string.IsNullOrWhiteSpace(txtMachineName.Text))
            {
                MessageBox.Show("Enter a Machine name.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (_viewModel.Context.Machines.Any(p => p.MachineName == txtMachineName.Text && p.IsActive))
            {
                MessageBox.Show("The Machine already exists and cannot not be added.", txtMachineName.Text, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            BuildNewMachine();

            if (cbxCollect.IsChecked == true)
            {
                // collect Machine info and return
                worker.DoWork += (o, ea) =>
                {
                    Dispatcher.Invoke((Action)(() =>
                    {
                        try
                        {
                            CollectMachineInformation();

                            _viewModel.Context.Machines.AddObject(_newMachine);
                            _viewModel.Save();
                            MessageBox.Show("The Machine has been added.", _newMachine.MachineName, MessageBoxButton.OK, MessageBoxImage.Information);
                            this.DialogResult = true;
                        }
                        catch (UnauthorizedAccessException)
                        {
                            MessageBox.Show(string.Format("Access denied to machine {0}.", _newMachine.MachineName), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                            LogHelper.WriteToLog(string.Format("Access denied to machine {0}.", _newMachine.MachineName), EventLogEntryType.FailureAudit);
                        }
                        catch (COMException e)
                        {
                            MessageBox.Show(string.Format("Access denied to machine {0}.", _newMachine.MachineName), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                            LogHelper.WriteToLog(string.Format("An error was encountered while attemping to collect information from the machine {0} : {1}", _newMachine.MachineName, e.Message), EventLogEntryType.Error);
                        }
                        catch (ManagementException e)
                        {
                            MessageBox.Show(string.Format("An error was encountered while attemping to collect information from the machine {0} : {1}", _newMachine.MachineName, e.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                            LogHelper.WriteToLog(string.Format("An error was encountered while attemping to collect information from the machine {0} : {1}", _newMachine.MachineName, e.Message), EventLogEntryType.Error);
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show(string.Format("An error was encountered while attemping to collect information from the machine {0} : {1}", _newMachine.MachineName, e.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                            LogHelper.WriteToLog(string.Format("An error was encountered while attemping to collect information from the machine {0} : {1}", _newMachine.MachineName, e.Message), EventLogEntryType.Error);
                        }
                    }));
                };
                worker.RunWorkerCompleted += (o, ea) =>
                {
                    bsyCollecting.IsBusy = false;
                };
                bsyCollecting.IsBusy = true;
                worker.RunWorkerAsync();
            }
            else
            {
                _viewModel.Context.Machines.AddObject(_newMachine);
                _viewModel.Save();



                MessageBox.Show("The Machine has been added.", _newMachine.MachineName, MessageBoxButton.OK, MessageBoxImage.Information);
                this.DialogResult = true;
            }
        }

        private void BuildNewMachine()
        {
            // add new machine to db
            _newMachine = new Machine();
            _newMachine.IsActive = true;
            _newMachine.MachineName = txtMachineName.Text;
            if (!string.IsNullOrWhiteSpace(txtDescription.Text))
            {
                _newMachine.Description = txtDescription.Text;
            }
            if (!string.IsNullOrWhiteSpace(txtNotes.Text))
            {
                _newMachine.Notes = txtNotes.Text;
            }
            if (cboEnvironments.SelectedIndex > -1)
            {
                _newMachine.Environment = cboEnvironments.SelectedItem as DBACentral.Data.Environment;
            }
            if (cboLocations.SelectedIndex > -1)
            {
                _newMachine.Location = cboLocations.SelectedItem as Location;
            }
        }
        private void CollectMachineInformation()
        {
            Collect.Machine(_newMachine);

            string clusterName = Collect.GetClusterName(_newMachine.MachineName);

            if (clusterName != null)
            {
                if (!_viewModel.Context.Clusters.Where(p => p.IsActive == true && p.ClusterName == clusterName).Any())
                {
                    var newCluster = new Cluster();
                    newCluster.IsActive = true;
                    Collect.Cluster(newCluster, _newMachine.MachineName);
                    newCluster.Notes = string.Format("Added to inventory while collecting information about {0}.", _newMachine.MachineName);
                    _viewModel.Context.Clusters.AddObject(newCluster);
                    _viewModel.Context.SaveChanges();
                }

                _newMachine.Cluster = _viewModel.Context.Clusters.Where(p => p.ClusterName == clusterName && p.IsActive == true).FirstOrDefault();
            }

        }
    }
}
