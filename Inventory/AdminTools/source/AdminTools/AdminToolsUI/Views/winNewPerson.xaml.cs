﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using AdminToolsUI.ViewModels;
using DBACentral.Data;

namespace AdminToolsUI.Views
{
    /// <summary>
    /// Interaction logic for winNewPerson.xaml
    /// </summary>
    public partial class winNewPerson : Window
    {
        public winNewPerson(InventoryViewModel viewModel)
        {
            InitializeComponent();

            _viewModel = viewModel;
            DataContext = _viewModel;
        }

        #region PROPERTIES
        public InventoryViewModel _viewModel
        {
            get;
            set;
        }
        #endregion

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtFirstName.Text))
            {
                System.Windows.MessageBox.Show("Enter a first name.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            var newPerson = new Person();
            newPerson.IsActive = true;
            newPerson.FirstName = txtFirstName.Text;
            if (!string.IsNullOrWhiteSpace(txtLastName.Text))
            {
                newPerson.LastName = txtLastName.Text;
            }
            _viewModel.Context.People.AddObject(newPerson);
            _viewModel.Save();


            this.DialogResult = true;
        }
    }
}
