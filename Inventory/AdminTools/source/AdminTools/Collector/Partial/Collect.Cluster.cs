﻿using System;
using System.Management;

using DBACentral.Data;
using System.Diagnostics;

namespace Collector
{
    public static partial class Collect
    {
        public static void Cluster(Cluster clusterEntity, string machineName)
        {
            try
            {
                ConnectionOptions connectionOptions = new ConnectionOptions();
                connectionOptions.Authentication = AuthenticationLevel.PacketPrivacy;
                ManagementObjectSearcher searcher = new ManagementObjectSearcher(
                                    new ManagementScope(string.Format("\\\\{0}\\root\\mscluster", machineName), connectionOptions)
                                    , new ObjectQuery("SELECT Name,QuorumType,QuorumPath,InstallDate FROM MSCluster_Cluster"));


                foreach (ManagementObject iQueryObj in searcher.Get())
                {
                    clusterEntity.ClusterName = iQueryObj["Name"].ToString().ToUpper();
                    clusterEntity.QuorumType = iQueryObj["QuorumType"].ToString(); // only supported by 2008 and up
                    clusterEntity.QuorumPath = iQueryObj["QuorumPath"].ToString(); // only supported by 2008 and up
                    try
                    {
                        clusterEntity.InstallDate = ManagementDateTimeConverter.ToDateTime(iQueryObj["InstallDate"].ToString()).Date; // not always availble and cannot be set to null
                    }
                    catch 
                    { 
                        clusterEntity.InstallDate = DateTime.MinValue;
                    }
                }

                clusterEntity.IP = Collect.GetIPv4(clusterEntity.ClusterName);
                clusterEntity.LastUpdated = DateTime.Now;
            }
            catch (Exception e)
            {
                if (!e.Message.Contains("Invalid query") && !e.Message.Contains("Invalid namespace")) // normal exceptions for nonclustered machines
                {
                    Debug.WriteLine(string.Format("Error collecting cluster information from {0} : {1}", clusterEntity.ClusterName.ToString(), e.ToString()));
                }
            }
        }
    }
}