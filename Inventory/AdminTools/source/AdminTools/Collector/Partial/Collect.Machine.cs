﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Management;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Security;
using Microsoft.Win32;

using DBACentral.Data;

namespace Collector
{
    public static partial class Collect
    {
        public static void Machine(Machine machineEntity)
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(
                                                new ManagementScope(string.Format("\\\\{0}\\root\\cimv2",machineEntity.MachineName))
                                               ,new ObjectQuery("SELECT Caption,Version,InstallDate,ServicePackMajorVersion FROM Win32_OperatingSystem"));


            // OS name, version, InstallDate - no try/catch because we want to know if there's a problem accessing the machine
            foreach (ManagementObject iQueryObj in searcher.Get())
            {
                machineEntity.OS = iQueryObj["Caption"].ToString();
                machineEntity.OSVersion = iQueryObj["Version"].ToString();
                machineEntity.InstallDate = ManagementDateTimeConverter.ToDateTime(iQueryObj["InstallDate"].ToString()).Date;
                machineEntity.OSServicePack = Convert.ToByte(iQueryObj["ServicePackMajorVersion"].ToString());
            }


            // IP
            machineEntity.IP = GetIPv4(machineEntity.MachineName);

            // Number of procs
            try
            {
                if (machineEntity.OSVersion != null)
                {
                    // NumberOfCores and NumberOfLogicalProcessors not supported in Windows Server 2003, Windows XP, and Windows 2000
                    if (Convert.ToDouble(machineEntity.OSVersion.Substring(0, 3)) > 5.2)
                    {
                        searcher.Query.QueryString = "SELECT NumberOfProcessors,NumberOfLogicalProcessors FROM Win32_ComputerSystem";
                        foreach (ManagementObject iQueryObj in searcher.Get())
                        {
                            machineEntity.NumberOfProcessors = Convert.ToInt16(iQueryObj["NumberOfProcessors"]);
                            machineEntity.NumberOfLogicalProcessors = Convert.ToInt16(iQueryObj["NumberOfLogicalProcessors"]);
                        }

                        searcher.Query.QueryString = "SELECT NumberOfLogicalProcessors,NumberOfCores FROM Win32_Processor";
                        foreach (ManagementObject iQueryObj in searcher.Get())
                        {
                            machineEntity.IsHTEnabled = (Convert.ToInt32(iQueryObj["NumberOfCores"]) < Convert.ToInt32(iQueryObj["NumberOfLogicalProcessors"]));
                        }
                    }
                    else
                    {
                        //Because the NumberOfLogicalProcessors property is not available, NumberOfProcessors indicates the number 
                        //of logical processors available in the system. In the case of a computer system that has two physical processors each 
                        //containing two logical processors, the value of NumberOfProcessors is 4.
                        searcher.Query.QueryString = "SELECT NumberOfProcessors FROM Win32_ComputerSystem";
                        foreach (ManagementObject iQueryObj in searcher.Get())
                        {
                            machineEntity.NumberOfLogicalProcessors = Convert.ToInt16(iQueryObj["NumberOfProcessors"]);
                        }
                    }
                }
            }
            catch { }

            // Domain, manufacturer, model
            try
            {
                searcher.Query.QueryString = "SELECT Domain,Manufacturer,Model FROM Win32_ComputerSystem";
                foreach (ManagementObject iQueryObj in searcher.Get())
                {
                    machineEntity.Domain = iQueryObj["Domain"].ToString();
                    machineEntity.Manufacturer = iQueryObj["Manufacturer"].ToString();
                    machineEntity.Model = iQueryObj["Model"].ToString();
                }
            }
            catch { }

            try
            {
                // check if it's physical or virtual - couldn't find any other way to do it
                if (machineEntity.Model.Contains("VMware"))
                {
                    machineEntity.IsVirtual = true;
                }
                else
                {
                    searcher.Query.QueryString = "SELECT Manufacturer FROM Win32_BaseBoard";
                    foreach (ManagementObject iQueryObj in searcher.Get())
                    {
                        if (iQueryObj["Manufacturer"].ToString() == "Microsoft Corporation")
                        {
                            machineEntity.IsVirtual = true;
                        }
                        else
                        {
                            machineEntity.IsVirtual = false;
                        }
                    }
                }
            }
            catch { }

            // Proc architecture and name
            try
            {
                searcher.Query.QueryString = "SELECT Name,AddressWidth,Architecture FROM Win32_Processor WHERE DeviceID=\"CPU0\"";
                foreach (ManagementObject iQueryObj in searcher.Get())
                {
                    machineEntity.Processor = iQueryObj["Name"].ToString();
                    machineEntity.ProcessorAddressWidth = Convert.ToInt16(iQueryObj["AddressWidth"]);
                    switch (Convert.ToInt16(iQueryObj["Architecture"].ToString()))
                    {
                        case 0:
                            machineEntity.ProcessorArchitecture = "x86";
                            break;
                        case 1:
                            machineEntity.ProcessorArchitecture = "MIPS";
                            break;
                        case 2:
                            machineEntity.ProcessorArchitecture = "Alpha";
                            break;
                        case 3:
                            machineEntity.ProcessorArchitecture = "PowerPC";
                            break;
                        case 6:
                            machineEntity.ProcessorArchitecture = "Itanium";
                            break;
                        case 9:
                            machineEntity.ProcessorArchitecture = "x64";
                            break;
                    }
                }
            }
            catch { }

            // OS architecture
            try
            {
                using (var key = (RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, machineEntity.MachineName)).OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\"))
                {
                    if (key.GetValue("ProgramFilesDir (x86)") != null)
                    {
                        machineEntity.OSArchitecture = 64;
                    }
                    else
                    {
                        machineEntity.OSArchitecture = 32;
                    }
                }
            }
            catch { }



            // Memory
            try
            {
                int total = 0;

                searcher.Query.QueryString = "SELECT Capacity FROM Win32_PhysicalMemory";
                foreach (ManagementObject iQueryObj in searcher.Get())
                {
                    total += (int)(Convert.ToUInt64(iQueryObj["Capacity"].ToString()) / 1048576);
                }

                machineEntity.PhysicalMemoryMB = total;
            }
            catch { }

            // Power plan
            try
            {
                searcher.Scope.Path = new ManagementPath(string.Format("\\\\{0}\\root\\cimv2\\power", machineEntity.MachineName));
                searcher.Query.QueryString = "SELECT ElementName FROM Win32_PowerPlan WHERE IsActive='TRUE'";
                foreach (ManagementObject iQueryObj in searcher.Get())
                {
                    machineEntity.PowerPlan = iQueryObj["ElementName"].ToString();
                }
            }
            catch { }

            machineEntity.LastUpdated = DateTime.Now;
        }
    }
}
