﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;

using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Smo.Mail;
using Microsoft.SqlServer.Management.Smo.Agent;

using DBACentral.Data;


namespace Collector
{
    public static partial class Collect
    {
        public static void SQLServer(SQLServer sqlEntity, string fullName)
        {

            Server sqlInstance = new Server(fullName);
            sqlInstance.ConnectionContext.ConnectTimeout = 5;
            sqlInstance.ConnectionContext.ApplicationName = "DBA Collector";

            // start collecting...
            sqlEntity.IsClustered = sqlInstance.IsClustered;
            sqlEntity.MinServerMemory = sqlInstance.Configuration.MinServerMemory.RunValue;
            sqlEntity.MaxServerMemory = sqlInstance.Configuration.MaxServerMemory.RunValue;
            sqlEntity.MaxDegreeOfParallelism = sqlInstance.Configuration.MaxDegreeOfParallelism.RunValue;
            sqlEntity.Platform = sqlInstance.Information.Platform;
            sqlEntity.Build = sqlInstance.Version.ToString();
            sqlEntity.ProductLevel = sqlInstance.ProductLevel;
            sqlEntity.NumberOfDatabases = sqlInstance.Databases.Count;
            sqlEntity.Language = sqlInstance.Language;
            sqlEntity.Collation = sqlInstance.Collation;
            sqlEntity.IsFullTextInstalled = sqlInstance.IsFullTextInstalled;
            sqlEntity.RootDirectory = sqlInstance.RootDirectory;
            sqlEntity.InstallDataDirectory = sqlInstance.InstallDataDirectory.ToString();
            sqlEntity.NumberOfLogFiles = sqlInstance.NumberOfLogFiles;
            
            sqlEntity.ErrorLogPath = sqlInstance.ErrorLogPath;
            sqlEntity.Edition = sqlInstance.Edition.ToString();
            if (sqlEntity.Edition.Contains("64"))
            {
                sqlEntity.Architecture = 64;
            }
            else
            {
                sqlEntity.Architecture = 32;
            }
            switch (sqlInstance.VersionMajor)
            {
                case 7:
                    sqlEntity.Version = "7.0";
                    break;
                case 8:
                    sqlEntity.Version = "2000";
                    break;
                case 9:
                    sqlEntity.Version = "2005";
                    break;
                case 10:
                    if (sqlInstance.VersionMinor == 5)
                    {
                        sqlEntity.Version = "2008 R2";
                    }
                    else
                    {
                        sqlEntity.Version = "2008";
                    }
                    break;
                case 11:
                    sqlEntity.Version = "2012";
                    break;
                default:
                    sqlEntity.Version = "UNKNOWN";
                    break;
            }

            // there's a bug that returns an empty string if the value in the registry is not set
            if (string.IsNullOrWhiteSpace(sqlInstance.Settings.DefaultFile))
            {
                sqlEntity.DefaultFile = null;
            }
            else
            {
                sqlEntity.DefaultFile = sqlInstance.Settings.DefaultFile;
            }
            if (string.IsNullOrWhiteSpace(sqlInstance.Settings.DefaultLog))
            {
                sqlEntity.DefaultLog = null;
            }
            else
            {
                sqlEntity.DefaultLog = sqlInstance.Settings.DefaultLog;
            }

            // linked servers
            if (sqlInstance.LinkedServers.Count > 0)
            {
                sqlEntity.LinkedServers = string.Join(",", sqlInstance.LinkedServers
                                                                      .OfType<LinkedServer>()
                                                                      .Select(ls => ls.Name));
            }
            else
            {
                sqlEntity.LinkedServers = null;
            }

            // edition dependent properties
            if (!sqlInstance.Edition.Contains("Desktop") && !sqlInstance.Edition.Contains("Express"))
            {
                sqlEntity.BackupDirectory = sqlInstance.BackupDirectory;
                sqlEntity.AgentJobCount = sqlInstance.JobServer.Jobs.Count;
                if (sqlInstance.JobServer.Alerts.Count > 0)
                {
                    sqlEntity.AgentAlerts = string.Join(",", sqlInstance.JobServer.Alerts
                                                                                  .OfType<Alert>()
                                                                                  .Select(p => p.Name));
                }
                else
                {
                    sqlEntity.AgentAlerts = null;
                }
                if (sqlInstance.JobServer.Operators.Count > 0)
                {
                    sqlEntity.AgentOperators = string.Join(",", sqlInstance.JobServer.Operators
                                                                                     .OfType<Operator>()
                                                                                     .Select(p => p.Name));
                }
                else
                {
                    sqlEntity.AgentOperators = null;
                }
            }

            // only >= 2005 stuff
            if (sqlInstance.VersionMajor > 8)
            {
                sqlEntity.EngineServiceStartMode = sqlInstance.ServiceStartMode.ToString();
                sqlEntity.InstallSharedDirectory = sqlInstance.InstallSharedDirectory.ToString();
                sqlEntity.SqlSortOrderName = sqlInstance.SqlSortOrderName.ToString();
                sqlEntity.IsTcpEnabled = sqlInstance.TcpEnabled;
                sqlEntity.EngineServiceAccount = sqlInstance.ServiceAccount;
                sqlEntity.BrowserServiceAccount = sqlInstance.BrowserServiceAccount;

                // global trace flags
                if (sqlInstance.EnumActiveGlobalTraceFlags().Rows.Count > 0)
                {
                    sqlEntity.GlobalTraceFlags = string.Join(",", sqlInstance.EnumActiveGlobalTraceFlags().AsEnumerable().Select(p => p[0].ToString()));
                }
                else
                {
                    sqlEntity.GlobalTraceFlags = null;
                }

                // endpoints
                if (sqlInstance.Endpoints.Count > 0)
                {
                    sqlEntity.Endpoints = string.Join(",", sqlInstance.Endpoints.OfType<Endpoint>()
                                                                                .Select(ls => ls.Name));
                }
                else
                {
                    sqlEntity.Endpoints = null;
                }

                // get edition dependent properties
                if (!sqlInstance.Edition.Contains("Desktop") && !sqlInstance.Edition.Contains("Express"))
                {
                    if (sqlInstance.Configuration.AgentXPsEnabled.ConfigValue == 1) // can only grab these values if agentxps are enabled
                    {
                        sqlEntity.AgentErrorLogFile = sqlInstance.JobServer.ErrorLogFile;
                        sqlEntity.AgentServiceAccount = sqlInstance.JobServer.ServiceAccount;
                    }


                    if (sqlInstance.Mail.Profiles.Count > 0)
                    {
                        sqlEntity.MailProfiles = string.Join(",", sqlInstance.Mail.Profiles.OfType<MailProfile>()
                                                                                           .Select(p => p.Name));
                    }
                    else
                    {
                        sqlEntity.MailProfiles = null;
                    }
                }
            }

            // only >= 2012 stuff
            if (sqlInstance.VersionMajor >= 11)
            {
                sqlEntity.AvailabilityGroups = string.Join(",", sqlInstance.AvailabilityGroups
                                                                           .OfType<AvailabilityGroup>()
                                                                           .Select(p => p.Name));
                sqlEntity.IsHADREnabled = sqlInstance.IsHadrEnabled;
            }

            sqlInstance.ConnectionContext.Disconnect();
            sqlEntity.LastUpdated = DateTime.Now;

        }
    }
}
