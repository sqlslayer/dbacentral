﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.Sockets;
using System.Text;

using DBACentral.Data;

namespace Collector
{
    public static partial class Collect
    {
        public static string GetClusterName(string machineName)
        {
            string clusterName = null;

            try
            {
                ConnectionOptions connectionOptions = new ConnectionOptions();
                connectionOptions.Authentication = AuthenticationLevel.PacketPrivacy;
                ManagementObjectSearcher searcher = new ManagementObjectSearcher(
                                    new ManagementScope(string.Format("\\\\{0}\\root\\mscluster",machineName),connectionOptions)
                                    , new ObjectQuery("SELECT Name FROM MSCluster_Cluster"));


                foreach (ManagementObject iQueryObj in searcher.Get())
                {
                    clusterName = iQueryObj["Name"].ToString().ToUpper();
                }

                return clusterName;
            }
            catch
            { 
                return null; 
            }

        }
        public static void UpdateClusterName(Machine machineEntity, int timeout)
        {
            try
            {
                using (var context = new DBACentralEntities())
                {
                    ManagementScope ms = new ManagementScope(string.Format("\\\\{0}\\root\\mscluster",machineEntity.MachineName));
                    ms.Options.Authentication = AuthenticationLevel.PacketPrivacy;
                    System.Management.ObjectQuery query = new System.Management.ObjectQuery("SELECT Name FROM MSCluster_Cluster");
                    ManagementObjectSearcher searcher = new ManagementObjectSearcher(ms, query);
                    searcher.Options.Timeout = new TimeSpan(0, 0, timeout);

                    try
                    {
                        Console.Write("Updating " + machineEntity.MachineName + "...");

                        // Get cluster name
                        foreach (ManagementObject iQueryObj in searcher.Get())
                        {
                            if (machineEntity.Cluster.ClusterName != iQueryObj["Name"].ToString().ToUpper())
                            {
                                machineEntity.Cluster.ClusterName = iQueryObj["Name"].ToString().ToUpper();
                            }
                        }
                    }
                    catch (Exception e) { Console.WriteLine(e.Message); }

                    context.SaveChanges();
                }
            }
            catch (Exception e) { Console.WriteLine(e.Message); }

            Console.WriteLine("done.");
        }
        public static string GetIPv4(string hostName)
        {
            string ip = null;

            try
            {
                IPAddress[] addresslist = Dns.GetHostAddresses(hostName);

                if (addresslist.Count() > 0)
                {
                    foreach (IPAddress newIP in addresslist)
                    {
                        if (newIP.AddressFamily == AddressFamily.InterNetwork)
                        {
                            ip = newIP.ToString();
                        }
                    }
                }

                return ip;
            }
            catch
            {
                return null;
            }
        }
        public static string GetIPv6(string hostName)
        {
            string ip = null;

            try
            {
                IPAddress[] addresslist = Dns.GetHostAddresses(hostName);

                if (addresslist.Count() > 0)
                {
                    foreach (IPAddress newIP in addresslist)
                    {
                        if (newIP.AddressFamily == AddressFamily.InterNetworkV6)
                        {
                            ip = newIP.ToString();
                        }
                    }
                }

                return ip;
            }
            catch
            {
                return null;
            }
        }
    }
}
