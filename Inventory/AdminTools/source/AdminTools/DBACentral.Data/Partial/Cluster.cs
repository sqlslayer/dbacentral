﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBACentral.Data
{
    public partial class Cluster : IEqualityComparer<Cluster>
    {
        public override string ToString()
        {
            return this.ClusterName;
        }

        public bool Equals(Cluster x, Cluster y)
        {
            return x.ClusterID.Equals(y.ClusterID);
        }

        public int GetHashCode(Cluster obj)
        {
            return obj.ClusterName.GetHashCode();
        }
    }
}