﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBACentral.Data
{
    public partial class Contact : IEqualityComparer<Contact>
    {
        public override string ToString()
        {
            return this.Person.FullName;
        }

        public bool Equals(Contact x, Contact y)
        {
            return (x.PersonID == y.PersonID && x.ContactTypeID == y.ContactTypeID && x.ItemTypeID == y.ItemTypeID && x.ItemID == y.ItemID);
        }

        public int GetHashCode(Contact obj)
        {
            return obj.Person.FullName.GetHashCode();
        }
    }
}