﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBACentral.Data
{
    public partial class ContactType : IEqualityComparer<ContactType>
    {
        public override string ToString()
        {
            return this.ContactTypeName;
        }

        public bool Equals(ContactType x, ContactType y)
        {
            return x.ContactTypeID.Equals(y.ContactTypeID);
        }

        public int GetHashCode(ContactType obj)
        {
            return obj.ContactTypeName.GetHashCode();
        }
    }
}