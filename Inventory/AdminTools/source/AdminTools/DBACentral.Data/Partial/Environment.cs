﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DBACentral.Data
{
    public partial class Environment : IEqualityComparer<Environment>
    {
        public override string ToString()
        {
            return this.EnvironmentName;
        }

        public bool Equals(Environment x, Environment y)
        {
            return x.EnvironmentID.Equals(y.EnvironmentID);
        }

        public int GetHashCode(Environment obj)
        {
            return obj.EnvironmentName.GetHashCode();
        }
    }
}
