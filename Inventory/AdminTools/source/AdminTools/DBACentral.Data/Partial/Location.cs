﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBACentral.Data
{
    public partial class Location : IEqualityComparer<Location>
    {
        public override string ToString()
        {
            return this.LocationName;
        }

        public bool Equals(Location x, Location y)
        {
            return x.LocationID.Equals(y.LocationID);
        }

        public int GetHashCode(Location obj)
        {
            return obj.LocationName.GetHashCode();
        }
    }
}
