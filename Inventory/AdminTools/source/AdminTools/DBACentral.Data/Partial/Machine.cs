﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBACentral.Data
{
    public partial class Machine : IEqualityComparer<Machine>
    {
        public override string ToString()
        {
            return this.MachineName;
        }

        public bool Equals(Machine x, Machine y)
        {
            return x.MachineID.Equals(y.MachineID);
        }

        public int GetHashCode(Machine obj)
        {
            return obj.MachineName.GetHashCode();
        }
    }
}
