﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBACentral.Data
{
    public partial class Person : IEqualityComparer<Person>
    {
        public override string ToString()
        {
            return this.FullName;
        }

        public bool Equals(Person x, Person y)
        {
            return x.PersonID.Equals(y.PersonID);
        }

        public int GetHashCode(Person obj)
        {
            return obj.FullName.GetHashCode();
        }
    }
}
