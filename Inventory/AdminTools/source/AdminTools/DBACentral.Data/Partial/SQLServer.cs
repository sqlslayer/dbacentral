﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBACentral.Data
{
    public partial class SQLServer : IEqualityComparer<SQLServer>
    {
        public string ConnectionString
        {
            get
            {
                return "Data Source=" + this.FullName + ";Initial Catalog=master;Integrated Security=SSPI;";
            }
        }

        public override string ToString()
        {
            return this.FullName;
        }

        public bool Equals(SQLServer x, SQLServer y)
        {
            return x.DBMSInstanceID.Equals(y.DBMSInstanceID);
        }

        public int GetHashCode(SQLServer obj)
        {
            return obj.FullName.GetHashCode();
        }
    }
}
