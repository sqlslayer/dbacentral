﻿using System;
using System.IO;
using System.Configuration;
using System.Net;
using System.Diagnostics;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Collector
{
    /// <summary>
    /// Class to enable logging to a configurable destination.
    /// </summary>
    public static class LogHelper
    {
        private const string EventLogLogType = "EventLog";
        private const string FileLogType = "File";
        private const string NoneLogType = "None";

        /// <summary>
        /// Type of destination (either EventLog or File).
        /// </summary>
        public static string LogType
        {
            get
            {
                return ConfigurationManager.AppSettings["LogType"];
            }
        }

        /// <summary>
        /// Path of log file (only used for LogType of File).
        /// </summary>
        public static string LogPath
        {
            get
            {
                return ConfigurationManager.AppSettings["LogPath"];
            }
        }

        /// <summary>
        /// Path of log file (only used for LogType of File).
        /// </summary>
        public static string EventLogName
        {
            get
            {
                return ConfigurationManager.AppSettings["EventLogName"];
            }
        }

        /// <summary>
        /// Writes a message to the log.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="type"></param>
        public static void WriteToLog(string message, EventLogEntryType type) 
        {
            if (LogType == NoneLogType)
            {
                return;
            }
            else if (LogType == EventLogLogType)
            {
                if (!EventLog.SourceExists(ConfigurationManager.AppSettings["ApplicationName"]))
                    EventLog.CreateEventSource(ConfigurationManager.AppSettings["ApplicationName"], EventLogName);

                EventLog.WriteEntry(ConfigurationManager.AppSettings["ApplicationName"], message, type);
            }
            else if (LogType == FileLogType)
            {
                try
                {
                    if (!File.Exists(LogPath))
                    {
                        FileStream fs = new FileStream(LogPath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                        fs.Close();
                    }

                    using (StreamWriter writer = new StreamWriter(LogPath, true))
                    {
                        writer.WriteLine("Type: " + type.ToString());
                        writer.WriteLine("Message: " + message);
                        writer.WriteLine("------------------------------------------------------------------------------------------------------------------------------------");
                        writer.Flush();
                    }
                }
                catch (Exception e)
                {
                    throw new ApplicationException("Log path is invalid.");
                }
            }
            else
            {
                throw new ApplicationException("Logger type is invalid");
            }
        }
    }
}
