﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Runtime.InteropServices;
using System.Text;
using Microsoft.SqlServer.Management.Common;

using Collector;
using DBACentral.Data;

namespace InventoryUpdateUtility
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new DBACentralEntities())
            {
                foreach (Machine iMachine in context.Machines.Where(p => p.IsActive).OrderBy(p => p.MachineName).ToList())
                {
                    try
                    {
                        Debug.WriteLine(iMachine.MachineName);

                        Collect.Machine(iMachine);

                        // check for cluster info
                        string clusterName = Collect.GetClusterName(iMachine.MachineName);
                        if (clusterName != null)
                        {
                            if (!context.Clusters.Where(p => p.IsActive == true && p.ClusterName == clusterName).Any())
                            {
                                var newCluster = new Cluster();
                                newCluster.IsActive = true;
                                Collect.Cluster(newCluster, iMachine.MachineName);
                                newCluster.Notes = string.Format("Added to inventory while collecting information about {0}.", iMachine.MachineName);
                                context.Clusters.AddObject(newCluster);
                                context.SaveChanges();
                            }

                            iMachine.Cluster = context.Clusters.Where(p => p.ClusterName == clusterName && p.IsActive == true).FirstOrDefault();

                            Collect.Cluster(iMachine.Cluster, iMachine.MachineName);
                        }

                        context.SaveChanges();
                    }
                    catch (UnauthorizedAccessException)
                    {
                        LogHelper.WriteToLog(string.Format("Access denied to machine {0}.", iMachine.MachineName), EventLogEntryType.FailureAudit);
                    }
                    catch (COMException e)
                    {
                        LogHelper.WriteToLog(string.Format("An error was encountered while attemping to collect information from the machine {0} : {1}", iMachine.MachineName, e.Message), EventLogEntryType.Error);
                    }
                    catch (ManagementException e)
                    {
                        LogHelper.WriteToLog(string.Format("An error was encountered while attemping to collect information from the machine {0} : {1}", iMachine.MachineName, e.Message), EventLogEntryType.Error);
                    }
                    catch (Exception e)
                    {
                        LogHelper.WriteToLog(string.Format("An error was encountered while attemping to collect information from the machine {0} : {1}", iMachine.MachineName, e.Message), EventLogEntryType.Error);
                    }
                }

                Debug.WriteLine("Collecting instance information...");

                foreach (SQLServer iSQLServer in context.DBMSInstances.OfType<SQLServer>().Where(p => p.IsActive).Where(p => p.FullName != "C2PLKB1").OrderBy(p => p.FullName).ToList())
                {
                    try
                    {
                        Debug.WriteLine(iSQLServer.FullName);

                        Collect.SQLServer(iSQLServer, iSQLServer.FullName);

                        if (iSQLServer.IsClustered)
                        {
                            string clusterName = Collect.GetClusterName(iSQLServer.VirtualServerName);
                            iSQLServer.IP = Collect.GetIPv4(iSQLServer.VirtualServerName);

                            // create cluster if it doesn't exist
                            if (!context.Clusters.Where(p => p.IsActive == true && p.ClusterName == clusterName).Any())
                            {
                                var newCluster = new Cluster();
                                newCluster.IsActive = true;
                                Collect.Cluster(newCluster, iSQLServer.VirtualServerName);
                                newCluster.Notes = string.Format("Added to inventory while collecting information about {0}.", iSQLServer.FullName);
                                context.Clusters.AddObject(newCluster);
                                context.SaveChanges();
                            }

                            iSQLServer.Cluster = context.Clusters.Where(p => p.IsActive == true && p.ClusterName == clusterName).FirstOrDefault();

                            Collect.Cluster(iSQLServer.Cluster, iSQLServer.VirtualServerName);
                        }
                        else
                        {
                            iSQLServer.IP = Collect.GetIPv4(iSQLServer.Machine.MachineName);
                        }

                        context.SaveChanges();
                    }
                    catch (UnauthorizedAccessException)
                    {
                        LogHelper.WriteToLog(string.Format("Access denied to SQL instance {0}.", iSQLServer.FullName), EventLogEntryType.FailureAudit);
                    }
                    catch (ConnectionFailureException e)
                    {
                        if (e.InnerException.Message.Contains("Login failed for user"))
                        {
                            LogHelper.WriteToLog(string.Format("Login failure attempting to connect to SQL instance {0}.", iSQLServer.FullName), EventLogEntryType.FailureAudit);
                        }
                        else if (e.InnerException.Message.Contains("Timeout Expired"))
                        {
                            LogHelper.WriteToLog(string.Format("A timeout occurred attempting to connect to the SQL instance {0}.", iSQLServer.FullName), EventLogEntryType.Warning);
                        }
                        else if (e.InnerException.Message.Contains("A network-related or instance-specific error occurred "))
                        {
                            LogHelper.WriteToLog(string.Format("SQL instance {0} was not found or was not accessible. Verify that the instance name is correct and that SQL Server is configured to allow remote connections.", iSQLServer.FullName), EventLogEntryType.Warning);
                        }
                        else
                        {
                            LogHelper.WriteToLog(string.Format("Unable to connect to SQL instance {0} : {1}", iSQLServer.FullName, e.ToString()), EventLogEntryType.Error);
                        }
                    }
                    catch (Exception e)
                    {
                        LogHelper.WriteToLog(string.Format("An error was encountered collecting information from the SQL instance {0} : {1}", iSQLServer.FullName, e.ToString()), EventLogEntryType.Error);
                    }
                }
            }
        }
    }
}