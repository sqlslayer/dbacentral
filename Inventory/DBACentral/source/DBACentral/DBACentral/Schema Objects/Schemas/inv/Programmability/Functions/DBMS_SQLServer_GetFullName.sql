﻿CREATE FUNCTION [inv].[DBMSInstance_SQLServer_GetFullName]
(
	@DBMSInstanceID	INT
)
RETURNS VARCHAR(256)
AS
BEGIN
RETURN (SELECT CASE 
				WHEN i.[DBMSInstanceName] IS NOT NULL AND i.[PortNumber] IS NOT NULL THEN ISNULL(ss.[VirtualServerName],m.[MachineName]) + '\' + i.[DBMSInstanceName] + ',' + CAST(i.[PortNumber] AS VARCHAR(10))
				WHEN i.[DBMSInstanceName] IS NOT NULL THEN ISNULL(ss.[VirtualServerName],m.[MachineName]) + '\' + i.[DBMSInstanceName]
				WHEN i.[PortNumber] IS NOT NULL THEN ISNULL(ss.[VirtualServerName],m.[MachineName]) + ',' + CAST(i.[PortNumber] AS VARCHAR(10))
				ELSE ISNULL(ss.[VirtualServerName],m.[MachineName])
			   END
		FROM [inv].[DBMSInstance_SQLServer] ss
		JOIN [inv].[DBMSInstance] i
			ON ss.DBMSInstanceID = i.DBMSInstanceID
		LEFT JOIN [inv].[Machine]	m
			ON ss.[MachineID] = m.[MachineID]
		WHERE ss.[DBMSInstanceID] = @DBMSInstanceID)
END
GO