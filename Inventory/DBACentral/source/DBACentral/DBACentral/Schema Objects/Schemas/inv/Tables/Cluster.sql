﻿CREATE TABLE [inv].[Cluster]
(
	[ClusterID]			INT IDENTITY(1,1) NOT NULL
	,[EnvironmentID]	INT
	,[ClusterName]		VARCHAR(128)
	,[IP]				VARCHAR(45)
	,[QuorumType]		VARCHAR(32)
	,[QuorumPath]		VARCHAR(1024)
	,[Description]		NVARCHAR(MAX)
	,[IsActive]			BIT				NOT NULL
	,[Notes]			NVARCHAR(MAX)
	,[InstallDate]		DATETIME2 NOT NULL	DEFAULT GETDATE()
	,[LastUpdated]		DATETIME2  DEFAULT GETDATE() NOT NULL
)
