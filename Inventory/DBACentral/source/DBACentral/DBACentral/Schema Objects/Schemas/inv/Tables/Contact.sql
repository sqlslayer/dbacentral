﻿CREATE TABLE [inv].[Contact]
(
	[ContactID]			INT IDENTITY(1,1) NOT NULL
	,[PersonID]			INT NOT NULL
    ,[ContactTypeID]	INT NOT NULL
	,[ItemTypeID]		INT NOT NULL
	,[ItemID]			INT NOT NULL
	,[Notes]			NVARCHAR(MAX)
)
