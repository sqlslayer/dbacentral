﻿CREATE TABLE [inv].[ContactType]
(
	ContactTypeID	INT IDENTITY(1,1)	NOT NULL
	,ContactTypeName	VARCHAR(128)		NOT NULL
)