﻿CREATE TABLE [inv].[DBMSInstance]
(
	[DBMSInstanceID]			INT IDENTITY(1,1)			NOT NULL
	,[DBMSInstanceName]			VARCHAR(128)
    ,[IP]						VARCHAR(45)
	,[EnvironmentID]			INT
	,[PortNumber]				INT
	,[Edition]					VARCHAR(128)
	,[Build]					VARCHAR(16)
	,[Version]					VARCHAR(16)
	,[ProductLevel]				VARCHAR(16)
	,[Platform]					VARCHAR(32)
	,[Architecture]				SMALLINT
	,[Description]				NVARCHAR(max)
	,[IsActive]					BIT							 NOT NULL
	,[Notes]					NVARCHAR(MAX)
	,[InstallDate]				DATETIME2  DEFAULT GETDATE() NOT NULL
	,[LastUpdated]				DATETIME2 DEFAULT GETDATE()  NOT NULL
)