﻿CREATE TABLE [inv].[DBMSInstance_SQLServer]
(
	[DBMSInstanceID]			INT				NOT NULL
	,[MachineID]				INT
	,[ClusterID]				INT
	,[VirtualServerName]		VARCHAR(128)
	,[FullName]					AS [inv].[DBMSInstance_SQLServer_GetFullName](DBMSInstanceID)
	,[NumberOfDatabases]		INT
	,[IsClustered]				BIT				NOT NULL
	,[IsHADREnabled]			BIT
	,[IsFullTextInstalled]		BIT
	,[IsTcpEnabled]				BIT
	,[Language]					VARCHAR(128)
	,[Collation]				VARCHAR(128)
	,[SqlSortOrderName]			VARCHAR(128)
	,[DefaultFile]				VARCHAR(2048)
	,[DefaultLog]				VARCHAR(2048)
	,[BackupDirectory]			VARCHAR(2048)
	,[RootDirectory]			VARCHAR(2048)
	,[InstallDataDirectory]		VARCHAR(2048)
	,[InstallSharedDirectory]	VARCHAR(2048)
	,[NumberOfLogFiles]			INT
	,[ErrorLogPath]				VARCHAR(2048)
	,[MaxServerMemory]			INT
	,[MinServerMemory]			INT
	,[MaxDegreeOfParallelism]	INT
	,[LinkedServers]			VARCHAR(2048)
	,[MailProfiles]				VARCHAR(2048)
	,[Endpoints]				VARCHAR(2048)
	,[AvailabilityGroups]		VARCHAR(2048)
	,[GlobalTraceFlags]			VARCHAR(512)
	,[EngineServiceStartMode]	VARCHAR(128)
	,[EngineServiceAccount]		VARCHAR(128)
	,[AgentServiceAccount]		VARCHAR(128)
	,[BrowserServiceAccount]	VARCHAR(128)
	,[AgentJobCount]			INT
	,[AgentAlerts]				VARCHAR(2048)
	,[AgentOperators]			VARCHAR(2048)
	,[AgentErrorLogFile]		VARCHAR(2048)
)