﻿CREATE TABLE [inv].[Environment]
(
	[EnvironmentID]		INT IDENTITY(1,1) NOT NULL 
	,[EnvironmentName]  VARCHAR(128) NOT NULL
	,[Description]		NVARCHAR(MAX)
	,[Notes]		NVARCHAR(MAX)
	,[IsActive]		BIT				NOT NULL
)
