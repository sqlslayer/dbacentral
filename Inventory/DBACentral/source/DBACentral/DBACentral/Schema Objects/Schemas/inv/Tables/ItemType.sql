﻿CREATE TABLE [inv].[ItemType]
(
	[ItemTypeID]	INT IDENTITY(1,1) NOT NULL
	,[ItemTypeName]	VARCHAR(64) NOT NULL
)
