﻿ALTER TABLE [inv].[Cluster]
ADD CONSTRAINT [FK__Cluster__EnvironmentID__Environment__EnvironmentID]
FOREIGN KEY ([EnvironmentID])
REFERENCES [inv].[Environment] ([EnvironmentID])