﻿ALTER TABLE [inv].[Cluster]
	ADD CONSTRAINT [PK__Cluster__ClusterID]
	PRIMARY KEY CLUSTERED ([ClusterID] ASC)