﻿ALTER TABLE [inv].[Contact]
	ADD CONSTRAINT [FK__Contact__ItemTypeID__ItemType__ItemTypeID] 
	FOREIGN KEY (ItemTypeID)
	REFERENCES [inv].[ItemType] (ItemTypeID)