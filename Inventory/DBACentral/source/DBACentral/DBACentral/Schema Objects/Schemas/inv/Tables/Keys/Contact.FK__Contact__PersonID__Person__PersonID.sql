﻿ALTER TABLE [inv].[Contact]
	ADD CONSTRAINT [FK__Contact__PersonID__Person__PersonID] 
	FOREIGN KEY ([PersonID])
	REFERENCES [inv].[Person] ([PersonID])