﻿ALTER TABLE [inv].[Contact]
	ADD CONSTRAINT [FK__Contact__ContactTypeID__ContactType__ContactTypeID] 
	FOREIGN KEY ([ContactTypeID])
	REFERENCES [inv].[ContactType] ([ContactTypeID])
