﻿ALTER TABLE [inv].[DBMSInstance_SQLServer]
	ADD CONSTRAINT [FK__DBMSInstance_SQLServer__MachineID__Machine__MachineID]
	FOREIGN KEY (MachineID)
	REFERENCES [inv].[Machine] (MachineID)
