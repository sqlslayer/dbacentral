﻿ALTER TABLE [inv].[Environment]
	ADD CONSTRAINT [PK__Environments__EnvironmentID]
	PRIMARY KEY CLUSTERED ([EnvironmentID] ASC)