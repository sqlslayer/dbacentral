﻿ALTER TABLE [inv].[Location]
	ADD CONSTRAINT [PK__Location__LocationID]
	PRIMARY KEY CLUSTERED ([LocationID] ASC)