﻿ALTER TABLE [inv].[Machine]
ADD CONSTRAINT [FK__Machine__EnvironmentID__Environment__EnvironmentID]
FOREIGN KEY ([EnvironmentID])
REFERENCES [inv].[Environment] ([EnvironmentID])