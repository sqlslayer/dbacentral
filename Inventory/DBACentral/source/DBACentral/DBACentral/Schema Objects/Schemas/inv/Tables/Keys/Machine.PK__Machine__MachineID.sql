﻿ALTER TABLE [inv].[Machine]
	ADD CONSTRAINT [PK__Machine__MachineID]
	PRIMARY KEY CLUSTERED ([MachineID] ASC)