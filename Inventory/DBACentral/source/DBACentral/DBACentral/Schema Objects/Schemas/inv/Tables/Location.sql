﻿CREATE TABLE [inv].[Location]
(
	[LocationID]	INT IDENTITY(1,1)	NOT NULL
	,[LocationName] VARCHAR(128)		NOT NULL
	,[Description]	NVARCHAR(MAX)
	,[Notes]		NVARCHAR(MAX)
	,[IsActive]		BIT					NOT NULL
)
