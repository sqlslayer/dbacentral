﻿CREATE TABLE [inv].[Machine]
(
	[MachineID]						INT IDENTITY(1,1) NOT NULL
	,[MachineName]					VARCHAR(128)	  NOT NULL
	,[ClusterID]					INT
	,[LocationID]					INT
	,[EnvironmentID]				INT
	,[IP]							VARCHAR(45)
	,[Domain]						VARCHAR(64)
	,[Description]					NVARCHAR(MAX)
	,[OS]							VARCHAR(128)
	,[OSVersion]					VARCHAR(64)
	,[OSArchitecture]				SMALLINT
	,[OSServicePack]				TINYINT
	,[PowerPlan]					VARCHAR(64)
	,[Processor]					VARCHAR(128)
	,[ProcessorAddressWidth]		SMALLINT
	,[ProcessorArchitecture]		VARCHAR(32)
	,[NumberOfProcessors]			SMALLINT
	,[NumberOfLogicalProcessors]	SMALLINT
	,[IsHTEnabled]					BIT
	,[PhysicalMemoryMB]				INT
	--,[NumberOfVolumes]			INT
	--,[TotalVolumeCapacityMB]		INT
	,[Manufacturer]					VARCHAR(128)
	,[Model]						VARCHAR(128)
	,[IsVirtual]					BIT
	,[IsActive]					BIT NOT NULL
	,[Notes]						NVARCHAR(MAX)
	,[InstallDate]					DATETIME2 NOT NULL	DEFAULT GETDATE()
	,[LastUpdated]					DATETIME2 NOT NULL	DEFAULT GETDATE()
)