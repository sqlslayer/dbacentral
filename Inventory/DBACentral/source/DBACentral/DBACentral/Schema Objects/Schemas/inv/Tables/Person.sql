﻿CREATE TABLE [inv].[Person]
(
	[PersonID]		INT IDENTITY(1,1) NOT NULL
	,[FirstName]	NVARCHAR(128) NOT NULL
	,[LastName]		NVARCHAR(128)
	,[FullName]		AS (FirstName + SPACE(1) + ISNULL(LastName,'')) PERSISTED
	,[Phone]		VARCHAR(64)
	,[EMail]		NVARCHAR(128)
	,[Company]		NVARCHAR(128)
	,[Description]	NVARCHAR(MAX)
	,[Notes]		NVARCHAR(MAX)
	,[IsActive]		BIT				NOT NULL
)