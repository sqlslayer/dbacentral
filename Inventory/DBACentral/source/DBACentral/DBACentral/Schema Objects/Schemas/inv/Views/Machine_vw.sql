﻿CREATE VIEW [inv].[Machine_vw]
AS
SELECT m.[MachineID]
	  ,m.[MachineName]
	  ,m.[IP]
	  ,m.[Description]
	  ,m.[Manufacturer]
	  ,m.[Model]
	  ,m.[OS]
	  ,m.[OSVersion]
	  ,m.[OSArchitecture]
	  ,m.[OSServicePack]
	  ,m.[Processor]
	  ,m.[ProcessorAddressWidth]
	  ,m.[ProcessorArchitecture]
	  ,m.[NumberOfProcessors]
	  ,m.[NumberOfLogicalProcessors]
	  ,m.[IsHTEnabled]
	  ,m.[PhysicalMemoryMB]
	  ,l.[LocationName] AS 'Location'
	  ,m.[Domain]
	  ,c.[ClusterName] AS 'Cluster'
	  ,m.[IsVirtual]
	  ,m.[IsActive]
	  ,m.[InstallDate]
	  ,m.[LastUpdated]
FROM [inv].[Machine] m
JOIN [inv].[Location] l
	ON m.[LocationID] = l.[LocationID]
LEFT JOIN [inv].[Cluster] c
	ON m.[ClusterID] = c.[ClusterID]
GO

