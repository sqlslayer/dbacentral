﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/


--IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[__RefactorLog]') AND type in (N'U'))

--DROP TABLE [dbo].[__RefactorLog]

--GO



-- SEED DATA
INSERT INTO [inv].[ItemType] ([ItemTypeName]) VALUES ('Cluster')
INSERT INTO [inv].[ItemType] ([ItemTypeName]) VALUES ('DBMSInstance')
INSERT INTO [inv].[ItemType] ([ItemTypeName]) VALUES ('Machine')
INSERT INTO [inv].[ContactType] ([ContactTypeName]) VALUES ('Owner')
GO
-- SEED DATA

CREATE PROCEDURE [inv].[Location_Upsert]
	@LocationID		INT				OUTPUT
	,@LocationName	VARCHAR(100)
	,@Description	NVARCHAR(MAX)   = NULL
AS
	SET NOCOUNT ON;
	SET @LocationID = NULL

	SELECT @LocationID = LocationID
	FROM [inv].[Location]
	WHERE LocationName = @LocationName

	IF (@LocationID IS NULL)
	BEGIN
		INSERT INTO [inv].[Location] ([LocationName],[Description],[IsActive]) VALUES (@LocationName,@Description,1)
	END

	SET @LocationID = COALESCE(@LocationID,SCOPE_IDENTITY())
GO


CREATE PROCEDURE [inv].[Environment_Upsert]
	@EnvironmentID		INT				OUTPUT
	,@EnvironmentName	VARCHAR(100)
	,@Description		NVARCHAR(MAX)	= NULL
AS
	SET NOCOUNT ON;
	SET @EnvironmentID = NULL

	SELECT @EnvironmentID = EnvironmentID
	FROM [inv].[Environment]
	WHERE EnvironmentName = @EnvironmentName

	IF (@EnvironmentID IS NULL)
	BEGIN
		INSERT INTO [inv].[Environment] ([EnvironmentName],[Description],[IsActive]) VALUES (@EnvironmentName,@Description,1)
	END


	SET @EnvironmentID = COALESCE(@EnvironmentID,SCOPE_IDENTITY())
GO



CREATE PROCEDURE [inv].[Person_Upsert]
	@PersonID		INT				OUTPUT
	,@PersonName	VARCHAR(50)
	,@Phone			VARCHAR(50)		= NULL
	,@EMail			VARCHAR(50)		= NULL
	,@Company		VARCHAR(50)		= NULL
AS
	SET NOCOUNT ON;
	SET @PersonID = NULL

	DECLARE @FirstName NVARCHAR(128)
	DECLARE @LastName  NVARCHAR(128)

	SELECT @FirstName = SUBSTRING(@PersonName, 1, CHARINDEX(' ', @PersonName) - 1),
		   @LastName = REPLACE(SUBSTRING(@PersonName, CHARINDEX(' ', @PersonName) + 1, LEN(@PersonName)),' (non IT&S)','')

	SELECT @PersonID = PersonID FROM [inv].[Person] WHERE [FirstName] = @FirstName AND [LastName] = @LastName

	IF (@PersonID IS NULL)
	BEGIN
		INSERT INTO [inv].[Person] ([FirstName],[LastName],[IsActive]) VALUES (@FirstName,@LastName,1)
	END 

	SET @PersonID = COALESCE(@PersonID,SCOPE_IDENTITY())
GO




CREATE PROCEDURE [inv].[Machine_Upsert]
	@MachineID		INT				OUTPUT
	,@MachineName	VARCHAR(128)
	,@ClusterID		INT				= NULL
	,@IsVirtual		BIT				= NULL
	,@LocationID	INT				= NULL
	,@Domain		VARCHAR(50)		= NULL
	,@Description	NVARCHAR(MAX)	= NULL
	,@OS			VARCHAR(50)		= NULL
	,@OSVersion		VARCHAR(20)		= NULL
	,@IP			VARCHAR(16)		= NULL
	,@IsActive		BIT				= NULL
	,@EnvironmentID INT
	,@InstallDate	DATETIME		= NULL
AS
	SET NOCOUNT ON;
	SET @MachineID = NULL
	SET @InstallDate = ISNULL(@InstallDate,GETDATE())
	SET @IsActive = ISNULL(@IsActive,0)



	SET @MachineID = (SELECT TOP 1 MachineID FROM [inv].[Machine] WHERE MachineName = @MachineName)

	IF (@MachineID IS NULL)
	BEGIN
		INSERT INTO [inv].[Machine] ([MachineName],[ClusterID],[IsVirtual],[LocationID],[Domain],[Description],[OS],[OSVersion],[IP],[IsActive],[EnvironmentID],[InstallDate]) 
		VALUES (@MachineName,@ClusterID,@IsVirtual,@LocationID,@Domain,@Description,@OS,@OSVersion,@IP,@IsActive,@EnvironmentID,@InstallDate)
	END

	SET @MachineID = COALESCE(@MachineID,SCOPE_IDENTITY())
GO


CREATE PROCEDURE [inv].[DBMSInstance_Insert]
	@DBMSInstanceID				INT					OUTPUT
	,@DBMSInstanceName			VARCHAR(100)				= NULL
	,@PortNumber		INT							= NULL
	,@Edition			VARCHAR(100)				= NULL
	,@Build				VARCHAR(16)					= NULL
	,@ProductLevel		VARCHAR(10)					= NULL
	,@EnvironmentID		INT							= NULL
	,@IsClustered		BIT							= 0
	,@Description		NVARCHAR(max)				= NULL
	,@MachineID			INT							= NULL
	,@IsActive			BIT							= NULL
	,@InstallDate		DATETIME2					= NULL
	,@LastUpdateDate	DATETIME2					= NULL
AS
	SET NOCOUNT ON
	SET @InstallDate = ISNULL(@InstallDate,GETDATE())
	SET @LastUpdateDate = ISNULL(@LastUpdateDate,GETDATE())

	INSERT INTO [inv].[DBMSInstance]	
		([DBMSInstanceName],[PortNumber],[Edition],[Build],[ProductLevel],[EnvironmentID],[Description],[IsActive],[InstallDate])
		VALUES (@DBMSInstanceName,@PortNumber,@Edition,@Build,@ProductLevel,@EnvironmentID,@Description,@IsActive,@InstallDate)

	SET @DBMSInstanceID = SCOPE_IDENTITY();

	IF (@IsClustered IS NULL OR @IsClustered = 0)
	BEGIN
		INSERT INTO [inv].[DBMSInstance_SQLServer] (DBMSInstanceID,IsClustered,MachineID) VALUES (@DBMSInstanceID,0,@MachineID)
	END
	ELSE
	BEGIN
		INSERT INTO [inv].[DBMSInstance_SQLServer] (DBMSInstanceID,IsClustered) VALUES (@DBMSInstanceID,@IsClustered)
	END
GO




CREATE PROCEDURE [inv].[Person_DBMSInstance_XRef_Insert]
	@DBMSInstanceID		INT 
	,@PersonID		INT
AS
	SET NOCOUNT ON


	IF NOT EXISTS (SELECT * FROM [inv].[Contact] WHERE PersonID = @PersonID AND ItemTypeID = 2 AND ItemID = @DBMSInstanceID)
	BEGIN
		INSERT INTO [inv].[Contact] (PersonID,ContactTypeID,ItemTypeID,ItemID) VALUES (@PersonID,1,2,@DBMSInstanceID)
	END
GO


CREATE PROCEDURE [inv].[Person_Machine_XRef_Insert]
	@MachineID		INT
	,@PersonID		INT
AS
	SET NOCOUNT ON

	IF NOT EXISTS (SELECT * FROM [inv].[Contact] WHERE PersonID = @PersonID AND ItemTypeID = 3 AND ItemID = @MachineID)
	BEGIN
		INSERT INTO [inv].[Contact] (PersonID,ContactTypeID,ItemTypeID,ItemID) VALUES (@PersonID,1,3,@MachineID)
	END
GO


CREATE PROCEDURE [inv].[Cluster_Upsert]
	@ClusterID		INT			OUTPUT
	,@ClusterName	VARCHAR(100)
AS
	SET NOCOUNT ON;
	SET @ClusterID = NULL

	MERGE [inv].[Cluster] c
	USING (SELECT @ClusterName) sp ([ClusterName])
	ON c.[ClusterName] = @ClusterName

	WHEN NOT MATCHED THEN INSERT ([ClusterName],[IsActive]) VALUES (@ClusterName,1)
	WHEN MATCHED THEN UPDATE SET @ClusterID = [ClusterID];

	SET @ClusterID = COALESCE(@ClusterID,SCOPE_IDENTITY())
GO




CREATE PROCEDURE [inv].[ClusterResource_Upsert]
	@ClusterResource		VARCHAR(100)	
	,@DBMSInstanceID		INT				= NULL
	,@ClusterID			INT				= NULL
AS
	SET NOCOUNT ON;


	UPDATE [inv].[DBMSInstance_SQLServer] SET VirtualServerName = @ClusterResource WHERE DBMSInstanceID = @DBMSInstanceID
	UPDATE [inv].[DBMSInstance_SQLServer] SET ClusterID = @ClusterID WHERE DBMSInstanceID = @DBMSInstanceID
GO



DECLARE @MachineNameC			VARCHAR(128)
DECLARE @DBMSInstanceNameC		VARCHAR(128)
DECLARE @PortNumberC			INT
DECLARE @IsClusteredC			BIT
DECLARE @NodeNamesC				VARCHAR(128)
DECLARE @LocationC				VARCHAR(512)
DECLARE @BuildC					VARCHAR(32)
DECLARE @EditionC				VARCHAR(32)
DECLARE @ProductLevelC			VARCHAR(32)
DECLARE @DescriptionC			VARCHAR(MAX)
DECLARE @InstallDateC			DATETIME2
DECLARE @CPUC					INT
DECLARE @ITOwnerC				VARCHAR(128)
DECLARE @OperatingSystemC		VARCHAR(128)
DECLARE @EnvironmentC			VARCHAR(32)
DECLARE @LastUpdateC			DATETIME2
DECLARE @SQLVersionC			VARCHAR(128)
DECLARE @NewLocationID			INT
DECLARE @NewEnvironmentID		INT
DECLARE @MachineID				INT
DECLARE @NewMachineID			INT
DECLARE @DBMSInstanceID			INT
DECLARE @ClusteredMachineName	VARCHAR(128)
DECLARE @NewClusterName			VARCHAR(128)
DECLARE @NewClusterID			INT
DECLARE @NewClusterResourceID	VARCHAR(128)
DECLARE @NewITOwnerID			INT
DECLARE @IsVirtualC				BIT

SET NOCOUNT ON

DECLARE db_cursor CURSOR FOR
SELECT 
	   ss.[ServerName]
      ,ss.[InstanceName]
      ,ss.[PortNumber]
      ,ss.[IsVirtual]
      ,CASE WHEN ss.[IsClustered] = 0 THEN NULL ELSE 1 END AS IsClustered
      ,ss.[NodeNames]
      ,ss.[Location]
      ,ss.[ProductVersion] AS Build
      ,ss.[Edition]
      ,ss.[ProductLevel]
      ,ss.[Description]
      ,CAST(ss.[InstallDate] AS DATETIME2)
      ,ss.[CPU]
      ,ss.[ITOwner]
      --,[DBAComments]
      ,ss.[OperatingSystem]
      ,CAST(ss.[LastUpdate] AS DATETIME2)
      ,ss.Environment
	  ,ss.SQLVersion
  FROM [DBAStage].[dbo].[SQLServers] ss
 -- WHERE ss.ID > 200 AND ss.ID < 300 AND ss.[ServerName] NOT LIKE 'C%'
  ORDER BY ss.ServerName, ss.InstanceName

OPEN db_cursor   
FETCH NEXT FROM db_cursor INTO @MachineNameC,@DBMSInstanceNameC,@PortNumberC,@IsVirtualC,@IsClusteredC,@NodeNamesC
							  ,@LocationC,@BuildC,@EditionC,@ProductLevelC,@DescriptionC
							  ,@InstallDateC,@CPUC,@ITOwnerC,@OperatingSystemC,@LastUpdateC,@EnvironmentC
							  ,@SQLVersionC

WHILE @@FETCH_STATUS = 0   
BEGIN


	EXEC [inv].[Location_Upsert] @NewLocationID OUTPUT, @LocationName = @LocationC
	EXEC [inv].[Environment_Upsert] @NewEnvironmentID OUTPUT, @EnvironmentName = @EnvironmentC
	EXEC [inv].[Person_Upsert]	@NewITOwnerID OUTPUT, @ITOwnerC

	IF (@BuildC IS NULL)
	BEGIN
		IF (@SQLVersionC = '2000')
		 BEGIN SET @BuildC = '8.00' END
		IF (@SQLVersionC = '2005')		
		 BEGIN SET @BuildC = '9.00' END
		IF (@SQLVersionC = '2008')
		 BEGIN SET @BuildC = '10.0' END
		IF (@SQLVersionC = '2008 R2')
		 BEGIN SET @BuildC = '10.5' END
		IF (@SQLVersionC = '2012')
		 BEGIN SET @BuildC = '11.0' END
	END

	-- non clustered Machine
	IF (@NodeNamesC IS NULL)
	BEGIN

		PRINT 'Migrating NonClustered Machine:' + @MachineNameC

		EXEC [inv].[Machine_Upsert] @NewMachineID OUTPUT
											,@MachineName = @MachineNameC
											,@ClusterID = NULL
											,@IsVirtual = @IsVirtualC
											,@LocationID = @NewLocationID
											,@Domain = null
											,@Description = @DescriptionC
											,@OS = 'Windows'
											,@IP = NULL
											,@IsActive = 1
											,@EnvironmentID = @NewEnvironmentID
											,@InstallDate = @InstallDateC

		PRINT 'DBMSInstance: ' + @DBMSInstanceNameC
		EXEC [inv].[DBMSInstance_Insert]   @DBMSInstanceID OUTPUT
												,@DBMSInstanceName = @DBMSInstanceNameC
												,@PortNumber = @PortNumberC
												,@Edition = @EditionC
												,@Build	= @BuildC
												,@ProductLevel = @ProductLevelC
												,@EnvironmentID	= @NewEnvironmentID
												,@Description = @DescriptionC
												,@MachineID	= @NewMachineID
												,@IsClustered = 0
												,@IsActive	= 1
												,@InstallDate = @InstallDateC
												,@LastUpdateDate = @LastUpdateC



		PRINT 'Associating Person of DBMSInstance:' + @DBMSInstanceNameC + '. Person:' + @ITOwnerC
		EXEC [inv].[Person_DBMSInstance_XRef_Insert] @DBMSInstanceID,@NewITOwnerID

		PRINT 'Associating Person of Machine:' + @MachineNameC + '. Person:' +@ITOwnerC
		EXEC [inv].[Person_Machine_XRef_Insert] @NewMachineID,@NewITOwnerID

	END

	-- clustered Machine
	ELSE
	BEGIN
		
		PRINT 'Migrating Clustered Machine (ClusterResource):' + @MachineNameC
		PRINT 'DBMSInstance: ' + @DBMSInstanceNameC
		-- transfer DBMSInstance info
		EXEC [inv].[DBMSInstance_Insert]   @DBMSInstanceID OUTPUT
												,@DBMSInstanceName = @DBMSInstanceNameC
												,@PortNumber = @PortNumberC
												,@Edition = @EditionC
												,@Build	= @BuildC
												,@ProductLevel = @ProductLevelC
												,@EnvironmentID	= @NewEnvironmentID
												,@Description = @DescriptionC
												,@MachineID	= NULL
												,@IsClustered = 1
												,@IsActive	= 1
												,@InstallDate = @InstallDateC
												,@LastUpdateDate = @LastUpdateC
		
		PRINT 'Associating Person of DBMSInstance:' + @DBMSInstanceNameC + '. Person:' + @ITOwnerC
		-- associate DBMSInstance Person
		EXEC [inv].[Person_DBMSInstance_XRef_Insert] @DBMSInstanceID,@NewITOwnerID
		
		-- get the cluster ID if the cluster already exists
		IF EXISTS (SELECT c.ClusterID
				   FROM [inv].[Cluster] c
				   JOIN [inv].[DBMSInstance_SQLServer] ss
						ON c.ClusterID = ss.ClusterID
				   WHERE ss.VirtualServerName IN (SELECT ServerName
											FROM [DBAStage].[dbo].[SQLServers] 
											WHERE NodeNames = @NodeNamesC))
		BEGIN
			PRINT 'Found cluster that was already created for this DBMSInstance.'
			SELECT TOP 1 @NewClusterID = c.ClusterID
				   FROM [inv].[Cluster] c
				   JOIN [inv].[DBMSInstance_SQLServer] ss
						ON c.ClusterID = ss.ClusterID
				   WHERE ss.VirtualServerName IN (SELECT ServerName
													FROM [DBAStage].[dbo].[SQLServers] 
													WHERE NodeNames = @NodeNamesC)
		END
		-- create new cluster and associate DBMSInstance & virtual name with the cluster
		ELSE
		BEGIN
				PRINT 'Creating a new cluster for this DBMSInstance.'
				SET @NewClusterName = NEWID()
				EXEC [inv].[Cluster_Upsert] @NewClusterID OUTPUT, @ClusterName = @NewClusterName
		END
		

		PRINT 'Associating ClusterResource\DBMSInstanceName with Cluster: ' + @NewClusterName
		-- add ClusterResource
		EXEC [inv].[ClusterResource_Upsert] @MachineNameC, @DBMSInstanceID, @NewClusterID


		PRINT 'Looping through nodes...'
		-- handle the nodes
		DECLARE db_cursor2 CURSOR FOR
			SELECT * FROM [dbo].[Split_fn](@NodeNamesC,',')

		OPEN db_cursor2   
			FETCH NEXT FROM db_cursor2 INTO @ClusteredMachineName

			WHILE @@FETCH_STATUS = 0   
			BEGIN
				
				PRINT 'Migrating Machine node: ' + @ClusteredMachineName

				EXEC [inv].[Machine_Upsert] @NewMachineID OUTPUT
										,@MachineName = @ClusteredMachineName
										,@ClusterID = @NewClusterID
										,@IsVirtual = @IsVirtualC
										,@LocationID = @NewLocationID
										,@Domain = null
										,@OS = 'Windows'
										,@Description = @DescriptionC
										,@IsActive = 1
										,@IP = NULL
										,@EnvironmentID = @NewEnvironmentID
										,@InstallDate = @InstallDateC

				PRINT 'Associating node with Person: ' + @ITOwnerC
				-- associate Machine Person
				EXEC [inv].[Person_Machine_XRef_Insert] @NewMachineID,@NewITOwnerID

			FETCH NEXT FROM db_cursor2 INTO @ClusteredMachineName
			END

		CLOSE db_cursor2 
		DEALLOCATE db_cursor2
	END

	PRINT ''
	PRINT ''

	FETCH NEXT FROM db_cursor INTO @MachineNameC,@DBMSInstanceNameC,@PortNumberC,@IsVirtualC,@IsClusteredC,@NodeNamesC
									,@LocationC,@BuildC,@EditionC,@ProductLevelC,@DescriptionC
									,@InstallDateC,@CPUC,@ITOwnerC,@OperatingSystemC,@LastUpdateC,@EnvironmentC
									,@SQLVersionC
END   

CLOSE db_cursor   
DEALLOCATE db_cursor



DROP PROCEDURE [inv].[Location_Upsert]
DROP PROCEDURE [inv].[Environment_Upsert]
DROP PROCEDURE [inv].[Person_Upsert]
DROP PROCEDURE [inv].[Machine_Upsert]
DROP PROCEDURE [inv].[DBMSInstance_Insert]
DROP PROCEDURE [inv].[Person_DBMSInstance_XRef_Insert]
DROP PROCEDURE [inv].[Person_Machine_XRef_Insert]
DROP PROCEDURE [inv].[Cluster_Upsert]
DROP PROCEDURE [inv].[ClusterResource_Upsert]



-- Enable CDC

--USE DBACentralTest

--EXEC sys.sp_cdc_enable_db 

--EXEC sys.sp_cdc_enable_table @source_schema = 'inv', @source_name = 'Person_Machine_XRef', @role_name = NULL
--							,@captured_column_list = '[MachineID]
--													 ,[PersonID]'

--EXEC sys.sp_cdc_enable_table @source_schema = 'inv', @source_name= 'Cluster', @role_name= NULL
--							,@captured_column_list = '[ClusterID]
--													 ,[ClusterName]
--													 ,[IP]
--													 ,[Description]'

--EXEC sys.sp_cdc_enable_table @source_schema = 'inv', @source_name= 'Application', @role_name= NULL
--							,@captured_column_list = '[ApplicationID]
--													 ,[ApplicationName]
--													 ,[Description]'

--EXEC sys.sp_cdc_enable_table @source_schema = 'inv', @source_name= 'Environment', @role_name= NULL
--							,@captured_column_list = '[EnvironmentID]
--													 ,[EnvironmentName]
--													 ,[Description]'

--EXEC sys.sp_cdc_enable_table @source_schema = 'inv', @source_name= 'Location', @role_name= NULL
--							,@captured_column_list = '[LocationID]
--													 ,[LocationName]
--													 ,[Description]'

--EXEC sys.sp_cdc_enable_table @source_schema = 'inv', @source_name= 'Person', @role_name= NULL
--							,@captured_column_list = '[PersonID]
--													 ,[PersonName]
--													 ,[Phone]
--													 ,[EMail]
--													 ,[Company]
--													 ,[Description]'

--EXEC sys.sp_cdc_enable_table @source_schema = 'inv', @source_name= 'ClusterResource', @role_name= NULL
--							,@captured_column_list = '[ClusterResourceID]
--													 ,[ClusterResourceName]
--													 ,[IP]
--													 ,[ClusterID]
--													 ,[DBMSInstanceID]
--													 ,[Description]'

--EXEC sys.sp_cdc_enable_table @source_schema = 'inv', @source_name= 'Machine_DBMSInstance_XRef', @role_name= NULL
--							,@captured_column_list = '[MachineID]
--													 ,[DBMSInstanceID]'

--EXEC sys.sp_cdc_enable_table @source_schema = 'inv', @source_name= 'Person_DBMSInstance_XRef', @role_name= NULL
--							,@captured_column_list = '[DBMSInstanceID]
--													 ,[PersonID]'

--EXEC sys.sp_cdc_enable_table @source_schema = 'inv', @source_name= 'DBMSInstance', @role_name= NULL
--							,@captured_column_list = '[DBMSInstanceID]
--													 ,[DBMSInstanceName]
--													 ,[PortNumber]
--													 ,[Type]
--													 ,[Edition]
--													 ,[Build]
--													 ,[ProductLevel]
--													 ,[Platform]
--													 ,[EnvironmentID]
--													 ,[Description]
--													 ,[NumberOfDatabases]
--													 ,[MachineID]
--													 ,[IsActive]'

--EXEC sys.sp_cdc_enable_table @source_schema = 'inv', @source_name= 'Machine', @role_name= NULL
--							,@captured_column_list = '[MachineID]
--													 ,[MachineName]
--													 ,[ClusterID]
--													 ,[IsVirtual]
--													 ,[LocationID]
--													 ,[Domain]
--													 ,[Description]
--													 ,[OS]
--													 ,[OSVersion]
--													 ,[OSArchitecture]
--													 ,[OSServicePack]
--													 ,[Processor]
--													 ,[ProcessorAddressWidth]
--													 ,[ProcessorArchitecture]
--													 ,[NumberOfProcessors]
--													 ,[NumberOfLogicalProcessors]
--													 ,[IsHTEnabled]
--													 ,[PhysicalMemoryMB]
--													 ,[Manufacturer]
--													 ,[Model]
--													 ,[IP]
--													 ,[IsActive]'


---- Set CDC retention (@retention -> minutes, @threshold -> Maximum number of delete entries that can be deleted using a single statement on cleanup)
--EXEC sp_cdc_change_job @job_type='cleanup', @retention=52494800, @threshold=10000









--ALTER TABLE [DBACentralTest].[dbo].[ServerInventory_SQL_AttributeMaster] 
--	ADD CONSTRAINT [PK__ServerInventory_SQL_AttributeMaster__AttribID]
--	PRIMARY KEY CLUSTERED ([AttributeID] ASC) WITH (FILLFACTOR = 85) ON [PRIMARY]
--GO

--ALTER TABLE [DBACentralTest].[dbo].[ServerInventory_SQL_AttributeList]
--	ADD CONSTRAINT [PK__ServerInventory_SQL_AttributeList__UniqueID] 
--	PRIMARY KEY NONCLUSTERED ([UniqueID] ASC) WITH (FILLFACTOR = 85) ON [PRIMARY]
--GO

--ALTER TABLE [DBACentralTest].[dbo].[ServerInventory_SQL_AttributeMaster] ADD  CONSTRAINT [DF__ServerInventory_SQL_AttributeMaster__IsCore]  DEFAULT ((0)) FOR [IsCore]
--GO

--ALTER TABLE [DBACentralTest].[dbo].[ServerInventory_SQL_AttributeMaster] ADD  CONSTRAINT [DF__ServerInventory_SQL_AttributeMaster__IsReadOnly]  DEFAULT ((0)) FOR [IsReadOnly]
--GO

--ALTER TABLE [DBACentralTest].[dbo].[ServerInventory_SQL_AttributeMaster] ADD  CONSTRAINT [DF__ServerInventory_SQL_AttributeMaster__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
--GO

--ALTER TABLE [DBACentralTest].[dbo].[ServerInventory_SQL_AttributeList] ADD  CONSTRAINT [DF__ServerInventory_SQL_AttributeList__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
--GO

--ALTER TABLE [DBACentralTest].[dbo].[ServerInventory_SQL_AttributeList] ADD  CONSTRAINT [DF__ServerInventory_SQL_AttributeList__LastModified]  DEFAULT (getdate()) FOR [LastModified]
--GO



--DECLARE @OldServerID INT
--DECLARE @NewServerID INT
--DECLARE @ServerName  VARCHAR(500)

--DECLARE db_cursor3 CURSOR FOR
--SELECT DISTINCT a.[ServerID]
--      ,s.FullName
--  FROM [DBACentral].[dbo].[ServerInventory_SQL_AttributeList] a
--  JOIN [DBACentral].[dbo].[ServerInventory_SQL_AllServers_vw] s
--	ON a.ServerID = s.ServerID
--OPEN db_cursor3   
--FETCH NEXT FROM db_cursor3 INTO @OldServerID,@ServerName

--WHILE @@FETCH_STATUS = 0   
--BEGIN

--SET @NewServerID = NULL
--SET @NewServerID = (SELECT DBMSInstanceID FROM [DBACentralTest].[inv].[SQLInstance_vw] WHERE FullName = @ServerName)

--IF (@NewServerID IS NOT NULL)
--BEGIN

--	PRINT 'Converting attributes over for ' + @ServerName
--	UPDATE [DBACentralTest].[dbo].[ServerInventory_SQL_AttributeList] SET ServerID = @NewServerID WHERE ServerID = @OldServerID
--END

--FETCH NEXT FROM db_cursor3 INTO @OldServerID,@ServerName

--END

--CLOSE db_cursor3
--DEALLOCATE db_cursor3













--ALTER TABLE [dbo].[ServerInventory_SQL_AttributeList]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_SQL_AttributeList__AttribID__ServerInventory_SQL_AttributeMaster__AttribID] FOREIGN KEY([AttributeID])
--REFERENCES [dbo].[ServerInventory_SQL_AttributeMaster] ([AttributeID])
--GO

--ALTER TABLE [dbo].[ServerInventory_SQL_AttributeList] CHECK CONSTRAINT [FK__ServerInventory_SQL_AttributeList__AttribID__ServerInventory_SQL_AttributeMaster__AttribID]
--GO

--ALTER TABLE [dbo].[ServerInventory_SQL_AttributeList]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_SQL_AttributeList__ServerID__DBMSInstance__DBMSInstanceID] FOREIGN KEY([ServerID])
--REFERENCES [inv].[DBMSInstance] ([DBMSInstanceID])
--ON DELETE CASCADE
--GO

