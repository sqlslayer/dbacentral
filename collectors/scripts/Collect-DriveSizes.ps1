﻿param(
	[switch]$Verbose
)

# Start logging things
$local:ErrorActionPreference = "Stop"

Import-Module DBACCollector -Force -Global

if ($Verbose){$VerbosePreference = "Continue"}
if ($Debug){
	$DebugPreference = "Continue"
	$VerbosePreference = "Continue"
	GetConfig
	}

#Log "Script Starting" "Begin" "" $MyInvocation.MyCommand.path

$DBAC = OpenDBAC;

if ($DBAC.State -ne "Open")
{
	$DBAC.Open()
}

#Query to get the server names
$Server_Query = @"
SELECT DISTINCT 
	[ServerName]
	,UPPER(LEFT(CONVERT(NVARCHAR(3),[physical_name],3),3)) RootDrivePath
INTO #tmpDriveRoot	
FROM [DBACentral].hist.[ServerInventory_SQL_SysMasterFiles_vw] AS SISSMFV 		
ORDER BY [ServerName]		

SELECT DISTINCT SSISM.[ServerName] + ISNULL('\' + SSISM.[InstanceName],'') ServerInstance
               ,SSISM.[ServerName] 
			   ,B.RootDrivePath
               ,A.[RunID]  
FROM  [DBACentral].dbo.[ServerInventory_SQL_Master] AS SSISM
CROSS APPLY(SELECT ISNULL(MAX([RunID]),0) + 1 AS [RunID] FROM [DBACentral].[hist].[SpaceUsed_DriveSizes_RunIDs]  AS SISRID)A		
LEFT JOIN (SELECT  [ServerName]
				  ,[RootDrivePath] 
		   FROM [#tmpDriveRoot] AS TDR
	)B ON B.[ServerName] = SSISM.[ServerName] + ISNULL('\' + SSISM.[InstanceName],'')
WHERE B.[RootDrivePath] IS NOT NULL

"@

# Retrieve server list
$ServerNames = GetServerList $DBAC $Server_Query

$ServerNames | ForEach-Object { 
	$ServerName     = $_.ServerName;
    $ServerInstance = $_.ServerInstance;
    $RunID          = $_.RunID;
    $RootDrivePath  = $_.RootDrivePath;
    
    
	# write-host $ServerName, $ServerInstance, $RootDrivePath, $RunID
    
	Log "Now collecting data" "Progress" $ServerName $MyInvocation.MyCommand.path
    
    try
	{
        
      $MountInfo = get-wmiobject Win32_Volume -ComputerName $ServerName | where-object {$_.Name -like “$RootDrivePath*”} | SELECT  SystemName,Label,Name, Caption, DriveLetter,DriveType,Capacity,Freespace 
    
    foreach($MountPoint in $MountInfo)
      {

         write-host $MountPoint.SystemName, $MountPoint.Label,$MountPoint.DriveLetter

         #Specifics To Win32_Volume
         $DriveName      = $MountPoint.Label
         $DriveFullName  = $MountPoint.Name
         $DrivePath      = $MountPoint.Caption
         $DriveLetter    = $MountPoint.DriveLetter
         $DriveType      = $MountPoint.DriveType
         $DriveFree      = [Math]::Round($MountPoint.FreeSpace / 1048576, 2)
         $DriveCapacity  = [Math]::Round($MountPoint.Capacity  / 1048576, 2)
         
         if($DriveLetter -eq $null) {$MountPointBit = 1} ELSE {$MountPointBit = 0}    
         if($DriveType -eq 3 -AND $DrivePath -like "c:\") {$DriveName  = "System Volume"} elseif ($DriveType -eq 5 -AND $MountPoint.Label -eq $null) {$DriveName  = "Optical Drive"} else {$DriveName = $MountPoint.Label}
           	
    			
         $destserver = "DBACentral"
         $destDB     = "DBACentral"
                   
         #Define SQL Insert Query Here
         $Query = "
         EXEC DBACentral.[hist].[SpaceUsed_DriveSizes_InsertDriveMaster] 
         @ServerName  = '$ServerInstance', -- varchar(256)
         @DriveLetter = '$DriveFullName', -- varchar(2)
         @DriveLabel  = '$DriveName', -- varchar(30)
         @FreeSpaceMB =  $DriveFree, -- int
         @CapacityMB  =  $DriveCapacity, -- int
         @MountPoint  =  $MountPointBit,
         @RunID       =  $RunID,
         @DrivePath   = '$DrivePath'
         "
         
         #Create SQL Connection String
         $conn = new-object System.Data.SqlClient.SQLConnection
         $ConnectionString = "Server={0};Database={1};Integrated Security=True;" -f $destserver,$destDB
         $conn.ConnectionString = $ConnectionString
         
         #Open Connection To SQL
         $conn.Open()
         $cmd = new-object system.Data.SqlClient.SqlCommand($Query,$conn)
         [void]$cmd.ExecuteNonQuery()
         $conn.Close()
                
      }
		
		Log "Finished collecting data" "Progress" $ServerName $MyInvocation.MyCommand.path
		
	}
    
	catch
	{	
		Log "$error[0].ToString()" "Error" $ServerName $MyInvocation.MyCommand.path
	}
    
	finally
	{
		Log "Script Execution Complete" "End" "" $MyInvocation.MyCommand.path	   
	}
    
}

