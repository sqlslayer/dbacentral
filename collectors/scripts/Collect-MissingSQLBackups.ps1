﻿param(
	[switch]$Verbose,
	[switch]$Debug
)

# Start logging things
$local:ErrorActionPreference = "Stop"

Import-Module DBACCollector -Force -Global

if ($Verbose){$VerbosePreference = "Continue"}
if ($Debug){
	$DebugPreference = "Continue"
	$VerbosePreference = "Continue"
	GetConfig
	}

Log "Script Starting" "Begin" "" $MyInvocation.MyCommand.path

$DBAC = OpenDBAC;

if ($DBAC.State -ne "Open")
{
	$DBAC.Open()
}

#Query to get the server names
$Server_Query = @"
SELECT 
	[ServerID]
    ,[FullName]
    ,[DotNetConnectionString]
FROM [dbo].[ServerInventory_SQL_AllServers_vw]
ORDER BY 2
"@

# Debug Query
if ($Debug) {
$Server_Query = @"
	SELECT 
		[ServerID]
	    ,[FullName]
	    ,[DotNetConnectionString]
	FROM [dbo].[ServerInventory_SQL_AllServers_vw]
	WHERE [FullName] IN ('SISBSS1') 
	ORDER BY 1
"@
}

# Get the last RunID
$RunID_Query = @"
SELECT ISNULL(MAX([RunID]),0) + 1 AS [RunID] FROM [etc].[MissingSQLBackups_RunIDs]
"@

# The query to collect missing backups
$Backup_Query = @"
SELECT
	s.[name]						AS [DatabaseName]
	,CASE s.[recovery_model_desc]		
		WHEN 'SIMPLE'		THEN 'S'
		WHEN 'FULL'			THEN 'F'
		WHEN 'BULK-LOGGED'	THEN 'B'
	END								AS [RecoveryModel]
	,CASE
		WHEN f.[type_desc] = 'LOG' THEN 'L'
		ELSE 'D'
	END								AS [BackupType]
	,bmf.[device_type]				AS [DeviceType]
	,MAX(bs.[backup_finish_date])	AS [LastBackupTime]
	,bmf.[physical_device_name]		AS [LastBackupLocation]
FROM [admin].[dbo].[sysdatabases_vw] s
JOIN [admin].[dbo].[sysmasterfiles_vw] f
	ON s.[database_id] = f.[database_id]
LEFT JOIN [msdb].[dbo].[backupset] bs
	ON s.[name] = bs.[database_name]
LEFT JOIN [msdb].[dbo].[backupmediafamily] bmf
	ON bs.[media_set_id] = bmf.[media_set_id]
JOIN
(
	SELECT
		s.[name]
		,MAX([backup_finish_date])	AS [backup_finish_date]
	FROM [admin].[dbo].[sysdatabases_vw] s
	LEFT JOIN [msdb].[dbo].[backupset] b
		ON s.[name] = b.[database_name]
	GROUP BY s.[name]
) t
	ON s.[name] = t.[name]
	AND ISNULL(bs.[backup_finish_date],'') = ISNULL(t.[backup_finish_date],'')
WHERE f.[file_id] IN (1,2)
AND s.[state_desc] = 'ONLINE'
AND s.[source_database_id] IS NULL
AND LOWER(s.[name]) NOT IN ('tempdb','pubs','northwind','admin')
AND LOWER(s.[name]) NOT LIKE '%adventureworks%'
AND (((s.[recovery_model_desc] != 'SIMPLE') AND (f.[type_desc] = 'LOG')) 
	OR (s.[recovery_model_desc] != 'SIMPLE' AND f.[type_desc] != 'LOG')
	OR (s.[recovery_model_desc] = 'SIMPLE' AND f.[type_desc] != 'LOG'))
AND s.[name] NOT IN 
(
	SELECT DISTINCT
		s.[name]
	FROM [admin].[dbo].[sysdatabases_vw] s
	JOIN [msdb].[dbo].[backupset] bs
		ON s.[name] = bs.[database_name]
	JOIN [msdb].[dbo].[backupmediafamily] bmf
		ON bs.[media_set_id] = bmf.[media_set_id]
		AND bs.[backup_start_date] > DATEADD(DAY,-1,GETDATE())
	WHERE bmf.[device_type] != 2 -- Only if searching for tape backups only, otherwise comment out
)
GROUP BY 
	s.[name]
	,s.[recovery_model_desc]
	,f.[type_desc]
	,bmf.[device_type]
	,bmf.[physical_device_name]
"@

# Retrieve RunID
$dtRunID = GetDataTable $DBAC $RunID_Query
$RunID = $dtRunID[0];
"RunID: $RunID"

# Retrieve server list
$dtServerList = GetServerList $DBAC $Server_Query

# Loop through each server from the server list and connect to it
$dtServerList | ForEach-Object { 
	$ServerID = $_.ServerID;
	$ServerName = $_.FullName;
	$ConString = $_.DotNetConnectionString;
	
	Log "Now collecting data" "Progress" $ServerName $MyInvocation.MyCommand.path

	try
	{
		# Connect to the target server
		$cn_TargetServer = New-Object System.Data.SqlClient.SqlConnection($ConString);
		$cn_TargetServer.Open();
		
		# Get a SQL command object to the target server
		$cmd_TargetServer = New-Object System.Data.SqlClient.SqlCommand ($Backup_Query, $cn_TargetServer);
	
		$cmd_TargetServer.CommandTimeout = 300;
		
		$Target_Reader = $cmd_TargetServer.ExecuteReader();
	
		While ($Target_Reader.Read())
		{
			if ($DBAC.State -ne "Open")
			{
				$DBAC.Open()
			}
			$Inserter = New-Object System.Data.SqlClient.SqlCommand("[etc].[MissingSQLBackups_InsertValue]",$DBAC);
			$Inserter.CommandType = "StoredProcedure";
	
			$Inserter.Parameters.Add("@RunID",$RunID) | Out-Null
			$Inserter.Parameters.Add("@ServerName",$ServerName) | Out-Null
			$Inserter.Parameters.Add("@DatabaseName",$Target_Reader["DatabaseName"]) | Out-Null
			$Inserter.Parameters.Add("@RecoveryModel",$Target_Reader["RecoveryModel"]) | Out-Null
			$Inserter.Parameters.Add("@BackupType",$Target_Reader["BackupType"]) | Out-Null
			$Inserter.Parameters.Add("@DeviceType",$Target_Reader["DeviceType"]) | Out-Null
			$Inserter.Parameters.Add("@LastBackupTime",$Target_Reader["LastBackupTime"]) | Out-Null
			$Inserter.Parameters.Add("@LastBackupLocation",$Target_Reader["LastBackupLocation"]) | Out-Null
			$Inserter.ExecuteNonQuery() | out-null
			$Inserter.Dispose()
		}
		
		$Target_Reader.Dispose()
		$cmd_TargetServer.Dispose()
		
		Log "Finished collecting data" "Progress" $ServerName $MyInvocation.MyCommand.path
		
	}
	catch
	{	
		Log "$error[0].ToString()" "Error" $ServerName $MyInvocation.MyCommand.path
	}
	finally
	{
		$Target_Reader.Dispose()
		$cmd_TargetServer.Dispose()
	}

	
};

Log "Script Execution Complete" "End" "" $MyInvocation.MyCommand.path

$DBAC.Close()
$DBAC.Dispose()