USE [master]
GO
/****** Object:  Database [DBACentral]    Script Date: 5/22/2017 1:13:44 PM ******/
CREATE DATABASE [DBACentral]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DBACentral_Data', FILENAME = N'E:\SQLData\DEFAULT\Data\DBACentral_Data.mdf' , SIZE = 1048576KB , MAXSIZE = UNLIMITED, FILEGROWTH = 524288KB ), 
 FILEGROUP [History] 
( NAME = N'DBACentral_History_Data', FILENAME = N'E:\SQLData\DEFAULT\Data\DBACentral_History_Data.ndf' , SIZE = 1048576KB , MAXSIZE = UNLIMITED, FILEGROWTH = 524288KB )
 LOG ON 
( NAME = N'DBACentral_Log', FILENAME = N'F:\SQLData\DEFAULT\Logs\DBACentral_Log.ldf' , SIZE = 1048576KB , MAXSIZE = 2048GB , FILEGROWTH = 524288KB )
GO
ALTER DATABASE [DBACentral] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DBACentral].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DBACentral] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DBACentral] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DBACentral] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DBACentral] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DBACentral] SET ARITHABORT ON 
GO
ALTER DATABASE [DBACentral] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DBACentral] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DBACentral] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DBACentral] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DBACentral] SET CURSOR_DEFAULT  LOCAL 
GO
ALTER DATABASE [DBACentral] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DBACentral] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DBACentral] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DBACentral] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DBACentral] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DBACentral] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DBACentral] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DBACentral] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DBACentral] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DBACentral] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DBACentral] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DBACentral] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DBACentral] SET RECOVERY FULL 
GO
ALTER DATABASE [DBACentral] SET  MULTI_USER 
GO
ALTER DATABASE [DBACentral] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DBACentral] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DBACentral] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DBACentral] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [DBACentral] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [DBACentral] SET QUERY_STORE = OFF
GO
USE [DBACentral]
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [DBACentral]
GO
/****** Object:  User [Readers]    Script Date: 5/22/2017 1:13:45 PM ******/
CREATE USER [Readers] FOR LOGIN [ETC\SQL_ETCPRODSQLUTL01_DEFAULT_Readers_sg]
GO
/****** Object:  User [PwrUsers]    Script Date: 5/22/2017 1:13:45 PM ******/
CREATE USER [PwrUsers] FOR LOGIN [ETC\SQL_ETCPRODSQLUTL01_DEFAULT_PwrUsers_sg]
GO
/****** Object:  User [Captains]    Script Date: 5/22/2017 1:13:45 PM ******/
CREATE USER [Captains] FOR LOGIN [ETC\SQL_ETCPRODSQLUTL01_DEFAULT_Captains_sg]
GO
/****** Object:  DatabaseRole [db_nt_reader]    Script Date: 5/22/2017 1:13:45 PM ******/
CREATE ROLE [db_nt_reader]
GO
/****** Object:  DatabaseRole [db_nt_pwruser]    Script Date: 5/22/2017 1:13:45 PM ******/
CREATE ROLE [db_nt_pwruser]
GO
/****** Object:  DatabaseRole [db_nt_captain]    Script Date: 5/22/2017 1:13:45 PM ******/
CREATE ROLE [db_nt_captain]
GO
ALTER ROLE [db_nt_reader] ADD MEMBER [Readers]
GO
ALTER ROLE [db_datareader] ADD MEMBER [Readers]
GO
ALTER ROLE [db_nt_pwruser] ADD MEMBER [PwrUsers]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [PwrUsers]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [PwrUsers]
GO
ALTER ROLE [db_datareader] ADD MEMBER [PwrUsers]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [PwrUsers]
GO
ALTER ROLE [db_owner] ADD MEMBER [Captains]
GO
/****** Object:  Schema [audit]    Script Date: 5/22/2017 1:13:45 PM ******/
CREATE SCHEMA [audit]
GO
/****** Object:  Schema [etc]    Script Date: 5/22/2017 1:13:45 PM ******/
CREATE SCHEMA [etc]
GO
/****** Object:  Schema [hist]    Script Date: 5/22/2017 1:13:45 PM ******/
CREATE SCHEMA [hist]
GO
/****** Object:  Schema [migrate]    Script Date: 5/22/2017 1:13:45 PM ******/
CREATE SCHEMA [migrate]
GO
/****** Object:  Schema [mon]    Script Date: 5/22/2017 1:13:45 PM ******/
CREATE SCHEMA [mon]
GO
/****** Object:  Schema [ref]    Script Date: 5/22/2017 1:13:45 PM ******/
CREATE SCHEMA [ref]
GO
/****** Object:  Schema [rpt]    Script Date: 5/22/2017 1:13:45 PM ******/
CREATE SCHEMA [rpt]
GO
/****** Object:  Schema [UH]    Script Date: 5/22/2017 1:13:45 PM ******/
CREATE SCHEMA [UH]
GO
/****** Object:  UserDefinedTableType [dbo].[AttributeListType]    Script Date: 5/22/2017 1:13:45 PM ******/
CREATE TYPE [dbo].[AttributeListType] AS TABLE(
	[AttributeName] [varchar](100) NULL,
	[AttributeValue] [nvarchar](1000) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[DatabaseListType]    Script Date: 5/22/2017 1:13:45 PM ******/
CREATE TYPE [dbo].[DatabaseListType] AS TABLE(
	[DBName] [nvarchar](128) NULL
)
GO
/****** Object:  Synonym [dbo].[SQLServers]    Script Date: 5/22/2017 1:13:45 PM ******/
CREATE SYNONYM [dbo].[SQLServers] FOR [dbo].[ServerInventory_SQL_AllServers_vw]
GO
/****** Object:  UserDefinedFunction [dbo].[Split_fn]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Split_fn]
(
	@sInputList VARCHAR(MAX)			-- List of delimited items
	,@sDelimiter VARCHAR(8)		= ','	-- delimiter that separates items
)
RETURNS @List TABLE ([item] VARCHAR(MAX)) 
AS
BEGIN
DECLARE @sItem VARCHAR(MAX) 
WHILE CHARINDEX(@sDelimiter,@sInputList,0) <> 0
BEGIN
	SELECT
		@sItem=RTRIM(LTRIM(SUBSTRING(@sInputList,1,CHARINDEX(@sDelimiter,@sInputList,0)-1)))
		,@sInputList=RTRIM(LTRIM(SUBSTRING(@sInputList,CHARINDEX(@sDelimiter,@sInputList,0)+LEN(@sDelimiter),LEN(@sInputList))))
	
	IF LEN(@sItem) > 0
		INSERT INTO @List SELECT @sItem
	END

	IF LEN(@sInputList) > 0
		INSERT INTO @List SELECT @sInputList-- Put the last item in
RETURN 
END

GO
/****** Object:  Table [hist].[ServerInventory_ServerIDs]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[ServerInventory_ServerIDs](
	[HistServerID] [int] IDENTITY(1,1) NOT NULL,
	[ServerName] [varchar](200) NULL,
 CONSTRAINT [PK__ServerInventory_ServerIDs__HistServerID] PRIMARY KEY NONCLUSTERED 
(
	[HistServerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UIX__ServerInventory_ServerIDs__ServerName__HistServerID]    Script Date: 5/22/2017 1:13:45 PM ******/
CREATE CLUSTERED INDEX [UIX__ServerInventory_ServerIDs__ServerName__HistServerID] ON [hist].[ServerInventory_ServerIDs]
(
	[HistServerID] ASC,
	[ServerName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
GO
/****** Object:  Table [hist].[ServerInventory_SQL_DatabaseIDs]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[ServerInventory_SQL_DatabaseIDs](
	[DatabaseID] [int] IDENTITY(1,1) NOT NULL,
	[DBName] [sysname] NULL,
 CONSTRAINT [PK__ServerInventory_SQL_DatabaseIDs__DatabaseID] PRIMARY KEY NONCLUSTERED 
(
	[DatabaseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  Table [hist].[SpaceUsed_DatabaseSizes]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[SpaceUsed_DatabaseSizes](
	[HistServerID] [int] NULL,
	[DatabaseID] [int] NULL,
	[DataSizeMB] [bigint] NULL,
	[LogSizeMB] [bigint] NULL,
	[SampleDate] [smalldatetime] NULL,
	[DataSizeUnusedMB] [bigint] NULL,
	[LogSizeUnusedMB] [bigint] NULL
) ON [History]

GO
/****** Object:  Index [IX__SpaceUsed_DBSizes__SampleDate]    Script Date: 5/22/2017 1:13:45 PM ******/
CREATE CLUSTERED INDEX [IX__SpaceUsed_DBSizes__SampleDate] ON [hist].[SpaceUsed_DatabaseSizes]
(
	[SampleDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [History]
GO
/****** Object:  View [hist].[SpaceUsed_DatabaseSizes_Delta_vw]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*******************************************************************************************************
**  Name:			[hist].[SpaceUsed_DatabaseSizes_Delta_vw]
**  Desc:			View to pull back the database sizes from the repository and show the daily delta
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			(unknown)
**  Debug:			
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090706	Matt Stanford	Fully backwards-compatible change to add DataSizeUnusedMB and LogSizeUnusedMB
**	20090716	Matt Stanford	Added DataPercentChange and LogPercentChange columns
**  20140407    Peng Chan		Included unused space into the calculation
********************************************************************************************************/
CREATE VIEW [hist].[SpaceUsed_DatabaseSizes_Delta_vw]
AS
WITH DBData AS (
	SELECT 
		[HistServerID]
		,[DatabaseID]
		,[DataSizeMB]
		,[LogSizeMB]
		,[DataSizeUnusedMB]
		,[LogSizeUnusedMB]
		,[SampleDate]
		,ROW_NUMBER() OVER (PARTITION BY [HistServerID], [DatabaseID] ORDER BY SampleDate) as rownum
	FROM [hist].[SpaceUsed_DatabaseSizes]
)
SELECT 
	s.[ServerName]
	,d.[DBName]
	,currow.[DataSizeMB]
--	,currow.[DataSizeMB] - prevrow.[DataSizeMB]					AS [DataSizeMBIncrease]
	,currow.[DataSizeMB] - prevrow.[DataSizeMB]	+ prevrow.[DataSizeUnusedMB] - currow.[DataSizeUnusedMB]		AS [DataSizeMBIncrease]
	,CASE
		WHEN prevrow.[DataSizeMB] = 0 THEN 0
--		ELSE CAST(CAST(currow.[DataSizeMB] - prevrow.[DataSizeMB] AS DECIMAL(10,2)) * 100 / prevrow.[DataSizeMB] AS DECIMAL(10,2))
		ELSE CAST(CAST(currow.[DataSizeMB] - prevrow.[DataSizeMB] + prevrow.DataSizeUnusedMB - currow.DataSizeUnusedMB AS DECIMAL(10,2)) * 100 / prevrow.[DataSizeMB] AS DECIMAL(10,2))
	END															AS [DataPercentChange]
	,currow.[LogSizeMB]
--	,currow.[LogSizeMB] - prevrow.[LogSizeMB]					AS [LogSizeMBIncrease]
	,currow.[LogSizeMB] - prevrow.[LogSizeMB] + prevrow.[LogSizeUnusedMB] - currow.[LogSizeUnusedMB]			AS [LogSizeMBIncrease]
	,CASE
		WHEN prevrow.[LogSizeMB] = 0 THEN 0
		ELSE CAST(CAST(currow.[LogSizeMB] - prevrow.[LogSizeMB] AS DECIMAL(10,2)) * 100 / prevrow.[LogSizeMB] AS DECIMAL(10,2))
	END															AS [LogPercentChange]
	,currow.[DataSizeUnusedMB]
	,currow.[DataSizeUnusedMB] - prevrow.[DataSizeUnusedMB]		AS [DataSizeUnusedMBIncrease]
	,currow.[LogSizeUnusedMB]
	,currow.[LogSizeUnusedMB] - prevrow.[LogSizeUnusedMB]		AS [LogSizeUnusedMBIncrease]
	,currow.[SampleDate]
FROM DBData currow
INNER JOIN [hist].[ServerInventory_ServerIDs] s
	ON s.[HistServerID] = currow.[HistServerID]
INNER JOIN [hist].[ServerInventory_SQL_DatabaseIDs] d
	ON d.[DatabaseID] = currow.[DatabaseID]
LEFT OUTER JOIN DBData prevrow
	ON prevrow.[HistServerID] = currow.[HistServerID]
	AND prevrow.[DatabaseID] = currow.[DatabaseID]
	AND currow.[rownum] = prevrow.[rownum] + 1





GO
/****** Object:  Table [dbo].[ServerInventory_Environments]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServerInventory_Environments](
	[EnvironmentID] [tinyint] IDENTITY(1,1) NOT NULL,
	[EnvironmentName] [varchar](100) NULL,
 CONSTRAINT [PK__ServerInventory_Environments__EnvironmentID] PRIMARY KEY NONCLUSTERED 
(
	[EnvironmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ServerInventory_BusinessUnits]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServerInventory_BusinessUnits](
	[BusinessUnitID] [smallint] IDENTITY(1,1) NOT NULL,
	[BusinessName] [varchar](100) NULL,
 CONSTRAINT [PK__ServerInventory_BusinessUnits__BusinessUnitID] PRIMARY KEY NONCLUSTERED 
(
	[BusinessUnitID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ServerInventory_SQL_Master]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServerInventory_SQL_Master](
	[ServerID] [int] IDENTITY(1,1) NOT NULL,
	[ServerName] [varchar](100) NOT NULL,
	[InstanceName] [varchar](100) NULL,
	[PortNumber] [int] NULL,
	[Description] [nvarchar](max) NULL,
	[SQLVersion] [varchar](8) NOT NULL,
	[Enabled] [bit] NOT NULL,
	[BusinessUnitID] [smallint] NULL,
	[EnvironmentID] [tinyint] NOT NULL,
	[EditionID] [smallint] NOT NULL,
	[UseCredential] [bit] NULL,
	[CredentialID] [int] NULL,
	[Owner] [varchar](30) NULL,
 CONSTRAINT [PK__ServerInventory_SQL_Master__ServerID] PRIMARY KEY NONCLUSTERED 
(
	[ServerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ServerInventory_SQL_ServerCredentials]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServerInventory_SQL_ServerCredentials](
	[CredentialID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](100) NULL,
	[Password] [nvarchar](500) NULL,
 CONSTRAINT [PK__ServerInventory_SQL_ServerCredentials__CredentialID] PRIMARY KEY CLUSTERED 
(
	[CredentialID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ServerInventory_SQL_Editions]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServerInventory_SQL_Editions](
	[EditionID] [smallint] IDENTITY(1,1) NOT NULL,
	[EditionName] [varchar](100) NULL,
 CONSTRAINT [PK__ServerInventory_SQL_Editions__EditionID] PRIMARY KEY NONCLUSTERED 
(
	[EditionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[ServerInventory_SQL_AllServers_vw]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[ServerInventory_SQL_AllServers_vw]
AS
SELECT 
	m.[ServerID]
	,m.[ServerName]
	,m.[InstanceName]
	,m.[PortNumber]
	,CASE 
		WHEN m.[InstanceName] IS NOT NULL AND m.[PortNumber] IS NOT NULL
			THEN m.[ServerName] + '\' + m.[InstanceName] + ',' + CAST(m.[PortNumber] AS VARCHAR(10))
		WHEN m.[InstanceName] IS NOT NULL
			THEN m.[ServerName] + '\' + m.[InstanceName]
		WHEN m.[PortNumber] IS NOT NULL
			THEN m.[ServerName] + ',' + CAST(m.[PortNumber] AS VARCHAR(10))
		ELSE m.[ServerName]
	END AS [FullName]
	,m.[SQLVersion]
	,bus.[BusinessName]
	,env.[EnvironmentName]	AS Environment
	,ed.[EditionName]		AS Edition
	,m.[Description]
	,m.[UseCredential]
	,'Data Source=' + CASE 
		WHEN m.[InstanceName] IS NOT NULL AND m.[PortNumber] IS NOT NULL
			THEN m.[ServerName] + '\' + m.[InstanceName] + ',' + CAST(m.[PortNumber] AS VARCHAR(10))
		WHEN m.[InstanceName] IS NOT NULL
			THEN m.[ServerName] + '\' + m.InstanceName
		WHEN m.[PortNumber] IS NOT NULL
			THEN m.[ServerName] + ',' + CAST(m.[PortNumber] AS VARCHAR(10))
		ELSE m.[ServerName]
	END + ';Initial Catalog=master;' + CASE
		WHEN m.[UseCredential] = 0 
			THEN 'Integrated Security=SSPI;'
		ELSE 'User Id=' + cred.[UserName] + ';Password=' + cred.[Password] + ';'
	END AS [DotNetConnectionString]
FROM [dbo].[ServerInventory_SQL_Master] m
INNER JOIN [dbo].[ServerInventory_Environments] env
	ON env.[EnvironmentID] = m.[EnvironmentID]
INNER JOIN [dbo].[ServerInventory_SQL_Editions] ed
	ON ed.[EditionID] = m.[EditionID]
LEFT JOIN [dbo].[ServerInventory_SQL_ServerCredentials] cred
	ON cred.[CredentialID] = m.[CredentialID]
LEFT JOIN [dbo].[ServerInventory_BusinessUnits] bus
	ON bus.[BusinessUnitID] = m.[BusinessUnitID]
WHERE m.[Enabled] = 1




GO
/****** Object:  View [rpt].[DBSizes_Summary_vw]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			[rpt].[DBSizes_Summary_vw]
**	Desc:			Reporting procedure for database sizes report
**	Auth:			Adam Bean (SQLSlayer.com)
**  Dependancies:	dbo.Split_fn
**	Date:			2010.08.11
*******************************************************************************
**  License
*******************************************************************************
**  Copyright © SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  YYYYDDMM	Full Name	
********************************************************************************************************/

CREATE VIEW [rpt].[DBSizes_Summary_vw]

AS

SELECT 
      s.[Environment]
      ,d.[ServerName]
      ,d.[DBName]             
      ,d.[DataSizeMB]
      ,d.[DataSizeMBIncrease]
      ,d.[DataPercentChange]
      ,d.[LogSizeMB]
      ,d.[LogSizeMBIncrease]
      ,d.[LogPercentChange]
      ,d.[DataSizeUnusedMB]
      ,d.[DataSizeUnusedMBIncrease]
      ,d.[LogSizeUnusedMB]
      ,d.[LogSizeUnusedMBIncrease]
      ,d.[SampleDate]
FROM [hist].[SpaceUsed_DatabaseSizes_Delta_vw] d
JOIN [dbo].[ServerInventory_SQL_AllServers_vw] s
	ON d.[ServerName] = s.[FullName]--COALESCE(s.[ServerName] + '\' + s.[InstanceName] + ',' + CAST(s.[PortNumber] AS VARCHAR), s.[ServerName] + '\' + s.[InstanceName], s.[ServerName])



GO
/****** Object:  View [rpt].[DBSizes_FileGrowth_vw]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			[rpt].[DBSizes_FileGrowth_vw]
**	Desc:			Reporting view to display growth per database
**	Auth:			Adam Bean (SQLSlayer.com)
**	Notes:			Date range is to ensure only recent databases are counted
**	Date:			2010.08.11
*******************************************************************************
**  License
*******************************************************************************
**  Copyright © SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  YYYYDDMM	Full Name	
********************************************************************************************************/

CREATE VIEW [rpt].[DBSizes_FileGrowth_vw]

AS

SELECT
	t1.[Environment]
	,t1.[ServerName]
	,t.[DBName]
	,t.[FirstSampleDate]
	,t1.[DataSizeMB]													AS [FirstDataSizeMB]
	,t1.[LogSizeMB]														AS [FirstLogSizeMB]
	,ISNULL(t1.[DataSizeUnusedMB],0)									AS [FirstDataSizeUnusedMB]
	,ISNULL(t1.[LogSizeUnusedMB],0)										AS [FirstLogSizeUnusedMB]
	,t.[LastSampleDate]
	,t2.[DataSizeMB]													AS [LastDataSizeMB]
	,t2.[LogSizeMB]														AS [LastLogSizeMB]
	,ISNULL(t2.[DataSizeUnusedMB],0)									AS [LastDataSizeUnusedMB]
	,ISNULL(t2.[LogSizeUnusedMB],0)										AS [LastLogSizeUnusedMB]
	,ISNULL(t2.[DataSizeMB],0) - ISNULL(t1.[DataSizeMB],0)				AS [DataSizeChangeMB]
	,ISNULL(t2.[LogSizeMB],0) - ISNULL(t1.[LogSizeMB],0)				AS [LogSizeChangeMB]
	,ISNULL(t2.[DataSizeUnusedMB],0) - ISNULL(t1.[DataSizeUnusedMB],0)	AS [DataSizeUnusedChangeMB]
	,ISNULL(t2.[LogSizeUnusedMB],0) - ISNULL(t1.[LogSizeUnusedMB],0)	AS [LogSizeUnusedChangeMB]
FROM 
(		
	SELECT
		[ServerName]
		,[DBName]
		,MIN([SampleDate]) AS [FirstSampleDate]
		,MAX([SampleDate]) AS [LastSampleDate]
	FROM [rpt].[DBSizes_Summary_vw]
	GROUP BY 
		[ServerName]
		,[DBName]
) t
JOIN [rpt].[DBSizes_Summary_vw] t1
	ON t.[ServerName] = t1.[ServerName]
	AND t.[DBName] = t1.[DBName]
	AND t.[FirstSampleDate] = t1.[SampleDate]
JOIN [rpt].[DBSizes_Summary_vw] t2
	ON t.[ServerName] = t2.[ServerName]
	AND t.[DBName] = t2.[DBName]
	AND t.[LastSampleDate] = t2.[SampleDate]

GO
/****** Object:  View [rpt].[DBSizes_DBCounts_vw]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			[rpt].[DBSizes_DBCounts_vw]
**	Desc:			Reporting view to display database counts
**	Auth:			Adam Bean (SQLSlayer.com)
**	Notes:			Date range is to ensure only recent databases are counted
**	Date:			2010.08.11
*******************************************************************************
**  License
*******************************************************************************
**  Copyright © SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  YYYYDDMM	Full Name	
********************************************************************************************************/

CREATE VIEW [rpt].[DBSizes_DBCounts_vw]

AS

SELECT
	[ServerName] 
	,SUM(t.[IsSystem])				AS [System]
	,SUM(t.[IsUser])				AS [User]
	,SUM(t.[IsUser] + t.[IsSystem])	AS [Total]
FROM
(
	SELECT DISTINCT
		[ServerName]
		,[DBName]
		,CASE
			WHEN [DBName] IN ('admin','master','model','msdb','tempdb') OR [DBName] LIKE '%adventureworks%' THEN 0
			ELSE 1
		END AS [IsUser]
		,CASE
			WHEN [DBName] IN ('admin','master','model','msdb','tempdb') OR [DBName] LIKE '%adventureworks%' THEN 1
			ELSE 0
		END AS [IsSystem]
	FROM [rpt].[DBSizes_Summary_vw]
	WHERE [SampleDate] > DATEADD(dd,-2,GETDATE())
) t
GROUP BY [ServerName]

GO
/****** Object:  Table [hist].[ServerInventory_SQL_IndexMaster]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[ServerInventory_SQL_IndexMaster](
	[HistIndexID] [int] IDENTITY(1,1) NOT NULL,
	[HistServerDBTableID] [int] NULL,
	[IndexName] [nvarchar](128) NULL,
	[IndexType] [tinyint] NULL,
	[is_unique] [bit] NULL,
	[ignore_dup_key] [bit] NULL,
	[is_primary_key] [bit] NULL,
	[fillfactor] [tinyint] NULL,
	[is_padded] [bit] NULL,
	[is_disabled] [bit] NULL,
	[allow_row_locks] [bit] NULL,
	[allow_page_locks] [bit] NULL,
	[has_filter] [bit] NULL,
	[DateCreated] [smalldatetime] NULL,
	[DateLastSeenOn] [smalldatetime] NULL,
 CONSTRAINT [PK__ServerInventory_SQL_IndexMaster__HistIndexID] PRIMARY KEY CLUSTERED 
(
	[HistIndexID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  Table [hist].[ServerInventory_SQL_IndexUsage]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[ServerInventory_SQL_IndexUsage](
	[HistIndexID] [int] NULL,
	[ReadCount] [bigint] NULL,
	[WriteCount] [bigint] NULL,
	[SampleDate] [smalldatetime] NULL,
	[SampleMSTicks] [bigint] NULL
) ON [History]

GO
/****** Object:  Table [hist].[ServerInventory_SQL_TableIDs]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[ServerInventory_SQL_TableIDs](
	[TableID] [int] IDENTITY(1,1) NOT NULL,
	[SchemaName] [sysname] NOT NULL,
	[TableName] [sysname] NOT NULL,
 CONSTRAINT [PK__ServerInventory_SQL_TableIDs__TableID] PRIMARY KEY NONCLUSTERED 
(
	[TableID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  Table [hist].[ServerInventory_SQL_ServerDBTableIDs]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[ServerInventory_SQL_ServerDBTableIDs](
	[ServerDBTableID] [int] IDENTITY(1,1) NOT NULL,
	[HistServerID] [int] NULL,
	[DatabaseID] [int] NULL,
	[TableID] [int] NULL,
	[LastUpdated] [smalldatetime] NULL,
 CONSTRAINT [PK__ServerInventory_SQL_ServerDBTableIDs__ServerDBTableID] PRIMARY KEY NONCLUSTERED 
(
	[ServerDBTableID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  View [hist].[ServerInventory_SQL_IndexUsage_vw]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[ServerInventory_SQL_IndexUsage_vw] 
**  Desc:			View index usage data over time
**  Auth:			Matt Stanford 
**  Date:			2009-10-13
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
********************************************************************************************************/
CREATE VIEW [hist].[ServerInventory_SQL_IndexUsage_vw] 
AS

SELECT 
	im.[HistIndexID]
	,s.[ServerName]
	,d.[DBName]
	,t.[SchemaName]
	,t.[TableName]
	,im.[IndexName]
	,iu.[ReadCount]
	,iu.[WriteCount]
	,iu.[SampleMSTicks]
	,iu.[SampleDate]
FROM [hist].[ServerInventory_SQL_IndexMaster] im
INNER JOIN [hist].[ServerInventory_SQL_IndexUsage] iu
	ON im.[HistIndexID] = iu.[HistIndexID]
INNER JOIN [hist].[ServerInventory_SQL_ServerDBTableIDs] sdbt
	ON sdbt.[ServerDBTableID] = im.[HistServerDBTableID]
INNER JOIN [hist].[ServerInventory_ServerIDs] s
	ON sdbt.[HistServerID] = s.[HistServerID]
INNER JOIN [hist].[ServerInventory_SQL_DatabaseIDs] d
	ON sdbt.[DatabaseID] = d.[DatabaseID]
INNER JOIN [hist].[ServerInventory_SQL_TableIDs] t
	ON sdbt.[TableID] = t.[TableID]


GO
/****** Object:  Table [hist].[ServerInventory_SQL_ColumnNames]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[ServerInventory_SQL_ColumnNames](
	[HistColumnID] [int] IDENTITY(1,1) NOT NULL,
	[ColumnName] [nvarchar](128) NULL,
	[ColumnTypeID] [int] NULL,
	[DateCreated] [smalldatetime] NULL,
 CONSTRAINT [PK__ServerInventory_SQL_ColumnNames__HistColumnID] PRIMARY KEY CLUSTERED 
(
	[HistColumnID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  Table [hist].[ServerInventory_SQL_IndexDetails]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[ServerInventory_SQL_IndexDetails](
	[HistIndexID] [int] NULL,
	[HistColumnID] [int] NULL,
	[Sequence] [tinyint] NULL,
	[IsDescending] [bit] NULL,
	[DateCreated] [smalldatetime] NULL
) ON [History]

GO
/****** Object:  View [hist].[ServerInventory_SQL_Indexes_vw]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************************************************************************
**  Name:			[hist].[ServerInventory_SQL_Indexes_vw]
**  Desc:			View index information
**  Auth:			Matt Stanford 
**  Date:			2009-10-13
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
********************************************************************************************************/
CREATE VIEW [hist].[ServerInventory_SQL_Indexes_vw]
AS

SELECT
	s.[ServerName]
	,d.[DBName]
	,t.[SchemaName]
	,t.[TableName]
	,im.[IndexName]
	,KeyCols.[Col1] + KeyCols.[Col2] + KeyCols.[Col3] + KeyCols.[Col4] + KeyCols.[Col5] 
		+ KeyCols.[Col6] + KeyCols.[Col7] + KeyCols.[Col8] + KeyCols.[Col9] + KeyCols.[Col10] + KeyCols.[Col11] as KeyCols
	,NonKeyCols.[Col1] + NonKeyCols.[Col2] + NonKeyCols.[Col3] + NonKeyCols.[Col4] + NonKeyCols.[Col5] 
		+ NonKeyCols.[Col6] + NonKeyCols.[Col7] + NonKeyCols.[Col8] + NonKeyCols.[Col9] + NonKeyCols.[Col10] + NonKeyCols.[Col11] as NonKeyCols
FROM [hist].[ServerInventory_SQL_IndexMaster] im
INNER JOIN [hist].[ServerInventory_SQL_ServerDBTableIDs] sdbt
	ON sdbt.[ServerDBTableID] = im.[HistServerDBTableID]
INNER JOIN [hist].[ServerInventory_ServerIDs] s
	ON sdbt.[HistServerID] = s.[HistServerID]
INNER JOIN [hist].[ServerInventory_SQL_DatabaseIDs] d
	ON sdbt.[DatabaseID] = d.[DatabaseID]
INNER JOIN [hist].[ServerInventory_SQL_TableIDs] t
	ON sdbt.[TableID] = t.[TableID]
LEFT OUTER JOIN 
	(
	SELECT
		[HistIndexID]
		,'[' + [1] + ']' AS 'Col1'
		,ISNULL(',[' + [2] + ']','') AS 'Col2'
		,ISNULL(',[' + [3] + ']','') AS 'Col3'
		,ISNULL(',[' + [4] + ']','') AS 'Col4'
		,ISNULL(',[' + [5] + ']','') AS 'Col5'
		,ISNULL(',[' + [6] + ']','') AS 'Col6'
		,ISNULL(',[' + [7] + ']','') AS 'Col7'
		,ISNULL(',[' + [8] + ']','') AS 'Col8'
		,ISNULL(',[' + [9] + ']','') AS 'Col9'
		,ISNULL(',[' + [10] + ']','') AS 'Col10'
		,ISNULL(',[' + [11] + ']','') AS 'Col11'
	FROM (
		SELECT 
			ic.[HistIndexID]
			,c.[ColumnName]
			,ROW_NUMBER() OVER (PARTITION BY ic.[HistIndexID] ORDER BY ic.[Sequence]) pt
		FROM [hist].[ServerInventory_SQL_IndexDetails] ic
		INNER JOIN [hist].[ServerInventory_SQL_ColumnNames] c
			ON ic.[HistColumnID] = c.[HistColumnID]
		WHERE ic.[Sequence] >= 1 -- No nonkey columns
	) as ST
	PIVOT
	(
		MAX([ColumnName])
		FOR pt IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11])
	) as PvT
) AS KeyCols
	ON im.[HistIndexID] = KeyCols.[HistIndexID]
LEFT OUTER JOIN  (
	SELECT
		[HistIndexID]
		,'[' + [1] + ']' AS 'Col1'
		,ISNULL(',[' + [2] + ']','') AS 'Col2'
		,ISNULL(',[' + [3] + ']','') AS 'Col3'
		,ISNULL(',[' + [4] + ']','') AS 'Col4'
		,ISNULL(',[' + [5] + ']','') AS 'Col5'
		,ISNULL(',[' + [6] + ']','') AS 'Col6'
		,ISNULL(',[' + [7] + ']','') AS 'Col7'
		,ISNULL(',[' + [8] + ']','') AS 'Col8'
		,ISNULL(',[' + [9] + ']','') AS 'Col9'
		,ISNULL(',[' + [10] + ']','') AS 'Col10'
		,ISNULL(',[' + [11] + ']','') AS 'Col11'
	FROM (
		SELECT 
			ic.[HistIndexID]
			,c.[ColumnName]
			,ROW_NUMBER() OVER (PARTITION BY ic.[HistIndexID] ORDER BY ic.[Sequence]) pt
		FROM [hist].[ServerInventory_SQL_IndexDetails] ic
		INNER JOIN [hist].[ServerInventory_SQL_ColumnNames] c
			ON ic.[HistColumnID] = c.[HistColumnID]
		WHERE ic.[Sequence] = 0 -- No key columns
	) as ST
	PIVOT
	(
		MAX([ColumnName])
		FOR pt IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11])
	) as PvT
) AS NonKeyCols
	ON KeyCols.[HistIndexID] = NonKeyCols.[HistIndexID]


GO
/****** Object:  Table [etc].[MissingSQLBackups_RunIDs]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [etc].[MissingSQLBackups_RunIDs](
	[RunID] [int] NULL,
	[DateCreated] [smalldatetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [etc].[MissingSQLBackups]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [etc].[MissingSQLBackups](
	[HistServerID] [int] NULL,
	[DatabaseID] [int] NULL,
	[RecoveryModel] [char](1) NULL,
	[BackupType] [char](1) NULL,
	[DeviceType] [char](1) NULL,
	[LastBackupTime] [smalldatetime] NULL,
	[LastBackupLocation] [varchar](256) NULL,
	[RunID] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  View [etc].[MissingSQLBackups_vw]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





/******************************************************************************
* Name
	[etc].[MissingSQLBackups_vw]

* Author
	Adam Bean
	
* Date
	2011.02.10
	
* Synopsis
	View to retrieve data for missing SQL Server backups

*******************************************************************************
* License
*******************************************************************************
	Copyright © SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20110516	Adam Bean		Updated HoursSinceLastBackup to reflect the current run vs GETDATE()
	20120702	Adam Bean		Updated to reflect new name of Exclusions table
******************************************************************************/

CREATE VIEW [etc].[MissingSQLBackups_vw]

AS

SELECT 
	id.[ServerName]
	,db.[DBName]											AS [DatabaseName]
	,CASE mb.[RecoveryModel]
		WHEN 'S' THEN 'SIMPLE'
		WHEN 'F' THEN 'FULL'
		WHEN 'B' THEN 'BULK-LOGGED'
	END														AS [RecoveryModel]
	,CASE mb.[BackupType]
		WHEN 'D' THEN 'DB'
		WHEN 'L' THEN 'TLOG'
	END														AS [BackupType]
	,CASE 
		WHEN mb.[DeviceType] = '*'			THEN '0'
		WHEN mb.[DeviceType] IN (2,102)		THEN 'DISK'
		WHEN mb.[DeviceType] IN (5,7,105)	THEN 'TAPE'
		ELSE CAST(mb.[DeviceType] AS VARCHAR)
	END														AS [DeviceType]
	,mb.[LastBackupTime]
	,DATEDIFF(HOUR, mb.[LastBackupTime], ri.[DateCreated])	AS 'HoursSinceLastBackup'
	,mb.[LastBackupLocation]
	,ri.[DateCreated]										AS [DatePolled]
FROM [etc].[MissingSQLBackups] mb
JOIN [hist].[ServerInventory_ServerIDs] id
	ON mb.[HistServerID] = id.[HistServerID]
JOIN [hist].[ServerInventory_SQL_DatabaseIDs] db
	ON db.[DatabaseID] = mb.[DatabaseID]
JOIN [etc].[MissingSQLBackups_RunIDs] ri
	ON ri.[RunID] = mb.[RunID]
--LEFT JOIN [etc].[ServerInventory_Exclusions] se
--	ON id.[ServerName] = se.[ServerName]
--	AND se.[Backups] = 1
--WHERE se.[ServerName] IS NULL





GO
/****** Object:  View [rpt].[DBSizes_FileGrowth_Overall_vw]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			[rpt].[DBSizes_FileGrowth_Overall_vw]
**	Desc:			Reporting view to display overall growth per database
**	Auth:			Adam Bean (SQLSlayer.com)
**	Notes:			Date range is to ensure only recent databases are counted
**	Date:			2010.08.11
*******************************************************************************
**  License
*******************************************************************************
**  Copyright © SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  YYYYDDMM	Full Name	
********************************************************************************************************/

CREATE VIEW [rpt].[DBSizes_FileGrowth_Overall_vw]

AS

SELECT
	t1.[Environment]
	,t1.[ServerName]
	,t.[DBName]
	,t.[FirstSampleDate]
	,t1.[DataSizeMB]													AS [FirstDataSizeMB]
	,t1.[LogSizeMB]														AS [FirstLogSizeMB]
	,ISNULL(t1.[DataSizeUnusedMB],0)									AS [FirstDataSizeUnusedMB]
	,ISNULL(t1.[LogSizeUnusedMB],0)										AS [FirstLogSizeUnusedMB]
	,t.[LastSampleDate]
	,t2.[DataSizeMB]													AS [LastDataSizeMB]
	,t2.[LogSizeMB]														AS [LastLogSizeMB]
	,ISNULL(t2.[DataSizeUnusedMB],0)									AS [LastDataSizeUnusedMB]
	,ISNULL(t2.[LogSizeUnusedMB],0)										AS [LastLogSizeUnusedMB]
	,ISNULL(t2.[DataSizeMB],0) - ISNULL(t1.[DataSizeMB],0)				AS [DataSizeChangeMB]
	,ISNULL(t2.[LogSizeMB],0) - ISNULL(t1.[LogSizeMB],0)				AS [LogSizeChangeMB]
	,ISNULL(t2.[DataSizeUnusedMB],0) - ISNULL(t1.[DataSizeUnusedMB],0)	AS [DataSizeUnusedChangeMB]
	,ISNULL(t2.[LogSizeUnusedMB],0) - ISNULL(t1.[LogSizeUnusedMB],0)	AS [LogSizeUnusedChangeMB]
FROM 
(		
	SELECT
		[ServerName]
		,[DBName]
		,MIN([SampleDate]) AS [FirstSampleDate]
		,MAX([SampleDate]) AS [LastSampleDate]
	FROM [rpt].[DBSizes_Summary_vw]
	GROUP BY 
		[ServerName]
		,[DBName]
) t
JOIN [rpt].[DBSizes_Summary_vw] t1
	ON t.[ServerName] = t1.[ServerName]
	AND t.[DBName] = t1.[DBName]
	AND t.[FirstSampleDate] = t1.[SampleDate]
JOIN [rpt].[DBSizes_Summary_vw] t2
	ON t.[ServerName] = t2.[ServerName]
	AND t.[DBName] = t2.[DBName]
	AND t.[LastSampleDate] = t2.[SampleDate]


GO
/****** Object:  View [rpt].[DBSizes_CurrentSizes_Summary_vw]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			[rpt].[DBSizes_CurrentSizes_Summary_vw]
**	Desc:			Reporting view to display current total sizes overall
**	Auth:			Adam Bean (SQLSlayer.com)
**	Date:			2010.08.11
*******************************************************************************
**  License
*******************************************************************************
**  Copyright © SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  YYYYDDMM	Full Name	
********************************************************************************************************/

CREATE VIEW [rpt].[DBSizes_CurrentSizes_Summary_vw]

AS

SELECT
	t1.[Environment]
	,t1.[ServerName]
	,t.[DBName]
	,t1.[DataSizeMB]													AS [LastDataSizeMB]
	,t1.[LogSizeMB]														AS [LastLogSizeMB]
	,ISNULL(t1.[DataSizeUnusedMB],0)									AS [LastDataSizeUnusedMB]
	,ISNULL(t1.[LogSizeUnusedMB],0)										AS [LastLogSizeUnusedMB]
FROM 
(		
	SELECT
		[ServerName]
		,[DBName]
		,MAX([SampleDate]) AS [LastSampleDate]
	FROM [rpt].[DBSizes_Summary_vw]
	GROUP BY 
		[ServerName]
		,[DBName]
) t
JOIN [rpt].[DBSizes_Summary_vw] t1
	ON t.[ServerName] = t1.[ServerName]
	AND t.[DBName] = t1.[DBName]
	AND t.[LastSampleDate] = t1.[SampleDate]
WHERE [LastSampleDate] > DATEADD(dd,-2,GETDATE())

GO
/****** Object:  Table [dbo].[ServerInventory_ApplicationOwners_Xref]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServerInventory_ApplicationOwners_Xref](
	[ApplicationID] [int] NULL,
	[OwnerID] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Index [CUIX__SI_ApplicationOwnerXref__ApplicationID__OwnerID]    Script Date: 5/22/2017 1:13:45 PM ******/
CREATE UNIQUE CLUSTERED INDEX [CUIX__SI_ApplicationOwnerXref__ApplicationID__OwnerID] ON [dbo].[ServerInventory_ApplicationOwners_Xref]
(
	[ApplicationID] ASC,
	[OwnerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ServerInventory_Applications]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServerInventory_Applications](
	[ApplicationID] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationName] [varchar](256) NULL,
 CONSTRAINT [PK__ServerInventory_Applications__ApplicationID] PRIMARY KEY CLUSTERED 
(
	[ApplicationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ServerInventory_Owners]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServerInventory_Owners](
	[OwnerID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerName] [varchar](256) NULL,
 CONSTRAINT [PK__ServerInventory_Owners__OwnerID] PRIMARY KEY CLUSTERED 
(
	[OwnerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[ServerInventory_ApplicationOwners_vw]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[dbo].[ServerInventory_ApplicationOwners_vw]
**  Desc:			View to pair owners up to thier applications
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			2009-07-21
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
********************************************************************************************************/
CREATE VIEW [dbo].[ServerInventory_ApplicationOwners_vw]
AS

SELECT
	a.[ApplicationName]
	,o.[OwnerName]
FROM [dbo].[ServerInventory_ApplicationOwners_Xref] ax
INNER JOIN [dbo].[ServerInventory_Applications] a
	ON ax.[ApplicationID] = a.[ApplicationID]
INNER JOIN [dbo].[ServerInventory_Owners] o
	ON ax.[OwnerID] = o.[OwnerID]

GO
/****** Object:  Table [hist].[DTSStore_PackageStore]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[DTSStore_PackageStore](
	[HistServerID] [int] NOT NULL,
	[PackageNameID] [int] NOT NULL,
	[VersionID] [uniqueidentifier] NOT NULL,
	[DescriptionID] [int] NULL,
	[CategoryID] [int] NULL,
	[CreateDate] [datetime] NULL,
	[OwnerID] [int] NULL,
	[PackageData] [image] NULL,
	[PackageType] [int] NOT NULL,
	[DateCreated] [smalldatetime] NULL,
 CONSTRAINT [PK__DTSStore_PackageStore__HistServerID__PackageNameID__VersionID] PRIMARY KEY CLUSTERED 
(
	[HistServerID] ASC,
	[PackageNameID] ASC,
	[VersionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History] TEXTIMAGE_ON [History]

GO
/****** Object:  Table [hist].[DTSStore_Descriptions]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[DTSStore_Descriptions](
	[DescriptionID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](1024) NULL,
	[DateCreated] [smalldatetime] NULL,
 CONSTRAINT [PK__DTSStore_Descriptions__DescriptionID] PRIMARY KEY CLUSTERED 
(
	[DescriptionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  Table [hist].[DTSStore_Categories]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[DTSStore_Categories](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryID_GUID] [uniqueidentifier] NOT NULL,
	[DateCreated] [smalldatetime] NULL,
 CONSTRAINT [PK__DTSStore_Categories__CategoryID] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  Table [hist].[DTSStore_PackageNames]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[DTSStore_PackageNames](
	[PackageNameID] [int] IDENTITY(1,1) NOT NULL,
	[PackageName] [sysname] NOT NULL,
	[PackageID] [uniqueidentifier] NOT NULL,
	[datecreated] [smalldatetime] NULL,
 CONSTRAINT [PK__DTSStore_PackageNames__PackageNameID] PRIMARY KEY CLUSTERED 
(
	[PackageNameID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  Table [hist].[DTSStore_Owners]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[DTSStore_Owners](
	[OwnerID] [int] IDENTITY(1,1) NOT NULL,
	[Owner] [sysname] NOT NULL,
	[Owner_sid] [varbinary](85) NOT NULL,
	[DateCreated] [smalldatetime] NULL,
 CONSTRAINT [PK__DTSStore_Owners__OwnerID] PRIMARY KEY CLUSTERED 
(
	[OwnerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  View [hist].[DTSStore_Packages_vw]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[DTSStore_Packages_vw]
**  Desc:			View to pull DTS package definitions from the repository
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			(unknown)
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090717	Matt Stanford	Changed because of HistServerID change
********************************************************************************************************/
CREATE VIEW [hist].[DTSStore_Packages_vw]
AS
SELECT 
	srvr.[ServerName]		AS [SourceServer]
	,names.[PackageName]	AS [name]
	,names.[PackageID]		AS [id]
	,ps.[VersionID]			AS [versionid]
	,descs.[Description]	AS [description]
	,cats.[CategoryID_GUID]	AS [categoryid]
	,ps.[CreateDate]		AS [createdate]
	,owners.[Owner]			AS [owner]
	,ps.[PackageData]		AS [packagedata]
	,owners.[Owner_sid]		AS [owner_sid]
	,ps.[PackageType]		AS [packagetype]
	,ps.[DateCreated]		AS [datecreated]
FROM [hist].[DTSStore_PackageStore] ps
INNER JOIN [hist].[DTSStore_PackageNames] names
	ON ps.[PackageNameID] = names.[PackageNameID]
INNER JOIN [hist].[DTSStore_Categories] cats
	ON ps.[CategoryID] = cats.[CategoryID]
INNER JOIN [hist].[DTSStore_Descriptions] descs
	ON ps.[DescriptionID] = descs.[DescriptionID]
INNER JOIN [hist].[DTSStore_Owners] owners
	ON ps.[OwnerID] = owners.[OwnerID]
INNER JOIN [hist].[ServerInventory_ServerIDs] srvr
	ON ps.[HistServerID] = srvr.[HistServerID]


GO
/****** Object:  Table [hist].[Jobs_SQL_Jobs]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[Jobs_SQL_Jobs](
	[HistJobID] [int] IDENTITY(1,1) NOT NULL,
	[HistServerID] [int] NULL,
	[job_id] [uniqueidentifier] NOT NULL,
	[name] [sysname] NOT NULL,
	[enabled] [tinyint] NOT NULL,
	[description] [nvarchar](512) NULL,
	[start_step_id] [int] NOT NULL,
	[category_id] [int] NOT NULL,
	[owner_sid] [varbinary](85) NOT NULL,
	[delete_level] [int] NOT NULL,
	[date_created] [datetime] NOT NULL,
	[date_modified] [datetime] NOT NULL,
	[version_number] [int] NOT NULL,
	[DateCreated] [smalldatetime] NULL,
	[LastSeenOn] [smalldatetime] NULL,
 CONSTRAINT [PK__Jobs_SQL_Jobs__HistJobID] PRIMARY KEY CLUSTERED 
(
	[HistJobID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  View [hist].[Jobs_SQL_Jobs_vw]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[Jobs_SQL_Jobs_vw]
**  Desc:			View to pull back the job definitions from the repository
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			(unknown)
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090717	Matt Stanford	Changed because of HistServerID change
**	20100125	Matt Stanford	Fixed bug, now actually pulls the latest jobs only
********************************************************************************************************/
CREATE VIEW [hist].[Jobs_SQL_Jobs_vw]
AS

WITH CurrentJobs (HistJobID)
AS
(
	SELECT
		MAX([HistJobID])
	FROM [hist].[Jobs_SQL_Jobs]
	GROUP BY [HistServerID], [name]
)
SELECT
	s.[ServerName]
	,j.[name]
	,j.[job_id]
	,j.[HistJobID]
	,j.[enabled]
	,j.[description]
	,j.[start_step_id]
	,j.[category_id]
	,j.[owner_sid]
	,j.[delete_level]
	,j.[date_created]
	,j.[date_modified]
	,j.[version_number]
	,j.[DateCreated]
	,j.[LastSeenOn]
FROM [hist].[Jobs_SQL_Jobs] j
INNER JOIN [hist].[ServerInventory_ServerIDs] s
	ON j.[HistServerID] = s.[HistServerID]
INNER JOIN [CurrentJobs] cj
	ON j.[HistJobID] = cj.[HistJobID]


GO
/****** Object:  View [rpt].[DBSizes_CurrentSizes_ByDatabase_vw]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			[rpt].[DBSizes_CurrentSizes_ByDatabase_vw]
**	Desc:			Reporting view to display current total sizes per database
**	Auth:			Adam Bean (SQLSlayer.com)
**	Date:			2010.08.11
*******************************************************************************
**  License
*******************************************************************************
**  Copyright © SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  YYYYDDMM	Full Name	
********************************************************************************************************/

CREATE VIEW [rpt].[DBSizes_CurrentSizes_ByDatabase_vw]

AS

SELECT 
	[Environment]
	,[ServerName]
	,[DBName]
	,[LastDataSizeMB]
	,[LastLogSizeMB]
	,[LastDataSizeMB] + [LastLogSizeMB] AS [TotalSizeMB]
FROM [rpt].[DBSizes_CurrentSizes_Summary_vw]


GO
/****** Object:  Table [hist].[Metrics_QueryStats]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[Metrics_QueryStats](
	[HistServerID] [int] NULL,
	[HistDatabaseID] [int] NULL,
	[HistObjectID] [int] NULL,
	[Count] [bigint] NULL,
	[Total_CPU_Time] [bigint] NULL,
	[Last_CPU] [bigint] NULL,
	[Min_CPU] [bigint] NULL,
	[Max_CPU] [bigint] NULL,
	[Total_Run_Time] [bigint] NULL,
	[Last_Run_Time] [bigint] NULL,
	[Min_Run_Time] [bigint] NULL,
	[Max_Run_Time] [bigint] NULL,
	[Total_Logical_Writes] [bigint] NULL,
	[Last_Logical_Writes] [bigint] NULL,
	[Min_Logical_Writes] [bigint] NULL,
	[Max_Logical_Writes] [bigint] NULL,
	[Total_Physical_Reads] [bigint] NULL,
	[Last_Physical_Reads] [bigint] NULL,
	[Min_Physical_Reads] [bigint] NULL,
	[Max_Physical_Reads] [bigint] NULL,
	[Total_Logical_Reads] [bigint] NULL,
	[Last_Logical_Reads] [bigint] NULL,
	[Min_Logical_Reads] [bigint] NULL,
	[Max_Logical_Reads] [bigint] NULL,
	[SampleDate] [smalldatetime] NULL
) ON [History]

GO
/****** Object:  Table [hist].[ServerInventory_SQL_ObjectIDs]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[ServerInventory_SQL_ObjectIDs](
	[ObjectID] [int] IDENTITY(1,1) NOT NULL,
	[SchemaName] [sysname] NULL,
	[ObjectName] [sysname] NOT NULL,
	[SQLType] [nvarchar](128) NULL,
 CONSTRAINT [PK__ServerInventory_SQL_ObjectIDs__ObjectID] PRIMARY KEY NONCLUSTERED 
(
	[ObjectID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  View [hist].[Metrics_QueryStats_vw]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[Metrics_QueryStats_vw]
**  Desc:			View to pull query usage metrics from the repository
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			2009.12.15
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20100218	Matt Stanford	Removed the average columns, now calculating them
********************************************************************************************************/
CREATE VIEW [hist].[Metrics_QueryStats_vw]
AS
SELECT 
	s.[ServerName]
	,d.[DBName]
	,o.[SchemaName]
	,o.[ObjectName]
	,qs.[Count]
	,qs.[Total_CPU_Time]
	,qs.[Total_CPU_Time]/qs.[Count]				AS [AVG_CPU_Time]
	,qs.[Last_CPU]
	,qs.[Min_CPU]
	,qs.[Max_CPU]
	,qs.[Total_Run_Time]
	,qs.[Total_Run_Time]/qs.[Count]				AS [AVG_Run_Time]
	,qs.[Last_Run_Time]
	,qs.[Min_Run_Time]
	,qs.[Max_Run_Time]
	,qs.[Total_Logical_Writes]
	,qs.[Total_Logical_Writes]/qs.[Count]		AS [Avg_Logical_Writes]
	,qs.[Last_Logical_Writes]
	,qs.[Min_Logical_Writes]
	,qs.[Max_Logical_Writes]
	,qs.[Total_Physical_Reads]
	,qs.[Total_Physical_Reads]/qs.[Count]		AS [Avg_Physical_Reads]
	,qs.[Last_Physical_Reads]
	,qs.[Min_Physical_Reads]
	,qs.[Max_Physical_Reads]
	,qs.[Total_Logical_Reads]
	,qs.[Total_Logical_Reads]/qs.[Count]		AS [Avg_Logical_Reads]
	,qs.[Last_Logical_Reads]
	,qs.[Min_Logical_Reads]
	,qs.[Max_Logical_Reads]
	,qs.[SampleDate]
FROM [hist].[Metrics_QueryStats] qs
INNER JOIN [hist].[ServerInventory_ServerIDs] s
	ON qs.[HistServerID] = s.[HistServerID]
INNER JOIN [hist].[ServerInventory_SQL_DatabaseIDs] d
	ON qs.[HistDatabaseID] = d.[DatabaseID]
INNER JOIN [hist].[ServerInventory_SQL_ObjectIDs] o
	ON qs.[HistObjectID] = o.[ObjectID]



GO
/****** Object:  Table [hist].[DatabaseMaintenance_CheckDB_Errors]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[DatabaseMaintenance_CheckDB_Errors](
	[HistServerID] [int] NOT NULL,
	[DatabaseID] [int] NOT NULL,
	[RunID] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[Error] [int] NULL,
	[Level] [int] NULL,
	[State] [int] NULL,
	[MessageText] [varchar](max) NULL,
	[RepairLevel] [varchar](128) NULL,
	[Status] [int] NULL,
	[ObjectID] [int] NULL,
	[IndexId] [int] NULL,
	[PartitionID] [bigint] NULL,
	[AllocUnitID] [bigint] NULL,
	[File] [int] NULL,
	[Page] [int] NULL,
	[Slot] [int] NULL,
	[RefFile] [int] NULL,
	[RefPage] [int] NULL,
	[RefSlot] [int] NULL,
	[Allocation] [int] NULL,
 CONSTRAINT [PK__DatabaseMaintenance_CheckDB_Errors__HistServerID__DatabaseID__RunID] PRIMARY KEY NONCLUSTERED 
(
	[HistServerID] ASC,
	[DatabaseID] ASC,
	[RunID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History] TEXTIMAGE_ON [History]

GO
/****** Object:  Table [hist].[DatabaseMaintenance_CheckDB_OK]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[DatabaseMaintenance_CheckDB_OK](
	[HistServerID] [int] NOT NULL,
	[DatabaseID] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[RunID] [int] NOT NULL,
 CONSTRAINT [PK__DatabaseMaintenance_CheckDB_OK__HistServerID__DatabaseID__RunID] PRIMARY KEY NONCLUSTERED 
(
	[HistServerID] ASC,
	[DatabaseID] ASC,
	[RunID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  View [hist].[DatabaseMaintenance_CheckDB_vw]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************************************************************************
**  Name:			[hist].[DatabaseMaintenance_CheckDB_vw]
**  Desc:			View to pull back the checkdb results from the repository
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			(unknown)
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090717	Matt Stanford	Changed because of HistServerID change
********************************************************************************************************/
CREATE VIEW [hist].[DatabaseMaintenance_CheckDB_vw]
AS

SELECT
	s.[ServerName]		AS [ServerName]
	,d.[DBName]			AS [DatabaseName]
	,CAST(1 AS BIT)		AS [EncounteredError]
	,e.[RunID]
	,e.[DateCreated]
	,e.[Error]
	,e.[Level]
	,e.[State]
	,e.[MessageText]
	,e.[RepairLevel]
	,e.[Status]
	,e.[ObjectID]
	,e.[IndexId]
	,e.[PartitionID]
	,e.[AllocUnitID]
	,e.[File]
	,e.[Page]
	,e.[Slot]
	,e.[RefFile]
	,e.[RefPage]
	,e.[RefSlot]
	,e.[Allocation]
FROM [hist].[DatabaseMaintenance_CheckDB_Errors] e
INNER JOIN [hist].[ServerInventory_ServerIDs] s
	ON s.[HistServerID] = e.[HistServerID]
INNER JOIN [hist].[ServerInventory_SQL_DatabaseIDs] d
	ON d.[DatabaseID] = e.[DatabaseID]

UNION ALL

SELECT
	s.[ServerName]		AS [ServerName]
	,d.[DBName]			AS [DatabaseName]
	,0
	,o.RunID
	,o.[DateCreated]
	,NULL
	,NULL
	,NULL
	,'No errors encountered'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
FROM [hist].[DatabaseMaintenance_CheckDB_OK] o
INNER JOIN [hist].[ServerInventory_ServerIDs] s
	ON s.[HistServerID] = o.[HistServerID]
INNER JOIN [hist].[ServerInventory_SQL_DatabaseIDs] d
	ON d.[DatabaseID] = o.[DatabaseID]




GO
/****** Object:  Table [hist].[SpaceUsed_DriveSizes_RunIDs]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[SpaceUsed_DriveSizes_RunIDs](
	[RunID] [int] NOT NULL,
	[DateCreated] [smalldatetime] NULL,
 CONSTRAINT [PK_SpaceUsed_DriveSizes_RunIDs] PRIMARY KEY CLUSTERED 
(
	[RunID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [hist].[SpaceUsed_DriveSizes_DriveMaster]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[SpaceUsed_DriveSizes_DriveMaster](
	[RunID] [int] NULL,
	[HistServerID] [int] NULL,
	[DriveLetterID] [int] NULL,
	[DriveLabelID] [int] NULL,
	[DrivePath] [varchar](100) NULL,
	[FreeSpaceMB] [bigint] NULL,
	[CapacityMB] [bigint] NULL,
	[MountPoint] [bit] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [hist].[SpaceUsed_DriveSizes_DriveLetter]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[SpaceUsed_DriveSizes_DriveLetter](
	[DriveLetterID] [int] IDENTITY(1,1) NOT NULL,
	[DriveLetter] [varchar](2) NULL,
 CONSTRAINT [PK__SpaceUsed_DriveSizes_DriveLetter__DriveLetterID] PRIMARY KEY CLUSTERED 
(
	[DriveLetterID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [hist].[SpaceUsed_DriveSizes_DriveLabel]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[SpaceUsed_DriveSizes_DriveLabel](
	[DriveLabelID] [int] IDENTITY(1,1) NOT NULL,
	[DriveLabel] [varchar](30) NULL,
 CONSTRAINT [PK__SpaceUsed_DriveSizes_DriveLabel__DriveLabelID] PRIMARY KEY CLUSTERED 
(
	[DriveLabelID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [hist].[SpaceUsed_DriveSizes_vw]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [hist].[SpaceUsed_DriveSizes_vw]
AS

SELECT 
	[ServerName]															AS [ServerName]
	,[DriveLabel]															AS [DriveLabel]
	,[DrivePath]															AS [DrivePath]
	,[FreeSpaceMB]															AS [FreeSpaceMB]
	,[CapacityMB]															AS [CapacityMB]
	,ISNULL(([FreeSpaceMB])* 100 / ((NULLIF([CapacityMB],0))),0)			AS [PercentUnused]
	,100 - (ISNULL(([FreeSpaceMB]) * 100 / ((NULLIF([CapacityMB],0))),0) )	AS [PercentUsed]
	,CASE	
		WHEN [MountPoint] = 1
			THEN 'Yes'
			ELSE 'No'
		END AS [MountPoint]
	,[SISRID].[RunID]
	,[SISRID].[DateCreated]
FROM  hist.[SpaceUsed_DriveSizes_DriveMaster] AS SISDM
JOIN hist.[ServerInventory_ServerIDs] AS SISID 
	ON [SISDM].[HistServerID] = [SISID].[HistServerID]
JOIN [hist].[SpaceUsed_DriveSizes_DriveLabel] AS SISDL 
	ON [SISDM].[DriveLabelID] = [SISDL].[DriveLabelID]
JOIN [hist].[SpaceUsed_DriveSizes_DriveLetter] AS SISDL2 
	ON SISDM.[DriveLetterID] = SISDL2.[DriveLetterID]
JOIN [hist].[SpaceUsed_DriveSizes_RunIDs] AS SISRID 
	ON SISRID.[RunID] = [SISDM].[RunID]  


GO
/****** Object:  Table [hist].[Deadlock_Deadlocks]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[Deadlock_Deadlocks](
	[HistDeadlockID] [int] IDENTITY(1,1) NOT NULL,
	[HistServerID] [int] NULL,
	[DeadlockSPID] [varchar](11) NULL,
	[VictimProcess] [varchar](20) NULL,
	[DeadlockDate] [datetime] NULL,
	[SampleDate] [datetime] NULL,
 CONSTRAINT [PK__Deadlock_Deadlocks__HistDeadlockID] PRIMARY KEY CLUSTERED 
(
	[HistDeadlockID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [hist].[Deadlock_Deadlocks_vw]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**	Name:			[hist].[Deadlock_Deadlocks_vw]
**	Desc:			View to assemble basic deadlock information
					Note that each deadlock is identified uniquely by the Server, SPID and DATE combination
**	Auth:			Matt Stanford
**	Date:			2010.12.20
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  
********************************************************************************************************/
CREATE VIEW [hist].[Deadlock_Deadlocks_vw]
AS

SELECT [HistDeadlockID]
      ,s.[ServerName]
      ,[DeadlockSPID]
      ,[VictimProcess]
      ,[DeadlockDate]
      ,[SampleDate]
FROM [hist].[Deadlock_Deadlocks] d
INNER JOIN [hist].[ServerInventory_ServerIDs] s
	ON d.[HistServerID] = s.[HistServerID]

	

GO
/****** Object:  View [hist].[SpaceUsed_drivesizes_delta_vw]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [hist].[SpaceUsed_drivesizes_delta_vw]
AS
WITH TableData AS (
	SELECT 
	RunID
	,HistServerID
	,DriveLetterID
	,DriveLabelID
	,DrivePath
	,FreeSpaceMB
	,CapacityMB
	,MountPoint
	,ROW_NUMBER() OVER (PARTITION BY [RunID] ORDER BY RunID) as rownum
	--,ROW_NUMBER() OVER (PARTITION BY [ServerDBTableID] ORDER BY SampleDate) as rownum
	FROM [hist].[SpaceUsed_drivesizes_drivemaster]
)
SELECT 
	si.[ServerName]
	,dl.[DriveLabel] AS [Drive Label]
	,currow.[DrivePath] AS [Drive Path]
	,currow.[FreeSpaceMB] AS [Free Space (MB)]
	,currow.[CapacityMB] AS [Capacity (MB)]

	,currow.[FreeSpaceMB] - prevrow.[FreeSpaceMB] as [FreeSpaceMBAdded]
	,currow.[CapacityMB]
	,currow.[CapacityMB] - prevrow.[CapacityMB] as [CapacityMBAdded]
FROM TableData currow
INNER JOIN hist.[ServerInventory_ServerIDs] AS si 
	ON currow.[HistServerID] = si.[HistServerID]
INNER JOIN [hist].[SpaceUsed_DriveSizes_DriveLabel] AS dl 
	ON [currow].[DriveLabelID] = dl.[DriveLabelID]
INNER JOIN [hist].[SpaceUsed_DriveSizes_DriveLetter] AS SISDL2 
	ON currow.[DriveLetterID] = SISDL2.[DriveLetterID]
INNER JOIN [hist].[SpaceUsed_DriveSizes_RunIDs] AS SISRID 
	ON SISRID.[RunID] = currow.[RunID]  
	

--INNER JOIN [hist].[ServerInventory_SQL_ServerDBTableIDs] m
--	ON m.[ServerDBTableID] = currow.[RunID]
--INNER JOIN [hist].[ServerInventory_ServerIDs] s
--	ON s.[HistServerID] = m.[HistServerID]
--INNER JOIN [hist].[ServerInventory_SQL_DatabaseIDs] d
--	ON d.[DatabaseID] = m.[DatabaseID]
--INNER JOIN [hist].[ServerInventory_SQL_TableIDs] t
--	ON t.[TableID] = m.[TableID]
LEFT OUTER JOIN TableData prevrow
	ON currow.rownum = prevrow.rownum + 1
	AND currow.RunID = prevrow.RunID




GO
/****** Object:  Table [hist].[ConnectionCounts_RunIDs]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[ConnectionCounts_RunIDs](
	[RunID] [int] NULL,
	[DateCreated] [smalldatetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [hist].[ConnectionCounts]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[ConnectionCounts](
	[HistServerID] [int] NULL,
	[RunID] [int] NULL,
	[CounterValue] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  View [hist].[ConnectionCounts_vw]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/******************************************************************************
* Name
	[audit].[ConnectionCounts_vw]

* Author
	Adam Bean
	
* Date
	2011.05.10
	
* Synopsis
	View to report on SQL connection counts

*******************************************************************************
* License
*******************************************************************************
	Copyright © SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20120716	Adam Bean		Switched to hist schema vs. audit
******************************************************************************/

CREATE VIEW [hist].[ConnectionCounts_vw]

AS

SELECT 
	sn.[ServerName]
	,sa.[Environment]
	,cc.[CounterValue]
	,ri.[DateCreated]
FROM [hist].[ServerInventory_ServerIDs] sn
JOIN [hist].[ConnectionCounts] cc
	ON sn.[HistServerID] = cc.[HistServerID]
JOIN [hist].[ConnectionCounts_RunIDs] ri
	ON cc.[RunID] = ri.[RunID]
JOIN [dbo].[ServerInventory_SQL_AllServers_vw] sa
	ON sn.[ServerName] = sa.[FullName]


GO
/****** Object:  Table [hist].[Backups_Types]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[Backups_Types](
	[BackupTypeID] [int] IDENTITY(1,1) NOT NULL,
	[BackupType] [char](1) NULL,
	[BackupTypeDesc] [varchar](30) NULL,
	[DateCreated] [smalldatetime] NULL,
 CONSTRAINT [PK__Backups_Types__BackupTypeID] PRIMARY KEY CLUSTERED 
(
	[BackupTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  Table [hist].[Backups_Devices]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[Backups_Devices](
	[DeviceID] [int] IDENTITY(1,1) NOT NULL,
	[DeviceName] [nvarchar](260) NULL,
	[DateCreated] [smalldatetime] NULL,
 CONSTRAINT [PK__Backups_Devices__DeviceID] PRIMARY KEY CLUSTERED 
(
	[DeviceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  Table [hist].[Users_UserNames]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[Users_UserNames](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [sysname] NOT NULL,
	[DateCreated] [smalldatetime] NULL,
 CONSTRAINT [PK__Users_UserNames__UserID] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  Table [hist].[Backups_History]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[Backups_History](
	[HistServerID] [int] NOT NULL,
	[MachineID] [int] NULL,
	[DatabaseID] [int] NOT NULL,
	[StartDate] [smalldatetime] NOT NULL,
	[EndDate] [smalldatetime] NULL,
	[SizeMB] [int] NULL,
	[BUTypeID] [int] NOT NULL,
	[LogicalDeviceID] [int] NULL,
	[PhysicalDeviceID] [int] NULL,
	[UserID] [int] NULL,
	[SizeMBCompressed] [int] NULL,
	[DeviceType] [int] NULL,
 CONSTRAINT [PK__Backups_History__HistServerID__DatabaseID__BUTypeID__StartDate] PRIMARY KEY CLUSTERED 
(
	[HistServerID] ASC,
	[DatabaseID] ASC,
	[BUTypeID] ASC,
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  View [hist].[Backups_History_vw]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************************************************************************
**  Name:			[hist].[Backups_History_vw]
**  Desc:			View to pull back the backup history from the repository
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			(unknown)
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090717	Matt Stanford	Changed because of HistServerID change
**	20120627	Adam Bean		Updates for new schema and additional data collection
********************************************************************************************************/
CREATE VIEW [hist].[Backups_History_vw]
AS

SELECT
	sn.[ServerName]								AS [ServerName]
	,mn.[ServerName]							AS [MachineName]
	,db.[DBName]
	,h.[StartDate]								AS [StartDate]
	,h.[EndDate]								AS [EndDate]
	,DATEDIFF(second,h.[StartDate],h.[EndDate]) AS [BUTime_Seconds]
	,h.[SizeMB]
	,h.[SizeMBCompressed]
	,CAST((h.[SizeMB] - h.[SizeMBCompressed])/1024/1024 AS INT)	AS [CompressedSavingsMB]
	,CASE
		WHEN CAST((NULLIF(h.[SizeMB],0) / NULLIF(h.[SizeMBCompressed],0)) AS DECIMAL(10,2)) IS NULL THEN 0
		WHEN CAST((NULLIF(h.[SizeMB],0) / NULLIF(h.[SizeMBCompressed],0)) AS DECIMAL(10,2)) = 1 THEN 0
		ELSE CAST((NULLIF(h.[SizeMB],0) / NULLIF(h.[SizeMBCompressed],0)) AS DECIMAL(10,2))
	END AS [CompressedSavingsPercent]
	,h.[BUTypeID]
	,t.[BackupType]								AS [BackupType]
	,t.[BackupTypeDesc]							AS [BackupTypeDesc]
	,u.[UserName]								AS [UserName]
	,CASE ld.[DeviceName]	
		WHEN 'NULL' THEN NULL
		ELSE ld.[DeviceName]	
	END						AS [LogicalDeviceName]
	,CASE pd.[DeviceName]		
		WHEN 'NULL' THEN NULL
		ELSE pd.[DeviceName] 
	END						AS [PhysicalDeviceName]
	,LEFT(pd.[DeviceName],LEN(pd.[DeviceName]) - CHARINDEX('\',REVERSE(pd.[DeviceName]))) as [BackupPath]
	,REPLACE(RIGHT(pd.[DeviceName],CHARINDEX('\',REVERSE(pd.[DeviceName]))),'\','') as [FileName]
	,h.[DeviceType]
FROM [hist].[Backups_History] h
INNER JOIN [hist].[ServerInventory_ServerIDs] sn
	ON h.[HistServerID] = sn.[HistServerID]
INNER JOIN [hist].[ServerInventory_ServerIDs] mn
	ON h.[HistServerID] = mn.[HistServerID]
INNER JOIN [hist].[ServerInventory_SQL_DatabaseIDs] db
	ON h.[DatabaseID] = db.[DatabaseID]
INNER JOIN [hist].[Backups_Types] t
	ON h.[BUTypeID] = t.[BackupTypeID]
INNER JOIN [hist].[Backups_Devices] ld
	ON h.[LogicalDeviceID] = ld.[DeviceID]
INNER JOIN [hist].[Backups_Devices] pd
	ON h.[PhysicalDeviceID] = pd.[DeviceID]
INNER JOIN [hist].[Users_UserNames] u
	ON h.[UserID] = u.[UserID]




GO
/****** Object:  Table [hist].[SQLRestarts_RunIDs]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[SQLRestarts_RunIDs](
	[RunID] [int] NULL,
	[DateCreated] [smalldatetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [hist].[SQLRestarts_History]    Script Date: 5/22/2017 1:13:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[SQLRestarts_History](
	[SQLRestartHistoryID] [int] IDENTITY(1,1) NOT NULL,
	[HistServerID] [int] NOT NULL,
	[TimeOfRestart] [datetime] NOT NULL,
	[Type] [bit] NULL,
	[DBAComments] [varchar](4096) NULL,
	[RunID] [int] NULL,
	[OutageInSeconds] [int] NULL,
 CONSTRAINT [PK__SQLRestarts_History__SQLServerStartup] PRIMARY KEY CLUSTERED 
(
	[SQLRestartHistoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  View [hist].[SQLRestarts_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/******************************************************************************
* Name
	[hist].[SQLRestarts_vw]

* Author
	Adam Bean
	
* Date
	2011.04.20
	
* Synopsis
	View to report on SQL restart collector data

*******************************************************************************
* License
*******************************************************************************
	Copyright © SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/

CREATE VIEW [hist].[SQLRestarts_vw]

AS

SELECT 
	rh.[SQLRestartHistoryID]
	,si.[HistServerID]
	,ri.[RunID]
	,si.[ServerName]
	,sa.[Environment]
	,rh.[TimeOfRestart]
	,rh.[OutageInSeconds]
	,CASE rh.[Type]
		WHEN 0 THEN 'Unexpected'
		ELSE 'Expected'
	END AS [Type]
	,rh.[DBAComments]
	,ri.[DateCreated]
FROM [hist].[ServerInventory_ServerIDs] si
JOIN [hist].[SQLRestarts_History] rh
	ON si.[HistServerID] = rh.[HistServerID]
JOIN [hist].[SQLRestarts_RunIDs] ri
	ON rh.[RunID] = ri.[RunID]
JOIN [dbo].[ServerInventory_SQL_AllServers_vw] sa
	ON si.[ServerName] = ISNULL(sa.[ServerName] + '\' + sa.[InstanceName], sa.[ServerName])



GO
/****** Object:  Table [hist].[ChangeControl_ScriptMaster]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[ChangeControl_ScriptMaster](
	[ScriptID] [int] IDENTITY(1,1) NOT NULL,
	[Definition] [nvarchar](max) NULL,
	[FileName] [varchar](500) NULL,
	[DateCreated] [smalldatetime] NULL,
	[UserName] [varchar](50) NULL,
 CONSTRAINT [PK__ChangeControl_ScriptMaster__ScriptID] PRIMARY KEY CLUSTERED 
(
	[ScriptID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History] TEXTIMAGE_ON [History]

GO
/****** Object:  Table [hist].[ChangeControl_DeployHistory]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[ChangeControl_DeployHistory](
	[HistServerID] [int] NULL,
	[InstanceName] [varchar](50) NULL,
	[EnvironmentName] [varchar](50) NULL,
	[DeployName] [varchar](50) NULL,
	[PackageName] [varchar](50) NULL,
	[ScriptID] [int] NULL,
	[Output] [nvarchar](max) NULL,
	[OutputType] [varchar](50) NULL,
	[IsError] [bit] NULL,
	[UserName] [varchar](50) NULL,
	[DateCreated] [smalldatetime] NULL
) ON [History] TEXTIMAGE_ON [History]

GO
/****** Object:  View [hist].[ChangeControl_DeployHistory_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[ChangeControl_DeployHistory_vw]
**  Desc:			View to assemble all history data
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			2009-09-04
**  Debug:			
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	
********************************************************************************************************/
CREATE VIEW [hist].[ChangeControl_DeployHistory_vw]
AS

SELECT
	h.[DeployName]
	,h.[PackageName]
	,s.[ServerName]
	,h.[InstanceName]
	,h.[EnvironmentName]
	,sm.[Definition]
	,sm.[FileName]
	,h.[Output]
	,h.[OutputType]
	,h.[DateCreated]
	,h.[UserName]
FROM [hist].[ChangeControl_DeployHistory] h
INNER JOIN [hist].[ServerInventory_ServerIDs] s
	ON h.[HistServerID] = s.[HistServerID]
INNER JOIN [hist].[ChangeControl_ScriptMaster] sm
	ON h.[ScriptID] = sm.[ScriptID]


GO
/****** Object:  View [hist].[SpaceUsed_DatabaseSizes_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[SpaceUsed_DatabaseSizes_vw]
**  Desc:			View to pull back the database sizes from the repository
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			(unknown)
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090706	Matt Stanford	Fully backwards-compatible change to add DataSizeUnusedMB and LogSizeUnusedMB
**	20090717	Matt Stanford	Changed because of HistServerID change
********************************************************************************************************/
CREATE VIEW [hist].[SpaceUsed_DatabaseSizes_vw]
AS
SELECT        s.ServerName, d.DBName, det.DataSizeMB, det.LogSizeMB, det.DataSizeUnusedMB, det.LogSizeUnusedMB, 
                         det.DataSizeMB - det.DataSizeUnusedMB AS DataSizeUsedMB, det.LogSizeMB - det.LogSizeUnusedMB AS LogSizeUsedMB, det.SampleDate
FROM            hist.SpaceUsed_DatabaseSizes AS det INNER JOIN
                         hist.ServerInventory_ServerIDs AS s ON s.HistServerID = det.HistServerID INNER JOIN
                         hist.ServerInventory_SQL_DatabaseIDs AS d ON d.DatabaseID = det.DatabaseID


GO
/****** Object:  Table [hist].[Collectors_StateIDs]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[Collectors_StateIDs](
	[HistCollectorLogStateID] [int] IDENTITY(1,1) NOT NULL,
	[State] [varchar](10) NULL,
 CONSTRAINT [PK__Collectors_StateIDs__HistCollectorLogStateID] PRIMARY KEY CLUSTERED 
(
	[HistCollectorLogStateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [hist].[Collectors_ScriptIDs]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[Collectors_ScriptIDs](
	[HistCollectorLogScriptID] [int] IDENTITY(1,1) NOT NULL,
	[ScriptName] [varchar](500) NULL,
	[DateCreated] [datetime] NULL,
 CONSTRAINT [PK__Collectors_ScriptIDs__HistCollectorLogScriptID] PRIMARY KEY CLUSTERED 
(
	[HistCollectorLogScriptID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [hist].[Collectors_Log]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[Collectors_Log](
	[HistLogID] [int] IDENTITY(1,1) NOT NULL,
	[HistUserID] [int] NULL,
	[HistServerID] [int] NULL,
	[HistCollectorLogScriptID] [int] NULL,
	[HistCollectorLogStateID] [int] NULL,
	[LogMessage] [varchar](2048) NULL,
	[LogVersion] [smallint] NULL,
	[DateCreated] [datetime] NULL,
 CONSTRAINT [PK__Collectors_Log__HistLogID] PRIMARY KEY CLUSTERED 
(
	[HistLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [hist].[Collectors_Log_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[Collectors_Log_vw]
**  Desc:			View to assemble the Collector Log information
**  Auth:			Matt Stanford 
**  Date:			2011-02-03
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
********************************************************************************************************/
CREATE VIEW [hist].[Collectors_Log_vw]
AS

SELECT 
	sids.[ScriptName]
	,s.[ServerName]
	,u.[UserName]
	,st.[State]
	,l.[LogMessage]
	,l.[LogVersion]
	,l.[DateCreated]
FROM [hist].[Collectors_Log] l
INNER JOIN [hist].[Collectors_ScriptIDs] sids
	ON l.[HistCollectorLogScriptID] = sids.[HistCollectorLogScriptID]
INNER JOIN [hist].[Collectors_StateIDs] st
	ON l.[HistCollectorLogStateID] = st.[HistCollectorLogStateID]
INNER JOIN [hist].[ServerInventory_ServerIDs] s
	ON l.[HistServerID] = s.[HistServerID]
INNER JOIN [hist].[Users_UserNames] u
	ON l.[HistUserID] = u.[UserID]

GO
/****** Object:  Table [ref].[ServerInventory_SQL_ConfigurationOptions]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ref].[ServerInventory_SQL_ConfigurationOptions](
	[RefConfigOptionID] [int] IDENTITY(1,1) NOT NULL,
	[configuration_id] [int] NULL,
	[name] [nvarchar](35) NULL,
	[minimum] [sql_variant] NULL,
	[maximum] [sql_variant] NULL,
	[description] [nvarchar](255) NULL,
	[is_dynamic] [bit] NULL,
	[is_advanced] [bit] NULL,
 CONSTRAINT [PK__ServerInventory_SQL_ConfigurationOptions__RefConfigOptionID] PRIMARY KEY CLUSTERED 
(
	[RefConfigOptionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [hist].[ServerInventory_SQL_ConfigurationValues]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[ServerInventory_SQL_ConfigurationValues](
	[HistConfigValueID] [int] IDENTITY(1,1) NOT NULL,
	[RefConfigOptionID] [int] NULL,
	[HistServerID] [int] NULL,
	[value] [sql_variant] NOT NULL,
	[value_in_use] [sql_variant] NOT NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[DateLastSeenOn] [smalldatetime] NOT NULL,
 CONSTRAINT [PK__SQL_ConfigValues__HistConfigValueID] PRIMARY KEY CLUSTERED 
(
	[HistConfigValueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  View [hist].[ServerInventory_SQL_ConfigurationValues_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[ServerInventory_SQL_ConfigurationValues_vw]
**  Desc:			View to show Configuration values of SQL servers
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			(unknown)
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090717	Matt Stanford	Changed because of HistServerID change
********************************************************************************************************/
CREATE VIEW [hist].[ServerInventory_SQL_ConfigurationValues_vw]
AS

SELECT 
	s.[ServerName]
	,o.[configuration_id]
	,o.[name]
	,v.[value]
	,o.[minimum]
	,o.[maximum]
	,v.[value_in_use]
	,o.[description]
	,o.[is_dynamic]
	,o.[is_advanced]
	,v.[DateCreated]
FROM [hist].[ServerInventory_SQL_ConfigurationValues] v
INNER JOIN [hist].[ServerInventory_ServerIDs] s
	ON v.[HistServerID] = s.[HistServerID]
INNER JOIN [ref].[ServerInventory_SQL_ConfigurationOptions] o
	ON v.[RefConfigOptionID] = o.[RefConfigOptionID]


GO
/****** Object:  View [hist].[ServerInventory_SQL_ConfigurationValues_Current_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[ServerInventory_SQL_ConfigurationValues_Current_vw]
**  Desc:			View display only the current (or most current) configuration values
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			2009-08-12
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
********************************************************************************************************/
CREATE VIEW [hist].[ServerInventory_SQL_ConfigurationValues_Current_vw]
AS

SELECT 
	s.[ServerName]
	,o.[configuration_id]
	,o.[name]
	,v.[value]
	,o.[minimum]
	,o.[maximum]
	,v.[value_in_use]
	,o.[description]
	,o.[is_dynamic]
	,o.[is_advanced]
	,v.[DateCreated]
	,v.[DateLastSeenOn]
FROM [hist].[ServerInventory_SQL_ConfigurationValues] v
INNER JOIN [hist].[ServerInventory_ServerIDs] s
	ON v.[HistServerID]			= s.[HistServerID]
INNER JOIN [ref].[ServerInventory_SQL_ConfigurationOptions] o
	ON v.[RefConfigOptionID]	= o.[RefConfigOptionID]
INNER JOIN (
	SELECT 
		[RefConfigOptionID]
		,[HistServerID]
		,MAX([DateCreated]) AS [DateCreated]
	FROM [hist].[ServerInventory_SQL_ConfigurationValues]
	GROUP BY [RefConfigOptionID], [HistServerID]
) a
	ON a.[RefConfigOptionID]		= v.[RefConfigOptionID]
	AND a.[HistServerID]			= v.[HistServerID]
	AND a.[DateCreated]				= v.[DateCreated]


GO
/****** Object:  Table [hist].[ServerInventory_SQL_DatabaseAttributeMaster]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[ServerInventory_SQL_DatabaseAttributeMaster](
	[DatabaseAttributeID] [int] IDENTITY(1,1) NOT NULL,
	[AttributeName] [varchar](500) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
 CONSTRAINT [PK__ServerInventory_SQL_DatabaseAttributeMaster__DatabaseAttributeID] PRIMARY KEY CLUSTERED 
(
	[DatabaseAttributeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  Table [hist].[ServerInventory_SQL_DatabaseAttributeValues]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[ServerInventory_SQL_DatabaseAttributeValues](
	[DatabaseAttributeValueID] [int] IDENTITY(1,1) NOT NULL,
	[HistServerID] [int] NULL,
	[HistDatabaseID] [int] NULL,
	[file_id] [int] NULL,
	[DatabaseAttributeID] [int] NULL,
	[AttributeValue] [sql_variant] NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateLastSeenOn] [datetime] NOT NULL,
 CONSTRAINT [PK__ServerInventory_SQL_DatabaseAttributeValues__DatabaseAttributeValueID] PRIMARY KEY CLUSTERED 
(
	[DatabaseAttributeValueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  View [hist].[ServerInventory_SQL_DatabaseAttributes_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [hist].[ServerInventory_SQL_DatabaseAttributes_vw]
AS

SELECT
	s.[ServerName]
	,d.[DBName]
	,v.[file_id]
	,a.[AttributeName]
	,v.[AttributeValue]
	,v.[DateCreated]
	,v.[DateLastSeenOn]
FROM [hist].[ServerInventory_SQL_DatabaseAttributeValues] v
INNER JOIN [hist].[ServerInventory_SQL_DatabaseAttributeMaster] a
	ON v.[DatabaseAttributeID] = a.[DatabaseAttributeID]
INNER JOIN [hist].[ServerInventory_ServerIDs] s
	ON v.[HistServerID] = s.[HistServerID]
INNER JOIN [hist].[ServerInventory_SQL_DatabaseIDs] d
	ON v.[HistDatabaseID] = d.[DatabaseID]

GO
/****** Object:  Table [dbo].[ServerInventory_SQL_ClusterNodes]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServerInventory_SQL_ClusterNodes](
	[NodeID] [int] IDENTITY(1,1) NOT NULL,
	[ServerID] [int] NULL,
	[HistServerID] [int] NULL,
	[DateCreated] [datetime] NULL,
 CONSTRAINT [PK__ServerInventory_SQL_ClusterNodes__NodeID] PRIMARY KEY CLUSTERED 
(
	[NodeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[ServerInventory_SQL_ClusterNodes_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[dbo].[ServerInventory_SQL_ClusterNodes_vw]
**  Desc:			View to show which servers are part of which clusters
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			20090706
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090717	Matt Stanford	Changed because of HistServerID change
********************************************************************************************************/
CREATE VIEW [dbo].[ServerInventory_SQL_ClusterNodes_vw]
AS

SELECT 
	m.[ServerID]					AS [ServerID]
	,m.[FullName]					AS [FullSQLInstanceName]
	,s.[ServerName]					AS [NodeName]
FROM [dbo].[ServerInventory_SQL_ClusterNodes] c
INNER JOIN [dbo].[ServerInventory_SQL_AllServers_vw] m
	ON c.[ServerID] = m.[ServerID]
INNER JOIN [hist].[ServerInventory_ServerIDs] s
	ON c.[HistServerID] = s.[HistServerID]
	

GO
/****** Object:  View [hist].[ServerInventory_SQL_SysMasterFiles_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[ServerInventory_SQL_SysMasterFiles_vw]
**  Desc:			Assembles all database file attributes into sysmasterfiles style view
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			2011.04.11
**  Debug:			
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
********************************************************************************************************/
CREATE VIEW [hist].[ServerInventory_SQL_SysMasterFiles_vw]
AS
WITH FILTER AS (
	SELECT 
		MAX(v.[DatabaseAttributeValueID]) AS [MaxAttributeValueID]
	FROM [hist].[ServerInventory_SQL_DatabaseAttributeValues] v
	WHERE [file_id] IS NOT NULL -- DB Files Only
	GROUP BY [HistServerID], [HistDatabaseID], [file_id], [DatabaseAttributeID]
), DBAttributes AS (
	SELECT
		s.[ServerName]
		,d.[DBName]
		,v.[file_id]
		,a.[AttributeName]
		,v.[AttributeValue]
	FROM [hist].[ServerInventory_SQL_DatabaseAttributeValues] v
	INNER JOIN FILTER f
		ON v.[DatabaseAttributeValueID] = f.[MaxAttributeValueID]
	INNER JOIN [hist].[ServerInventory_SQL_DatabaseAttributeMaster] a
		ON v.[DatabaseAttributeID] = a.[DatabaseAttributeID]
	INNER JOIN [hist].[ServerInventory_ServerIDs] s
		ON v.[HistServerID] = s.[HistServerID]
	INNER JOIN [hist].[ServerInventory_SQL_DatabaseIDs] d
		ON v.[HistDatabaseID] = d.[DatabaseID]
)
SELECT
	[ServerName]
	,[DBName]						AS [database_name]
	,[file_id]
	,pvt.[type_desc]
	,pvt.[data_space_id]
	,pvt.[name]
	,pvt.[physical_name]
	,pvt.[state_desc]
	,pvt.[size]
	,CAST(CONVERT(DECIMAL(12,2),pvt.[size]) * 8 / 1024. AS DECIMAL(12,2))	AS [size_MB]
	,pvt.[max_size]
	,CAST(CONVERT(DECIMAL(12,2),pvt.[max_size]) * 8 / 1024. AS DECIMAL(12,2))	AS [max_size_MB]
	,pvt.[growth]
	,CAST(CASE 
		WHEN pvt.[is_percent_growth] = 0 
			THEN CONVERT(DECIMAL(12,2),pvt.[growth]) * 8 / 1024.
		ELSE CONVERT(DECIMAL(12,2),pvt.[size]) * 8 / 1024. * CONVERT(DECIMAL(12,2),pvt.[growth]) / 100
 	END	AS DECIMAL(12,2))							AS [next_growth_MB]
	,pvt.[is_media_read_only]
	,pvt.[is_read_only]
	,pvt.[is_sparse]
	,pvt.[is_percent_growth]
	,pvt.[is_name_reserved]
FROM
(
	SELECT
		[ServerName]
		,[DBName]
		,[file_id]
		,[AttributeName]
		,[AttributeValue]
	FROM DBAttributes
) p
PIVOT (
	MAX(p.[AttributeValue])
	FOR [p].[AttributeName] IN (
		[type_desc]
		,[max_size]
		,[is_sparse]
		,[physical_name]
		,[is_read_only]
		,[is_percent_growth]
		,[state_desc]
		,[name]
		,[growth]
		,[size]
		,[is_name_reserved]
		,[data_space_id]
		,[is_media_read_only]
	)
) pvt


GO
/****** Object:  View [hist].[ServerInventory_SQL_SysDatabases_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[ServerInventory_SQL_SysDatabases_vw]
**  Desc:			Assembles all database attributes into sysdatabases style view
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			2011.04.11
**  Debug:			
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
********************************************************************************************************/
CREATE VIEW [hist].[ServerInventory_SQL_SysDatabases_vw]
AS

WITH FILTER AS (
	SELECT 
		MAX(v.[DatabaseAttributeValueID]) AS [MaxAttributeValueID]
	FROM [hist].[ServerInventory_SQL_DatabaseAttributeValues] v
	WHERE [file_id] IS NULL -- DB Options only
	GROUP BY [HistServerID], [HistDatabaseID], [DatabaseAttributeID]
), DBAttributes AS (
	SELECT
		s.[ServerName]
		,d.[DBName]
		,a.[AttributeName]
		,v.[AttributeValue]
		,v.[DateCreated]
		,v.[DateLastSeenOn]
	FROM [hist].[ServerInventory_SQL_DatabaseAttributeValues] v
	INNER JOIN FILTER f
		ON v.[DatabaseAttributeValueID] = f.[MaxAttributeValueID]
	INNER JOIN [hist].[ServerInventory_SQL_DatabaseAttributeMaster] a
		ON v.[DatabaseAttributeID] = a.[DatabaseAttributeID]
	INNER JOIN [hist].[ServerInventory_ServerIDs] s
		ON v.[HistServerID] = s.[HistServerID]
	INNER JOIN [hist].[ServerInventory_SQL_DatabaseIDs] d
		ON v.[HistDatabaseID] = d.[DatabaseID]
)
SELECT 
	[ServerName1]					AS [ServerName]
	,[DBName1]						AS [database_name]
	,pvt.[database_id]
	,pvt.[source_database_id]
	,pvt.[owner_sid]
	,pvt.[create_date]
	,pvt.[compatibility_level]
	,pvt.[collation_name]
	,pvt.[user_access_desc]
	,pvt.[is_read_only]
	,pvt.[is_auto_close_on]
	,pvt.[is_auto_shrink_on]
	,pvt.[state_desc]
	,pvt.[is_in_standby]
	,pvt.[is_cleanly_shutdown]
	,pvt.[is_supplemental_logging_enabled]
	,pvt.[snapshot_isolation_state_desc]
	,pvt.[is_read_committed_snapshot_on]
	,pvt.[recovery_model_desc]
	,pvt.[page_verify_option_desc]
	,pvt.[is_auto_create_stats_on]
	,pvt.[is_auto_update_stats_on]
	,pvt.[is_auto_update_stats_async_on]
	,pvt.[is_ansi_null_default_on]
	,pvt.[is_ansi_nulls_on]
	,pvt.[is_ansi_padding_on]
	,pvt.[is_ansi_warnings_on]
	,pvt.[is_arithabort_on]
	,pvt.[is_concat_null_yields_null_on]
	,pvt.[is_numeric_roundabort_on]
	,pvt.[is_quoted_identifier_on]
	,pvt.[is_recursive_triggers_on]
	,pvt.[is_cursor_close_on_commit_on]
	,pvt.[is_local_cursor_default]
	,pvt.[is_fulltext_enabled]
	,pvt.[is_trustworthy_on]
	,pvt.[is_db_chaining_on]
	,pvt.[is_parameterization_forced]
	,pvt.[is_master_key_encrypted_by_server]
	,pvt.[is_published]
	,pvt.[is_subscribed]
	,pvt.[is_merge_published]
	,pvt.[is_distributor]
	,pvt.[is_sync_with_backup]
	,pvt.[service_broker_guid]
	,pvt.[is_broker_enabled]
	,pvt.[log_reuse_wait_desc]
	,pvt.[is_date_correlation_on]
	,pvt.[is_cdc_enabled]
	,pvt.[is_encrypted]
	,pvt.[is_honor_broker_priority_on]
	,(SELECT MIN([DateCreated])
	FROM DBAttributes
	WHERE [ServerName] = [ServerName1]
	AND [DBName] = [DBName1]
	GROUP BY [ServerName], [DBName])  AS [DateCreated]
	,(SELECT MAX([DateLastSeenOn])
	FROM DBAttributes
	WHERE [ServerName] = [ServerName1]
	AND [DBName] = [DBName1]
	GROUP BY [ServerName], [DBName])  AS [DateLastSeenOn]
FROM (
	SELECT
		[ServerName] AS [ServerName1]
		,[DBName] AS [DBName1]
		,[AttributeName]
		,[AttributeValue]
	FROM DBAttributes
) p
PIVOT (
	MAX(p.[AttributeValue])
	FOR [p].[AttributeName] IN (
		[database_id]
		,[source_database_id]
		,[owner_sid]
		,[create_date]
		,[compatibility_level]
		,[collation_name]
		,[user_access_desc]
		,[is_read_only]
		,[is_auto_close_on]
		,[is_auto_shrink_on]
		,[state_desc]
		,[is_in_standby]
		,[is_cleanly_shutdown]
		,[is_supplemental_logging_enabled]
		,[snapshot_isolation_state_desc]
		,[is_read_committed_snapshot_on]
		,[recovery_model_desc]
		,[page_verify_option_desc]
		,[is_auto_create_stats_on]
		,[is_auto_update_stats_on]
		,[is_auto_update_stats_async_on]
		,[is_ansi_null_default_on]
		,[is_ansi_nulls_on]
		,[is_ansi_padding_on]
		,[is_ansi_warnings_on]
		,[is_arithabort_on]
		,[is_concat_null_yields_null_on]
		,[is_numeric_roundabort_on]
		,[is_quoted_identifier_on]
		,[is_recursive_triggers_on]
		,[is_cursor_close_on_commit_on]
		,[is_local_cursor_default]
		,[is_fulltext_enabled]
		,[is_trustworthy_on]
		,[is_db_chaining_on]
		,[is_parameterization_forced]
		,[is_master_key_encrypted_by_server]
		,[is_published]
		,[is_subscribed]
		,[is_merge_published]
		,[is_distributor]
		,[is_sync_with_backup]
		,[service_broker_guid]
		,[is_broker_enabled]
		,[log_reuse_wait_desc]
		,[is_date_correlation_on]
		,[is_cdc_enabled]
		,[is_encrypted]
		,[is_honor_broker_priority_on]
		)
) pvt
GO
/****** Object:  Table [dbo].[ServerInventory_ServerOwners_Xref]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServerInventory_ServerOwners_Xref](
	[ServerID] [int] NULL,
	[OwnerID] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Index [CUIX__SI_SrvOwnerXref__ServerID__OwnerID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE CLUSTERED INDEX [CUIX__SI_SrvOwnerXref__ServerID__OwnerID] ON [dbo].[ServerInventory_ServerOwners_Xref]
(
	[ServerID] ASC,
	[OwnerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ServerInventory_ServerOwners_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[dbo].[ServerInventory_ServerOwners_vw]
**  Desc:			View to pair owners up to thier servers
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			2009-07-21
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
********************************************************************************************************/
CREATE VIEW [dbo].[ServerInventory_ServerOwners_vw]
AS

SELECT
	s.[ServerID]
	,s.[ServerName] + ISNULL('\' + s.[InstanceName],'') AS [ServerName]
	,o.[OwnerName]
	,s.[Environment]
	,s.[Description]
FROM [dbo].[ServerInventory_ServerOwners_Xref] sx
INNER JOIN [dbo].[ServerInventory_Owners] o
	ON o.[OwnerID] = sx.[OwnerID]
INNER JOIN [dbo].[ServerInventory_SQL_AllServers_vw] s
	ON s.[ServerID] = sx.[ServerID]

GO
/****** Object:  Table [hist].[SpaceUsed_TableSizes]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[SpaceUsed_TableSizes](
	[ServerDBTableID] [int] NULL,
	[RowCount] [bigint] NULL,
	[ReservedSpaceKB] [bigint] NULL,
	[DataSpaceKB] [bigint] NULL,
	[IndexSizeKB] [bigint] NULL,
	[UnusedKB] [bigint] NULL,
	[SampleDate] [smalldatetime] NULL
) ON [History]

GO
/****** Object:  View [rpt].[SpaceUsed_Table_Servers_DBs_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [rpt].[SpaceUsed_Table_Servers_DBs_vw]
AS
SELECT
	s.[ServerName]
	,d.[DBName]
	,sz.[SampleDate]
FROM [hist].[SpaceUsed_TableSizes] sz
INNER JOIN [hist].[ServerInventory_SQL_ServerDBTableIDs] sdt
	ON sz.[ServerDBTableID] = sdt.[ServerDBTableID]
INNER JOIN [hist].[ServerInventory_ServerIDs] s
	ON sdt.[HistServerID] = s.[HistServerID]
INNER JOIN [hist].[ServerInventory_SQL_DatabaseIDs] d
	ON sdt.[DatabaseID] = d.[DatabaseID]
GO
/****** Object:  View [dbo].[ServerInventory_SQL_AllServers_Compatibility_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ServerInventory_SQL_AllServers_Compatibility_vw]
AS

SELECT 
	m.[ServerID]
	,m.[ServerName]
	,m.[InstanceName]
	,m.[PortNumber]
	,CASE 
		WHEN m.[InstanceName] IS NOT NULL AND m.[PortNumber] IS NOT NULL
			THEN m.[ServerName] + '\' + m.[InstanceName] + ',' + CAST(m.[PortNumber] AS VARCHAR(10))
		WHEN m.[InstanceName] IS NOT NULL
			THEN m.[ServerName] + '\' + m.[InstanceName]
		WHEN m.[PortNumber] IS NOT NULL
			THEN m.[ServerName] + ',' + CAST(m.[PortNumber] AS VARCHAR(10))
		ELSE m.[ServerName]
	END AS [FullName]
	,m.[SQLVersion]
	,ISNULL(m.[ServerName] + '\' + m.[InstanceName],m.[ServerName]) AS [ServerNameNoPort]
	,bus.[BusinessName]
	,env.[EnvironmentName]	AS Environment
	,ed.[EditionName]		AS Edition
	,m.[Description]
	,cred.[UserName]
	,cred.[Password]
	,m.[UseCredential]
	,'Data Source=' + CASE 
		WHEN m.[InstanceName] IS NOT NULL AND m.[PortNumber] IS NOT NULL
			THEN m.[ServerName] + '\' + m.[InstanceName] + ',' + CAST(m.[PortNumber] AS VARCHAR(10))
		WHEN m.[InstanceName] IS NOT NULL
			THEN m.[ServerName] + '\' + m.InstanceName
		WHEN m.[PortNumber] IS NOT NULL
			THEN m.[ServerName] + ',' + CAST(m.[PortNumber] AS VARCHAR(10))
		ELSE m.[ServerName]
	END + ';Initial Catalog=master;' + CASE
		WHEN m.[UseCredential] = 0 
			THEN 'Integrated Security=SSPI;'
		ELSE 'User Id=' + cred.[UserName] + ';Password=' + cred.[Password] + ';'
	END AS [DotNetConnectionString]
FROM [dbo].[ServerInventory_SQL_Master] m
INNER JOIN [dbo].[ServerInventory_Environments] env
	ON env.[EnvironmentID] = m.[EnvironmentID]
INNER JOIN [dbo].[ServerInventory_SQL_Editions] ed
	ON ed.[EditionID] = m.[EditionID]
LEFT JOIN [dbo].[ServerInventory_SQL_ServerCredentials] cred
	ON cred.[CredentialID] = m.[CredentialID]
LEFT JOIN [dbo].[ServerInventory_BusinessUnits] bus
	ON bus.[BusinessUnitID] = m.[BusinessUnitID]
WHERE m.[Enabled] = 1


GO
/****** Object:  View [rpt].[SpaceUsed_Table_Servers_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [rpt].[SpaceUsed_Table_Servers_vw]
AS
SELECT
	s.[ServerName]
	,sz.[SampleDate]
FROM [hist].[SpaceUsed_TableSizes] sz
INNER JOIN [hist].[ServerInventory_SQL_ServerDBTableIDs] sdt
	ON sz.[ServerDBTableID] = sdt.[ServerDBTableID]
INNER JOIN [hist].[ServerInventory_ServerIDs] s
	ON sdt.[HistServerID] = s.[HistServerID]
GO
/****** Object:  View [hist].[SpaceUsed_TableSizes_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[SpaceUsed_TableSizes_vw]
**  Desc:			View to show all table sizes
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			(unknown)
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090717	Matt Stanford	Changed because of HistServerID change
********************************************************************************************************/
CREATE VIEW [hist].[SpaceUsed_TableSizes_vw]
AS

SELECT 
	s.[ServerName]
	,d.[DBName]
	,t.[SchemaName]
	,t.[TableName]
	,det.[RowCount]
	,det.[ReservedSpaceKB]
	,det.[DataSpaceKB]
	,det.[IndexSizeKB]
	,det.[UnusedKB]
	,CASE WHEN det.[RowCount] = 0 THEN 0
	ELSE CAST(CAST(det.[ReservedSpaceKB] AS DECIMAL) / det.[RowCount] AS DECIMAL(10,3))
	END as [KB/Row]
	,det.[SampleDate]
FROM [hist].[SpaceUsed_TableSizes] det
INNER JOIN [hist].[ServerInventory_SQL_ServerDBTableIDs] m
	ON m.[ServerDBTableID] = det.[ServerDBTableID]
INNER JOIN [hist].[ServerInventory_ServerIDs] s
	ON s.[HistServerID] = m.[HistServerID]
INNER JOIN [hist].[ServerInventory_SQL_DatabaseIDs] d
	ON d.[DatabaseID] = m.[DatabaseID]
INNER JOIN [hist].[ServerInventory_SQL_TableIDs] t
	ON t.[TableID] = m.[TableID]


GO
/****** Object:  View [hist].[SpaceUsed_TableSizes_Delta_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[SpaceUsed_TableSizes_Delta_vw]
**  Desc:			View to show all table size differences day over day
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			(unknown)
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090717	Matt Stanford	Changed because of HistServerID change
********************************************************************************************************/
CREATE VIEW [hist].[SpaceUsed_TableSizes_Delta_vw]
AS
WITH TableData AS (
	SELECT 
		[ServerDBTableID]
		,[RowCount]
		,[ReservedSpaceKB]
		,[DataSpaceKB]
		,[IndexSizeKB]
		,[UnusedKB]
		,[SampleDate]
		,ROW_NUMBER() OVER (PARTITION BY [ServerDBTableID] ORDER BY SampleDate) as rownum
	FROM [hist].[SpaceUsed_TableSizes]
)
SELECT 
	s.[ServerName]
	,d.[DBName]
	,t.[SchemaName]
	,t.[TableName]
	,currow.[RowCount]
	,currow.[RowCount] - prevrow.[RowCount] as [RowsAdded]
	,currow.[ReservedSpaceKB]
	,currow.[ReservedSpaceKB] - prevrow.[ReservedSpaceKB] as [ReservedSpaceKBAdded]
	,currow.[DataSpaceKB]
	,currow.[DataSpaceKB] - prevrow.[DataSpaceKB] as [DataSpaceKBAdded]
	,currow.[IndexSizeKB]
	,currow.[IndexSizeKB] - prevrow.[IndexSizeKB] as [IndexSizeKBAdded]
	,currow.[UnusedKB]
	,currow.[UnusedKB] - prevrow.[UnusedKB] as [UnusedKBAdded]
	,currow.[SampleDate] 
FROM TableData currow
INNER JOIN [hist].[ServerInventory_SQL_ServerDBTableIDs] m
	ON m.[ServerDBTableID] = currow.[ServerDBTableID]
INNER JOIN [hist].[ServerInventory_ServerIDs] s
	ON s.[HistServerID] = m.[HistServerID]
INNER JOIN [hist].[ServerInventory_SQL_DatabaseIDs] d
	ON d.[DatabaseID] = m.[DatabaseID]
INNER JOIN [hist].[ServerInventory_SQL_TableIDs] t
	ON t.[TableID] = m.[TableID]
LEFT OUTER JOIN TableData prevrow
	ON currow.rownum = prevrow.rownum + 1
	AND currow.ServerDBTableID = prevrow.ServerDBTableID


GO
/****** Object:  Table [hist].[General_FullFileName]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[General_FullFileName](
	[HistPathFileNameID] [int] IDENTITY(1,1) NOT NULL,
	[HistPathID] [int] NULL,
	[HistFileNameID] [int] NULL,
	[DateCreated] [date] NULL,
 CONSTRAINT [PK__General_FullFileName__HistPathFileNameID] PRIMARY KEY CLUSTERED 
(
	[HistPathFileNameID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  Table [hist].[General_Paths]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[General_Paths](
	[HistPathID] [int] IDENTITY(1,1) NOT NULL,
	[Path] [varchar](900) NOT NULL,
	[DateCreated] [date] NULL,
 CONSTRAINT [PK__General_Paths__HistPathID] PRIMARY KEY CLUSTERED 
(
	[HistPathID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  Table [hist].[General_FileNames]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[General_FileNames](
	[HistFileNameID] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [varchar](900) NOT NULL,
	[DateCreated] [date] NULL,
 CONSTRAINT [PK__General_FileNames__HistFileNameID] PRIMARY KEY CLUSTERED 
(
	[HistFileNameID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  Table [hist].[SpaceUsed_FileSizes]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[SpaceUsed_FileSizes](
	[HistServerID] [int] NOT NULL,
	[HistPathFileNameID] [int] NOT NULL,
	[FileAttribute] [varchar](5) NULL,
	[FileSizeKB] [bigint] NULL,
	[SampleDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK__SpaceUsed_FileSizes__HistServerID__SampleDate__HistPathFileNameID] PRIMARY KEY CLUSTERED 
(
	[HistServerID] ASC,
	[SampleDate] ASC,
	[HistPathFileNameID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  View [hist].[SpaceUsed_FileSizes_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[SpaceUsed_FileSizes_vw]
**  Desc:			View to pull file size information from the repository
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			(unknown)
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090717	Matt Stanford	Changed because of HistServerID change
********************************************************************************************************/
CREATE VIEW [hist].[SpaceUsed_FileSizes_vw]
AS

SELECT
	s.[ServerName]
	,p.[Path]
	,fn.[FileName]
	,CASE WHEN SUBSTRING(fs.[FileAttribute],1,1) = 'd'
		THEN 1
		ELSE 0
	END AS [IsDirectory]
	,CASE WHEN SUBSTRING(fs.[FileAttribute],2,1) = 'a'
		THEN 1
		ELSE 0
	END AS [IsArchiveSet]
	,CASE WHEN SUBSTRING(fs.[FileAttribute],3,1) = '?'
		THEN 1
		ELSE 0
	END AS [IsSomething]
	,CASE WHEN SUBSTRING(fs.[FileAttribute],4,1) = 'h'
		THEN 1
		ELSE 0
	END AS [IsHidden]
	,fs.[FileAttribute]
	,fs.[FileSizeKB]
	,fs.[SampleDate]
FROM [hist].[SpaceUsed_FileSizes] fs
INNER JOIN [hist].[ServerInventory_ServerIDs] s
	ON fs.[HistServerID] = s.[HistServerID]
INNER JOIN [hist].[General_FullFileName] ffn
	ON ffn.[HistPathFileNameID] = fs.[HistPathFileNameID]
INNER JOIN [hist].[General_Paths] p
	ON ffn.[HistPathID] = p.[HistPathID]
INNER JOIN [hist].[General_FileNames] fn
	ON ffn.[HistFileNameID] = fn.[HistFileNameID]


GO
/****** Object:  Table [hist].[ChangeTracking_SQL_ServerDBObjectActionIDs]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[ChangeTracking_SQL_ServerDBObjectActionIDs](
	[HistServerID] [int] NOT NULL,
	[DatabaseID] [int] NOT NULL,
	[ObjectID] [int] NOT NULL,
	[ActionID] [int] NOT NULL,
	[DateModified] [date] NOT NULL,
	[DateCreated] [smalldatetime] NULL,
 CONSTRAINT [PK__ChangeTracking_SQL_ServerDBObjectActionIDs__HistServerID__DatabaseID__ObjectID__ActionID__DateModified] PRIMARY KEY CLUSTERED 
(
	[HistServerID] ASC,
	[DatabaseID] ASC,
	[ObjectID] ASC,
	[ActionID] ASC,
	[DateModified] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 60) ON [History]
) ON [History]

GO
/****** Object:  Table [hist].[ChangeTracking_SQL_ActionIDs]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[ChangeTracking_SQL_ActionIDs](
	[ActionID] [int] IDENTITY(1,1) NOT NULL,
	[ActionType] [varchar](255) NULL,
 CONSTRAINT [PK__ChangeTracking_SQL_ActionIDs__ActionID] PRIMARY KEY CLUSTERED 
(
	[ActionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  Table [hist].[ChangeTracking_SQL_ObjectTypeIDs]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[ChangeTracking_SQL_ObjectTypeIDs](
	[ObjectTypeId] [int] IDENTITY(1,1) NOT NULL,
	[SQLType] [nvarchar](128) NULL,
	[RefType] [nvarchar](128) NULL,
	[SqlDesc] [varchar](50) NULL,
 CONSTRAINT [PK__ChangeTracking_SQL_ObjectTypeIDs__ObjectTypeId] PRIMARY KEY CLUSTERED 
(
	[ObjectTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  Table [hist].[ChangeTracking_SQL_ObjectIDs]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[ChangeTracking_SQL_ObjectIDs](
	[ObjectID] [int] IDENTITY(1,1) NOT NULL,
	[SchemaName] [sysname] NULL,
	[ObjectName] [sysname] NOT NULL,
	[ObjectTypeID] [int] NULL,
 CONSTRAINT [PK__ChangeTracking_SQL_ObjectIDs__ObjectID] PRIMARY KEY NONCLUSTERED 
(
	[ObjectID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  View [hist].[ChangeTracking_AllDatabaseChanges_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[ChangeTracking_AllDatabaseChanges_vw]
**  Desc:			View to show all database changes
**  Auth:			Kathy Toth (SQLSlayer)
**  Date:			(unknown)
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090717	Matt Stanford	Changed because of HistServerID change
********************************************************************************************************/
CREATE VIEW [hist].[ChangeTracking_AllDatabaseChanges_vw]
AS
SELECT 
	m.[ServerName]
	,d.[DBName]
	,ot.[SchemaName]
	,ot.[ObjectName]
	,ob.[SQLType] AS [SQL_Object_Type]
	,ob.[RefType] AS [Reference_Object_Type]
	,ob.[SQLDesc] AS [Description]
	,ac.[ActionType] AS [Action]
	,id.[DateModified]
		
FROM [hist].[ChangeTracking_SQL_ServerDBObjectActionIDs] id
INNER JOIN [hist].[ChangeTracking_SQL_ObjectIDs] ot
	ON ot.[ObjectID] = id.[ObjectID]
INNER JOIN [hist].[ServerInventory_ServerIDs] m
	ON m.[HistServerID] = id.[HistServerID]
INNER JOIN [hist].[ChangeTracking_SQL_ObjectTypeIDs] ob
	ON ob.[ObjectTypeID] = ot.[ObjectTypeID]
INNER JOIN [hist].[ChangeTracking_SQL_ActionIDs] ac
	ON ac.[ActionID] = id.[ActionID]
INNER JOIN [hist].[ServerInventory_SQL_DatabaseIDs]d
	ON d.[DatabaseID] = id.[DatabaseID]


GO
/****** Object:  View [rpt].[Backup_Summary_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**	Name:			[rpt].[Backup_Summary_vw]
**	Desc:			View to retrieve backup history SSRS report
**	Auth:			Adam Bean (SQLSlayer.com)
**	Date:			2009.04.02
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  20090716	Matt Stanford	Changed the join criteria to be based on name, not ID
********************************************************************************************************/
CREATE VIEW [rpt].[Backup_Summary_vw]

AS

SELECT 
	ISNULL(s.[Environment],'Server Was Removed') AS [Environment]
	,b.[ServerName]
	,b.[DatabaseName]			AS [DBName]
	,b.[StartDate]
	,b.[EndDate]
	,b.[BUTime_Seconds]
	,b.[BUTime_Seconds] / 60	AS [BUTime_Minutes]
	,b.[Size_MBytes]			AS [Size_MB]
	,b.[Size_MBytes] / 1024		AS [Size_GB]
	,b.[BackupType]
	,b.[UserName]
	,b.[PhysicalDeviceName]
	,b.[BackupPath]
	,b.[FileName]
FROM [hist].[Backups_History_vw] b
LEFT OUTER JOIN [dbo].[ServerInventory_SQL_AllServers_vw] s
	ON CASE 
		WHEN s.[InstanceName] IS NOT NULL THEN s.[ServerName] + '\' + s.[InstanceName]
		ELSE s.[ServerName]
	END = b.[ServerName]


GO
/****** Object:  Table [hist].[Deadlock_ProcessList]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[Deadlock_ProcessList](
	[HistDeadlockProcessID] [int] IDENTITY(1,1) NOT NULL,
	[HistDeadlockID] [int] NULL,
	[processid] [varchar](20) NULL,
	[clientapp] [varchar](128) NULL,
	[currentdb] [int] NULL,
	[hostnameHistServerID] [int] NULL,
	[hostpid] [smallint] NULL,
	[RefIsolationLevelID] [int] NULL,
	[kpid] [smallint] NULL,
	[lastbatchstarted] [datetime] NULL,
	[lastbatchcompleted] [datetime] NULL,
	[lasttranstarted] [datetime] NULL,
	[modeRefLockModeID] [int] NULL,
	[loginnameHistUserID] [int] NULL,
	[priority] [smallint] NULL,
	[taskpriority] [smallint] NULL,
	[sbid] [smallint] NULL,
	[schedulerid] [tinyint] NULL,
	[spid] [smallint] NULL,
	[RefRunStatusID] [int] NULL,
	[transactionname] [varchar](128) NULL,
	[transcount] [smallint] NULL,
	[waitresource] [varchar](128) NULL,
	[waittime] [int] NULL,
	[DateCreated] [datetime] NULL,
 CONSTRAINT [PK__Deadlock_ProcessList__HistDeadlockProcessID] PRIMARY KEY CLUSTERED 
(
	[HistDeadlockProcessID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [ref].[SQLServer_RunStatus]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ref].[SQLServer_RunStatus](
	[RefRunStatusID] [int] IDENTITY(1,1) NOT NULL,
	[RunStatus] [varchar](50) NULL,
	[DateCreated] [datetime] NULL,
 CONSTRAINT [PK__SQLServer_RunStatus__RefRunStatusID] PRIMARY KEY CLUSTERED 
(
	[RefRunStatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [ref].[SQLServer_LockModes]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ref].[SQLServer_LockModes](
	[RefLockModeID] [int] IDENTITY(1,1) NOT NULL,
	[LockMode] [varchar](10) NULL,
	[LockName] [varchar](50) NULL,
	[Description] [varchar](1000) NULL,
	[DateCreated] [datetime] NULL,
 CONSTRAINT [PK__SQLServer_LockModes__RefLockModeID] PRIMARY KEY CLUSTERED 
(
	[RefLockModeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [ref].[SQLServer_IsolationLevels]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ref].[SQLServer_IsolationLevels](
	[RefIsolationLevelID] [int] IDENTITY(1,1) NOT NULL,
	[IsolationLevel] [varchar](50) NULL,
	[DateCreated] [datetime] NULL,
 CONSTRAINT [PK__SQLServer_IsolationLevels__RefIsolationLevelID] PRIMARY KEY CLUSTERED 
(
	[RefIsolationLevelID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [hist].[Deadlock_ProcessList_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**	Name:			[hist].[Deadlock_ProcessList_vw]
**	Desc:			View to assemble the process list information for each deadlock
**	Auth:			Matt Stanford
**	Date:			2010.12.20
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  
********************************************************************************************************/
CREATE VIEW [hist].[Deadlock_ProcessList_vw]
AS
SELECT 
	dl.[HistDeadlockID]				AS [HistDeadlockID]
	,p.[HistDeadlockProcessID]		AS [HistDeadlockProcessID]
	,dl_sn.[ServerName]				AS [Deadlock_ServerName]
	,dl.[DeadlockSPID]				AS [Deadlock_SPID]
	,dl.[DeadlockDate]				AS [DeadlockDate]
	,CASE WHEN dl.[VictimProcess] = p.[processid] THEN 1 
		ELSE 0
	END								AS [Was_Deadlock_Victim]
	,p.[processid]					AS [processid]
	,p.[clientapp]					AS [clientapp]
	,p.[currentdb]					AS [DBID]
	,p_sn.[ServerName]				AS [Process_ServerName]
	,p.[hostpid]					AS [hostpid]
	,iso.[IsolationLevel]			AS [IsolationLevel]
	,p.[kpid]						AS [kpid]
	,p.[lastbatchstarted]
	,p.[lastbatchcompleted]
	,p.[lasttranstarted]
	,lm.[LockMode]
	,lm.[LockName]					AS [LockName]
	,lm.[Description]				AS [LockDesc]
	,u.[UserName]					AS [loginname]
	,p.[priority]
	,p.[taskpriority]
	,p.[sbid]
	,p.[schedulerid]
	,p.[spid]
	,stat.[RunStatus]				AS [status]
	,p.[transactionname]
	,p.[transcount]
	,p.[waitresource]
	,p.[waittime]
	,p.[DateCreated]
FROM [hist].[Deadlock_Deadlocks] dl
INNER JOIN [hist].[Deadlock_ProcessList] p
	ON dl.[HistDeadlockID] = p.[HistDeadlockID]
INNER JOIN [hist].[ServerInventory_ServerIDs] dl_sn
	ON dl.[HistServerID] = dl_sn.[HistServerID]
INNER JOIN [hist].[ServerInventory_ServerIDs] p_sn
	ON p.[hostnameHistServerID] = p_sn.[HistServerID]
LEFT OUTER JOIN [ref].[SQLServer_IsolationLevels] iso
	ON p.[RefIsolationLevelID] = iso.[RefIsolationLevelID]
LEFT OUTER JOIN [hist].[Users_UserNames] u
	ON p.[loginnameHistUserID] = u.[UserID]
LEFT OUTER JOIN [ref].[SQLServer_RunStatus] stat
	ON p.[RefRunStatusID] = stat.[RefRunStatusID]
LEFT OUTER JOIN [ref].[SQLServer_LockModes] lm
	ON p.[modeRefLockModeID] = lm.[RefLockModeID]
	

GO
/****** Object:  Table [hist].[Deadlock_ResourceList]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[Deadlock_ResourceList](
	[HistDeadlockResourceID] [int] IDENTITY(1,1) NOT NULL,
	[HistDeadlockID] [int] NULL,
	[id] [varchar](20) NULL,
	[locktype] [varchar](10) NULL,
	[objectnameHistTableID] [int] NULL,
	[dbnameHistDatabaseID] [int] NULL,
	[indexname] [nvarchar](128) NULL,
	[hobtid] [varchar](50) NULL,
	[modeRefLockModeID] [int] NULL,
	[associatedObjectID] [varchar](50) NULL,
	[dbid] [smallint] NULL,
	[fileid] [smallint] NULL,
	[pageid] [int] NULL,
	[DateCreated] [datetime] NULL,
 CONSTRAINT [PK__Deadlock_ResourceList__HistDeadlockResourceID] PRIMARY KEY CLUSTERED 
(
	[HistDeadlockResourceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [hist].[Deadlock_ResourceList_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**	Name:			[hist].[Deadlock_ResourceList_vw]
**	Desc:			View to assemble the resource list information for each deadlock
**	Auth:			Matt Stanford
**	Date:			2010.12.20
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  
********************************************************************************************************/
CREATE VIEW [hist].[Deadlock_ResourceList_vw]
AS
SELECT 
	rl.[HistDeadlockResourceID]
	,rl.[HistDeadlockID]
	,rl.[id]
	,rl.[locktype]
	,db.[DBName]
	,tbl.[SchemaName]
	,tbl.[TableName]
	,rl.[indexname]
	,rl.[hobtid]
	,lm.[LockMode]
	,lm.[LockName]					AS [LockName]
	,lm.[Description]				AS [LockDesc]
	,rl.[associatedObjectID]
	,rl.[dbid]
	,rl.[fileid]
	,rl.[pageid]
	,rl.[DateCreated]
FROM [hist].[Deadlock_ResourceList] rl
LEFT OUTER JOIN [hist].[ServerInventory_SQL_DatabaseIDs] db
	ON rl.[dbnameHistDatabaseID] = db.[DatabaseID]
LEFT OUTER JOIN [hist].[ServerInventory_SQL_TableIDs] tbl
	ON rl.[objectnameHistTableID] = tbl.[TableID]
INNER JOIN [ref].[SQLServer_LockModes] lm
	ON rl.[modeRefLockModeID] = lm.[RefLockModeID]

GO
/****** Object:  Table [dbo].[NTPermissions_EnvironmentExceptions]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NTPermissions_EnvironmentExceptions](
	[EnvironmentID] [tinyint] NULL,
	[StatementID] [int] NULL,
	[RunInAdditionToDefault] [bit] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NTPermissions_PermissionSQLStatements]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NTPermissions_PermissionSQLStatements](
	[StatementID] [int] IDENTITY(1,1) NOT NULL,
	[SQLToExecute] [nvarchar](max) NULL,
	[Description] [varchar](100) NULL,
	[CompatVersionStart] [int] NULL,
	[CompatVersionEnd] [int] NULL,
 CONSTRAINT [PK__NTPermissions_PermissionSQLStatements__StatementID] PRIMARY KEY CLUSTERED 
(
	[StatementID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NTPermissions_ServerExceptions]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NTPermissions_ServerExceptions](
	[ServerID] [int] NULL,
	[StatementID] [int] NULL,
	[RunInAdditionToDefault] [bit] NULL
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[NTPermissions_SQLStatements_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[NTPermissions_SQLStatements_vw]
AS
-- Select all servers that don't have a "instead of" flag set
-- Select all environment specific things
-- Select all server specific things

-- Default action for all servers that don't have the "instead of" flag set
SELECT
	srv.[ServerID]
	,srv.[FullName]
	,srv.[DotNetConnectionString]
	,N'EXEC [admin].[dbo].[NTPermissions_AllDBs]' AS [SQLToExecute]
	,'Default' AS [Description]
	,1 AS [Sequence]
FROM
	[dbo].[ServerInventory_SQL_AllServers_vw] srv
WHERE srv.ServerID NOT IN (
	SELECT
		ISNULL(s.[ServerID],-1)
	FROM [dbo].[ServerInventory_SQL_Master] s
	FULL OUTER JOIN [dbo].[NTPermissions_EnvironmentExceptions] e_ex
		ON s.[EnvironmentID] = e_ex.[EnvironmentID]
	FULL OUTER JOIN [dbo].[NTPermissions_ServerExceptions] s_ex
		ON s.[ServerID] = s_ex.[ServerID]
	WHERE e_ex.[RunInAdditionToDefault] = 0
	OR s_ex.[RunInAdditionToDefault] = 0
)
	
UNION ALL
	
-- Run the environment specific statements
SELECT 
	s.[ServerID]
	,srv.[FullName]
	,srv.[DotNetConnectionString]
	,sql_sta.[SQLToExecute] AS [SQLToExecute]
	,sql_sta.[Description]
	,2 AS [Sequence]
FROM [dbo].[ServerInventory_SQL_Master] s
INNER JOIN [dbo].[ServerInventory_SQL_AllServers_vw] srv
	ON s.[ServerID] = srv.[ServerID]
INNER JOIN [dbo].[NTPermissions_EnvironmentExceptions] e_ex
	ON s.[EnvironmentID] = e_ex.[EnvironmentID]
INNER JOIN [dbo].[NTPermissions_PermissionSQLStatements] sql_sta
	ON e_ex.[StatementID] = sql_sta.[StatementID]
	AND ISNULL(sql_sta.[CompatVersionStart],2000) <= srv.[SQLVersion]
	AND ISNULL(sql_sta.[CompatVersionEnd],5000) >= srv.[SQLVersion]

UNION ALL
	
-- Lastly, run the server specific statements
SELECT 
	srv.[ServerID]
	,srv.[FullName]
	,srv.[DotNetConnectionString]
	,sql_sta.[SQLToExecute] AS [SQLToExecute]
	,sql_sta.[Description]
	,3 AS [Sequence]
FROM [dbo].[ServerInventory_SQL_AllServers_vw] srv
INNER JOIN [dbo].[NTPermissions_ServerExceptions] s_ex
	ON srv.[ServerID] = s_ex.[ServerID]
INNER JOIN [dbo].[NTPermissions_PermissionSQLStatements] sql_sta
	ON s_ex.[StatementID] = sql_sta.[StatementID]
	AND ISNULL(sql_sta.[CompatVersionStart],2000) <= srv.[SQLVersion]
	AND ISNULL(sql_sta.[CompatVersionEnd],5000) >= srv.[SQLVersion]


GO
/****** Object:  View [dbo].[NTPermissions_ShowMappings_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[NTPermissions_ShowMappings_vw]
AS

SELECT
	sql_sta.[Description]
	,env.[EnvironmentName] as [Name]
	,'Environment' as [EnvOrSrv]
	,sql_sta.[SQLToExecute]
FROM [dbo].[NTPermissions_EnvironmentExceptions] e_ex
INNER JOIN [dbo].[NTPermissions_PermissionSQLStatements] sql_sta
	ON e_ex.[StatementID] = sql_sta.[StatementID]
INNER JOIN [dbo].[ServerInventory_Environments] env
	ON e_ex.[EnvironmentID] = env.[EnvironmentID]
	
UNION ALL

SELECT
	sql_sta.[Description]
	,srv.[FullName] as [ItemName]
	,'Server' as [EnvOrSrv]
	,sql_sta.[SQLToExecute]
FROM [dbo].[NTPermissions_ServerExceptions] s_ex
INNER JOIN [dbo].[NTPermissions_PermissionSQLStatements] sql_sta
	ON s_ex.[StatementID] = sql_sta.[StatementID]
INNER JOIN [dbo].[ServerInventory_SQL_AllServers_vw] srv
	ON s_ex.[ServerID] = srv.[ServerID]

GO
/****** Object:  Table [hist].[Jobs_SQL_JobSteps]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[Jobs_SQL_JobSteps](
	[HistJobID] [int] NOT NULL,
	[step_id] [int] NOT NULL,
	[step_name] [sysname] NOT NULL,
	[subsystem] [nvarchar](40) NOT NULL,
	[command] [nvarchar](max) NULL,
	[flags] [int] NOT NULL,
	[additional_parameters] [ntext] NULL,
	[cmdexec_success_code] [int] NOT NULL,
	[on_success_action] [tinyint] NOT NULL,
	[on_success_step_id] [int] NOT NULL,
	[on_fail_action] [tinyint] NOT NULL,
	[on_fail_step_id] [int] NOT NULL,
	[HistServerIDForServerCol] [int] NULL,
	[HistDatabaseID] [int] NULL,
	[database_user_name] [sysname] NULL,
	[retry_attempts] [int] NOT NULL,
	[retry_interval] [int] NOT NULL,
	[os_run_priority] [int] NOT NULL,
	[output_file_name] [nvarchar](200) NULL,
	[DateCreated] [smalldatetime] NULL,
 CONSTRAINT [PK__Jobs_SQL_JobSteps__HistJobID__step_id] PRIMARY KEY CLUSTERED 
(
	[HistJobID] ASC,
	[step_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History] TEXTIMAGE_ON [History]

GO
/****** Object:  View [hist].[Jobs_SQL_JobSteps_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[Jobs_SQL_JobSteps_vw]
**  Desc:			View to pull back the job step definitions from the repository
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			(unknown)
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090717	Matt Stanford	Changed because of HistServerID change
********************************************************************************************************/
CREATE VIEW [hist].[Jobs_SQL_JobSteps_vw]
AS

WITH [CurrentJobs] (HistJobID)
AS
(
	SELECT
		MAX([HistJobID])
	FROM [hist].[Jobs_SQL_Jobs]
	GROUP BY [HistServerID], [job_id]
)
SELECT
	job_s.[ServerName]
	,j.[name]				AS [JobName]
	,j.[job_id]
	,js.[HistJobID]
	,js.[step_id]
	,js.[step_name]
	,js.[subsystem]
	,js.[command]
	,js.[flags]
	,js.[additional_parameters]
	,js.[cmdexec_success_code]
	,js.[on_success_action]
	,js.[on_success_step_id]
	,js.[on_fail_action]
	,js.[on_fail_step_id]
	,s.[ServerName]			AS [server]
	,d.[DBName]				AS [database_name]
	,js.[database_user_name]
	,js.[retry_attempts]
	,js.[retry_interval]
	,js.[os_run_priority]
	,js.[output_file_name]
	,js.[DateCreated]
FROM [hist].[Jobs_SQL_JobSteps] js
INNER JOIN [hist].[Jobs_SQL_Jobs] j
	ON js.[HistJobID] = j.[HistJobID]
INNER JOIN [hist].[ServerInventory_ServerIDs] job_s
	ON job_s.[HistServerID] = j.[HistServerID]
INNER JOIN [hist].[ServerInventory_ServerIDs] s
	ON s.[HistServerID] = js.[HistServerIDForServerCol]
INNER JOIN [hist].[ServerInventory_SQL_DatabaseIDs] d
	ON js.[HistDatabaseID] = d.[DatabaseID]
INNER JOIN [CurrentJobs] cj
	ON js.[HistJobID] = cj.[HistJobID]


GO
/****** Object:  Table [rpt].[Reporting_Environments]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [rpt].[Reporting_Environments](
	[Environment] [varchar](100) NULL,
	[EnvironmentCategory] [varchar](100) NULL
) ON [PRIMARY]

GO
/****** Object:  View [rpt].[Reporting_Environments_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [rpt].[Reporting_Environments_vw]
AS

SELECT     Environment
           ,EnvironmentCategory
FROM         rpt.Reporting_Environments

GO
/****** Object:  Table [hist].[Jobs_SQL_JobHistory]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[Jobs_SQL_JobHistory](
	[HistJobID] [int] NOT NULL,
	[instance_id] [int] NOT NULL,
	[step_id] [int] NOT NULL,
	[step_name] [sysname] NOT NULL,
	[sql_message_id] [int] NOT NULL,
	[sql_severity] [int] NOT NULL,
	[message] [nvarchar](1024) NULL,
	[run_status] [int] NOT NULL,
	[run_datetime] [datetime] NOT NULL,
	[run_duration] [int] NOT NULL,
	[operator_id_emailed] [int] NOT NULL,
	[operator_id_netsent] [int] NOT NULL,
	[operator_id_paged] [int] NOT NULL,
	[retries_attempted] [int] NOT NULL,
	[HistServerID] [int] NULL,
	[DateCreated] [smalldatetime] NULL,
 CONSTRAINT [PK__Jobs_SQL_JobHistory__HistJobID__instance_id] PRIMARY KEY CLUSTERED 
(
	[HistJobID] ASC,
	[instance_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  View [hist].[Jobs_SQL_JobHistory_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[Jobs_SQL_JobHistory_vw]
**  Desc:			View to pull back the job history results from the repository
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			(unknown)
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090717	Matt Stanford	Changed because of HistServerID change
********************************************************************************************************/
CREATE VIEW [hist].[Jobs_SQL_JobHistory_vw]
AS

SELECT
	s.[ServerName]
	,j.[name]					AS [JobName]
	,jh.[instance_id]
	,jh.[step_id]
	,jh.[step_name]
	,jh.[sql_message_id]
	,jh.[sql_severity]
	,jh.[message]
	,jh.[run_status]
	,jh.[run_datetime]
	,jh.[run_duration]
	,jh.[operator_id_emailed]
	,jh.[operator_id_netsent]
	,jh.[operator_id_paged]
	,jh.[retries_attempted]
	,s2.[ServerName]			AS [server]
FROM [hist].[Jobs_SQL_JobHistory] jh
INNER JOIN [hist].[Jobs_SQL_Jobs] j
	ON j.[HistJobID] = jh.[HistJobID]
INNER JOIN [hist].[ServerInventory_ServerIDs] s
	ON j.[HistServerID] = s.[HistServerID]
INNER JOIN [hist].[ServerInventory_ServerIDs] s2
	ON jh.[HistServerID] = s2.[HistServerID]
	


GO
/****** Object:  Table [dbo].[ServerInventory_DatabaseOwners_Xref]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServerInventory_DatabaseOwners_Xref](
	[ServerID] [int] NULL,
	[DatabaseID] [int] NULL,
	[OwnerID] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Index [CUIX__DBOwnerXref__ServerID__DatabaseID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE CLUSTERED INDEX [CUIX__DBOwnerXref__ServerID__DatabaseID] ON [dbo].[ServerInventory_DatabaseOwners_Xref]
(
	[ServerID] ASC,
	[DatabaseID] ASC,
	[OwnerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ServerInventory_DatabaseOwners_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[dbo].[ServerInventory_DatabaseOwners_vw]
**  Desc:			View to pair databases up to thier owners
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			2009-07-21
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
********************************************************************************************************/
CREATE VIEW [dbo].[ServerInventory_DatabaseOwners_vw]
AS

SELECT
	s.[ServerName] + ISNULL('\' + s.[InstanceName],'') AS [ServerName]
	,d.[DBName]
	,o.[OwnerName]
FROM [dbo].[ServerInventory_DatabaseOwners_Xref] dox
INNER JOIN [dbo].[ServerInventory_Owners] o
	ON dox.[OwnerID] = o.[OwnerID]
INNER JOIN [dbo].[ServerInventory_SQL_AllServers_vw] s
	ON dox.[ServerID] = s.[ServerID]
INNER JOIN [hist].[ServerInventory_SQL_DatabaseIDs] d
	ON dox.[DatabaseID] = d.[DatabaseID]


GO
/****** Object:  Table [dbo].[ServerInventory_ServerApplications_Xref]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServerInventory_ServerApplications_Xref](
	[ServerID] [int] NULL,
	[ApplicationID] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Index [CUIX__SrvAppXref__ServerID__ApplicationID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE CLUSTERED INDEX [CUIX__SrvAppXref__ServerID__ApplicationID] ON [dbo].[ServerInventory_ServerApplications_Xref]
(
	[ServerID] ASC,
	[ApplicationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ServerInventory_ServerApplications_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[dbo].[ServerInventory_ServerApplications_vw]
**  Desc:			View to pair servers up to thier applications
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			2009-07-21
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
********************************************************************************************************/
CREATE VIEW [dbo].[ServerInventory_ServerApplications_vw]
AS

SELECT 
	s.[ServerID]
	,s.[ServerName] + ISNULL('\' + s.[InstanceName],'') AS [ServerName]
	,a.[ApplicationName]
	,s.[Environment]
	,s.[Description]
FROM [dbo].[ServerInventory_ServerApplications_Xref] sax
INNER JOIN [dbo].[ServerInventory_SQL_AllServers_vw] s
	ON sax.[ServerID] = s.[ServerID]
INNER JOIN [dbo].[ServerInventory_Applications] a
	ON sax.[ApplicationID] = a.[ApplicationID]

GO
/****** Object:  Table [dbo].[ServerInventory_SQL_AttributeList]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServerInventory_SQL_AttributeList](
	[UniqueID] [int] IDENTITY(1,1) NOT NULL,
	[ServerID] [int] NOT NULL,
	[AttributeID] [int] NOT NULL,
	[AttributeValue] [nvarchar](1000) NULL,
	[DateCreated] [smalldatetime] NULL,
	[LastModified] [smalldatetime] NULL,
 CONSTRAINT [PK__ServerInventory_SQL_AttributeList__UniqueID] PRIMARY KEY NONCLUSTERED 
(
	[UniqueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ServerInventory_SQL_AttributeMaster]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServerInventory_SQL_AttributeMaster](
	[AttributeID] [int] IDENTITY(1,1) NOT NULL,
	[AttributeName] [varchar](100) NOT NULL,
	[IsCore] [bit] NULL,
	[IsReadOnly] [bit] NULL,
	[DateCreated] [datetime] NULL,
 CONSTRAINT [PK__ServerInventory_SQL_AttributeMaster__AttribID] PRIMARY KEY CLUSTERED 
(
	[AttributeID] ASC
)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[ServerInventory_SQL_ServerAttributes_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
* Name
	[dbo].[ServerInventory_SQL_ServerAttributes_vw]

* Author
	Matt Stanford
	
* Date
	2011.04.27
	
* Synopsis
	Reporting view to show latest SQL Server backups
	
* Description
	Combining the data of server inventory, backup history, database attributes and the 
	collectors log,	this view will show all supporting information for the latest backups 	
	for all	servers and databases.

* Dependencies
	Collecotrs: BackupHistory, DatabaseAttributes

*******************************************************************************
* License
*******************************************************************************
	Copyright © SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/
CREATE VIEW [dbo].[ServerInventory_SQL_ServerAttributes_vw]
AS

SELECT 
	m.[ServerID]
	,m.[ServerName]				AS HostName
	,m.[FullName]				AS ServerName
	,m.[SQLVersion]
	,m.[Environment]			AS EnvironmentName
	,am.[AttributeName]
	,attrib.[AttributeValue]
FROM [dbo].[ServerInventory_SQL_AllServers_vw] m
INNER JOIN [dbo].[ServerInventory_SQL_AttributeList] attrib
	ON attrib.[ServerID] = m.[ServerID]
INNER JOIN [dbo].[ServerInventory_SQL_AttributeMaster] am
	ON attrib.[AttributeID] = am.[AttributeID]



GO
/****** Object:  View [dbo].[ServerInventory_Owners_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[dbo].[ServerInventory_Owners_vw]
**  Desc:			View to show all of an owner's items
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			2009-07-21
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
********************************************************************************************************/
CREATE VIEW [dbo].[ServerInventory_Owners_vw]
AS

SELECT
	'SERVER'												AS [Type]
	,NULL													AS [ParentItem]
	,[ServerName]											AS [Item]
	,[OwnerName]											AS [OwnerName]
FROM [dbo].[ServerInventory_ServerOwners_vw]

UNION ALL

SELECT
	'APPLICATION'											AS [Type]
	,NULL													AS [ParentItem]
	,[ApplicationName]										AS [Item]
	,[OwnerName]											AS [OwnerName]
FROM [dbo].[ServerInventory_ApplicationOwners_vw]

UNION ALL

SELECT
	'DATABASE'												AS [Type]
	,[ServerName]											AS [ParentItem]
	,[DBName]												AS [Item]
	,[OwnerName]											AS [OwnerName]
FROM [dbo].[ServerInventory_DatabaseOwners_vw]
	

GO
/****** Object:  Table [dbo].[ServerInventory_SQL_BuildLevelDesc]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServerInventory_SQL_BuildLevelDesc](
	[ProductVersion] [varchar](100) NOT NULL,
	[SQLVersion] [smallint] NOT NULL,
	[Build] [int] NOT NULL,
	[ProductLevel] [nvarchar](128) NOT NULL,
	[ServicePack] [tinyint] NOT NULL,
	[CumulativeUpdate] [tinyint] NOT NULL,
	[Description] [varchar](4000) NULL,
	[ReleaseDate] [smalldatetime] NULL,
	[KBArticle] [char](6) NULL,
	[Link] [varchar](500) NULL,
 CONSTRAINT [PK__ServerInventory_SQL_BuildLevelDesc__ProductVersion] PRIMARY KEY CLUSTERED 
(
	[ProductVersion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[ServerInventory_SQL_ServerInfo_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
* Name
	[rpt].[LatestSQLBackupHistory_vw]

* Author
	Matt Stanford
	
* Date
	2011.04.27
	
* Synopsis
	Reporting view to show latest SQL Server backups
	
* Description
	Combining the data of server inventory, backup history, database attributes and the 
	collectors log,	this view will show all supporting information for the latest backups 	
	for all	servers and databases.

* Dependencies
	Collecotrs: BackupHistory, DatabaseAttributes

*******************************************************************************
* License
*******************************************************************************
	Copyright © SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/
CREATE VIEW [dbo].[ServerInventory_SQL_ServerInfo_vw]
AS

SELECT 
	srv.[ServerID]
	,srv.[FullName] as [ServerName]
	,srv.[Environment]
	,srv.[Description]
	,srv.[SQLVersion]							AS [DBAC_SQLVersion]
	,srv.[Edition]								AS [DBAC_Edition]
	,att.[SQLServer_ServicePack]
	,att.[CumulativeUpdate]
	,att.[Description]							AS [ProductVersionDescription]
	,att.[SQLVersion]							AS [SQLServer_SQLVersion]
	,att.[SQLServer_Build]
	,att.[SQLServer_Edition]
	,att.[SQLServer_@@ServerName]
	,att.[SQLServer_MachineName]
	,att.[SQLServer_PhysicalName]
	,att.[SQLServer_ServerName]
	,att.[SQLServer_InstanceName]
	,att.[SQLServer_LicenseType]
	,att.[SQLServer_IsClustered]
FROM [dbo].[ServerInventory_SQL_AllServers_vw] srv
LEFT OUTER JOIN (
	SELECT 
		PVT.[ServerName]
		,[SQLServer_Build]
		,bd.[SQLVersion]
		,bd.[ProductLevel]
		,bd.[CumulativeUpdate]
		,[SQLServer_Edition]
		,[SQLServer_ServicePack]
		,bd.[Description]
		,[SQLServer_@@ServerName]
		,[SQLServer_MachineName]
		,[SQLServer_PhysicalName]
		,[SQLServer_ServerName]
		,[SQLServer_InstanceName]
		,[SQLServer_LicenseType]
		,[SQLServer_IsClustered]
	FROM
	(
		SELECT 
			s.FullName as ServerName
			,sa.AttributeName
			,sa.AttributeValue
		FROM [dbo].[ServerInventory_SQL_AllServers_vw] s
		LEFT OUTER JOIN [dbo].[ServerInventory_SQL_ServerAttributes_vw] sa
			ON sa.ServerName = s.FullName
		WHERE sa.[AttributeName] IN ('SQLServer_Build','SQLServer_ServicePack','SQLServer_Edition','SQLServer_Engine','SQLServer_@@ServerName','SQLServer_InstanceName','SQLServer_IsClustered',
		'SQLServer_LicenseType','SQLServer_MachineName','SQLServer_PhysicalName','SQLServer_ServerName')
	) as st
	PIVOT
	(
		MAX(AttributeValue)
		FOR AttributeName 
			IN ([SQLServer_Build],[SQLServer_ServicePack],[SQLServer_Edition],[SQLServer_Engine],[SQLServer_@@ServerName],[SQLServer_MachineName], 
			[SQLServer_PhysicalName], [SQLServer_ServerName], [SQLServer_InstanceName], [SQLServer_LicenseType], [SQLServer_IsClustered])
	) as PVT
	LEFT OUTER JOIN [dbo].[ServerInventory_SQL_BuildLevelDesc] bd
		ON bd.[ProductVersion] = PVT.[SQLServer_Build]
) att
ON att.[ServerName] = srv.[FullName]

GO
/****** Object:  View [dbo].[ServerInventory_AllServers_Compatibility_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ServerInventory_AllServers_Compatibility_vw]
AS
SELECT *
FROM dbo.ServerInventory_SQL_AllServers_Compatibility_vw

GO
/****** Object:  View [dbo].[ServerInventory_SQL_BackupLicensing_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
* Name
	[rpt].[LatestSQLBackupHistory_vw]

* Author
	Matt Stanford
	
* Date
	2011.04.27
	
* Synopsis
	Reporting view to show latest SQL Server backups
	
* Description
	Combining the data of server inventory, backup history, database attributes and the 
	collectors log,	this view will show all supporting information for the latest backups 	
	for all	servers and databases.

* Dependencies
	Collecotrs: BackupHistory, DatabaseAttributes

*******************************************************************************
* License
*******************************************************************************
	Copyright © SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/
CREATE VIEW [dbo].[ServerInventory_SQL_BackupLicensing_vw]
AS
SELECT DISTINCT
	 vw.[servername]
	,am.[AttributeName] AS [Software]
	,at.[AttributeValue] AS [Status]
FROM [dbo].[ServerInventory_SQL_AllServers_vw] vw
INNER JOIN [dbo].[ServerInventory_SQL_AttributeList] at
	ON vw.[serverid] = at.[serverid]
INNER JOIN [dbo].[ServerInventory_SQL_AttributeMaster] am
	ON at.[AttributeID] = am.[AttributeID]
WHERE am.[AttributeName] = 'redgate'


GO
/****** Object:  View [dbo].[ServerInventory_SQL_TestServers_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
* Name
	[dbo].[ServerInventory_SQL_TestServers_vw]

* Author
	Matt Stanford
	
* Date
	2011.04.28
	
* Synopsis
	Reporting view to show latest SQL Server backups
	
* Description
	Combining the data of server inventory, backup history, database attributes and the 
	collectors log,	this view will show all supporting information for the latest backups 	
	for all	servers and databases.

* Dependencies
	[dbo].[ServerInventory_SQL_ServerAttributes_vw]
	[dbo].[ServerInventory_SQL_AllServers_vw]
	
*******************************************************************************
* License
*******************************************************************************
	Copyright © SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/
CREATE VIEW [dbo].[ServerInventory_SQL_TestServers_vw]
AS
SELECT 
	s.[ServerID]
	,s.[FullName]
	,s.[DotNetConnectionString]
FROM [dbo].[ServerInventory_SQL_AllServers_vw] s
INNER JOIN [dbo].[ServerInventory_SQL_ServerAttributes_vw] sa
	ON s.[ServerID] = sa.[ServerID]
WHERE sa.[AttributeName] = 'UsedForTesting' AND sa.[AttributeValue] = 'TRUE'

GO
/****** Object:  View [dbo].[SpaceUsed_CollectTableOrDatabase_vw]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
* Name
	[dbo].[SpaceUsed_CollectTableOrDatabase_vw]

* Author
	Matt Stanford
	
* Date
	<2011.04.28
	
* Synopsis
	View to drive the spaceused collectors
	
* Description
	View to drive the spaceused collectors

* Dependencies
	[dbo].[ServerInventory_SQL_ServerAttributes_vw]
	[dbo].[ServerInventory_SQL_AllServers_vw]
	
*******************************************************************************
* License
*******************************************************************************
	Copyright © SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/
CREATE VIEW [dbo].[SpaceUsed_CollectTableOrDatabase_vw]
AS

WITH TableOrDB
AS
(
	SELECT 
		a.[ServerID]
		,a.[ServerName]
		,a.[SQLVersion]
		,a.[AttributeName]
		,a.[AttributeValue]
		,s.[DotNetConnectionString]
	FROM [dbo].[ServerInventory_SQL_ServerAttributes_vw] a
	INNER JOIN [dbo].[ServerInventory_SQL_AllServers_vw] s
		ON a.ServerID = s.ServerID
	WHERE a.[AttributeName] IN ('SpaceUsed_Collect_Database', 'SpaceUsed_Collect_Table','UsedForTesting')
)
SELECT 
	COALESCE(t.[ServerName],d.[ServerName]) as ServerName
	,COALESCE(t.[SQLVersion],d.[SQLVersion]) as SQLVersion
	,CASE WHEN t.[AttributeValue] IS NULL 
		THEN 0
		ELSE 1
	END as CollectTable
	,CASE WHEN d.[AttributeValue] IS NULL 
		THEN 0
		ELSE 1
	END as CollectDatabase
	,CASE WHEN x.[AttributeValue] IS NULL 
		THEN 0
		ELSE 1
	END as UsedForTesting
	,COALESCE(t.[DotNetConnectionString],d.[DotNetConnectionString]) as ConnectionString
FROM (SELECT * FROM TableOrDB WHERE [AttributeName] = 'SpaceUsed_Collect_Table' AND [AttributeValue] = 'TRUE') t
FULL OUTER JOIN (SELECT * FROM TableOrDB WHERE [AttributeName] = 'SpaceUsed_Collect_Database' AND [AttributeValue] = 'TRUE') d
	ON t.[ServerID] = d.[ServerID]
LEFT OUTER JOIN (SELECT * FROM TableOrDB WHERE [AttributeName] = 'UsedForTesting' AND [AttributeValue] = 'TRUE') x
	ON COALESCE(t.[ServerID],d.[ServerID]) = x.[ServerID]



GO
/****** Object:  Table [dbo].[Backups_BackupAgents]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Backups_BackupAgents](
	[BackupAgentID] [int] IDENTITY(1,1) NOT NULL,
	[BackupAgentKey] [varchar](10) NULL,
	[Description] [varchar](100) NULL,
	[DateCreated] [smalldatetime] NULL,
 CONSTRAINT [PK__Backups_BackupAgents__BackupAgentID] PRIMARY KEY CLUSTERED 
(
	[BackupAgentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Backups_BackupCommands]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Backups_BackupCommands](
	[CommandID] [int] IDENTITY(1,1) NOT NULL,
	[BackupTypeID] [int] NULL,
	[BackupAgentID] [int] NULL,
	[Command] [varchar](1024) NULL,
	[DateCreated] [smalldatetime] NULL,
 CONSTRAINT [PK__Backups_BackupCommands__CommandID] PRIMARY KEY CLUSTERED 
(
	[CommandID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Backups_BackupTypes]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Backups_BackupTypes](
	[BackupTypeID] [int] IDENTITY(1,1) NOT NULL,
	[BackupTypeKey] [varchar](10) NULL,
	[DateCreated] [smalldatetime] NULL,
 CONSTRAINT [PK__Backups_BackupTypes__BackupTypeID] PRIMARY KEY CLUSTERED 
(
	[BackupTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Backups_DBsToBackup]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Backups_DBsToBackup](
	[DBsToBackupID] [int] IDENTITY(1,1) NOT NULL,
	[JobScheduleID] [int] NULL,
	[DatabaseName] [sysname] NOT NULL,
	[DateCreated] [smalldatetime] NULL,
 CONSTRAINT [PK__Backups_DBsToBackup__DBsToBackupID] PRIMARY KEY CLUSTERED 
(
	[DBsToBackupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Backups_DBsToNotBackup]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Backups_DBsToNotBackup](
	[DBsToNotBackupID] [int] IDENTITY(1,1) NOT NULL,
	[JobScheduleID] [int] NULL,
	[DatabaseName] [sysname] NOT NULL,
	[DateCreated] [smalldatetime] NULL,
 CONSTRAINT [PK__Backups_DBsToNotBackup__DBsToNotBackupID] PRIMARY KEY CLUSTERED 
(
	[DBsToNotBackupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Backups_Jobs]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Backups_Jobs](
	[JobID] [int] IDENTITY(1,1) NOT NULL,
	[JobName] [varchar](50) NULL,
	[ServerID] [int] NULL,
	[TargetServer] [varchar](50) NULL,
	[TargetShare] [varchar](50) NULL,
	[RecordHistory] [bit] NULL,
	[AlertLevel] [tinyint] NULL,
	[Enabled] [bit] NULL,
	[LoggingLevel] [tinyint] NULL,
	[CompressionLevel] [tinyint] NULL,
	[ThreadCount] [tinyint] NULL,
	[DateCreated] [smalldatetime] NULL,
 CONSTRAINT [PK__Backups_Jobs__JobID] PRIMARY KEY CLUSTERED 
(
	[JobID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Backups_JobSchedules]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Backups_JobSchedules](
	[JobScheduleID] [int] IDENTITY(1,1) NOT NULL,
	[JobID] [int] NULL,
	[BackupAgentID] [int] NULL,
	[BackupTypeID] [int] NULL,
	[Schedule] [varchar](255) NULL,
	[DateCreated] [smalldatetime] NULL,
 CONSTRAINT [PK__Backups_JobSchedules__JobScheduleID] PRIMARY KEY CLUSTERED 
(
	[JobScheduleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Backups_SrvSettings]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Backups_SrvSettings](
	[id] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[jobName] [varchar](50) NULL,
	[srvName] [varchar](50) NULL,
	[targetSrv] [varchar](50) NULL,
	[targetShare] [varchar](50) NULL,
	[dbRecord] [bit] NULL,
	[buExceptions] [bit] NULL,
	[buAgent] [varchar](20) NULL,
	[alertLevel] [int] NULL,
	[jobEnabled] [bit] NULL,
	[buType] [varchar](5) NULL,
	[loggingLevel] [int] NULL,
	[compressLevel] [int] NULL,
	[buTHreads] [int] NULL,
	[buStart] [varchar](255) NULL,
	[buTime] [varchar](100) NULL,
	[buDuration] [int] NULL,
	[nagHost] [varchar](50) NULL,
	[nagSvc] [varchar](50) NULL,
	[checkFile1] [varchar](255) NULL,
	[checkFile2] [varchar](255) NULL,
	[runJob1] [varchar](255) NULL,
	[runJob2] [varchar](255) NULL,
	[runEXE] [varchar](255) NULL,
	[lsPath] [varchar](255) NULL,
	[sqlVersion] [varchar](10) NULL,
	[lsVersion] [varchar](10) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_SrvSettings]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE CLUSTERED INDEX [IX_SrvSettings] ON [dbo].[Backups_SrvSettings]
(
	[srvName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Calendar]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Calendar](
	[CalendarDate] [date] NOT NULL,
	[CalendarYear] [int] NOT NULL,
	[CalendarMonth] [int] NOT NULL,
	[CalendarDay] [int] NOT NULL,
	[DayOfWeekName] [varchar](10) NOT NULL,
	[FirstDateOfWeek] [date] NOT NULL,
	[LastDateOfWeek] [date] NOT NULL,
	[FirstDateOfMonth] [date] NOT NULL,
	[LastDateOfMonth] [date] NOT NULL,
	[FirstDateOfQuarter] [date] NOT NULL,
	[LastDateOfQuarter] [date] NOT NULL,
	[FirstDateOfYear] [date] NOT NULL,
	[LastDateOfYear] [date] NOT NULL,
	[BusinessDay] [bit] NOT NULL,
	[NonBusinessDay] [bit] NOT NULL,
	[Weekend] [bit] NOT NULL,
	[Holiday] [bit] NOT NULL,
	[Weekday] [bit] NOT NULL,
	[CalendarDateDescription] [varchar](50) NULL,
 CONSTRAINT [PK_Calendar] PRIMARY KEY CLUSTERED 
(
	[CalendarDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ChangeControl_ChangeSet]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChangeControl_ChangeSet](
	[ChangeSetID] [int] IDENTITY(1,1) NOT NULL,
	[PackageID] [int] NULL,
	[Description] [nvarchar](255) NULL,
	[InstanceName] [varchar](50) NULL,
	[Sequence] [smallint] NULL,
	[UserName] [varchar](50) NULL,
	[DateCreated] [smalldatetime] NULL,
 CONSTRAINT [PK__ChangeControl_ChangeSet__PackageDetailID] PRIMARY KEY CLUSTERED 
(
	[ChangeSetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ChangeControl_ChangeSetDetail]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChangeControl_ChangeSetDetail](
	[ChangeSetID] [int] NULL,
	[ScriptID] [int] NULL,
	[Sequence] [smallint] NULL,
	[IsRemoved] [bit] NULL,
	[UserName] [varchar](50) NULL,
	[DateCreated] [smalldatetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ChangeControl_DeployDetail]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChangeControl_DeployDetail](
	[DeployID] [int] NULL,
	[ScriptID] [int] NULL,
	[Sequence] [smallint] NULL,
	[IsEnabled] [bit] NULL,
	[UserName] [varchar](50) NULL,
	[DateCreated] [smalldatetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Index [IX__ChangeControl_DeployDetail__DeployID__ScriptID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE CLUSTERED INDEX [IX__ChangeControl_DeployDetail__DeployID__ScriptID] ON [dbo].[ChangeControl_DeployDetail]
(
	[DeployID] ASC,
	[ScriptID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChangeControl_DeployMaster]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChangeControl_DeployMaster](
	[DeployID] [int] IDENTITY(1,1) NOT NULL,
	[DeployName] [varchar](50) NULL,
	[UserName] [varchar](50) NULL,
	[DateCreated] [smalldatetime] NULL,
 CONSTRAINT [FK__ChangeControl_DeployMaster__DeployID] PRIMARY KEY CLUSTERED 
(
	[DeployID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ChangeControl_PackageMaster]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChangeControl_PackageMaster](
	[PackageID] [int] IDENTITY(1,1) NOT NULL,
	[PackageLabel] [varchar](255) NULL,
	[Year] [smallint] NULL,
	[Type] [varchar](10) NULL,
	[Number] [smallint] NULL,
	[Revision] [tinyint] NULL,
	[Description] [varchar](255) NULL,
	[UserName] [varchar](50) NULL,
	[DateCreated] [smalldatetime] NULL,
 CONSTRAINT [PK__ChangeControl_PackageMaster__PackageID] PRIMARY KEY CLUSTERED 
(
	[PackageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ServerInventory_SQL_sysjobs]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServerInventory_SQL_sysjobs](
	[JobID] [int] IDENTITY(1,1) NOT NULL,
	[ServerID] [int] NULL,
	[job_id] [uniqueidentifier] NOT NULL,
	[originating_server_id] [int] NOT NULL,
	[name] [sysname] NOT NULL,
	[enabled] [tinyint] NOT NULL,
	[description] [nvarchar](512) NULL,
	[start_step_id] [int] NOT NULL,
	[category_id] [int] NOT NULL,
	[owner_sid] [varbinary](85) NOT NULL,
	[notify_level_eventlog] [int] NOT NULL,
	[notify_level_email] [int] NOT NULL,
	[notify_level_netsend] [int] NOT NULL,
	[notify_level_page] [int] NOT NULL,
	[notify_email_operator_id] [int] NOT NULL,
	[notify_netsend_operator_id] [int] NOT NULL,
	[notify_page_operator_id] [int] NOT NULL,
	[delete_level] [int] NOT NULL,
	[date_created] [datetime] NOT NULL,
	[date_modified] [datetime] NOT NULL,
	[version_number] [int] NOT NULL,
 CONSTRAINT [PK__ServerInventory_SQL_sysjobs__JobID] PRIMARY KEY CLUSTERED 
(
	[JobID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [hist].[Backups_BackupJobHistory]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[Backups_BackupJobHistory](
	[JobName] [sysname] NOT NULL,
	[ServerName] [sysname] NOT NULL,
	[StartTime] [smalldatetime] NOT NULL,
	[FinishTime] [smalldatetime] NOT NULL,
	[nbrDBs] [int] NOT NULL,
	[sizeDBs] [bigint] NULL,
	[jobOutcome] [varchar](20) NULL,
	[buLocation] [varchar](255) NULL,
	[sqlbuSize] [int] NULL,
	[Schedule] [varchar](255) NULL,
	[DateCreated] [smalldatetime] NULL
) ON [History]

GO
/****** Object:  Table [hist].[ChangeControl_ScriptDatabaseXref]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[ChangeControl_ScriptDatabaseXref](
	[ScriptID] [int] NOT NULL,
	[DatabaseID] [int] NOT NULL,
 CONSTRAINT [PK__ChangeControl_ScriptDatabaseXref__ScriptID__DatabaseID] PRIMARY KEY CLUSTERED 
(
	[ScriptID] ASC,
	[DatabaseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
) ON [History]

GO
/****** Object:  Table [hist].[Deadlock_Process_ExecutionStack]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[Deadlock_Process_ExecutionStack](
	[HistDeadlockExecutionStackID] [int] IDENTITY(1,1) NOT NULL,
	[HistDeadlockProcessID] [int] NULL,
	[Sequence] [smallint] NULL,
	[Stack] [nvarchar](4000) NULL,
 CONSTRAINT [PK__Deadlock_Process_ExecutionStack__HistDeadlockExecutionStackID] PRIMARY KEY CLUSTERED 
(
	[HistDeadlockExecutionStackID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [hist].[Deadlock_ResourceOwners]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[Deadlock_ResourceOwners](
	[HistDeadlockResourceID] [int] NULL,
	[HistDeadlockProcessID] [int] NULL,
	[modeRefLockModeID] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [hist].[Deadlock_ResourceWaiters]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[Deadlock_ResourceWaiters](
	[HistDeadlockResourceID] [int] NULL,
	[HistDeadlockProcessID] [int] NULL,
	[modeRefLockModeID] [int] NULL,
	[requestType] [varchar](10) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [hist].[who_CommandIDs]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[who_CommandIDs](
	[CommandID] [int] IDENTITY(1,1) NOT NULL,
	[Command] [varchar](128) NULL,
 CONSTRAINT [PK__who_CommandIDs__CommandID] PRIMARY KEY NONCLUSTERED 
(
	[CommandID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [hist].[who_DatabaseIDs]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[who_DatabaseIDs](
	[DatabaseID] [int] IDENTITY(1,1) NOT NULL,
	[DBName] [varchar](128) NULL,
 CONSTRAINT [PK__who_DatabaseIDs__DatabaseID] PRIMARY KEY NONCLUSTERED 
(
	[DatabaseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [hist].[who_HostIDs]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[who_HostIDs](
	[HostID] [int] IDENTITY(1,1) NOT NULL,
	[HostName] [varchar](128) NULL,
 CONSTRAINT [PK__who_HostIDs__HostID] PRIMARY KEY NONCLUSTERED 
(
	[HostID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [hist].[who_LockIDs]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[who_LockIDs](
	[LockID] [int] IDENTITY(1,1) NOT NULL,
	[LockMode] [varchar](128) NULL,
 CONSTRAINT [PK__who_LockIDs__LockID] PRIMARY KEY NONCLUSTERED 
(
	[LockID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [hist].[who_Logging]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[who_Logging](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ServerID] [int] NULL,
	[SPID] [int] NULL,
	[KPID] [int] NULL,
	[DatabaseID] [int] NULL,
	[QueryID] [int] NULL,
	[LoginID] [int] NULL,
	[HostID] [int] NULL,
	[StatusID] [int] NULL,
	[CommandID] [int] NULL,
	[BlkBy] [int] NULL,
	[TranCount] [int] NULL,
	[ReadLockCount] [int] NULL,
	[WriteLockCount] [int] NULL,
	[SchemaLockCount] [int] NULL,
	[WaitID] [int] NULL,
	[PercentComplete] [decimal](5, 2) NULL,
	[EstCompTime] [int] NULL,
	[CPU] [int] NULL,
	[IO] [int] NULL,
	[Reads] [int] NULL,
	[Writes] [int] NULL,
	[LastRead] [smalldatetime] NULL,
	[LastWrite] [smalldatetime] NULL,
	[StartTime] [smalldatetime] NULL,
	[LastBatch] [smalldatetime] NULL,
	[ProgramID] [int] NULL,
	[DateCreated] [smalldatetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [hist].[who_LoginIDs]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[who_LoginIDs](
	[LoginID] [int] IDENTITY(1,1) NOT NULL,
	[Login] [varchar](128) NULL,
 CONSTRAINT [PK__who_LoginIDs__LoginID] PRIMARY KEY NONCLUSTERED 
(
	[LoginID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [hist].[who_ProgramIDs]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[who_ProgramIDs](
	[ProgramID] [int] IDENTITY(1,1) NOT NULL,
	[ProgramName] [varchar](128) NULL,
 CONSTRAINT [PK__who_ProgramIDs__ProgramID] PRIMARY KEY NONCLUSTERED 
(
	[ProgramID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [hist].[who_QueryIDs]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[who_QueryIDs](
	[QueryID] [int] IDENTITY(1,1) NOT NULL,
	[Query] [varchar](4000) NULL,
 CONSTRAINT [PK__who_QueryIDs__QueryID] PRIMARY KEY NONCLUSTERED 
(
	[QueryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [hist].[who_StatusIDs]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[who_StatusIDs](
	[StatusID] [int] IDENTITY(1,1) NOT NULL,
	[Status] [varchar](128) NULL,
 CONSTRAINT [PK__who_StatusIDs__StatusID] PRIMARY KEY NONCLUSTERED 
(
	[StatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [hist].[who_WaitIDs]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[who_WaitIDs](
	[WaitID] [int] IDENTITY(1,1) NOT NULL,
	[WaitType] [varchar](128) NULL,
 CONSTRAINT [PK__who_WaitIDs__WaitID] PRIMARY KEY NONCLUSTERED 
(
	[WaitID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [hist].[Win32VolumeSizes]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hist].[Win32VolumeSizes](
	[RunID] [int] NOT NULL,
	[Win32VolumeID] [int] NOT NULL,
	[DBMSInstanceID] [int] NOT NULL,
	[CapacityMB] [int] NULL,
	[FreeSpaceMB] [int] NULL,
	[DateCreated] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Win32VolumeSizes] PRIMARY KEY CLUSTERED 
(
	[Win32VolumeID] ASC,
	[RunID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [ref].[ServerInventory_SQL_DataTypes]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ref].[ServerInventory_SQL_DataTypes](
	[RefDataTypeID] [int] IDENTITY(1,1) NOT NULL,
	[RefSQLVersionID] [int] NULL,
	[name] [nvarchar](128) NULL,
	[system_type_id] [int] NULL,
	[max_length] [smallint] NULL,
	[precision] [tinyint] NULL,
	[scale] [tinyint] NULL,
 CONSTRAINT [PK__ServerInventory_SQL_DataTypes__RefDataTypeID] PRIMARY KEY CLUSTERED 
(
	[RefDataTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [ref].[ServerInventory_SQL_ServerVersions]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ref].[ServerInventory_SQL_ServerVersions](
	[RefSQLVersionID] [int] IDENTITY(1,1) NOT NULL,
	[SQLVersionText] [varchar](10) NULL,
	[SQLVersion] [decimal](5, 2) NULL,
	[StartingBuild] [decimal](7, 2) NULL,
	[EndingBuild] [decimal](7, 2) NULL,
 CONSTRAINT [PK__ServerInventory_SQL_ServerVersions__RefSQLVersionID] PRIMARY KEY CLUSTERED 
(
	[RefSQLVersionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UIX__ServerInventory__Environments__EnvironmentName]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [UIX__ServerInventory__Environments__EnvironmentName] ON [dbo].[ServerInventory_Environments]
(
	[EnvironmentName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AttribID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX_AttribID] ON [dbo].[ServerInventory_SQL_AttributeList]
(
	[AttributeID] ASC
)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
/****** Object:  Index [IX_ServerID_AttribID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX_ServerID_AttribID] ON [dbo].[ServerInventory_SQL_AttributeList]
(
	[ServerID] ASC,
	[AttributeID] ASC
)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_AttribName]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AttribName] ON [dbo].[ServerInventory_SQL_AttributeMaster]
(
	[AttributeName] ASC
)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX__ServerInventory_SQL_Master__Enabled__WITH_INCLUDES]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__ServerInventory_SQL_Master__Enabled__WITH_INCLUDES] ON [dbo].[ServerInventory_SQL_Master]
(
	[Enabled] ASC
)
INCLUDE ( 	[InstanceName],
	[PortNumber],
	[ServerName]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_DoNotAllowDuplicateServers]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_DoNotAllowDuplicateServers] ON [dbo].[ServerInventory_SQL_Master]
(
	[ServerName] ASC,
	[InstanceName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UIX__ServerInventory__SQL__Master__ServerName__InstanceName]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [UIX__ServerInventory__SQL__Master__ServerName__InstanceName] ON [dbo].[ServerInventory_SQL_Master]
(
	[ServerName] ASC,
	[InstanceName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UIX_sysjobs_ServerID_job_id]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX_sysjobs_ServerID_job_id] ON [dbo].[ServerInventory_SQL_sysjobs]
(
	[ServerID] ASC,
	[job_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX__MissingSQLBackups__DatabaseID__HistServerID__RunID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__MissingSQLBackups__DatabaseID__HistServerID__RunID] ON [etc].[MissingSQLBackups]
(
	[DatabaseID] ASC,
	[HistServerID] ASC,
	[RunID] ASC
)
INCLUDE ( 	[BackupType],
	[DeviceType],
	[LastBackupLocation],
	[LastBackupTime],
	[RecoveryModel]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX__MissingSQLBackups__DatabaseID__LastBackupTime__RecoveryModel__RunID__HistServerID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__MissingSQLBackups__DatabaseID__LastBackupTime__RecoveryModel__RunID__HistServerID] ON [etc].[MissingSQLBackups]
(
	[DatabaseID] ASC,
	[LastBackupTime] ASC,
	[RecoveryModel] ASC,
	[RunID] ASC,
	[HistServerID] ASC
)
INCLUDE ( 	[BackupType],
	[DeviceType],
	[LastBackupLocation]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX__MissingSQLBackups__HistServerID__LastBackupTime__RecoveryModel__RunID__DatabaseID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__MissingSQLBackups__HistServerID__LastBackupTime__RecoveryModel__RunID__DatabaseID] ON [etc].[MissingSQLBackups]
(
	[HistServerID] ASC,
	[LastBackupTime] ASC,
	[RecoveryModel] ASC,
	[RunID] ASC,
	[DatabaseID] ASC
)
INCLUDE ( 	[BackupType],
	[DeviceType],
	[LastBackupLocation]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX__MissingSQLBackups__LastBackupTime__RecoveryModel__DatabaseID__RunID__HistServerID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__MissingSQLBackups__LastBackupTime__RecoveryModel__DatabaseID__RunID__HistServerID] ON [etc].[MissingSQLBackups]
(
	[LastBackupTime] ASC,
	[RecoveryModel] ASC,
	[DatabaseID] ASC,
	[RunID] ASC,
	[HistServerID] ASC
)
INCLUDE ( 	[BackupType],
	[DeviceType],
	[LastBackupLocation]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX__MissingSQLBackups__RunID__DatabaseID__DatabaseServerID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__MissingSQLBackups__RunID__DatabaseID__DatabaseServerID] ON [etc].[MissingSQLBackups]
(
	[RunID] ASC,
	[DatabaseID] ASC,
	[HistServerID] ASC
)
INCLUDE ( 	[BackupType],
	[DeviceType],
	[LastBackupLocation],
	[LastBackupTime],
	[RecoveryModel]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX__MissingSQLBackups_RunIDs__RunID__DateCreated]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__MissingSQLBackups_RunIDs__RunID__DateCreated] ON [etc].[MissingSQLBackups_RunIDs]
(
	[RunID] ASC,
	[DateCreated] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX__Backup_Devices__DeviceID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__Backup_Devices__DeviceID] ON [hist].[Backups_Devices]
(
	[DeviceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UIX_Backups_Devices]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Backups_Devices] ON [hist].[Backups_Devices]
(
	[DeviceName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
GO
/****** Object:  Index [IX__Backups_History__DatabaseID__StartDate]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__Backups_History__DatabaseID__StartDate] ON [hist].[Backups_History]
(
	[DatabaseID] ASC,
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
GO
/****** Object:  Index [IX__Backups_History__HistServerID__DatabaseID__EndDate__LogicalDeviceID__PhysicalDeviceID__UserID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__Backups_History__HistServerID__DatabaseID__EndDate__LogicalDeviceID__PhysicalDeviceID__UserID] ON [hist].[Backups_History]
(
	[BUTypeID] ASC
)
INCLUDE ( 	[DatabaseID],
	[EndDate],
	[HistServerID],
	[LogicalDeviceID],
	[PhysicalDeviceID],
	[UserID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
GO
/****** Object:  Index [IX__Backups_History__HistServerID__UserID__PhysicalDeviceID__LogicalDeviceID__DatabaseID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__Backups_History__HistServerID__UserID__PhysicalDeviceID__LogicalDeviceID__DatabaseID] ON [hist].[Backups_History]
(
	[HistServerID] ASC,
	[UserID] ASC,
	[PhysicalDeviceID] ASC,
	[LogicalDeviceID] ASC,
	[DatabaseID] ASC
)
INCLUDE ( 	[StartDate]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
GO
/****** Object:  Index [UIX__BackupsHistory__UserID__EndDate__LogicalDeviceID__PhysicalDeviceID__HistServerID__DatabaseID__BUTypeID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [UIX__BackupsHistory__UserID__EndDate__LogicalDeviceID__PhysicalDeviceID__HistServerID__DatabaseID__BUTypeID] ON [hist].[Backups_History]
(
	[UserID] ASC,
	[EndDate] ASC,
	[LogicalDeviceID] ASC,
	[PhysicalDeviceID] ASC,
	[HistServerID] ASC,
	[DatabaseID] ASC,
	[BUTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UIX_Backups_Types]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Backups_Types] ON [hist].[Backups_Types]
(
	[BackupType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_ChangeTracking_SQL_ObjectIDs]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX_ChangeTracking_SQL_ObjectIDs] ON [hist].[ChangeTracking_SQL_ObjectIDs]
(
	[SchemaName] ASC,
	[ObjectName] ASC,
	[ObjectTypeID] ASC
)
INCLUDE ( 	[ObjectID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 60) ON [History]
GO
/****** Object:  Index [IX__ConnectionCounts__HistServerID__RunID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__ConnectionCounts__HistServerID__RunID] ON [hist].[ConnectionCounts]
(
	[HistServerID] ASC,
	[RunID] ASC
)
INCLUDE ( 	[CounterValue]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX__ConnectionCounts_RunIDs__DateCreated]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__ConnectionCounts_RunIDs__DateCreated] ON [hist].[ConnectionCounts_RunIDs]
(
	[DateCreated] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX__ConnectionCounts_RunIDs__RunID__DateCreated]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__ConnectionCounts_RunIDs__RunID__DateCreated] ON [hist].[ConnectionCounts_RunIDs]
(
	[RunID] ASC,
	[DateCreated] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UIX__Deadlock_Deadlocks__HistServerID__DeadlockSPID__DeadlockDate]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX__Deadlock_Deadlocks__HistServerID__DeadlockSPID__DeadlockDate] ON [hist].[Deadlock_Deadlocks]
(
	[HistServerID] ASC,
	[DeadlockSPID] ASC,
	[DeadlockDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UIX__Deadlock_Process_ExecutionStack__HistDeadlockProcessID__Sequence]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX__Deadlock_Process_ExecutionStack__HistDeadlockProcessID__Sequence] ON [hist].[Deadlock_Process_ExecutionStack]
(
	[HistDeadlockProcessID] ASC,
	[Sequence] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UIX__Deadlock_ProcessList__HistDeadlockID__processid]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX__Deadlock_ProcessList__HistDeadlockID__processid] ON [hist].[Deadlock_ProcessList]
(
	[HistDeadlockID] ASC,
	[processid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UIX__Deadlock_ResourceOwners__HistDeadlockResourceID__HistDeadlockProcessID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX__Deadlock_ResourceOwners__HistDeadlockResourceID__HistDeadlockProcessID] ON [hist].[Deadlock_ResourceOwners]
(
	[HistDeadlockResourceID] ASC,
	[HistDeadlockProcessID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UIX__Deadlock_ResourceWaiters__HistDeadlockResourceID__HistDeadlockProcessID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX__Deadlock_ResourceWaiters__HistDeadlockResourceID__HistDeadlockProcessID] ON [hist].[Deadlock_ResourceWaiters]
(
	[HistDeadlockResourceID] ASC,
	[HistDeadlockProcessID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UIX_Categories_1]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Categories_1] ON [hist].[DTSStore_Categories]
(
	[CategoryID_GUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UIX_Descriptions_1]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Descriptions_1] ON [hist].[DTSStore_Descriptions]
(
	[Description] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UIX_Owners_1]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Owners_1] ON [hist].[DTSStore_Owners]
(
	[Owner] ASC,
	[Owner_sid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UIX_PackageNames_1]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX_PackageNames_1] ON [hist].[DTSStore_PackageNames]
(
	[PackageName] ASC,
	[PackageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UIX_FileNames]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX_FileNames] ON [hist].[General_FileNames]
(
	[FileName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [History]
GO
/****** Object:  Index [UIX_FullFileName]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX_FullFileName] ON [hist].[General_FullFileName]
(
	[HistPathID] ASC,
	[HistFileNameID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [History]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UIX_Paths]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Paths] ON [hist].[General_Paths]
(
	[Path] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [History]
GO
/****** Object:  Index [IX__Jobs_SQL_JobHistory__run_datetime_HistJobID_HistServerID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__Jobs_SQL_JobHistory__run_datetime_HistJobID_HistServerID] ON [hist].[Jobs_SQL_JobHistory]
(
	[run_datetime] ASC
)
INCLUDE ( 	[HistJobID],
	[HistServerID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [History]
GO
/****** Object:  Index [IX__Jobs_SQL_JobHistory__step_id_run_status_run_datetime_histJobID_HistServerID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__Jobs_SQL_JobHistory__step_id_run_status_run_datetime_histJobID_HistServerID] ON [hist].[Jobs_SQL_JobHistory]
(
	[step_id] ASC,
	[run_status] ASC,
	[run_datetime] ASC
)
INCLUDE ( 	[HistJobID],
	[HistServerID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [History]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UIX_hist_Jobs_SQL_JobHistory_stepid_runstatus_ServerID_JobID_datetime_WITH_Includes]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [UIX_hist_Jobs_SQL_JobHistory_stepid_runstatus_ServerID_JobID_datetime_WITH_Includes] ON [hist].[Jobs_SQL_JobHistory]
(
	[step_id] ASC,
	[run_status] ASC,
	[HistServerID] ASC,
	[HistJobID] ASC,
	[run_datetime] ASC
)
INCLUDE ( 	[message],
	[run_duration],
	[step_name]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UIX_hist_Jobs_SQL_JobHistory_HistServerID_name]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [UIX_hist_Jobs_SQL_JobHistory_HistServerID_name] ON [hist].[Jobs_SQL_Jobs]
(
	[HistServerID] ASC,
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
GO
/****** Object:  Index [UIX_sysjobs_ServerID_job_id]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX_sysjobs_ServerID_job_id] ON [hist].[Jobs_SQL_Jobs]
(
	[HistServerID] ASC,
	[job_id] ASC,
	[date_modified] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UIX__ServerInventory__ServerIDs__HistServerID__ServerName]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [UIX__ServerInventory__ServerIDs__HistServerID__ServerName] ON [hist].[ServerInventory_ServerIDs]
(
	[HistServerID] ASC,
	[ServerName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UIX_ServerIDs_ServerName]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ServerIDs_ServerName] ON [hist].[ServerInventory_ServerIDs]
(
	[ServerName] ASC
)
INCLUDE ( 	[HistServerID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UIX__ServerInventory_SQL_DatabaseAttributeMaster__AttributeName]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX__ServerInventory_SQL_DatabaseAttributeMaster__AttributeName] ON [hist].[ServerInventory_SQL_DatabaseAttributeMaster]
(
	[AttributeName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [History]
GO
/****** Object:  Index [IX__DatabaseAttributeValues__DatabaseAttributeID__HistDatabaseID__HistServerID__DatabaseAttributeValueID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__DatabaseAttributeValues__DatabaseAttributeID__HistDatabaseID__HistServerID__DatabaseAttributeValueID] ON [hist].[ServerInventory_SQL_DatabaseAttributeValues]
(
	[DatabaseAttributeID] ASC,
	[HistDatabaseID] ASC,
	[HistServerID] ASC,
	[DatabaseAttributeValueID] ASC
)
INCLUDE ( 	[DateLastSeenOn]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
GO
/****** Object:  Index [IX__DatabaseAttributeValues__file_id]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__DatabaseAttributeValues__file_id] ON [hist].[ServerInventory_SQL_DatabaseAttributeValues]
(
	[file_id] ASC
)
INCLUDE ( 	[DatabaseAttributeValueID],
	[HistServerID],
	[HistDatabaseID],
	[DatabaseAttributeID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [History]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX__ServerInventory_SQL_DatabaseIDs__DatabaseID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__ServerInventory_SQL_DatabaseIDs__DatabaseID] ON [hist].[ServerInventory_SQL_DatabaseIDs]
(
	[DatabaseID] ASC
)
INCLUDE ( 	[DBName]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UIX_DatabaseIDs_DatabaseName]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX_DatabaseIDs_DatabaseName] ON [hist].[ServerInventory_SQL_DatabaseIDs]
(
	[DBName] ASC
)
INCLUDE ( 	[DatabaseID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
GO
/****** Object:  Index [IX__ServerInventory_SQL_ServerDBTableIDs__DatabaseID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__ServerInventory_SQL_ServerDBTableIDs__DatabaseID] ON [hist].[ServerInventory_SQL_ServerDBTableIDs]
(
	[DatabaseID] ASC
)
INCLUDE ( 	[HistServerID],
	[ServerDBTableID],
	[TableID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
GO
/****** Object:  Index [IX__ServerInventory_SQL_ServerDBTableIDs__HistServerID_DatabaseID_TableID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__ServerInventory_SQL_ServerDBTableIDs__HistServerID_DatabaseID_TableID] ON [hist].[ServerInventory_SQL_ServerDBTableIDs]
(
	[HistServerID] ASC,
	[DatabaseID] ASC,
	[TableID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [History]
GO
/****** Object:  Index [IX__ServerInventory_SQL_ServerDBTableIDs__TableID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__ServerInventory_SQL_ServerDBTableIDs__TableID] ON [hist].[ServerInventory_SQL_ServerDBTableIDs]
(
	[TableID] ASC
)
INCLUDE ( 	[DatabaseID],
	[HistServerID],
	[ServerDBTableID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX__ServerInventory_SQL_TableIDs__SchemaName_TableName]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__ServerInventory_SQL_TableIDs__SchemaName_TableName] ON [hist].[ServerInventory_SQL_TableIDs]
(
	[SchemaName] ASC,
	[TableName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [History]
GO
/****** Object:  Index [IX__SpaceUsed_DatabaseSizes__HistServerID_DatabaseID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__SpaceUsed_DatabaseSizes__HistServerID_DatabaseID] ON [hist].[SpaceUsed_DatabaseSizes]
(
	[HistServerID] ASC
)
INCLUDE ( 	[DatabaseID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [History]
GO
/****** Object:  Index [IX__SpaceUsed_DBSizes__DatabaseID__SampleDate]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__SpaceUsed_DBSizes__DatabaseID__SampleDate] ON [hist].[SpaceUsed_DatabaseSizes]
(
	[DatabaseID] ASC,
	[SampleDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [History]
GO
/****** Object:  Index [IX__SpaceUsed_DBSizes__HistServerID__SampleDate]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__SpaceUsed_DBSizes__HistServerID__SampleDate] ON [hist].[SpaceUsed_DatabaseSizes]
(
	[HistServerID] ASC,
	[SampleDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [History]
GO
/****** Object:  Index [UIX__SpaceUsed__DatabaseSizes__SampleDate__WITH__Includes]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [UIX__SpaceUsed__DatabaseSizes__SampleDate__WITH__Includes] ON [hist].[SpaceUsed_DatabaseSizes]
(
	[SampleDate] ASC
)
INCLUDE ( 	[DatabaseID],
	[DataSizeMB],
	[DataSizeUnusedMB],
	[HistServerID],
	[LogSizeMB],
	[LogSizeUnusedMB]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
GO
/****** Object:  Index [UIX__SpaceUsed_DriveSizes_DriveMaster__RunID_HistServerID_DriveLetterID_DriveLabelID]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [UIX__SpaceUsed_DriveSizes_DriveMaster__RunID_HistServerID_DriveLetterID_DriveLabelID] ON [hist].[SpaceUsed_DriveSizes_DriveMaster]
(
	[RunID] ASC,
	[HistServerID] ASC,
	[DriveLetterID] ASC,
	[DriveLabelID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX__SpaceUsed_TableSizes__ServerDBTableID_SampleDate_RowCount_ReservedSpaceKB_DataSpaceKBIndexSizeKB_UnusedKB]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX__SpaceUsed_TableSizes__ServerDBTableID_SampleDate_RowCount_ReservedSpaceKB_DataSpaceKBIndexSizeKB_UnusedKB] ON [hist].[SpaceUsed_TableSizes]
(
	[ServerDBTableID] ASC,
	[SampleDate] ASC
)
INCLUDE ( 	[DataSpaceKB],
	[IndexSizeKB],
	[ReservedSpaceKB],
	[RowCount],
	[UnusedKB]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [History]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UIX_UserNames_UserName]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX_UserNames_UserName] ON [hist].[Users_UserNames]
(
	[UserName] ASC
)
INCLUDE ( 	[UserID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [History]
GO
/****** Object:  Index [IX_MI_who_Logging_57_56]    Script Date: 5/22/2017 1:13:46 PM ******/
CREATE NONCLUSTERED INDEX [IX_MI_who_Logging_57_56] ON [hist].[who_Logging]
(
	[ServerID] ASC
)
INCLUDE ( 	[CommandID],
	[DatabaseID],
	[DateCreated],
	[HostID],
	[LastBatch],
	[LoginID],
	[ProgramID],
	[QueryID],
	[StartTime],
	[StatusID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Backups_BackupAgents] ADD  CONSTRAINT [DF__Backups_BackupAgents__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[Backups_BackupCommands] ADD  CONSTRAINT [DF__Backups_BackupCommands__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[Backups_BackupTypes] ADD  CONSTRAINT [DF__Backups_BackupTypes__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[Backups_Jobs] ADD  CONSTRAINT [DF__Backups_Jobs__RecordHistory]  DEFAULT ((1)) FOR [RecordHistory]
GO
ALTER TABLE [dbo].[Backups_Jobs] ADD  CONSTRAINT [DF__Backups_Jobs__Enabled]  DEFAULT ((1)) FOR [Enabled]
GO
ALTER TABLE [dbo].[Backups_Jobs] ADD  CONSTRAINT [DF__Backups_Jobs__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[Backups_JobSchedules] ADD  CONSTRAINT [DF__Backups_JobSchedules__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[Backups_SrvSettings] ADD  CONSTRAINT [DF_SrvSettings_targetSrv]  DEFAULT ('oecfp3') FOR [targetSrv]
GO
ALTER TABLE [dbo].[Backups_SrvSettings] ADD  CONSTRAINT [DF_SrvSettings_targetShare]  DEFAULT ('sqlbu$') FOR [targetShare]
GO
ALTER TABLE [dbo].[Backups_SrvSettings] ADD  CONSTRAINT [DF_SrvSettings_dbRecord]  DEFAULT ((1)) FOR [dbRecord]
GO
ALTER TABLE [dbo].[Backups_SrvSettings] ADD  CONSTRAINT [DF_SrvSettings_buExeptions]  DEFAULT ((0)) FOR [buExceptions]
GO
ALTER TABLE [dbo].[Backups_SrvSettings] ADD  CONSTRAINT [DF_SrvSettings_alertLevel]  DEFAULT ((2)) FOR [alertLevel]
GO
ALTER TABLE [dbo].[Backups_SrvSettings] ADD  CONSTRAINT [DF_SrvSettings_jobEnabled]  DEFAULT ((1)) FOR [jobEnabled]
GO
ALTER TABLE [dbo].[Backups_SrvSettings] ADD  CONSTRAINT [DF_SrvSettings_loggingLevel]  DEFAULT ((0)) FOR [loggingLevel]
GO
ALTER TABLE [dbo].[ChangeControl_ChangeSet] ADD  CONSTRAINT [DF__ChangeControl_ChangeSet__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[ChangeControl_ChangeSetDetail] ADD  CONSTRAINT [DF__ChangeControl_ChangeSetDetail__IsRemoved]  DEFAULT ((0)) FOR [IsRemoved]
GO
ALTER TABLE [dbo].[ChangeControl_ChangeSetDetail] ADD  CONSTRAINT [DF__ChangeControl_ChangeSetDetail__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[ChangeControl_DeployDetail] ADD  CONSTRAINT [DF__ChangeControl_DeployMaster__IsEnabled]  DEFAULT ((1)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[ChangeControl_DeployDetail] ADD  CONSTRAINT [DF__ChangeControl_DeployDetail__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[ChangeControl_DeployMaster] ADD  CONSTRAINT [DF__ChangeControl_DeployMaster__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[NTPermissions_EnvironmentExceptions] ADD  CONSTRAINT [DF__NTPermissions_EnvironmentExceptions__RunInAdditionToDefault]  DEFAULT ((1)) FOR [RunInAdditionToDefault]
GO
ALTER TABLE [dbo].[NTPermissions_ServerExceptions] ADD  CONSTRAINT [DF__NTPermissions_ServerExceptions__RunInAdditionToDefault]  DEFAULT ((1)) FOR [RunInAdditionToDefault]
GO
ALTER TABLE [dbo].[ServerInventory_SQL_AttributeList] ADD  CONSTRAINT [DF__ServerInventory_SQL_AttributeList__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[ServerInventory_SQL_AttributeList] ADD  CONSTRAINT [DF__ServerInventory_SQL_AttributeList__LastModified]  DEFAULT (getdate()) FOR [LastModified]
GO
ALTER TABLE [dbo].[ServerInventory_SQL_AttributeMaster] ADD  CONSTRAINT [DF__ServerInventory_SQL_AttributeMaster__IsCore]  DEFAULT ((0)) FOR [IsCore]
GO
ALTER TABLE [dbo].[ServerInventory_SQL_AttributeMaster] ADD  CONSTRAINT [DF__ServerInventory_SQL_AttributeMaster__IsReadOnly]  DEFAULT ((0)) FOR [IsReadOnly]
GO
ALTER TABLE [dbo].[ServerInventory_SQL_AttributeMaster] ADD  CONSTRAINT [DF__ServerInventory_SQL_AttributeMaster__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[ServerInventory_SQL_ClusterNodes] ADD  CONSTRAINT [DF__SI_SQL_ClusterNodes__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[ServerInventory_SQL_Master] ADD  CONSTRAINT [DF__ServerInventory_SQL_Master__Enabled]  DEFAULT ((1)) FOR [Enabled]
GO
ALTER TABLE [dbo].[ServerInventory_SQL_Master] ADD  CONSTRAINT [DF__ServerInventory_SQL_Master__UseCredential]  DEFAULT ((0)) FOR [UseCredential]
GO
ALTER TABLE [etc].[MissingSQLBackups_RunIDs] ADD  CONSTRAINT [DF_MissingSQLBackups_RunIDs_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[Backups_BackupJobHistory] ADD  CONSTRAINT [DF__Backups_BackupJobHistory__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[Backups_Devices] ADD  CONSTRAINT [DF__Backups_Devices__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[Backups_Types] ADD  CONSTRAINT [DF__Backups_Types__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[ChangeControl_DeployHistory] ADD  CONSTRAINT [DF__ChangeControl_DeployHistory__IsError]  DEFAULT ((0)) FOR [IsError]
GO
ALTER TABLE [hist].[ChangeControl_DeployHistory] ADD  CONSTRAINT [DF__ChangeControl_DeployHistory__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[ChangeTracking_SQL_ServerDBObjectActionIDs] ADD  CONSTRAINT [DF__ChangeTracking_SQL_ServerDBObjectActionIDs__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[Collectors_Log] ADD  CONSTRAINT [DF__Collectors_Log__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[Collectors_ScriptIDs] ADD  CONSTRAINT [DF__Collectors_ScriptIDs__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[ConnectionCounts_RunIDs] ADD  CONSTRAINT [DF_ConnectionCounts_RunIDs_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[Deadlock_Deadlocks] ADD  CONSTRAINT [DF__Deadlock_Deadlocks__SampleDate]  DEFAULT (getdate()) FOR [SampleDate]
GO
ALTER TABLE [hist].[Deadlock_ProcessList] ADD  CONSTRAINT [DF__Deadlock_ProcessList__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[Deadlock_ResourceList] ADD  CONSTRAINT [DF__Deadlock_ResourceList__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[DTSStore_Categories] ADD  CONSTRAINT [DF__DTSStore_Categories__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[DTSStore_Descriptions] ADD  CONSTRAINT [DF__DTSStore_Descriptions__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[DTSStore_Owners] ADD  CONSTRAINT [DF__DTSStore_Owners__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[DTSStore_PackageNames] ADD  CONSTRAINT [DF__DTSStore_PackageNames__datecreated]  DEFAULT (getdate()) FOR [datecreated]
GO
ALTER TABLE [hist].[DTSStore_PackageStore] ADD  CONSTRAINT [DF__DTSStore_PackageStore__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[General_FileNames] ADD  CONSTRAINT [DF__General_FileNames__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[General_FullFileName] ADD  CONSTRAINT [DF__General_FullFileName__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[General_Paths] ADD  CONSTRAINT [DF__General_Paths__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[Jobs_SQL_JobHistory] ADD  CONSTRAINT [DF__Jobs_SQL___DateC__6BE40491]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[Jobs_SQL_Jobs] ADD  CONSTRAINT [DF__Jobs_SQL___DateC__6442E2C9]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[Jobs_SQL_Jobs] ADD  CONSTRAINT [DF__Jobs_SQL___LastS__65370702]  DEFAULT (getdate()) FOR [LastSeenOn]
GO
ALTER TABLE [hist].[Jobs_SQL_JobSteps] ADD  CONSTRAINT [DF__Jobs_SQL___DateC__690797E6]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[Metrics_QueryStats] ADD  CONSTRAINT [DF__Metrics_QueryStats__SampleDate]  DEFAULT (getdate()) FOR [SampleDate]
GO
ALTER TABLE [hist].[ServerInventory_SQL_ColumnNames] ADD  CONSTRAINT [DF__ServerInventory_SQL_ColumnNames__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[ServerInventory_SQL_ConfigurationValues] ADD  CONSTRAINT [DF__SQL_ConfigValues__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[ServerInventory_SQL_ConfigurationValues] ADD  CONSTRAINT [DF__SQL_ConfigValues__DateLastSeenOn]  DEFAULT (getdate()) FOR [DateLastSeenOn]
GO
ALTER TABLE [hist].[ServerInventory_SQL_DatabaseAttributeMaster] ADD  CONSTRAINT [DF__ServerInventory_SQL_DatabaseAttributeMaster__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[ServerInventory_SQL_DatabaseAttributeValues] ADD  CONSTRAINT [DF__ServerInventory_SQL_DatabaseAttributeValues__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[ServerInventory_SQL_DatabaseAttributeValues] ADD  CONSTRAINT [DF__ServerInventory_SQL_DatabaseAttributeValues__DateLastSeenOn]  DEFAULT (getdate()) FOR [DateLastSeenOn]
GO
ALTER TABLE [hist].[ServerInventory_SQL_IndexDetails] ADD  CONSTRAINT [DF__ServerInventory_SQL_IndexDetails__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[ServerInventory_SQL_IndexMaster] ADD  CONSTRAINT [DF__ServerInventory_SQL_IndexMaster__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[ServerInventory_SQL_IndexMaster] ADD  CONSTRAINT [DF__ServerInventory_SQL_IndexMaster__DateLastSeenOn]  DEFAULT (getdate()) FOR [DateLastSeenOn]
GO
ALTER TABLE [hist].[ServerInventory_SQL_IndexUsage] ADD  CONSTRAINT [DF__ServerInventory_SQL_IndexUsage__SampleDate]  DEFAULT (getdate()) FOR [SampleDate]
GO
ALTER TABLE [hist].[SpaceUsed_DatabaseSizes] ADD  CONSTRAINT [DF__SpaceUsed_DatabaseSizes__SampleDate]  DEFAULT (getdate()) FOR [SampleDate]
GO
ALTER TABLE [hist].[SpaceUsed_FileSizes] ADD  CONSTRAINT [DF__SpaceUsed_FileSizes__SampleDate]  DEFAULT (getdate()) FOR [SampleDate]
GO
ALTER TABLE [hist].[SQLRestarts_History] ADD  CONSTRAINT [DF__SQLRestarts_History__Type]  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [hist].[SQLRestarts_RunIDs] ADD  CONSTRAINT [DF_SQLRestarts_RunIDs_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[Users_UserNames] ADD  CONSTRAINT [DF__Users_UserNames__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[who_Logging] ADD  CONSTRAINT [who_Logging_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [hist].[Win32VolumeSizes] ADD  CONSTRAINT [DF__Win32Volu__DateC__1B9317B3]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [ref].[SQLServer_IsolationLevels] ADD  CONSTRAINT [DF__SQLServer_IsolationLevels__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [ref].[SQLServer_LockModes] ADD  CONSTRAINT [DF__SQLServer_LockModes__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [ref].[SQLServer_RunStatus] ADD  CONSTRAINT [DF__SQLServer_RunStatus__DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[Backups_BackupCommands]  WITH CHECK ADD  CONSTRAINT [FK__Backups_BackupCommands__BackupAgentID__Backups_BackupAgents__BackupAgentID] FOREIGN KEY([BackupAgentID])
REFERENCES [dbo].[Backups_BackupAgents] ([BackupAgentID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Backups_BackupCommands] CHECK CONSTRAINT [FK__Backups_BackupCommands__BackupAgentID__Backups_BackupAgents__BackupAgentID]
GO
ALTER TABLE [dbo].[Backups_BackupCommands]  WITH CHECK ADD  CONSTRAINT [FK__Backups_BackupCommands__BackupTypeID__Backups_BackupTypes__BackupTypeID] FOREIGN KEY([BackupTypeID])
REFERENCES [dbo].[Backups_BackupTypes] ([BackupTypeID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Backups_BackupCommands] CHECK CONSTRAINT [FK__Backups_BackupCommands__BackupTypeID__Backups_BackupTypes__BackupTypeID]
GO
ALTER TABLE [dbo].[Backups_DBsToBackup]  WITH CHECK ADD  CONSTRAINT [FK__Backups_DBsToBackup__JobScheduleID__Backups_JobSchedules__JobScheduleID] FOREIGN KEY([JobScheduleID])
REFERENCES [dbo].[Backups_JobSchedules] ([JobScheduleID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Backups_DBsToBackup] CHECK CONSTRAINT [FK__Backups_DBsToBackup__JobScheduleID__Backups_JobSchedules__JobScheduleID]
GO
ALTER TABLE [dbo].[Backups_DBsToNotBackup]  WITH CHECK ADD  CONSTRAINT [FK__Backups_DBsToNotBackup__JobScheduleID__Backups_JobSchedules__JobScheduleID] FOREIGN KEY([JobScheduleID])
REFERENCES [dbo].[Backups_JobSchedules] ([JobScheduleID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Backups_DBsToNotBackup] CHECK CONSTRAINT [FK__Backups_DBsToNotBackup__JobScheduleID__Backups_JobSchedules__JobScheduleID]
GO
ALTER TABLE [dbo].[Backups_Jobs]  WITH CHECK ADD  CONSTRAINT [FK__Backups_Jobs__ServerID__ServerInventory_SQL_Master__ServerID] FOREIGN KEY([ServerID])
REFERENCES [dbo].[ServerInventory_SQL_Master] ([ServerID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Backups_Jobs] CHECK CONSTRAINT [FK__Backups_Jobs__ServerID__ServerInventory_SQL_Master__ServerID]
GO
ALTER TABLE [dbo].[Backups_JobSchedules]  WITH CHECK ADD  CONSTRAINT [FK__Backups_JobSchedules__BackupAgentID__Backups_BackupAgents__BackupAgentID] FOREIGN KEY([BackupAgentID])
REFERENCES [dbo].[Backups_BackupAgents] ([BackupAgentID])
GO
ALTER TABLE [dbo].[Backups_JobSchedules] CHECK CONSTRAINT [FK__Backups_JobSchedules__BackupAgentID__Backups_BackupAgents__BackupAgentID]
GO
ALTER TABLE [dbo].[Backups_JobSchedules]  WITH CHECK ADD  CONSTRAINT [FK__Backups_JobSchedules__BackupTypeID__Backups_BackupTypes__BackupTypeID] FOREIGN KEY([BackupTypeID])
REFERENCES [dbo].[Backups_BackupTypes] ([BackupTypeID])
GO
ALTER TABLE [dbo].[Backups_JobSchedules] CHECK CONSTRAINT [FK__Backups_JobSchedules__BackupTypeID__Backups_BackupTypes__BackupTypeID]
GO
ALTER TABLE [dbo].[Backups_JobSchedules]  WITH CHECK ADD  CONSTRAINT [FK__Backups_JobSchedules__JobID__Backups_Jobs__JobID] FOREIGN KEY([JobID])
REFERENCES [dbo].[Backups_Jobs] ([JobID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Backups_JobSchedules] CHECK CONSTRAINT [FK__Backups_JobSchedules__JobID__Backups_Jobs__JobID]
GO
ALTER TABLE [dbo].[ChangeControl_ChangeSet]  WITH CHECK ADD  CONSTRAINT [FK__ChangeControl_ChangeSet__PackageID__ChangeControl_PackageMaster_PackageID] FOREIGN KEY([PackageID])
REFERENCES [dbo].[ChangeControl_PackageMaster] ([PackageID])
GO
ALTER TABLE [dbo].[ChangeControl_ChangeSet] CHECK CONSTRAINT [FK__ChangeControl_ChangeSet__PackageID__ChangeControl_PackageMaster_PackageID]
GO
ALTER TABLE [dbo].[ChangeControl_ChangeSetDetail]  WITH CHECK ADD  CONSTRAINT [FK__ChangeControl_ChangeSet__ScriptID__ChangeControl_ScriptMaster__ScriptID] FOREIGN KEY([ScriptID])
REFERENCES [hist].[ChangeControl_ScriptMaster] ([ScriptID])
GO
ALTER TABLE [dbo].[ChangeControl_ChangeSetDetail] CHECK CONSTRAINT [FK__ChangeControl_ChangeSet__ScriptID__ChangeControl_ScriptMaster__ScriptID]
GO
ALTER TABLE [dbo].[ChangeControl_ChangeSetDetail]  WITH CHECK ADD  CONSTRAINT [FK__ChangeControl_ChangeSetDetail__ChangeSetID__ChangeControl_ChangeSet__ChangeSetID] FOREIGN KEY([ChangeSetID])
REFERENCES [dbo].[ChangeControl_ChangeSet] ([ChangeSetID])
GO
ALTER TABLE [dbo].[ChangeControl_ChangeSetDetail] CHECK CONSTRAINT [FK__ChangeControl_ChangeSetDetail__ChangeSetID__ChangeControl_ChangeSet__ChangeSetID]
GO
ALTER TABLE [dbo].[ChangeControl_DeployDetail]  WITH CHECK ADD  CONSTRAINT [FK__ChangeControl_DeployDetail__DeployID__ChangeControl_DeployMaster__DeployID] FOREIGN KEY([DeployID])
REFERENCES [dbo].[ChangeControl_DeployMaster] ([DeployID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ChangeControl_DeployDetail] CHECK CONSTRAINT [FK__ChangeControl_DeployDetail__DeployID__ChangeControl_DeployMaster__DeployID]
GO
ALTER TABLE [dbo].[ChangeControl_DeployDetail]  WITH CHECK ADD  CONSTRAINT [FK__ChangeControl_DeployMaster__ScriptID__ChangeControl_ScriptMaster__ScriptID] FOREIGN KEY([ScriptID])
REFERENCES [hist].[ChangeControl_ScriptMaster] ([ScriptID])
GO
ALTER TABLE [dbo].[ChangeControl_DeployDetail] CHECK CONSTRAINT [FK__ChangeControl_DeployMaster__ScriptID__ChangeControl_ScriptMaster__ScriptID]
GO
ALTER TABLE [dbo].[NTPermissions_EnvironmentExceptions]  WITH CHECK ADD  CONSTRAINT [FK__NTPermissions_EnvironmentExceptions__EnvironmentID__ServerInventory_Environments__EnvironmentID] FOREIGN KEY([EnvironmentID])
REFERENCES [dbo].[ServerInventory_Environments] ([EnvironmentID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[NTPermissions_EnvironmentExceptions] CHECK CONSTRAINT [FK__NTPermissions_EnvironmentExceptions__EnvironmentID__ServerInventory_Environments__EnvironmentID]
GO
ALTER TABLE [dbo].[NTPermissions_EnvironmentExceptions]  WITH CHECK ADD  CONSTRAINT [FK__NTPermissions_EnvironmentExceptions__StatementID__NTPermissions_PermissionSQLStatements__StatementID] FOREIGN KEY([StatementID])
REFERENCES [dbo].[NTPermissions_PermissionSQLStatements] ([StatementID])
GO
ALTER TABLE [dbo].[NTPermissions_EnvironmentExceptions] CHECK CONSTRAINT [FK__NTPermissions_EnvironmentExceptions__StatementID__NTPermissions_PermissionSQLStatements__StatementID]
GO
ALTER TABLE [dbo].[NTPermissions_ServerExceptions]  WITH CHECK ADD  CONSTRAINT [FK__NTPermissions_ServerExceptions__ServerID__ServerInventory_SQL_Master__ServerID] FOREIGN KEY([ServerID])
REFERENCES [dbo].[ServerInventory_SQL_Master] ([ServerID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[NTPermissions_ServerExceptions] CHECK CONSTRAINT [FK__NTPermissions_ServerExceptions__ServerID__ServerInventory_SQL_Master__ServerID]
GO
ALTER TABLE [dbo].[NTPermissions_ServerExceptions]  WITH CHECK ADD  CONSTRAINT [FK__NTPermissions_ServerExceptions__StatementID__NTPermissions_PermissionSQLStatements__StatementID] FOREIGN KEY([StatementID])
REFERENCES [dbo].[NTPermissions_PermissionSQLStatements] ([StatementID])
GO
ALTER TABLE [dbo].[NTPermissions_ServerExceptions] CHECK CONSTRAINT [FK__NTPermissions_ServerExceptions__StatementID__NTPermissions_PermissionSQLStatements__StatementID]
GO
ALTER TABLE [dbo].[ServerInventory_ApplicationOwners_Xref]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_ApplicationOwners_Xref__ApplicationID__ServerInventory_Applications__ApplicationID] FOREIGN KEY([ApplicationID])
REFERENCES [dbo].[ServerInventory_Applications] ([ApplicationID])
GO
ALTER TABLE [dbo].[ServerInventory_ApplicationOwners_Xref] CHECK CONSTRAINT [FK__ServerInventory_ApplicationOwners_Xref__ApplicationID__ServerInventory_Applications__ApplicationID]
GO
ALTER TABLE [dbo].[ServerInventory_ApplicationOwners_Xref]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_ApplicationOwners_Xref__OwnerID__ServerInventory_Owners__OwnerID] FOREIGN KEY([OwnerID])
REFERENCES [dbo].[ServerInventory_Owners] ([OwnerID])
GO
ALTER TABLE [dbo].[ServerInventory_ApplicationOwners_Xref] CHECK CONSTRAINT [FK__ServerInventory_ApplicationOwners_Xref__OwnerID__ServerInventory_Owners__OwnerID]
GO
ALTER TABLE [dbo].[ServerInventory_DatabaseOwners_Xref]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_DatabaseOwners_Xref__DatabaseID__ServerInventory_SQL_DatabaseIDs__DatabaseID] FOREIGN KEY([DatabaseID])
REFERENCES [hist].[ServerInventory_SQL_DatabaseIDs] ([DatabaseID])
GO
ALTER TABLE [dbo].[ServerInventory_DatabaseOwners_Xref] CHECK CONSTRAINT [FK__ServerInventory_DatabaseOwners_Xref__DatabaseID__ServerInventory_SQL_DatabaseIDs__DatabaseID]
GO
ALTER TABLE [dbo].[ServerInventory_DatabaseOwners_Xref]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_DatabaseOwners_Xref__OwnerID__ServerInventory_Owners__OwnerID] FOREIGN KEY([OwnerID])
REFERENCES [dbo].[ServerInventory_Owners] ([OwnerID])
GO
ALTER TABLE [dbo].[ServerInventory_DatabaseOwners_Xref] CHECK CONSTRAINT [FK__ServerInventory_DatabaseOwners_Xref__OwnerID__ServerInventory_Owners__OwnerID]
GO
ALTER TABLE [dbo].[ServerInventory_DatabaseOwners_Xref]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_DatabaseOwners_Xref__ServerID__ServerInventory_SQL_Master__ServerID] FOREIGN KEY([ServerID])
REFERENCES [dbo].[ServerInventory_SQL_Master] ([ServerID])
GO
ALTER TABLE [dbo].[ServerInventory_DatabaseOwners_Xref] CHECK CONSTRAINT [FK__ServerInventory_DatabaseOwners_Xref__ServerID__ServerInventory_SQL_Master__ServerID]
GO
ALTER TABLE [dbo].[ServerInventory_ServerApplications_Xref]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_ServerApplications_Xref__ApplicationID__ServerInventory_Applications__ApplicationID] FOREIGN KEY([ApplicationID])
REFERENCES [dbo].[ServerInventory_Applications] ([ApplicationID])
GO
ALTER TABLE [dbo].[ServerInventory_ServerApplications_Xref] CHECK CONSTRAINT [FK__ServerInventory_ServerApplications_Xref__ApplicationID__ServerInventory_Applications__ApplicationID]
GO
ALTER TABLE [dbo].[ServerInventory_ServerApplications_Xref]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_ServerApplications_Xref__ServerID__ServerInventory_SQL_Master__ServerID] FOREIGN KEY([ServerID])
REFERENCES [dbo].[ServerInventory_SQL_Master] ([ServerID])
GO
ALTER TABLE [dbo].[ServerInventory_ServerApplications_Xref] CHECK CONSTRAINT [FK__ServerInventory_ServerApplications_Xref__ServerID__ServerInventory_SQL_Master__ServerID]
GO
ALTER TABLE [dbo].[ServerInventory_ServerOwners_Xref]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_ServerOwners_Xref__OwnerID__ServerInventory_Owners__OwnerID] FOREIGN KEY([OwnerID])
REFERENCES [dbo].[ServerInventory_Owners] ([OwnerID])
GO
ALTER TABLE [dbo].[ServerInventory_ServerOwners_Xref] CHECK CONSTRAINT [FK__ServerInventory_ServerOwners_Xref__OwnerID__ServerInventory_Owners__OwnerID]
GO
ALTER TABLE [dbo].[ServerInventory_ServerOwners_Xref]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_ServerOwners_Xref__ServerID__ServerInventory_SQL_Master__ServerID] FOREIGN KEY([ServerID])
REFERENCES [dbo].[ServerInventory_SQL_Master] ([ServerID])
GO
ALTER TABLE [dbo].[ServerInventory_ServerOwners_Xref] CHECK CONSTRAINT [FK__ServerInventory_ServerOwners_Xref__ServerID__ServerInventory_SQL_Master__ServerID]
GO
ALTER TABLE [dbo].[ServerInventory_SQL_AttributeList]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_SQL_AttributeList__AttribID__ServerInventory_SQL_AttributeMaster__AttribID] FOREIGN KEY([AttributeID])
REFERENCES [dbo].[ServerInventory_SQL_AttributeMaster] ([AttributeID])
GO
ALTER TABLE [dbo].[ServerInventory_SQL_AttributeList] CHECK CONSTRAINT [FK__ServerInventory_SQL_AttributeList__AttribID__ServerInventory_SQL_AttributeMaster__AttribID]
GO
ALTER TABLE [dbo].[ServerInventory_SQL_ClusterNodes]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_SQL_ClusterNodes__HistServerID__ServerInventory_ServerIDs__HistServerID] FOREIGN KEY([HistServerID])
REFERENCES [hist].[ServerInventory_ServerIDs] ([HistServerID])
GO
ALTER TABLE [dbo].[ServerInventory_SQL_ClusterNodes] CHECK CONSTRAINT [FK__ServerInventory_SQL_ClusterNodes__HistServerID__ServerInventory_ServerIDs__HistServerID]
GO
ALTER TABLE [dbo].[ServerInventory_SQL_ClusterNodes]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_SQL_ClusterNodes__ServerID__ServerInventory_SQL_Master__ServerID] FOREIGN KEY([ServerID])
REFERENCES [dbo].[ServerInventory_SQL_Master] ([ServerID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ServerInventory_SQL_ClusterNodes] CHECK CONSTRAINT [FK__ServerInventory_SQL_ClusterNodes__ServerID__ServerInventory_SQL_Master__ServerID]
GO
ALTER TABLE [dbo].[ServerInventory_SQL_Master]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_SQL_Master__BusinessUnitID__ServerInventory_BusinessUnits__BusinessUnitID] FOREIGN KEY([BusinessUnitID])
REFERENCES [dbo].[ServerInventory_BusinessUnits] ([BusinessUnitID])
GO
ALTER TABLE [dbo].[ServerInventory_SQL_Master] CHECK CONSTRAINT [FK__ServerInventory_SQL_Master__BusinessUnitID__ServerInventory_BusinessUnits__BusinessUnitID]
GO
ALTER TABLE [dbo].[ServerInventory_SQL_Master]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_SQL_Master__EditionID__ServerInventory_SQL_Editions__EditionID] FOREIGN KEY([EditionID])
REFERENCES [dbo].[ServerInventory_SQL_Editions] ([EditionID])
GO
ALTER TABLE [dbo].[ServerInventory_SQL_Master] CHECK CONSTRAINT [FK__ServerInventory_SQL_Master__EditionID__ServerInventory_SQL_Editions__EditionID]
GO
ALTER TABLE [dbo].[ServerInventory_SQL_Master]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_SQL_Master__EnvironmentID__ServerInventory_Environments__EnvironmentID] FOREIGN KEY([EnvironmentID])
REFERENCES [dbo].[ServerInventory_Environments] ([EnvironmentID])
GO
ALTER TABLE [dbo].[ServerInventory_SQL_Master] CHECK CONSTRAINT [FK__ServerInventory_SQL_Master__EnvironmentID__ServerInventory_Environments__EnvironmentID]
GO
ALTER TABLE [dbo].[ServerInventory_SQL_sysjobs]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_SQL_sysjobs__ServerID__ServerInventory_SQL_Master__ServerID] FOREIGN KEY([ServerID])
REFERENCES [dbo].[ServerInventory_SQL_Master] ([ServerID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ServerInventory_SQL_sysjobs] CHECK CONSTRAINT [FK__ServerInventory_SQL_sysjobs__ServerID__ServerInventory_SQL_Master__ServerID]
GO
ALTER TABLE [hist].[Backups_History]  WITH CHECK ADD  CONSTRAINT [FK__Backups_History__BUTypeID__Backups_Types__BackupTypeID] FOREIGN KEY([BUTypeID])
REFERENCES [hist].[Backups_Types] ([BackupTypeID])
GO
ALTER TABLE [hist].[Backups_History] CHECK CONSTRAINT [FK__Backups_History__BUTypeID__Backups_Types__BackupTypeID]
GO
ALTER TABLE [hist].[Backups_History]  WITH CHECK ADD  CONSTRAINT [FK__Backups_History__DatabaseID__ServerInventory_SQL_DatabaseIDs__DatabaseID] FOREIGN KEY([DatabaseID])
REFERENCES [hist].[ServerInventory_SQL_DatabaseIDs] ([DatabaseID])
GO
ALTER TABLE [hist].[Backups_History] CHECK CONSTRAINT [FK__Backups_History__DatabaseID__ServerInventory_SQL_DatabaseIDs__DatabaseID]
GO
ALTER TABLE [hist].[Backups_History]  WITH CHECK ADD  CONSTRAINT [FK__Backups_History__HistServerID__ServerInventory_ServerIDs__HistServerID] FOREIGN KEY([HistServerID])
REFERENCES [hist].[ServerInventory_ServerIDs] ([HistServerID])
GO
ALTER TABLE [hist].[Backups_History] CHECK CONSTRAINT [FK__Backups_History__HistServerID__ServerInventory_ServerIDs__HistServerID]
GO
ALTER TABLE [hist].[Backups_History]  WITH CHECK ADD  CONSTRAINT [FK__Backups_History__LogicalDeviceID__Backups_Devices__DeviceID] FOREIGN KEY([LogicalDeviceID])
REFERENCES [hist].[Backups_Devices] ([DeviceID])
GO
ALTER TABLE [hist].[Backups_History] CHECK CONSTRAINT [FK__Backups_History__LogicalDeviceID__Backups_Devices__DeviceID]
GO
ALTER TABLE [hist].[Backups_History]  WITH CHECK ADD  CONSTRAINT [FK__Backups_History__MachineID__ServerInventory_ServerIDs__HistServerID] FOREIGN KEY([MachineID])
REFERENCES [hist].[ServerInventory_ServerIDs] ([HistServerID])
GO
ALTER TABLE [hist].[Backups_History] CHECK CONSTRAINT [FK__Backups_History__MachineID__ServerInventory_ServerIDs__HistServerID]
GO
ALTER TABLE [hist].[Backups_History]  WITH CHECK ADD  CONSTRAINT [FK__Backups_History__PhysicalDeviceID__Backups_Devices__DeviceID] FOREIGN KEY([PhysicalDeviceID])
REFERENCES [hist].[Backups_Devices] ([DeviceID])
GO
ALTER TABLE [hist].[Backups_History] CHECK CONSTRAINT [FK__Backups_History__PhysicalDeviceID__Backups_Devices__DeviceID]
GO
ALTER TABLE [hist].[Backups_History]  WITH CHECK ADD  CONSTRAINT [FK__Backups_History__UserID__Users_UserNames__UserID] FOREIGN KEY([UserID])
REFERENCES [hist].[Users_UserNames] ([UserID])
GO
ALTER TABLE [hist].[Backups_History] CHECK CONSTRAINT [FK__Backups_History__UserID__Users_UserNames__UserID]
GO
ALTER TABLE [hist].[ChangeControl_DeployHistory]  WITH CHECK ADD  CONSTRAINT [FK__ChangeControl_DeployHistory__HistServerID__ServerInventory_ServerIDs__HistServerID] FOREIGN KEY([HistServerID])
REFERENCES [hist].[ServerInventory_ServerIDs] ([HistServerID])
GO
ALTER TABLE [hist].[ChangeControl_DeployHistory] CHECK CONSTRAINT [FK__ChangeControl_DeployHistory__HistServerID__ServerInventory_ServerIDs__HistServerID]
GO
ALTER TABLE [hist].[ChangeControl_DeployHistory]  WITH CHECK ADD  CONSTRAINT [FK__ChangeControl_DeployHistory__ScriptID__ChangeControl_ScriptMaster__ScriptID] FOREIGN KEY([ScriptID])
REFERENCES [hist].[ChangeControl_ScriptMaster] ([ScriptID])
GO
ALTER TABLE [hist].[ChangeControl_DeployHistory] CHECK CONSTRAINT [FK__ChangeControl_DeployHistory__ScriptID__ChangeControl_ScriptMaster__ScriptID]
GO
ALTER TABLE [hist].[ChangeControl_ScriptDatabaseXref]  WITH CHECK ADD  CONSTRAINT [FK__ChangeControl_ScriptDatabaseXref__DatabaseID__ServerInventory_SQL_DatabaseIDs__DatabaseID] FOREIGN KEY([DatabaseID])
REFERENCES [hist].[ServerInventory_SQL_DatabaseIDs] ([DatabaseID])
GO
ALTER TABLE [hist].[ChangeControl_ScriptDatabaseXref] CHECK CONSTRAINT [FK__ChangeControl_ScriptDatabaseXref__DatabaseID__ServerInventory_SQL_DatabaseIDs__DatabaseID]
GO
ALTER TABLE [hist].[ChangeControl_ScriptDatabaseXref]  WITH CHECK ADD  CONSTRAINT [FK__ChangeControl_ScriptDatabaseXref__ScriptID__ChangeControl_ScriptMaster__ScriptID] FOREIGN KEY([ScriptID])
REFERENCES [hist].[ChangeControl_ScriptMaster] ([ScriptID])
GO
ALTER TABLE [hist].[ChangeControl_ScriptDatabaseXref] CHECK CONSTRAINT [FK__ChangeControl_ScriptDatabaseXref__ScriptID__ChangeControl_ScriptMaster__ScriptID]
GO
ALTER TABLE [hist].[ChangeTracking_SQL_ObjectIDs]  WITH CHECK ADD  CONSTRAINT [FK__ChangeTracking_SQL_ObjectIDs__ObjectTypeID__ChangeTracking_SQL_ObjectTypeIDs__ObjectTypeId] FOREIGN KEY([ObjectTypeID])
REFERENCES [hist].[ChangeTracking_SQL_ObjectTypeIDs] ([ObjectTypeId])
GO
ALTER TABLE [hist].[ChangeTracking_SQL_ObjectIDs] CHECK CONSTRAINT [FK__ChangeTracking_SQL_ObjectIDs__ObjectTypeID__ChangeTracking_SQL_ObjectTypeIDs__ObjectTypeId]
GO
ALTER TABLE [hist].[ChangeTracking_SQL_ServerDBObjectActionIDs]  WITH CHECK ADD  CONSTRAINT [FK__ChangeTracking_SQL_ServerDBObjectActionIDs__ActionID__ChangeTracking_SQL_ActionIDs__ActionID] FOREIGN KEY([ActionID])
REFERENCES [hist].[ChangeTracking_SQL_ActionIDs] ([ActionID])
GO
ALTER TABLE [hist].[ChangeTracking_SQL_ServerDBObjectActionIDs] CHECK CONSTRAINT [FK__ChangeTracking_SQL_ServerDBObjectActionIDs__ActionID__ChangeTracking_SQL_ActionIDs__ActionID]
GO
ALTER TABLE [hist].[ChangeTracking_SQL_ServerDBObjectActionIDs]  WITH CHECK ADD  CONSTRAINT [FK__ChangeTracking_SQL_ServerDBObjectActionIDs__DatabaseID__ServerInventory_SQL_DatabaseIDs__DatabaseID] FOREIGN KEY([DatabaseID])
REFERENCES [hist].[ServerInventory_SQL_DatabaseIDs] ([DatabaseID])
GO
ALTER TABLE [hist].[ChangeTracking_SQL_ServerDBObjectActionIDs] CHECK CONSTRAINT [FK__ChangeTracking_SQL_ServerDBObjectActionIDs__DatabaseID__ServerInventory_SQL_DatabaseIDs__DatabaseID]
GO
ALTER TABLE [hist].[ChangeTracking_SQL_ServerDBObjectActionIDs]  WITH CHECK ADD  CONSTRAINT [FK__ChangeTracking_SQL_ServerDBObjectActionIDs__HistServerID__ServerInventory_ServerIDs__HistServerID] FOREIGN KEY([HistServerID])
REFERENCES [hist].[ServerInventory_ServerIDs] ([HistServerID])
GO
ALTER TABLE [hist].[ChangeTracking_SQL_ServerDBObjectActionIDs] CHECK CONSTRAINT [FK__ChangeTracking_SQL_ServerDBObjectActionIDs__HistServerID__ServerInventory_ServerIDs__HistServerID]
GO
ALTER TABLE [hist].[ChangeTracking_SQL_ServerDBObjectActionIDs]  WITH CHECK ADD  CONSTRAINT [FK__ChangeTracking_SQL_ServerDBObjectActionIDs__ObjectID__ChangeTracking_SQL_ObjectIDs__ObjectID] FOREIGN KEY([ObjectID])
REFERENCES [hist].[ChangeTracking_SQL_ObjectIDs] ([ObjectID])
GO
ALTER TABLE [hist].[ChangeTracking_SQL_ServerDBObjectActionIDs] CHECK CONSTRAINT [FK__ChangeTracking_SQL_ServerDBObjectActionIDs__ObjectID__ChangeTracking_SQL_ObjectIDs__ObjectID]
GO
ALTER TABLE [hist].[Collectors_Log]  WITH CHECK ADD  CONSTRAINT [FK__Collectors_Log__HistCollectorLogScriptID__Collectors_ScriptIDs__HistCollectorLogScriptID] FOREIGN KEY([HistCollectorLogScriptID])
REFERENCES [hist].[Collectors_ScriptIDs] ([HistCollectorLogScriptID])
GO
ALTER TABLE [hist].[Collectors_Log] CHECK CONSTRAINT [FK__Collectors_Log__HistCollectorLogScriptID__Collectors_ScriptIDs__HistCollectorLogScriptID]
GO
ALTER TABLE [hist].[Collectors_Log]  WITH CHECK ADD  CONSTRAINT [FK__Collectors_Log__HistCollectorLogStateID__Collectors_StateIDs__HistCollectorLogStateID] FOREIGN KEY([HistCollectorLogStateID])
REFERENCES [hist].[Collectors_StateIDs] ([HistCollectorLogStateID])
GO
ALTER TABLE [hist].[Collectors_Log] CHECK CONSTRAINT [FK__Collectors_Log__HistCollectorLogStateID__Collectors_StateIDs__HistCollectorLogStateID]
GO
ALTER TABLE [hist].[Collectors_Log]  WITH CHECK ADD  CONSTRAINT [FK__Collectors_Log__HistServerID__ServerInventory_ServerIDs__HistServerID] FOREIGN KEY([HistServerID])
REFERENCES [hist].[ServerInventory_ServerIDs] ([HistServerID])
GO
ALTER TABLE [hist].[Collectors_Log] CHECK CONSTRAINT [FK__Collectors_Log__HistServerID__ServerInventory_ServerIDs__HistServerID]
GO
ALTER TABLE [hist].[Collectors_Log]  WITH CHECK ADD  CONSTRAINT [FK__Collectors_Log__HistUserID__Users_UserNames__UserID] FOREIGN KEY([HistUserID])
REFERENCES [hist].[Users_UserNames] ([UserID])
GO
ALTER TABLE [hist].[Collectors_Log] CHECK CONSTRAINT [FK__Collectors_Log__HistUserID__Users_UserNames__UserID]
GO
ALTER TABLE [hist].[DatabaseMaintenance_CheckDB_Errors]  WITH CHECK ADD  CONSTRAINT [FK__DatabaseMaintenance_CheckDB_Errors__DatabaseID__ServerInventory_SQL_DatabaseIDs__DatabaseID] FOREIGN KEY([DatabaseID])
REFERENCES [hist].[ServerInventory_SQL_DatabaseIDs] ([DatabaseID])
GO
ALTER TABLE [hist].[DatabaseMaintenance_CheckDB_Errors] CHECK CONSTRAINT [FK__DatabaseMaintenance_CheckDB_Errors__DatabaseID__ServerInventory_SQL_DatabaseIDs__DatabaseID]
GO
ALTER TABLE [hist].[DatabaseMaintenance_CheckDB_Errors]  WITH CHECK ADD  CONSTRAINT [FK__DatabaseMaintenance_CheckDB_Errors__HistServerID__ServerInventory_ServerIDs__HistServerID] FOREIGN KEY([HistServerID])
REFERENCES [hist].[ServerInventory_ServerIDs] ([HistServerID])
GO
ALTER TABLE [hist].[DatabaseMaintenance_CheckDB_Errors] CHECK CONSTRAINT [FK__DatabaseMaintenance_CheckDB_Errors__HistServerID__ServerInventory_ServerIDs__HistServerID]
GO
ALTER TABLE [hist].[DatabaseMaintenance_CheckDB_OK]  WITH CHECK ADD  CONSTRAINT [FK__DatabaseMaintenance_CheckDB_OK__DatabaseID__ServerInventory_SQL_DatabaseIDs__DatabaseID] FOREIGN KEY([DatabaseID])
REFERENCES [hist].[ServerInventory_SQL_DatabaseIDs] ([DatabaseID])
GO
ALTER TABLE [hist].[DatabaseMaintenance_CheckDB_OK] CHECK CONSTRAINT [FK__DatabaseMaintenance_CheckDB_OK__DatabaseID__ServerInventory_SQL_DatabaseIDs__DatabaseID]
GO
ALTER TABLE [hist].[DatabaseMaintenance_CheckDB_OK]  WITH CHECK ADD  CONSTRAINT [FK__DatabaseMaintenance_CheckDB_OK__HistServerID__ServerInventory_ServerIDs__HistServerID] FOREIGN KEY([HistServerID])
REFERENCES [hist].[ServerInventory_ServerIDs] ([HistServerID])
GO
ALTER TABLE [hist].[DatabaseMaintenance_CheckDB_OK] CHECK CONSTRAINT [FK__DatabaseMaintenance_CheckDB_OK__HistServerID__ServerInventory_ServerIDs__HistServerID]
GO
ALTER TABLE [hist].[Deadlock_Deadlocks]  WITH CHECK ADD  CONSTRAINT [FK__Deadlock_Deadlocks__HistServerID__ServerInventory_ServerIDs__HistServerID] FOREIGN KEY([HistServerID])
REFERENCES [hist].[ServerInventory_ServerIDs] ([HistServerID])
GO
ALTER TABLE [hist].[Deadlock_Deadlocks] CHECK CONSTRAINT [FK__Deadlock_Deadlocks__HistServerID__ServerInventory_ServerIDs__HistServerID]
GO
ALTER TABLE [hist].[Deadlock_Process_ExecutionStack]  WITH CHECK ADD  CONSTRAINT [FK__Deadlock_Process_ExecutionStack__HistDeadlockProcessID__Deadlock_ProcessList__HistDeadlockProcessID] FOREIGN KEY([HistDeadlockProcessID])
REFERENCES [hist].[Deadlock_ProcessList] ([HistDeadlockProcessID])
GO
ALTER TABLE [hist].[Deadlock_Process_ExecutionStack] CHECK CONSTRAINT [FK__Deadlock_Process_ExecutionStack__HistDeadlockProcessID__Deadlock_ProcessList__HistDeadlockProcessID]
GO
ALTER TABLE [hist].[Deadlock_ProcessList]  WITH CHECK ADD  CONSTRAINT [FK__Deadlock_ProcessList__HistDeadlockID__Deadlock_Deadlocks__HistDeadlockID] FOREIGN KEY([HistDeadlockID])
REFERENCES [hist].[Deadlock_Deadlocks] ([HistDeadlockID])
ON DELETE CASCADE
GO
ALTER TABLE [hist].[Deadlock_ProcessList] CHECK CONSTRAINT [FK__Deadlock_ProcessList__HistDeadlockID__Deadlock_Deadlocks__HistDeadlockID]
GO
ALTER TABLE [hist].[Deadlock_ProcessList]  WITH CHECK ADD  CONSTRAINT [FK__Deadlock_ProcessList__HistServerID__ServerInventory_ServerIDs__HistServerID] FOREIGN KEY([hostnameHistServerID])
REFERENCES [hist].[ServerInventory_ServerIDs] ([HistServerID])
GO
ALTER TABLE [hist].[Deadlock_ProcessList] CHECK CONSTRAINT [FK__Deadlock_ProcessList__HistServerID__ServerInventory_ServerIDs__HistServerID]
GO
ALTER TABLE [hist].[Deadlock_ProcessList]  WITH CHECK ADD  CONSTRAINT [FK__Deadlock_ProcessList__loginnameHistUserID__Users_UserNames__HistUserID] FOREIGN KEY([loginnameHistUserID])
REFERENCES [hist].[Users_UserNames] ([UserID])
GO
ALTER TABLE [hist].[Deadlock_ProcessList] CHECK CONSTRAINT [FK__Deadlock_ProcessList__loginnameHistUserID__Users_UserNames__HistUserID]
GO
ALTER TABLE [hist].[Deadlock_ProcessList]  WITH CHECK ADD  CONSTRAINT [FK__Deadlock_ProcessList__modeRefLockModeID__SQLServer_LockModes__RefLockModeID] FOREIGN KEY([modeRefLockModeID])
REFERENCES [ref].[SQLServer_LockModes] ([RefLockModeID])
GO
ALTER TABLE [hist].[Deadlock_ProcessList] CHECK CONSTRAINT [FK__Deadlock_ProcessList__modeRefLockModeID__SQLServer_LockModes__RefLockModeID]
GO
ALTER TABLE [hist].[Deadlock_ProcessList]  WITH CHECK ADD  CONSTRAINT [FK__Deadlock_ProcessList__RefIsolationLevelID__SQLServer_IsolationLevels__RefIsolationLevelID] FOREIGN KEY([RefIsolationLevelID])
REFERENCES [ref].[SQLServer_IsolationLevels] ([RefIsolationLevelID])
GO
ALTER TABLE [hist].[Deadlock_ProcessList] CHECK CONSTRAINT [FK__Deadlock_ProcessList__RefIsolationLevelID__SQLServer_IsolationLevels__RefIsolationLevelID]
GO
ALTER TABLE [hist].[Deadlock_ProcessList]  WITH CHECK ADD  CONSTRAINT [FK__Deadlock_ProcessList__RefRunStatusID__SQLServer_RunStatus__RefRunStatusID] FOREIGN KEY([RefRunStatusID])
REFERENCES [ref].[SQLServer_RunStatus] ([RefRunStatusID])
GO
ALTER TABLE [hist].[Deadlock_ProcessList] CHECK CONSTRAINT [FK__Deadlock_ProcessList__RefRunStatusID__SQLServer_RunStatus__RefRunStatusID]
GO
ALTER TABLE [hist].[Deadlock_ResourceList]  WITH CHECK ADD  CONSTRAINT [FK__Deadlock_ResourceList__dbnameHistDatabaseID__ServerInventory_SQL_DatabaseIDs__DatabaseID] FOREIGN KEY([dbnameHistDatabaseID])
REFERENCES [hist].[ServerInventory_SQL_DatabaseIDs] ([DatabaseID])
GO
ALTER TABLE [hist].[Deadlock_ResourceList] CHECK CONSTRAINT [FK__Deadlock_ResourceList__dbnameHistDatabaseID__ServerInventory_SQL_DatabaseIDs__DatabaseID]
GO
ALTER TABLE [hist].[Deadlock_ResourceList]  WITH CHECK ADD  CONSTRAINT [FK__Deadlock_ResourceList__HistDeadlockID__Deadlock_Deadlocks__HistDeadlockID] FOREIGN KEY([HistDeadlockID])
REFERENCES [hist].[Deadlock_Deadlocks] ([HistDeadlockID])
ON DELETE CASCADE
GO
ALTER TABLE [hist].[Deadlock_ResourceList] CHECK CONSTRAINT [FK__Deadlock_ResourceList__HistDeadlockID__Deadlock_Deadlocks__HistDeadlockID]
GO
ALTER TABLE [hist].[Deadlock_ResourceList]  WITH CHECK ADD  CONSTRAINT [FK__Deadlock_ResourceList__modeRefLockModeID__SQLServer_LockModes__RefLockModeID] FOREIGN KEY([modeRefLockModeID])
REFERENCES [ref].[SQLServer_LockModes] ([RefLockModeID])
GO
ALTER TABLE [hist].[Deadlock_ResourceList] CHECK CONSTRAINT [FK__Deadlock_ResourceList__modeRefLockModeID__SQLServer_LockModes__RefLockModeID]
GO
ALTER TABLE [hist].[Deadlock_ResourceList]  WITH CHECK ADD  CONSTRAINT [FK__Deadlock_ResourceList__objectnameHistTableID__ServerInventory_SQL_TableIDs__TableID] FOREIGN KEY([objectnameHistTableID])
REFERENCES [hist].[ServerInventory_SQL_TableIDs] ([TableID])
GO
ALTER TABLE [hist].[Deadlock_ResourceList] CHECK CONSTRAINT [FK__Deadlock_ResourceList__objectnameHistTableID__ServerInventory_SQL_TableIDs__TableID]
GO
ALTER TABLE [hist].[Deadlock_ResourceOwners]  WITH CHECK ADD  CONSTRAINT [FK__Deadlock_ResourceOwners__HistDeadlockProcessID__Deadlock_ProcessList__HistDeadlockProcessID] FOREIGN KEY([HistDeadlockProcessID])
REFERENCES [hist].[Deadlock_ProcessList] ([HistDeadlockProcessID])
GO
ALTER TABLE [hist].[Deadlock_ResourceOwners] CHECK CONSTRAINT [FK__Deadlock_ResourceOwners__HistDeadlockProcessID__Deadlock_ProcessList__HistDeadlockProcessID]
GO
ALTER TABLE [hist].[Deadlock_ResourceOwners]  WITH CHECK ADD  CONSTRAINT [FK__Deadlock_ResourceOwners__HistDeadlockResourceID__Deadlock_ResourceList__HistDeadlockResourceID] FOREIGN KEY([HistDeadlockResourceID])
REFERENCES [hist].[Deadlock_ResourceList] ([HistDeadlockResourceID])
GO
ALTER TABLE [hist].[Deadlock_ResourceOwners] CHECK CONSTRAINT [FK__Deadlock_ResourceOwners__HistDeadlockResourceID__Deadlock_ResourceList__HistDeadlockResourceID]
GO
ALTER TABLE [hist].[Deadlock_ResourceOwners]  WITH CHECK ADD  CONSTRAINT [FK__Deadlock_ResourceOwners__modeRefLockModeID__SQLServer_LockModes__RefLockModeID] FOREIGN KEY([modeRefLockModeID])
REFERENCES [ref].[SQLServer_LockModes] ([RefLockModeID])
GO
ALTER TABLE [hist].[Deadlock_ResourceOwners] CHECK CONSTRAINT [FK__Deadlock_ResourceOwners__modeRefLockModeID__SQLServer_LockModes__RefLockModeID]
GO
ALTER TABLE [hist].[Deadlock_ResourceWaiters]  WITH CHECK ADD  CONSTRAINT [FK__Deadlock_ResourceWaiters__HistDeadlockProcessID__Deadlock_ProcessList__HistDeadlockProcessID] FOREIGN KEY([HistDeadlockProcessID])
REFERENCES [hist].[Deadlock_ProcessList] ([HistDeadlockProcessID])
GO
ALTER TABLE [hist].[Deadlock_ResourceWaiters] CHECK CONSTRAINT [FK__Deadlock_ResourceWaiters__HistDeadlockProcessID__Deadlock_ProcessList__HistDeadlockProcessID]
GO
ALTER TABLE [hist].[Deadlock_ResourceWaiters]  WITH CHECK ADD  CONSTRAINT [FK__Deadlock_ResourceWaiters__HistDeadlockResourceID__Deadlock_ResourceList__HistDeadlockResourceID] FOREIGN KEY([HistDeadlockResourceID])
REFERENCES [hist].[Deadlock_ResourceList] ([HistDeadlockResourceID])
GO
ALTER TABLE [hist].[Deadlock_ResourceWaiters] CHECK CONSTRAINT [FK__Deadlock_ResourceWaiters__HistDeadlockResourceID__Deadlock_ResourceList__HistDeadlockResourceID]
GO
ALTER TABLE [hist].[Deadlock_ResourceWaiters]  WITH CHECK ADD  CONSTRAINT [FK__Deadlock_ResourceWaiters__modeRefLockModeID__SQLServer_LockModes__RefLockModeID] FOREIGN KEY([modeRefLockModeID])
REFERENCES [ref].[SQLServer_LockModes] ([RefLockModeID])
GO
ALTER TABLE [hist].[Deadlock_ResourceWaiters] CHECK CONSTRAINT [FK__Deadlock_ResourceWaiters__modeRefLockModeID__SQLServer_LockModes__RefLockModeID]
GO
ALTER TABLE [hist].[DTSStore_PackageStore]  WITH CHECK ADD  CONSTRAINT [FK__DTSStore_PackageStore__CategoryID__DTSStore_Categories__CategoryID] FOREIGN KEY([CategoryID])
REFERENCES [hist].[DTSStore_Categories] ([CategoryID])
GO
ALTER TABLE [hist].[DTSStore_PackageStore] CHECK CONSTRAINT [FK__DTSStore_PackageStore__CategoryID__DTSStore_Categories__CategoryID]
GO
ALTER TABLE [hist].[DTSStore_PackageStore]  WITH CHECK ADD  CONSTRAINT [FK__DTSStore_PackageStore__DescriptionID__DTSStore_Descriptions__DescriptionID] FOREIGN KEY([DescriptionID])
REFERENCES [hist].[DTSStore_Descriptions] ([DescriptionID])
GO
ALTER TABLE [hist].[DTSStore_PackageStore] CHECK CONSTRAINT [FK__DTSStore_PackageStore__DescriptionID__DTSStore_Descriptions__DescriptionID]
GO
ALTER TABLE [hist].[DTSStore_PackageStore]  WITH CHECK ADD  CONSTRAINT [FK__DTSStore_PackageStore__HistServerID__ServerInventory_ServerIDs__HistServerID] FOREIGN KEY([HistServerID])
REFERENCES [hist].[ServerInventory_ServerIDs] ([HistServerID])
GO
ALTER TABLE [hist].[DTSStore_PackageStore] CHECK CONSTRAINT [FK__DTSStore_PackageStore__HistServerID__ServerInventory_ServerIDs__HistServerID]
GO
ALTER TABLE [hist].[DTSStore_PackageStore]  WITH CHECK ADD  CONSTRAINT [FK__DTSStore_PackageStore__OwnerID__DTSStore_Owners__OwnerID] FOREIGN KEY([OwnerID])
REFERENCES [hist].[DTSStore_Owners] ([OwnerID])
GO
ALTER TABLE [hist].[DTSStore_PackageStore] CHECK CONSTRAINT [FK__DTSStore_PackageStore__OwnerID__DTSStore_Owners__OwnerID]
GO
ALTER TABLE [hist].[DTSStore_PackageStore]  WITH CHECK ADD  CONSTRAINT [FK__DTSStore_PackageStore__PackageNameID__DTSStore_PackageNames__PackageNameID] FOREIGN KEY([PackageNameID])
REFERENCES [hist].[DTSStore_PackageNames] ([PackageNameID])
GO
ALTER TABLE [hist].[DTSStore_PackageStore] CHECK CONSTRAINT [FK__DTSStore_PackageStore__PackageNameID__DTSStore_PackageNames__PackageNameID]
GO
ALTER TABLE [hist].[General_FullFileName]  WITH CHECK ADD  CONSTRAINT [FK__General_FullFileName__HistFileNameID__General_FileNames__HistFileNameID] FOREIGN KEY([HistFileNameID])
REFERENCES [hist].[General_FileNames] ([HistFileNameID])
ON DELETE CASCADE
GO
ALTER TABLE [hist].[General_FullFileName] CHECK CONSTRAINT [FK__General_FullFileName__HistFileNameID__General_FileNames__HistFileNameID]
GO
ALTER TABLE [hist].[General_FullFileName]  WITH CHECK ADD  CONSTRAINT [FK__General_FullFileName__HistPathID__General_Paths__HistPathID] FOREIGN KEY([HistPathID])
REFERENCES [hist].[General_Paths] ([HistPathID])
ON DELETE CASCADE
GO
ALTER TABLE [hist].[General_FullFileName] CHECK CONSTRAINT [FK__General_FullFileName__HistPathID__General_Paths__HistPathID]
GO
ALTER TABLE [hist].[Jobs_SQL_JobHistory]  WITH CHECK ADD  CONSTRAINT [FK__Jobs_SQL_JobHistory__HistJobID__Jobs_SQL_Jobs__HistJobID] FOREIGN KEY([HistJobID])
REFERENCES [hist].[Jobs_SQL_Jobs] ([HistJobID])
GO
ALTER TABLE [hist].[Jobs_SQL_JobHistory] CHECK CONSTRAINT [FK__Jobs_SQL_JobHistory__HistJobID__Jobs_SQL_Jobs__HistJobID]
GO
ALTER TABLE [hist].[Jobs_SQL_JobHistory]  WITH CHECK ADD  CONSTRAINT [FK__Jobs_SQL_JobHistory__HistServerID__ServerInventory_ServerIDs__HistServerID] FOREIGN KEY([HistServerID])
REFERENCES [hist].[ServerInventory_ServerIDs] ([HistServerID])
GO
ALTER TABLE [hist].[Jobs_SQL_JobHistory] CHECK CONSTRAINT [FK__Jobs_SQL_JobHistory__HistServerID__ServerInventory_ServerIDs__HistServerID]
GO
ALTER TABLE [hist].[Jobs_SQL_Jobs]  WITH CHECK ADD  CONSTRAINT [FK__Jobs_SQL_Jobs__HistServerID__ServerInventory_ServerIDs__HistServerID] FOREIGN KEY([HistServerID])
REFERENCES [hist].[ServerInventory_ServerIDs] ([HistServerID])
GO
ALTER TABLE [hist].[Jobs_SQL_Jobs] CHECK CONSTRAINT [FK__Jobs_SQL_Jobs__HistServerID__ServerInventory_ServerIDs__HistServerID]
GO
ALTER TABLE [hist].[Jobs_SQL_JobSteps]  WITH CHECK ADD  CONSTRAINT [FK__Jobs_SQL_JobSteps__HistDatabaseID__ServerInventory_SQL_DatabaseIDs__DatabaseID] FOREIGN KEY([HistDatabaseID])
REFERENCES [hist].[ServerInventory_SQL_DatabaseIDs] ([DatabaseID])
GO
ALTER TABLE [hist].[Jobs_SQL_JobSteps] CHECK CONSTRAINT [FK__Jobs_SQL_JobSteps__HistDatabaseID__ServerInventory_SQL_DatabaseIDs__DatabaseID]
GO
ALTER TABLE [hist].[Jobs_SQL_JobSteps]  WITH CHECK ADD  CONSTRAINT [FK__Jobs_SQL_JobSteps__HistJobID__Jobs_SQL_Jobs__HistJobID] FOREIGN KEY([HistJobID])
REFERENCES [hist].[Jobs_SQL_Jobs] ([HistJobID])
GO
ALTER TABLE [hist].[Jobs_SQL_JobSteps] CHECK CONSTRAINT [FK__Jobs_SQL_JobSteps__HistJobID__Jobs_SQL_Jobs__HistJobID]
GO
ALTER TABLE [hist].[Jobs_SQL_JobSteps]  WITH CHECK ADD  CONSTRAINT [FK__Jobs_SQL_JobSteps__HistServerIDForServerCol__ServerInventory_ServerIDs__HistServerID] FOREIGN KEY([HistServerIDForServerCol])
REFERENCES [hist].[ServerInventory_ServerIDs] ([HistServerID])
GO
ALTER TABLE [hist].[Jobs_SQL_JobSteps] CHECK CONSTRAINT [FK__Jobs_SQL_JobSteps__HistServerIDForServerCol__ServerInventory_ServerIDs__HistServerID]
GO
ALTER TABLE [hist].[Metrics_QueryStats]  WITH CHECK ADD  CONSTRAINT [FK__Metrics_QueryStats__HistDatabaseID__ServerInventory_SQL_DatabaseIDs__DatabaseID] FOREIGN KEY([HistDatabaseID])
REFERENCES [hist].[ServerInventory_SQL_DatabaseIDs] ([DatabaseID])
GO
ALTER TABLE [hist].[Metrics_QueryStats] CHECK CONSTRAINT [FK__Metrics_QueryStats__HistDatabaseID__ServerInventory_SQL_DatabaseIDs__DatabaseID]
GO
ALTER TABLE [hist].[Metrics_QueryStats]  WITH CHECK ADD  CONSTRAINT [FK__Metrics_QueryStats__HistObjectID__ServerInventory_SQL_ObjectIDs__HistObjectID] FOREIGN KEY([HistObjectID])
REFERENCES [hist].[ServerInventory_SQL_ObjectIDs] ([ObjectID])
GO
ALTER TABLE [hist].[Metrics_QueryStats] CHECK CONSTRAINT [FK__Metrics_QueryStats__HistObjectID__ServerInventory_SQL_ObjectIDs__HistObjectID]
GO
ALTER TABLE [hist].[Metrics_QueryStats]  WITH CHECK ADD  CONSTRAINT [FK__Metrics_QueryStats__HistServerID__ServerInventory_ServerIDs__HistServerID] FOREIGN KEY([HistServerID])
REFERENCES [hist].[ServerInventory_ServerIDs] ([HistServerID])
GO
ALTER TABLE [hist].[Metrics_QueryStats] CHECK CONSTRAINT [FK__Metrics_QueryStats__HistServerID__ServerInventory_ServerIDs__HistServerID]
GO
ALTER TABLE [hist].[ServerInventory_SQL_ColumnNames]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_SQL_ColumnNames__ColumnTypeID__ServerInventory_SQL_DataTypes__RefDataTypeID] FOREIGN KEY([ColumnTypeID])
REFERENCES [ref].[ServerInventory_SQL_DataTypes] ([RefDataTypeID])
GO
ALTER TABLE [hist].[ServerInventory_SQL_ColumnNames] CHECK CONSTRAINT [FK__ServerInventory_SQL_ColumnNames__ColumnTypeID__ServerInventory_SQL_DataTypes__RefDataTypeID]
GO
ALTER TABLE [hist].[ServerInventory_SQL_ConfigurationValues]  WITH CHECK ADD  CONSTRAINT [FK__SQL_ConfigValues__ConfigOptions] FOREIGN KEY([RefConfigOptionID])
REFERENCES [ref].[ServerInventory_SQL_ConfigurationOptions] ([RefConfigOptionID])
GO
ALTER TABLE [hist].[ServerInventory_SQL_ConfigurationValues] CHECK CONSTRAINT [FK__SQL_ConfigValues__ConfigOptions]
GO
ALTER TABLE [hist].[ServerInventory_SQL_ConfigurationValues]  WITH CHECK ADD  CONSTRAINT [FK__SQL_ConfigValues__HistServerID] FOREIGN KEY([HistServerID])
REFERENCES [hist].[ServerInventory_ServerIDs] ([HistServerID])
GO
ALTER TABLE [hist].[ServerInventory_SQL_ConfigurationValues] CHECK CONSTRAINT [FK__SQL_ConfigValues__HistServerID]
GO
ALTER TABLE [hist].[ServerInventory_SQL_DatabaseAttributeValues]  WITH CHECK ADD  CONSTRAINT [FK__DatabaseAttributeValues__DatabaseAttributeID__DatabaseAttributeMaster__DatabaseAttributeID] FOREIGN KEY([DatabaseAttributeID])
REFERENCES [hist].[ServerInventory_SQL_DatabaseAttributeMaster] ([DatabaseAttributeID])
GO
ALTER TABLE [hist].[ServerInventory_SQL_DatabaseAttributeValues] CHECK CONSTRAINT [FK__DatabaseAttributeValues__DatabaseAttributeID__DatabaseAttributeMaster__DatabaseAttributeID]
GO
ALTER TABLE [hist].[ServerInventory_SQL_DatabaseAttributeValues]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_SQL_DatabaseAttributeValues__HistDatabaseID__ServerInventory_SQL_DatabaseIDs__DatabaseID] FOREIGN KEY([HistDatabaseID])
REFERENCES [hist].[ServerInventory_SQL_DatabaseIDs] ([DatabaseID])
GO
ALTER TABLE [hist].[ServerInventory_SQL_DatabaseAttributeValues] CHECK CONSTRAINT [FK__ServerInventory_SQL_DatabaseAttributeValues__HistDatabaseID__ServerInventory_SQL_DatabaseIDs__DatabaseID]
GO
ALTER TABLE [hist].[ServerInventory_SQL_DatabaseAttributeValues]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_SQL_DatabaseAttributeValues__HistServerID__ServerInventory_SQL_ServerIDs__HistServerID] FOREIGN KEY([HistServerID])
REFERENCES [hist].[ServerInventory_ServerIDs] ([HistServerID])
GO
ALTER TABLE [hist].[ServerInventory_SQL_DatabaseAttributeValues] CHECK CONSTRAINT [FK__ServerInventory_SQL_DatabaseAttributeValues__HistServerID__ServerInventory_SQL_ServerIDs__HistServerID]
GO
ALTER TABLE [hist].[ServerInventory_SQL_IndexDetails]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_SQL_IndexDetails__HistColumnID__ServerInventory_SQL_ColumnNames__HistColumnID] FOREIGN KEY([HistColumnID])
REFERENCES [hist].[ServerInventory_SQL_ColumnNames] ([HistColumnID])
GO
ALTER TABLE [hist].[ServerInventory_SQL_IndexDetails] CHECK CONSTRAINT [FK__ServerInventory_SQL_IndexDetails__HistColumnID__ServerInventory_SQL_ColumnNames__HistColumnID]
GO
ALTER TABLE [hist].[ServerInventory_SQL_IndexDetails]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_SQL_IndexDetails__HistIndexID__ServerInventory_SQL_IndexMaster__HistIndexID] FOREIGN KEY([HistIndexID])
REFERENCES [hist].[ServerInventory_SQL_IndexMaster] ([HistIndexID])
GO
ALTER TABLE [hist].[ServerInventory_SQL_IndexDetails] CHECK CONSTRAINT [FK__ServerInventory_SQL_IndexDetails__HistIndexID__ServerInventory_SQL_IndexMaster__HistIndexID]
GO
ALTER TABLE [hist].[ServerInventory_SQL_IndexMaster]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_SQL_IndexMaster__HistServerDBTableID__ServerInventory_SQL_ServerDBTableIDs__ServerDBTableID] FOREIGN KEY([HistServerDBTableID])
REFERENCES [hist].[ServerInventory_SQL_ServerDBTableIDs] ([ServerDBTableID])
GO
ALTER TABLE [hist].[ServerInventory_SQL_IndexMaster] CHECK CONSTRAINT [FK__ServerInventory_SQL_IndexMaster__HistServerDBTableID__ServerInventory_SQL_ServerDBTableIDs__ServerDBTableID]
GO
ALTER TABLE [hist].[ServerInventory_SQL_IndexUsage]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_SQL_IndexUsage__HistIndexID__ServerInventory_SQL_IndexMaster__HistIndexID] FOREIGN KEY([HistIndexID])
REFERENCES [hist].[ServerInventory_SQL_IndexMaster] ([HistIndexID])
GO
ALTER TABLE [hist].[ServerInventory_SQL_IndexUsage] CHECK CONSTRAINT [FK__ServerInventory_SQL_IndexUsage__HistIndexID__ServerInventory_SQL_IndexMaster__HistIndexID]
GO
ALTER TABLE [hist].[ServerInventory_SQL_ServerDBTableIDs]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_SQL_ServerDBTableIDs__HistServerID__ServerInventory_ServerIDs__HistServerID] FOREIGN KEY([HistServerID])
REFERENCES [hist].[ServerInventory_ServerIDs] ([HistServerID])
GO
ALTER TABLE [hist].[ServerInventory_SQL_ServerDBTableIDs] CHECK CONSTRAINT [FK__ServerInventory_SQL_ServerDBTableIDs__HistServerID__ServerInventory_ServerIDs__HistServerID]
GO
ALTER TABLE [hist].[SpaceUsed_DatabaseSizes]  WITH CHECK ADD  CONSTRAINT [FK__SpaceUsed_DatabaseSizes__DatabaseID__ServerInventory_SQL_DatabaseIDs__DatabaseID] FOREIGN KEY([DatabaseID])
REFERENCES [hist].[ServerInventory_SQL_DatabaseIDs] ([DatabaseID])
GO
ALTER TABLE [hist].[SpaceUsed_DatabaseSizes] CHECK CONSTRAINT [FK__SpaceUsed_DatabaseSizes__DatabaseID__ServerInventory_SQL_DatabaseIDs__DatabaseID]
GO
ALTER TABLE [hist].[SpaceUsed_DatabaseSizes]  WITH CHECK ADD  CONSTRAINT [FK__SpaceUsed_DatabaseSizes__HistServerID__ServerInventory_ServerIDs__HistServerID] FOREIGN KEY([HistServerID])
REFERENCES [hist].[ServerInventory_ServerIDs] ([HistServerID])
GO
ALTER TABLE [hist].[SpaceUsed_DatabaseSizes] CHECK CONSTRAINT [FK__SpaceUsed_DatabaseSizes__HistServerID__ServerInventory_ServerIDs__HistServerID]
GO
ALTER TABLE [hist].[SpaceUsed_DriveSizes_DriveMaster]  WITH CHECK ADD  CONSTRAINT [FK__SpaceUsed_DriveSizes_DriveMaster__DriveLabelID__SpaceUsed_DriveSizes_DriveLabel__DriveLabelID] FOREIGN KEY([DriveLabelID])
REFERENCES [hist].[SpaceUsed_DriveSizes_DriveLabel] ([DriveLabelID])
GO
ALTER TABLE [hist].[SpaceUsed_DriveSizes_DriveMaster] CHECK CONSTRAINT [FK__SpaceUsed_DriveSizes_DriveMaster__DriveLabelID__SpaceUsed_DriveSizes_DriveLabel__DriveLabelID]
GO
ALTER TABLE [hist].[SpaceUsed_DriveSizes_DriveMaster]  WITH CHECK ADD  CONSTRAINT [FK__SpaceUsed_DriveSizes_DriveMaster__DriveLetterID__SpaceUsed_DriveSizes_DriveLetter__DriveLetterID] FOREIGN KEY([DriveLetterID])
REFERENCES [hist].[SpaceUsed_DriveSizes_DriveLetter] ([DriveLetterID])
GO
ALTER TABLE [hist].[SpaceUsed_DriveSizes_DriveMaster] CHECK CONSTRAINT [FK__SpaceUsed_DriveSizes_DriveMaster__DriveLetterID__SpaceUsed_DriveSizes_DriveLetter__DriveLetterID]
GO
ALTER TABLE [hist].[SpaceUsed_DriveSizes_DriveMaster]  WITH CHECK ADD  CONSTRAINT [FK__SpaceUsed_DriveSizes_DriveMaster__HistServerID__ServerInventory_ServerIDs__HistServerID] FOREIGN KEY([HistServerID])
REFERENCES [hist].[ServerInventory_ServerIDs] ([HistServerID])
GO
ALTER TABLE [hist].[SpaceUsed_DriveSizes_DriveMaster] CHECK CONSTRAINT [FK__SpaceUsed_DriveSizes_DriveMaster__HistServerID__ServerInventory_ServerIDs__HistServerID]
GO
ALTER TABLE [hist].[SpaceUsed_DriveSizes_DriveMaster]  WITH CHECK ADD  CONSTRAINT [FK__SpaceUsed_DriveSizes_DriveMaster__RunID__SpaceUsed_DriveSizes_RunIDs__RunID] FOREIGN KEY([RunID])
REFERENCES [hist].[SpaceUsed_DriveSizes_RunIDs] ([RunID])
GO
ALTER TABLE [hist].[SpaceUsed_DriveSizes_DriveMaster] CHECK CONSTRAINT [FK__SpaceUsed_DriveSizes_DriveMaster__RunID__SpaceUsed_DriveSizes_RunIDs__RunID]
GO
ALTER TABLE [hist].[SpaceUsed_FileSizes]  WITH CHECK ADD  CONSTRAINT [FK__SpaceUsed_FileSizes__HistPathFileNameID__General_FullFileName__HistPathFileNameID] FOREIGN KEY([HistPathFileNameID])
REFERENCES [hist].[General_FullFileName] ([HistPathFileNameID])
ON DELETE CASCADE
GO
ALTER TABLE [hist].[SpaceUsed_FileSizes] CHECK CONSTRAINT [FK__SpaceUsed_FileSizes__HistPathFileNameID__General_FullFileName__HistPathFileNameID]
GO
ALTER TABLE [hist].[SpaceUsed_FileSizes]  WITH CHECK ADD  CONSTRAINT [FK__SpaceUsed_FileSizes__HistServerID__ServerInventory_ServerIDs__HistServerID] FOREIGN KEY([HistServerID])
REFERENCES [hist].[ServerInventory_ServerIDs] ([HistServerID])
GO
ALTER TABLE [hist].[SpaceUsed_FileSizes] CHECK CONSTRAINT [FK__SpaceUsed_FileSizes__HistServerID__ServerInventory_ServerIDs__HistServerID]
GO
ALTER TABLE [hist].[SpaceUsed_TableSizes]  WITH CHECK ADD  CONSTRAINT [FK__SpaceUsed_TableSizes__ServerDBTableID__ServerInventory_SQL_ServerDBTableIDs__ServerDBTableID] FOREIGN KEY([ServerDBTableID])
REFERENCES [hist].[ServerInventory_SQL_ServerDBTableIDs] ([ServerDBTableID])
GO
ALTER TABLE [hist].[SpaceUsed_TableSizes] CHECK CONSTRAINT [FK__SpaceUsed_TableSizes__ServerDBTableID__ServerInventory_SQL_ServerDBTableIDs__ServerDBTableID]
GO
ALTER TABLE [hist].[SQLRestarts_History]  WITH CHECK ADD  CONSTRAINT [FK__SQLRestarts_History__HistServerID__ServerIDs__HistServerID] FOREIGN KEY([HistServerID])
REFERENCES [hist].[ServerInventory_ServerIDs] ([HistServerID])
GO
ALTER TABLE [hist].[SQLRestarts_History] CHECK CONSTRAINT [FK__SQLRestarts_History__HistServerID__ServerIDs__HistServerID]
GO
ALTER TABLE [ref].[ServerInventory_SQL_DataTypes]  WITH CHECK ADD  CONSTRAINT [FK__ServerInventory_SQL_DataTypes__RefSQLVersionID__ServerInventory_SQL_ServerVersions__RefSQLVersionID] FOREIGN KEY([RefSQLVersionID])
REFERENCES [ref].[ServerInventory_SQL_ServerVersions] ([RefSQLVersionID])
GO
ALTER TABLE [ref].[ServerInventory_SQL_DataTypes] CHECK CONSTRAINT [FK__ServerInventory_SQL_DataTypes__RefSQLVersionID__ServerInventory_SQL_ServerVersions__RefSQLVersionID]
GO
ALTER TABLE [hist].[ServerInventory_SQL_IndexMaster]  WITH CHECK ADD  CONSTRAINT [CK__ServerInventory_SQL_IndexMaster__fillfactor] CHECK  (([fillfactor]>=(100)))
GO
ALTER TABLE [hist].[ServerInventory_SQL_IndexMaster] CHECK CONSTRAINT [CK__ServerInventory_SQL_IndexMaster__fillfactor]
GO
/****** Object:  StoredProcedure [dbo].[ServerInventory_SQL_DeleteClusterNode]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[dbo].[ServerInventory_SQL_DeleteClusterNode]
**  Desc:			Procedure to manage deleting data from the ClusterNodes table
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			2009.06.25
**  Debug:			
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
********************************************************************************************************/
CREATE PROCEDURE [dbo].[ServerInventory_SQL_DeleteClusterNode] (
	@ServerName						VARCHAR(100)
	,@InstanceName					VARCHAR(100) = NULL
	,@NodeName						VARCHAR(200)
)
AS

SET NOCOUNT ON

DECLARE
	@ServerID		INT
	,@HistServerID	INT

-- Lookup the master server id
SELECT
	@ServerID = [ServerID]
FROM [dbo].[ServerInventory_SQL_Master]
WHERE [ServerName] = @ServerName
AND ISNULL([InstanceName],'') = ISNULL(@InstanceName,'')

-- Lookup the hist server id for the node name
EXEC [hist].[ServerInventory_GetServerID] @NodeName, @HistServerID OUTPUT

IF @ServerID IS NOT NULL AND @HistServerID IS NOT NULL
BEGIN
	-- Try to delete
	DELETE FROM [dbo].[ServerInventory_SQL_ClusterNodes] WHERE [ServerID] = @ServerID AND [HistServerID] = @HistServerID
END 
ELSE
	PRINT('Server Not Found')


GO
/****** Object:  StoredProcedure [dbo].[ServerInventory_SQL_GetAttributeID]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
* Name
	[dbo].[ServerInventory_SQL_GetAttributeID]

* Author
	Matt Stanford
	
* Date
	2011.04.28
	
* Synopsis
	Procedure to manage inserting data into the AttributeList table
	
* Description
	Procedure to manage inserting data into the AttributeList table

* Dependencies
	[dbo].[ServerInventory_SQL_AttributeMaster]
	
*******************************************************************************
* License
*******************************************************************************
	Copyright © SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
******************************************************************************/
CREATE PROCEDURE [dbo].[ServerInventory_SQL_GetAttributeID] (
	@AttributeName			VARCHAR(100)
	,@AttributeID			INT OUTPUT
	,@CreateIfNecessary		BIT = 0
)
AS
	
SET @AttributeID = (SELECT [AttributeID] FROM [dbo].[ServerInventory_SQL_AttributeMaster] WHERE [AttributeName] = @AttributeName)

IF @AttributeID IS NULL AND @CreateIfNecessary = 1
BEGIN

	INSERT INTO [dbo].[ServerInventory_SQL_AttributeMaster] ([AttributeName], [IsCore], [IsReadOnly])
	VALUES (@AttributeName, 0, 0)
	
	SET @AttributeID = SCOPE_IDENTITY()

END


GO
/****** Object:  StoredProcedure [dbo].[ServerInventory_SQL_InsertClusterNode]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[dbo].[ServerInventory_SQL_InsertClusterNode]
**  Desc:			Procedure to manage inserting data into the ClusterNodes table
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			2009.06.25
**  Debug:			
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
********************************************************************************************************/
CREATE PROCEDURE [dbo].[ServerInventory_SQL_InsertClusterNode] (
	@ServerName						VARCHAR(100)
	,@InstanceName					VARCHAR(100) = NULL
	,@NodeName						VARCHAR(200)
)
AS

SET NOCOUNT ON

DECLARE
	@ServerID		INT
	,@HistServerID	INT

-- Lookup the master server id
SELECT
	@ServerID = [ServerID]
FROM [dbo].[ServerInventory_SQL_Master]
WHERE [ServerName] = @ServerName
AND ISNULL([InstanceName],'') = ISNULL(@InstanceName,'')

-- Lookup the hist server id for the node name
EXEC [hist].[ServerInventory_GetServerID] @NodeName, @HistServerID OUTPUT

IF @ServerID IS NOT NULL AND @HistServerID IS NOT NULL
BEGIN
	-- Try to insert
	IF NOT EXISTS (SELECT * FROM [dbo].[ServerInventory_SQL_ClusterNodes] WHERE [ServerID] = @ServerID AND [HistServerID] = @HistServerID)
		INSERT INTO [dbo].[ServerInventory_SQL_ClusterNodes] ([ServerID],[HistServerID])
		VALUES(@ServerID,@HistServerID)
	ELSE
		PRINT('Node already attached to this instance')
END 
ELSE
	PRINT('Server Not Found')


GO
/****** Object:  StoredProcedure [dbo].[ServerInventory_SQL_InsertServer]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[dbo].[ServerInventory_SQL_InsertServer]
**  Desc:			Adds a new instance to the SQL_Master table
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			2009.06.25
**  Debug:			
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
********************************************************************************************************/
CREATE PROCEDURE [dbo].[ServerInventory_SQL_InsertServer] (
	@ServerName						VARCHAR(100)
	,@InstanceName					VARCHAR(100) = NULL
	,@PortNumber					INT = NULL
	,@Description					NVARCHAR(MAX)
	,@SQLVersion					INT
	,@Enabled						BIT = 1
	,@EnvrionmentName				VARCHAR(100)
	,@EditionName					VARCHAR(100)
	,@AttributesToAdd				[dbo].[AttributeListType] READONLY
	,@ServerID						INT OUTPUT
)

AS

SET NOCOUNT ON

DECLARE
	@EnvironmentID					INT
	,@EditionID						INT

-- Lookup the Environment ID
SELECT
	@EnvironmentID = [EnvironmentID]
FROM [dbo].[ServerInventory_Environments]
WHERE [EnvironmentName] = @EnvrionmentName

IF @EnvironmentID IS NULL
BEGIN
	PRINT('Environment is not recognized')
	RETURN 255
END

-- Lookup the Edition ID
SELECT
	@EditionID = [EditionID]
FROM [dbo].[ServerInventory_SQL_Editions]
WHERE [EditionName] = @EditionName

IF @EditionID IS NULL
BEGIN
	PRINT('Edition is not recognized')
	RETURN 255
END

-- Make sure there isn't already a server like this
IF NOT EXISTS (
	SELECT * 
	FROM [dbo].[ServerInventory_SQL_Master] 
	WHERE [ServerName] = @ServerName 
	AND ISNULL([InstanceName],'') = ISNULL(@InstanceName,'')
	AND ISNULL([PortNumber],0) = ISNULL(@PortNumber,0)
	AND [EditionID] = @EditionID
	AND [EnvironmentID] = @EnvironmentID
	)
BEGIN
	-- All clear, do the insert
	INSERT INTO [dbo].[ServerInventory_SQL_Master] ([ServerName], [InstanceName], [PortNumber], [Description], [SQLVersion], [Enabled], [EnvironmentID], [EditionID], [UseCredential])
	VALUES(@ServerName,@InstanceName,@PortNumber,@Description,@SQLVersion,@Enabled,@EnvironmentID,@EditionID,0)
	
	SET @ServerID = SCOPE_IDENTITY()
	
	EXEC [dbo].[ServerInventory_SQL_SaveAttributes] @ServerID, @AttributesToAdd

END
ELSE
	PRINT('Server already in the table')

GO
/****** Object:  StoredProcedure [dbo].[ServerInventory_SQL_SaveAttribute]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
* Name
	[dbo].[ServerInventory_SQL_SaveAttribute]

* Author
	Matt Stanford
	
* Date
	2008.12.29
	
* Synopsis
	Procedure to manage inserting data into the AttributeList table
	
* Description
	Procedure to manage inserting data into the AttributeList table

* Dependencies
	[dbo].[ServerInventory_SQL_GetAttributeID]
	[dbo].[ServerInventory_SQL_AttributeList]
	
*******************************************************************************
* License
*******************************************************************************
	Copyright © SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20110428	Matt Stanford	Modified to call the GetAttributeID procedure for dynamic attribute creation
******************************************************************************/
CREATE PROCEDURE [dbo].[ServerInventory_SQL_SaveAttribute] (
	@ServerID				INT
	,@AttributeName			VARCHAR(100)
	,@AttributeValue		NVARCHAR(1000)
)
AS

DECLARE
	@AttributeID		INT
	
EXEC [dbo].[ServerInventory_SQL_GetAttributeID] @AttributeName, @AttributeID = @AttributeID OUTPUT, @CreateIfNecessary = 1

IF @AttributeID IS NOT NULL
BEGIN
	IF NOT EXISTS(SELECT * FROM [dbo].[ServerInventory_SQL_AttributeList] WHERE ServerID = @ServerID AND AttributeID = @AttributeID)
	BEGIN
		-- Its an insert!
		INSERT INTO [dbo].[ServerInventory_SQL_AttributeList] ([ServerID], [AttributeID], [AttributeValue])
		VALUES (@ServerID, @AttributeID, @AttributeValue)
	END
	ELSE IF NOT EXISTS(SELECT * FROM [dbo].[ServerInventory_SQL_AttributeList] WHERE ServerID = @ServerID AND AttributeID = @AttributeID AND AttributeValue = @AttributeValue)
	BEGIN 
		-- Its an update!
		UPDATE al SET al.AttributeValue = @AttributeValue
		FROM [dbo].[ServerInventory_SQL_AttributeList] al
		WHERE al.ServerID = @ServerID 
		AND al.AttributeID = @AttributeID
	END
	ELSE
	BEGIN
		-- Its a trick!
		PRINT('Nothing to do here')
	END
END
ELSE
BEGIN
	PRINT('Attribute does not exist')
END


GO
/****** Object:  StoredProcedure [dbo].[ServerInventory_SQL_SaveAttributes]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[dbo].[ServerInventory_SQL_SaveAttributes]
**  Desc:			Procedure to manage inserting data into the Attributes table
					Contains a cursor... but it should only run when servers are added to the master table
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			2009.06.25
**  Debug:			
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
********************************************************************************************************/
CREATE PROCEDURE [dbo].[ServerInventory_SQL_SaveAttributes] (
	@ServerID						INT
	,@AttributesToAdd				[dbo].[AttributeListType] READONLY
)
AS

DECLARE 
	@AttribName						VARCHAR(100)
	,@AttribValue					NVARCHAR(1000)

DECLARE #savAtt CURSOR LOCAL STATIC FOR
SELECT
	AttributeName
	,AttributeValue
FROM @AttributesToAdd

OPEN #savAtt

FETCH NEXT FROM #savAtt INTO @AttribName, @AttribValue
WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC [dbo].[ServerInventory_SQL_SaveAttribute] @ServerID, @AttribName, @AttribValue
	FETCH NEXT FROM #savAtt INTO @AttribName, @AttribValue
END

CLOSE #savAtt
DEALLOCATE #savAtt


GO
/****** Object:  StoredProcedure [dbo].[ServerInventory_SQL_SQLBackup_Audit_InsertValue]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[dbo].[ServerInventory_SQL_SQLBackup_Audit_InsertValue]
**  Desc:			Saves RedGate SQLBackup specific information to the attributes
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			2010-08-02
**  Debug:			
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
********************************************************************************************************/
CREATE PROCEDURE [dbo].[ServerInventory_SQL_SQLBackup_Audit_InsertValue] (
	@ServerID					INT
	,@Version					NVARCHAR(1000)
	,@License					NVARCHAR(1000)
	,@SerialNumber				NVARCHAR(1000)
)
AS

DECLARE @ALT dbo.AttributeListType

INSERT INTO @ALT (AttributeName,AttributeValue)
VALUES 
	('SQLBackup_Version',@Version)
	,('SQLBackup_License',@License)
	,('SQLBackup_SerialNumber',@SerialNumber)
	,('SQLBackup_AuditDate',CAST(GETDATE() AS NVARCHAR(1000)))

EXEC [dbo].[ServerInventory_SQL_SaveAttributes] @ServerID, @ALT


GO
/****** Object:  StoredProcedure [etc].[MissingSQLBackups_InsertValue]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
* Name
	[etc].[MissingSQLBackups_InsertValue]

* Author
	Adam Bean
	
* Date
	2011.02.07
	
* Synopsis
	Support procedure to insert data from powershell collector
	
* Examples
	Only to be called from powershell collector - Collect-MissingSQLBackups

*******************************************************************************
* License
*******************************************************************************
	Copyright © SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20110516	Adam Bean		Changed @ServerID to @ServerName
******************************************************************************/

CREATE PROCEDURE [etc].[MissingSQLBackups_InsertValue]
(
	@RunID					INT
	,@ServerName			VARCHAR(256)
	,@DatabaseName			VARCHAR(256)
	,@RecoveryModel			CHAR(1)
	,@BackupType			CHAR(1)
	,@DeviceType			CHAR(1)
	,@LastBackupTime		DATETIME
	,@LastBackupLocation	VARCHAR(512)
)

AS

SET NOCOUNT ON

DECLARE 
	@HistServerID			INT
	,@DatabaseID			INT

-- Find the RunID
IF NOT EXISTS
(
	SELECT [RunID]
	FROM [etc].[MissingSQLBackups_RunIDs]
	WHERE [RunID] = @RunID
)
BEGIN
	INSERT INTO [etc].[MissingSQLBackups_RunIDs]
	([RunID])
	SELECT @RunID
END

-- Find the server ID	
EXEC [hist].[ServerInventory_GetServerID] @ServerName = @ServerName, @ServerID = @HistServerID OUTPUT

-- Find the DatabaseID
EXEC [hist].[ServerInventory_SQL_GetDatabaseID] @DatabaseName, @DatabaseID OUTPUT

-- Insert the data
INSERT INTO [etc].[MissingSQLBackups]
([HistServerID], [DatabaseID], [RecoveryModel], [BackupType], [DeviceType], [LastBackupTime], [LastBackupLocation], [RunID])
VALUES
(@HistServerID, @DatabaseID, @RecoveryModel, @BackupType, @DeviceType, @LastBackupTime, @LastBackupLocation, @RunID)

SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [hist].[Backups_GetDeviceID]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [hist].[Backups_GetDeviceID] (
	@DeviceName				NVARCHAR(260)
	,@DeviceID				INT OUTPUT
)
AS

DECLARE 
	@Msg					NVARCHAR(4000)
	,@DeviceNameToInsert	NVARCHAR(260)

IF (@DeviceName IS NULL)
	SET @DeviceNameToInsert = 'NULL'
ELSE IF (@DeviceName LIKE 'Red Gate SQL Backup (%')
	SET @DeviceNameToInsert = 'Red Gate SQL Backup'
ELSE IF (@DeviceName LIKE 'SQLBACKUP_%')
	SET @DeviceNameToInsert = 'Red Gate SQL Backup'
ELSE
	SET @DeviceNameToInsert = @DeviceName

SELECT
	@DeviceID = id.[DeviceID]
FROM [hist].[Backups_Devices] id
WHERE id.[DeviceName] = @DeviceNameToInsert

IF (@DeviceID IS NULL)
BEGIN
	BEGIN TRY
		INSERT INTO [hist].[Backups_Devices] ([DeviceName])
		VALUES (@DeviceNameToInsert)
		
		SET @DeviceID = SCOPE_IDENTITY()
	END TRY
	BEGIN CATCH
		SET @Msg = 'Just tried to insert ' + @DeviceNameToInsert
		RAISERROR(@Msg,16,2)
	END CATCH
END


GO
/****** Object:  StoredProcedure [hist].[Backups_GetTypeID]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [hist].[Backups_GetTypeID] (
	@BackupType				CHAR(1)
	,@BackupTypeID			INT OUTPUT
)
AS

SELECT
	@BackupTypeID = id.[BackupTypeID]
FROM [hist].[Backups_Types] id
WHERE id.[BackupType] = @BackupType

IF (@BackupTypeID IS NULL)
BEGIN
	INSERT INTO [hist].[Backups_Types] ([BackupType])
	VALUES (@BackupType)
	
	SET @BackupTypeID = SCOPE_IDENTITY()
END


GO
/****** Object:  StoredProcedure [hist].[Backups_SaveBackupHistory]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[Backups_SaveBackupHistory]
**  Desc:			Adds a backup history into repository
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			(unknown)
**  Debug:			
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090728	Matt Stanford	Emergency change because of broken HistServerID
**	20120627	Adam Bean		Updates for new schema and additional data collection
********************************************************************************************************/
CREATE PROCEDURE [hist].[Backups_SaveBackupHistory] (
	@ServerName				SYSNAME
	,@MachineName			SYSNAME
	,@DBName				SYSNAME
	,@StartDate				SMALLDATETIME
	,@EndDate				SMALLDATETIME
	,@SizeMB				INT
	,@SizeMBCompressed		INT
	,@BUType				CHAR(1)
	,@UserName				SYSNAME
	,@LogicalDevice			NVARCHAR(128)
	,@PhysicalDevice		NVARCHAR(260)
	,@DeviceType			INT
)
AS
SET NOCOUNT ON

DECLARE 
	@ServerID				INT
	,@MachineID				INT
	,@DatabaseID			INT
	,@BackupTypeID			INT
	,@LogicalID				INT
	,@PhysicalID			INT
	,@UserID				INT
	
EXEC [hist].[ServerInventory_GetServerID] @ServerName, @ServerID OUTPUT
EXEC [hist].[ServerInventory_GetServerID] @MachineName, @MachineID OUTPUT
EXEC [hist].[ServerInventory_SQL_GetDatabaseID] @DBName, @DatabaseID OUTPUT
EXEC [hist].[Backups_GetTypeID] @BUType, @BackupTypeID OUTPUT
EXEC [hist].[Backups_GetDeviceID] @LogicalDevice, @LogicalID OUTPUT
EXEC [hist].[Backups_GetDeviceID] @PhysicalDevice, @PhysicalID OUTPUT
EXEC [hist].[Users_GetUserID] @UserName, @UserID OUTPUT

IF NOT EXISTS (
	SELECT * 
	FROM [hist].[Backups_History] 
	WHERE [HistServerID] = @ServerID
	AND [DatabaseID] = @DatabaseID
	AND [BUTypeID] = @BackupTypeID
	AND [StartDate] = @StartDate
	)
BEGIN

	INSERT INTO [hist].[Backups_History] ([HistServerID], [MachineID], [DatabaseID], [StartDate], [EndDate], [SizeMB], [SizeMBCompressed],[BUTypeID], [UserID], [LogicalDeviceID], [PhysicalDeviceID],[DeviceType])
	VALUES (@ServerID,@MachineID,@DatabaseID,@StartDate,@EndDate,@SizeMB,@SizeMBCompressed,@BackupTypeID,@UserID,@LogicalID,@PhysicalID,@DeviceType)

END


GO
/****** Object:  StoredProcedure [hist].[ChangeControl_DeployHistory_Insert]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[ChangeControl_DeployHistory_Insert]
**  Desc:			Procedure to insert history values
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			2009-09-04
**  Debug:			
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	
********************************************************************************************************/
CREATE PROCEDURE [hist].[ChangeControl_DeployHistory_Insert] (
	@ServerName				VARCHAR(200)
	,@InstanceName			VARCHAR(50)
	,@EnvironmentName		VARCHAR(50)
	,@DeployName			VARCHAR(50) = NULL
	,@PackageName			VARCHAR(50)
	,@ScriptID				INT
	,@Output				NVARCHAR(MAX)
	,@OutputType			VARCHAR(50)
	,@IsError				BIT
	,@UserName				VARCHAR(50)
	,@DateCreated			SMALLDATETIME
)
AS

-- Lookup the server id
DECLARE
	@HistServerID			INT
	
EXEC [hist].[ServerInventory_GetServerID] @ServerName, @HistServerID OUTPUT

-- insert the data
INSERT INTO [hist].[ChangeControl_DeployHistory] ([HistServerID], [InstanceName], [EnvironmentName], [ScriptID], [Output], [OutputType], [UserName], [DateCreated])
VALUES (@HistServerID,@InstanceName,@EnvironmentName,@ScriptID,@Output,@OutputType,@UserName,@DateCreated)


GO
/****** Object:  StoredProcedure [hist].[ChangeTracking_SQL_GETActionID]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [hist].[ChangeTracking_SQL_GETActionID]    Script Date: 02/20/2009 16:41:20 ******/

CREATE PROCEDURE [hist].[ChangeTracking_SQL_GETActionID] (
	@ActionType		VARCHAR (255)
	,@ActionID			INT OUTPUT
)
AS

DECLARE @msg NVARCHAR (4000)

-- Find the ActionID
SELECT 
	@ActionID = ad.[ActionID]
FROM [hist].[ChangeTracking_SQL_ActionIDs]
	 ad
WHERE ad.[ActionType] = @Actiontype

IF @ActionID IS NULL
BEGIN
	SET @msg = 'This Action type "' + ISNULL (@ActionType,'NULL') + '" does not exist'
	Raiserror (@msg,15,255)
END


GO
/****** Object:  StoredProcedure [hist].[ChangeTracking_SQL_GetObjectIDs]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [hist].[ChangeTracking_SQL_GetObjectIDs] (
	@SchemaName		SYSNAME
	,@ObjectName	SYSNAME
	,@RefType		NVARCHAR (128)
	,@ObjectId		INT OUTPUT
)

AS

DECLARE @ObjectTypeID INT

--get the object type id
EXEC [hist].[ChangeTracking_SQL_GETObjectTypeIDs] @RefType, @ObjectTypeID OUTPUT

-- Find the ObjectTypeID
SELECT 
	@ObjectID = ot.[ObjectID]
FROM [hist].[ChangeTracking_SQL_ObjectIDs] 
	 ot
WHERE ot.[SchemaName] = @SchemaName
	AND ot.[ObjectName] = @ObjectName
	AND ot.[ObjectTypeID]	= @ObjectTypeID

-- Add the  combo if necessary
IF @ObjectId IS NULL
BEGIN
	INSERT INTO [hist].[ChangeTracking_SQL_ObjectIDs]  ([SchemaName], [ObjectName], [ObjectTypeID]) 
	VALUES (@SchemaName, @ObjectName, @ObjectTypeID)
	SET @ObjectId = SCOPE_IDENTITY()
END


GO
/****** Object:  StoredProcedure [hist].[ChangeTracking_SQL_GETObjectTypeIDs]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [hist].[ChangeTracking_SQL_GETObjectTypeIDs] (
	@RefType		NVARCHAR (128)
	,@ObjectTypeID		INT OUTPUT
)
AS

DECLARE @MSG NVARCHAR (4000)

-- Find the ObjectID
SELECT 
	@ObjectTypeID = od.[ObjectTypeID]
FROM [hist].[ChangeTracking_SQL_ObjectTypeIDs]
	 od
WHERE od.[RefType] = @RefType

IF @ObjectTypeID IS NULL
BEGIN
	SET @MSG = 'This reference type "' + @RefType + '" do not exist in the ChangeTracking_SQL_ObjectTypeIDs table.'
	Raiserror (@MSG,15,255)
END


GO
/****** Object:  StoredProcedure [hist].[ChangeTracking_SQL_GetServerDBObjectActionIDs]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[ChangeTracking_SQL_GetServerDBObjectActionIDs]
**  Desc:			Procedure to save object change into the repository
**  Auth:			Kathy Toth (SQLSlayer)
**  Date:			(unknown)
**  Debug:			
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090728	Matt Stanford	Emergency change because of broken HistServerID
********************************************************************************************************/
CREATE PROCEDURE [hist].[ChangeTracking_SQL_GetServerDBObjectActionIDs] (
	@ServerName		SYSNAME
	,@DatabaseName	SYSNAME 
	,@SchemaName	SYSNAME
	,@ObjectName	SYSNAME
	,@RefType		NVARCHAR (128)
	,@ActionType	VARCHAR (255)
	,@DateModified	DATE
)
AS

DECLARE @DatabaseID INT
DECLARE @ServerID INT
DECLARE @ActionID INT
DECLARE @ObjectID INT
DECLARE @RecordID INT

-- Find the ServerID
EXEC [hist].[ServerInventory_GetServerID] @ServerName, @ServerID OUTPUT

-- Find the DatabaseID
EXEC [hist].[ServerInventory_SQL_GetDatabaseID] @DatabaseName, @DatabaseID OUTPUT

--Find the ActionID
EXEC [hist].[ChangeTracking_SQL_GETActionID] @ActionType, @ActionID OUTPUT

--find the Objects and their types
EXEC [hist].[ChangeTracking_SQL_GetObjectIDs] @SchemaName, @ObjectName, @RefType, @ObjectID OUTPUT


--Find the DateModified -hmm not to sure how to handle this

-- Add the  combo if necessary
IF NOT EXISTS (SELECT [HistServerID], [DatabaseID], [ObjectID], [ActionID], [DateModified]
				FROM [hist].[ChangeTracking_SQL_ServerDBObjectActionIDs]
				WHERE [HistServerID] = @ServerID
				AND DatabaseID = @DatabaseID
				AND ObjectID = @ObjectID
				AND ActionID = @ActionID
				AND DateModified = @DateModified
				)
BEGIN
	INSERT INTO [hist].[ChangeTracking_SQL_ServerDBObjectActionIDs] ([HistServerID], [DatabaseID], [ObjectID], [ActionID], [DateModified], [DateCreated]) 
	VALUES (@ServerID, @DatabaseID, @ObjectID, @ActionID, @DateModified, GETDATE())
END


GO
/****** Object:  StoredProcedure [hist].[Collectors_GetScriptID]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[Collectors_GetScriptID]
**  Desc:			Proc to get/create the script ID for this script
**  Auth:			Matt Stanford 
**  Date:			2011-02-03
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
********************************************************************************************************/
CREATE PROCEDURE [hist].[Collectors_GetScriptID] (
	@ScriptName				VARCHAR(500)
	,@ScriptID				INT OUTPUT
)
AS

SELECT
	@ScriptID = [HistCollectorLogScriptID]
FROM [hist].[Collectors_ScriptIDs] sids
WHERE sids.[ScriptName] = @ScriptName

IF @ScriptID IS NULL
BEGIN
	INSERT INTO [hist].[Collectors_ScriptIDs] ([ScriptName])
	VALUES (@ScriptName)
	
	SET @ScriptID = SCOPE_IDENTITY()
END



GO
/****** Object:  StoredProcedure [hist].[Collectors_Log_InsertValue]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************************************************************************
**  Name:			[hist].[Collectors_Log_InsertValue]
**  Desc:			Procedure to add logging information about the collectors
**  Auth:			Matt Stanford 
**  Date:			2011-02-03
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
********************************************************************************************************/
CREATE PROCEDURE [hist].[Collectors_Log_InsertValue] (
	@ServerName				VARCHAR(200)
	,@LoginName				NVARCHAR(50)
	,@ScriptName			VARCHAR(500)
	,@State					VARCHAR(10)
	,@LogMessage			VARCHAR(500)
	,@LogVersion			SMALLINT
	,@RetentionDays			SMALLINT = 60
)
AS
SET NOCOUNT ON

DECLARE 
	@HistServerID			INT
	,@HistUserID			INT
	,@RetentionDate			DATETIME
	,@ScriptID				INT
	,@StateID				INT

-- Get the server ID	
EXEC [hist].[ServerInventory_GetServerID] @ServerName = @ServerName, @ServerID = @HistServerID OUTPUT

-- Get the user ID
EXEC [hist].[Users_GetUserID] @UserName = @LoginName, @UserID = @HistUserID OUTPUT

-- Get the script ID
EXEC [hist].[Collectors_GetScriptID] @ScriptName = @ScriptName, @ScriptID = @ScriptID OUTPUT

-- Get the state ID
SELECT
	@StateID = st.[HistCollectorLogStateID]
FROM [hist].[Collectors_StateIDs] st
WHERE st.[State] = @State

-- Insert the row
INSERT INTO [hist].[Collectors_Log] ([HistUserID],[HistServerID],[HistCollectorLogScriptID],[HistCollectorLogStateID],[LogMessage],[LogVersion],[DateCreated])
VALUES (@HistUserID,@HistServerID,@ScriptID,@StateID,@LogMessage,@LogVersion,GETDATE())

SET @RetentionDate = DATEADD(day,-@RetentionDays,GETDATE())

-- Trim up the data
--DELETE l
--FROM [hist].[Collectors_Log] l
--INNER JOIN [hist].[Collectors_ScriptIDs] sids
--	ON l.[HistCollectorLogScriptID] = sids.[HistCollectorLogScriptID]
--WHERE l.[DateCreated] < @RetentionDate
--AND sids.[ScriptName] = @ScriptName
--AND l.[HistServerID] = @HistServerID




GO
/****** Object:  StoredProcedure [hist].[ConnectionCounts_Collector]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			[hist].[ConnectionCounts_Collector]
**	Desc:			Procedure to manage inserting connection counts from a remote instance
**	Auth:			Adam Bean (SQLSlayer.com)
**	Date:			2010.10.16
*******************************************************************************
**  License
*******************************************************************************
**  Copyright © SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  20110510	Adam Bean		Updated to use ServerName instead of ServerID
**	20120716	Adam Bean		Updated to switch to hist schema vs. audit
********************************************************************************************************/

CREATE PROCEDURE [hist].[ConnectionCounts_Collector]
(
	@RunID			INT
	,@ServerName	VARCHAR(256)
	,@CntrValue		INT
)

AS

SET NOCOUNT ON

DECLARE 
	@HistServerID	INT
	
-- Get the server ID	
EXEC [hist].[ServerInventory_GetServerID] @ServerName = @ServerName, @ServerID = @HistServerID OUTPUT

-- Insert the new RunID
IF NOT EXISTS
(
	SELECT [RunID]
	FROM [hist].[ConnectionCounts_RunIDs]
	WHERE [RunID] = @RunID
)
BEGIN
	INSERT INTO [hist].[ConnectionCounts_RunIDs]
	([RunID])
	SELECT @RunID
END

-- Insert the values
IF NOT EXISTS 
(
	SELECT 
		[HistServerID]
		,[RunID]
	FROM [hist].[ConnectionCounts]
	WHERE [HistServerID] = @HistServerID
	AND [RunID] = @RunID
)
BEGIN
	INSERT INTO [hist].[ConnectionCounts]
	([HistServerID],[RunID],[CounterValue])
	SELECT
		@HistServerID
		,@RunID
		,@CntrValue
END

SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [hist].[DatabaseMaintenance_InsertCheckDBResults]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [hist].[DatabaseMaintenance_InsertCheckDBResults]
(
	@ServerName			SYSNAME
	,@DatabaseName		SYSNAME
	,@CheckDBID			BIGINT = NULL -- No longer used, left for compatibility
	,@RunID				INT
	,@DateCreated		DATETIME
	,@Error				INT
	,@Level				INT
	,@State				INT
	,@MessageText		VARCHAR(MAX)
	,@RepairLevel		VARCHAR(128)
	,@Status			INT
	,@ObjectID			INT
	,@IndexID			INT
	,@PartitionID		BIGINT
	,@AllocUnitID		BIGINT
	,@File				INT
	,@Page				INT
	,@Slot				INT
	,@RefFile			INT
	,@RefPage			INT
	,@RefSlot			INT
	,@Allocation		INT
)
AS

SET NOCOUNT ON

DECLARE
	@IsOK			BIT
	,@HistServerID	INT
	,@DatabaseID	INT
	
-- Get the IDs for the insert
EXEC [hist].[ServerInventory_GetServerID] @ServerName, @HistServerID OUTPUT
EXEC [hist].[ServerInventory_SQL_GetDatabaseID] @DatabaseName, @DatabaseID OUTPUT

-- Logic to determine if the run was successful or not
IF COALESCE(@Error,@Level,@State) IS NOT NULL
	SET @IsOK = 0
ELSE
	SET @IsOK = 1
	
IF @IsOK = 1
BEGIN
	MERGE INTO [hist].[DatabaseMaintenance_CheckDB_OK] AS t
	USING (SELECT @HistServerID, @DatabaseID, @RunID) AS s (HistServerID, DatabaseID, RunID)
	ON s.[HistServerID] = t.[HistServerID]
	AND s.[DatabaseID] = t.[DatabaseID]
	AND s.[RunID] = t.[RunID]
	WHEN NOT MATCHED THEN
	INSERT ([HistServerID],[DatabaseID],[RunID], [DateCreated])
	VALUES (@HistServerID,@DatabaseID,@RunID,@DateCreated);
END
ELSE
BEGIN
	MERGE INTO [hist].[DatabaseMaintenance_CheckDB_Errors] AS t
	USING (SELECT @HistServerID, @DatabaseID, @RunID) AS s (HistServerID, DatabaseID, RunID)
	ON s.[HistServerID] = t.[HistServerID]
	AND s.[DatabaseID] = t.[DatabaseID]
	AND s.[RunID] = t.[RunID]
	WHEN NOT MATCHED THEN
	INSERT ([HistServerID],[DatabaseID],[RunID],[DateCreated],[Error],[Level],[State],[MessageText],[RepairLevel],[Status],[ObjectID],[IndexID]
	,[PartitionID],[AllocUnitID],[File],[Page],[Slot],[RefFile],[RefPage],[RefSlot],[Allocation])
	VALUES (@HistServerID,@DatabaseID,@RunID,@DateCreated,@Error,@Level,@State,@MessageText,@RepairLevel,@Status,@ObjectID,@IndexID
	,@PartitionID,@AllocUnitID,@File,@Page,@Slot,@RefFile,@RefPage,@RefSlot,@Allocation);
END




GO
/****** Object:  StoredProcedure [hist].[Deadlock_GetDeadlockInfo]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**	Name:			[hist].[Deadlock_GetDeadlockInfo]
**	Desc:			Procedure to gather all of the deadlock info and output it all at once
**	Auth:			Matt Stanford
**	Date:			2010.12.20
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  
********************************************************************************************************/
CREATE PROCEDURE [hist].[Deadlock_GetDeadlockInfo] (
	@HistDeadlockID					INT
)
AS

SELECT 
	'Deadlock'						AS [WhatIsThis]
	,*
FROM [hist].[Deadlock_Deadlocks_vw] 
WHERE [HistDeadlockID] = @HistDeadlockID

SELECT
	'ProcessList'					AS [WhatIsThis]
	,*
FROM [hist].[Deadlock_ProcessList_vw]
WHERE [HistDeadlockID] = @HistDeadlockID

SELECT
	'ExecutionStack'				AS [WhatIsThis]
	,p.[processid]
	,st.[Sequence]
	,st.[Stack]
FROM [hist].[Deadlock_Process_ExecutionStack] st
INNER JOIN [hist].[Deadlock_ProcessList_vw] p
	ON st.[HistDeadlockProcessID] = p.[HistDeadlockProcessID]
WHERE p.[HistDeadlockID] = @HistDeadlockID
ORDER BY p.[processid], st.[Sequence]

SELECT
	'ResourceList'					AS [WhatIsThis]
	,*
FROM [hist].[Deadlock_ResourceList_vw]
WHERE [HistDeadlockID] = @HistDeadlockID

SELECT
	'ResourceOwners'				AS [WhatIsThis]
	,r.[id]							AS [ResourceID]
	,p.[processid]
	,lm.[LockMode]
	,lm.[LockName]
	,ISNULL(db.[DBName],'') + ISNULL('.' + tbl.[SchemaName],'') + ISNULL('.' + tbl.[TableName],'') AS [ObjectName]
	,r.[IndexName]					AS [IndexName]
FROM [hist].[Deadlock_ResourceOwners] ro
INNER JOIN [hist].[Deadlock_ResourceList] r
	ON ro.[HistDeadlockResourceID] = r.[HistDeadlockResourceID]
INNER JOIN [hist].[Deadlock_ProcessList] p
	ON ro.[HistDeadlockProcessID] = p.[HistDeadlockProcessID]
INNER JOIN [ref].[SQLServer_LockModes] lm
	ON ro.[modeRefLockModeID] = lm.[RefLockModeID]
LEFT OUTER JOIN [hist].[ServerInventory_SQL_DatabaseIDs] db
	ON r.[dbnameHistDatabaseID] = db.[DatabaseID]
LEFT OUTER JOIN [hist].[ServerInventory_SQL_TableIDs] tbl
	ON r.[objectnameHistTableID] = tbl.[TableID]
WHERE r.[HistDeadlockID] = @HistDeadlockID

SELECT
	'ResourceWaiters'				AS [WhatIsThis]
	,r.[id]							AS [ResourceID]
	,p.[processid]
	,rw.[requestType]
	,lm.[LockMode]
	,lm.[LockName]
	,ISNULL(db.[DBName],'') + ISNULL('.' + tbl.[SchemaName],'') + ISNULL('.' + tbl.[TableName],'') AS [ObjectName]
	,r.[IndexName]					AS [IndexName]
FROM [hist].[Deadlock_ResourceWaiters] rw
INNER JOIN [hist].[Deadlock_ResourceList] r
	ON rw.[HistDeadlockResourceID] = r.[HistDeadlockResourceID]
INNER JOIN [hist].[Deadlock_ProcessList] p
	ON rw.[HistDeadlockProcessID] = p.[HistDeadlockProcessID]
INNER JOIN [ref].[SQLServer_LockModes] lm
	ON rw.[modeRefLockModeID] = lm.[RefLockModeID]
LEFT OUTER JOIN [hist].[ServerInventory_SQL_DatabaseIDs] db
	ON r.[dbnameHistDatabaseID] = db.[DatabaseID]
LEFT OUTER JOIN [hist].[ServerInventory_SQL_TableIDs] tbl
	ON r.[objectnameHistTableID] = tbl.[TableID]
WHERE r.[HistDeadlockID] = @HistDeadlockID

GO
/****** Object:  StoredProcedure [hist].[Deadlock_NewDeadlock_InsertValue]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**	Name:			[hist].[Deadlock_NewDeadlock_InsertValue]
**	Desc:			Procedure to inset new deadlocks into schema
**	Auth:			Matt Stanford
**	Date:			2010.12.20
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  
********************************************************************************************************/
CREATE PROCEDURE [hist].[Deadlock_NewDeadlock_InsertValue] (
	@ServerName						VARCHAR(200)
	,@DeadlockSPID					VARCHAR(11)
	,@DeadlockDate					DATETIME
	,@VictimProcess					VARCHAR(20)
	,@HistDeadlockID				INT OUTPUT
)
AS

SET NOCOUNT ON

DECLARE
	@HistServerID			INT

EXEC [hist].[ServerInventory_GetServerID] @ServerName, @HistServerID OUTPUT

SELECT 
	@HistDeadlockID = [HistDeadlockID]
FROM [hist].[Deadlock_Deadlocks] 
WHERE [HistServerID] = @HistServerID 
AND [DeadlockSPID] = @DeadlockSPID 
AND [DeadlockDate] = @DeadlockDate

IF (@HistDeadlockID IS NULL)
BEGIN
	INSERT INTO [hist].[Deadlock_Deadlocks] ([HistServerID], [DeadlockSPID], [DeadlockDate], [VictimProcess])
	VALUES (@HistServerID, @DeadlockSPID, @DeadlockDate, @VictimProcess)
	
	SET @HistDeadlockID = SCOPE_IDENTITY()
END


GO
/****** Object:  StoredProcedure [hist].[Deadlock_Process_ExecutionStack_InsertValue]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**	Name:			[hist].[Deadlock_Process_ExecutionStack_InsertValue]
**	Desc:			Procedure to save execution stack information for each deadlock process
**	Auth:			Matt Stanford
**	Date:			2010.12.20
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  
********************************************************************************************************/
CREATE PROCEDURE [hist].[Deadlock_Process_ExecutionStack_InsertValue] (
	@HistDeadlockID					INT
	,@processid						VARCHAR(20)
	,@Stack							NVARCHAR(4000)
	,@sequence						SMALLINT
)
AS

DECLARE
	@HistDeadlockProcessID			INT
	
-- Lookup process id
SELECT
	@HistDeadlockProcessID = [HistDeadlockProcessID]
FROM [hist].[Deadlock_ProcessList]
WHERE [HistDeadlockID] = @HistDeadlockID
AND [processid] = @processid

IF @HistDeadlockProcessID IS NOT NULL AND @Stack NOT IN ('inputbuf','sp_executesql     ','EXECUTE sp_executeSQL @SQL     ')
BEGIN
	IF NOT EXISTS (SELECT * FROM [hist].[Deadlock_Process_ExecutionStack] WHERE [HistDeadlockProcessID] = @HistDeadlockProcessID AND [Sequence] = @sequence)
	BEGIN
		INSERT INTO [hist].[Deadlock_Process_ExecutionStack] ([HistDeadlockProcessID],[Stack],[sequence])
		VALUES (@HistDeadlockProcessID,@Stack,@sequence)
	END
END


GO
/****** Object:  StoredProcedure [hist].[Deadlock_ProcessList_InsertValue]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**	Name:			[hist].[Deadlock_ProcessList_InsertValue]
**	Desc:			Procedure to save process info for each deadlock
**	Auth:			Matt Stanford
**	Date:			2010.12.20
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  
********************************************************************************************************/
CREATE PROCEDURE [hist].[Deadlock_ProcessList_InsertValue] (
	@HistDeadlockID					INT
	,@clientapp						VARCHAR(128)	= NULL
	,@currentdb						INT				= NULL
	,@hostname						VARCHAR(200)	= NULL
	,@hostpid						SMALLINT		= NULL
	,@isolationlevel				VARCHAR(50)		= NULL
	,@kpid							SMALLINT		= NULL
	,@lastbatchstarted				DATETIME		= NULL
	,@lastbatchcompleted			DATETIME		= NULL
	,@lasttranstarted				DATETIME		= NULL
	,@lockmode						VARCHAR(10)		= NULL
	,@loginname						NVARCHAR(128)	= NULL
	,@priority						SMALLINT		= NULL
	,@processid						VARCHAR(20)
	,@taskpriority					SMALLINT		= NULL
	,@sbid							SMALLINT		= NULL
	,@schedulerid					TINYINT			= NULL
	,@spid							SMALLINT		= NULL
	,@runstatus						VARCHAR(50)		= NULL
	,@transactionname				VARCHAR(128)	= NULL
	,@transcount					SMALLINT		= NULL
	,@waitresource					VARCHAR(128)	= NULL
	,@waittime						INT				= NULL
)
AS

DECLARE
	@hostnameHistServerID		INT
	,@loginnameHistUserID		INT
	,@RefIsolationLevelID		INT
	,@RefRunStatusID			INT
	,@RefLockModeID				INT
	
SET @loginname = ISNULL(@loginname,'')
SET @hostname = ISNULL(@hostname,'')

EXEC [hist].[Users_GetUserID] @loginname, @loginnameHistUserID OUTPUT
EXEC [hist].[ServerInventory_GetServerID] @hostname, @hostnameHistServerID OUTPUT

SELECT
	@RefIsolationLevelID = [RefIsolationLevelID]
FROM [ref].[SQLServer_IsolationLevels]
WHERE [IsolationLevel] = @isolationlevel

SELECT
	@RefRunStatusID = [RefRunStatusID]
FROM [ref].[SQLServer_RunStatus]
WHERE [RunStatus] = @runstatus

SELECT 
	@RefLockModeID = [RefLockModeID]
FROM [ref].[SQLServer_LockModes]
WHERE [LockMode] = @lockmode

IF NOT EXISTS (SELECT * FROM [hist].[Deadlock_ProcessList] WHERE [HistDeadlockID] = @HistDeadlockID AND [processid] = @processid)
BEGIN
	INSERT INTO [hist].[Deadlock_ProcessList] ([HistDeadlockID],[clientapp],[currentdb],[hostnameHistServerID],[hostpid],[RefIsolationLevelID],[kpid]
		,[lastbatchstarted],[lastbatchcompleted],[lasttranstarted],[modeRefLockModeID],[loginnameHistUserID],[priority]
		,[processid],[taskpriority],[sbid],[schedulerid],[spid],[RefRunStatusID],[transactionname],[transcount],[waitresource],[waittime])
	VALUES (@HistDeadlockID, @clientapp,@currentdb,@hostnameHistServerID,@hostpid,@RefIsolationLevelID,@kpid
		,@lastbatchstarted,@lastbatchcompleted,@lasttranstarted,@RefLockModeID,@loginnameHistUserID,@priority
		,@processid,@taskpriority,@sbid,@schedulerid,@spid,@RefRunStatusID,@transactionname,@transcount,@waitresource,@waittime)
END


GO
/****** Object:  StoredProcedure [hist].[Deadlock_ResourceList_InsertValue]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**	Name:			[hist].[Deadlock_ResourceList_InsertValue]
**	Desc:			Procedure to save resource info for each deadlock
**	Auth:			Matt Stanford
**	Date:			2010.12.20
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  
********************************************************************************************************/
CREATE PROCEDURE [hist].[Deadlock_ResourceList_InsertValue] (
	@HistDeadlockID					INT
	,@id							VARCHAR(20)
	,@locktype						VARCHAR(10)		= NULL
	,@objectname					VARCHAR(500)	= NULL
	,@indexname						VARCHAR(128)	= NULL
	,@hobtid						VARCHAR(50)		= NULL
	,@mode							VARCHAR(10)		= NULL
	,@associatedObjectID			VARCHAR(50)		= NULL
	,@dbid							SMALLINT		= NULL
	,@fileid						SMALLINT		= NULL
	,@pageid						INT				= NULL
)
AS 

DECLARE
	@ServerName						VARCHAR(200)
	,@HistTableID					INT
	,@HistDatabaseID				INT
	,@vDBName						VARCHAR(128)
	,@vSchemaName					VARCHAR(128)
	,@vObjectName					VARCHAR(128)
	,@firstDot						INT
	,@secondDot						INT
	,@RefLockModeID					INT

SELECT 
	@RefLockModeID = [RefLockModeID]
FROM [ref].[SQLServer_LockModes]
WHERE [LockMode] = @mode

IF @locktype = 'dblock'
BEGIN
	-- In here, @objectname is the database name
	SET @vDBName = @objectname
END 
ELSE
BEGIN
	SET @firstDot = CHARINDEX('.',@objectname)
	SET @secondDot = CHARINDEX('.',@objectname,@firstDot + 1)
		
	IF (@firstDot > 0 AND @secondDot > 0)
	BEGIN
		SET @vDBName = LEFT(@objectname,@firstDot - 1)
		SET @vSchemaName = SUBSTRING(@objectName,@firstdot + 1, @seconddot - @firstdot - 1)
		SET @vObjectName = RIGHT(@objectname,LEN(@objectname) - @seconddot)

		EXEC [hist].[ServerInventory_SQL_GetTableID] @vObjectName, @vSchemaName, @HistTableID OUTPUT
	END
END

-- Get database id
EXEC [hist].[ServerInventory_SQL_GetDatabaseID] @vDBName, @HistDatabaseID OUTPUT

IF NOT EXISTS (SELECT * FROM [hist].[Deadlock_ResourceList] WHERE [HistDeadlockID] = @HistDeadlockID AND [id] = @id)
BEGIN
	INSERT INTO [hist].[Deadlock_ResourceList] ([HistDeadlockID], [id], [locktype], [objectnameHistTableID], [dbnameHistDatabaseID], [indexname],
		[hobtid],[modeRefLockModeID],[associatedObjectID],[dbid],[fileid],[pageid])
	VALUES (@HistDeadlockID, @id, @locktype, @HistTableID, @HistDatabaseID, @indexname,
		@hobtid, @RefLockModeID, @associatedObjectID, @dbid, @fileid, @pageid)
END


GO
/****** Object:  StoredProcedure [hist].[Deadlock_ResourceOwner_InsertValue]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**	Name:			[hist].[Deadlock_ResourceOwner_InsertValue]
**	Desc:			Procedure to save resource owner information for each deadlock
**	Auth:			Matt Stanford
**	Date:			2010.12.20
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  
********************************************************************************************************/
CREATE PROCEDURE [hist].[Deadlock_ResourceOwner_InsertValue] (
	@HistDeadlockID					INT
	,@ResourceID					VARCHAR(20)
	,@ProcessID						VARCHAR(20)
	,@mode							VARCHAR(10)		= NULL
)
AS

DECLARE
	@HistDeadlockResourceID			INT
	,@HistDeadlockProcessID			INT
	,@RefLockModeID					INT
	
SELECT
	@HistDeadlockProcessID = [HistDeadlockProcessID]
FROM [hist].[Deadlock_ProcessList]
WHERE [HistDeadlockID] = @HistDeadlockID
AND [processid] = @ProcessID

SELECT
	@HistDeadlockResourceID = [HistDeadlockResourceID] 
FROM [hist].[Deadlock_ResourceList]
WHERE [HistDeadlockID] = @HistDeadlockID
AND [id] = @ResourceID

SELECT 
	@RefLockModeID = [RefLockModeID]
FROM [ref].[SQLServer_LockModes]
WHERE [LockMode] = @mode

IF NOT EXISTS (SELECT * FROM [hist].[Deadlock_ResourceOwners] WHERE [HistDeadlockProcessID] = @HistDeadlockProcessID 
		AND [HistDeadlockResourceID] = @HistDeadlockResourceID)
BEGIN
	INSERT INTO [hist].[Deadlock_ResourceOwners] ([HistDeadlockProcessID], [HistDeadlockResourceID], [modeRefLockModeID])
	VALUES (@HistDeadlockProcessID, @HistDeadlockResourceID, @RefLockModeID)
END


GO
/****** Object:  StoredProcedure [hist].[Deadlock_ResourceWaiter_InsertValue]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**	Name:			[hist].[Deadlock_ResourceWaiter_InsertValue]
**	Desc:			Procedure to save waiter information for each deadlock
**	Auth:			Matt Stanford
**	Date:			2010.12.20
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  
********************************************************************************************************/
CREATE PROCEDURE [hist].[Deadlock_ResourceWaiter_InsertValue] (
	@HistDeadlockID					INT
	,@ResourceID					VARCHAR(20)
	,@ProcessID						VARCHAR(20)
	,@mode							VARCHAR(10)		= NULL
	,@requestType					VARCHAR(10)		= NULL
)
AS

DECLARE
	@HistDeadlockResourceID			INT
	,@HistDeadlockProcessID			INT
	,@RefLockModeID					INT
	
SELECT
	@HistDeadlockProcessID = [HistDeadlockProcessID]
FROM [hist].[Deadlock_ProcessList]
WHERE [HistDeadlockID] = @HistDeadlockID
AND [processid] = @ProcessID

SELECT
	@HistDeadlockResourceID = [HistDeadlockResourceID] 
FROM [hist].[Deadlock_ResourceList]
WHERE [HistDeadlockID] = @HistDeadlockID
AND [id] = @ResourceID

SELECT 
	@RefLockModeID = [RefLockModeID]
FROM [ref].[SQLServer_LockModes]
WHERE [LockMode] = @mode

IF NOT EXISTS (SELECT * FROM [hist].[Deadlock_ResourceWaiters] WHERE [HistDeadlockProcessID] = @HistDeadlockProcessID 
		AND [HistDeadlockResourceID] = @HistDeadlockResourceID)
BEGIN
	INSERT INTO [hist].[Deadlock_ResourceWaiters] ([HistDeadlockProcessID], [HistDeadlockResourceID], [modeRefLockModeID], [requestType])
	VALUES (@HistDeadlockProcessID, @HistDeadlockResourceID, @RefLockModeID, @requestType)
END


GO
/****** Object:  StoredProcedure [hist].[DTSStore_GetCategoryID]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [hist].[DTSStore_GetCategoryID] (
	@CategoryID_GUID		UNIQUEIDENTIFIER
	,@CategoryID			INT OUTPUT
)
AS

-- Find the CategoryID
SELECT 
	@CategoryID = id.[CategoryID]
FROM 
	[hist].[DTSStore_Categories] id
WHERE id.[CategoryID_GUID] = @CategoryID_GUID

IF @CategoryID IS NULL
BEGIN
	INSERT INTO [hist].[DTSStore_Categories] ([CategoryID_GUID])
	VALUES (@CategoryID_GUID)
	
	SET @CategoryID = SCOPE_IDENTITY()
END


GO
/****** Object:  StoredProcedure [hist].[DTSStore_GetDescriptionID]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [hist].[DTSStore_GetDescriptionID] (
	@Description			NVARCHAR(1024)
	,@DescriptionID			INT OUTPUT
)
AS

-- Find the DescriptionID
SELECT
	@DescriptionID = id.[DescriptionID]
FROM [hist].[DTSStore_Descriptions] id
WHERE id.[Description] = @Description

IF @DescriptionID IS NULL
BEGIN
	INSERT INTO [hist].[DTSStore_Descriptions] ([Description])
	VALUES (@Description)
	
	SET @DescriptionID = SCOPE_IDENTITY()
END


GO
/****** Object:  StoredProcedure [hist].[DTSStore_GetOwnerID]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [hist].[DTSStore_GetOwnerID] (
	@Owner					SYSNAME
	,@Owner_SID				VARBINARY(85)
	,@OwnerID				INT OUTPUT
)
AS

-- Find the OwnerID
SELECT
	@OwnerID = id.[OwnerID]
FROM [hist].[DTSStore_Owners] id
WHERE id.[Owner] = @Owner
AND id.[Owner_sid] = @Owner_SID

IF @OwnerID IS NULL
BEGIN
	INSERT INTO [hist].[DTSStore_Owners] ([Owner],[Owner_sid])
	VALUES (@Owner,@Owner_SID)
	
	SET @OwnerID = SCOPE_IDENTITY()
END


GO
/****** Object:  StoredProcedure [hist].[DTSStore_GetPackageNameID]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [hist].[DTSStore_GetPackageNameID] (
	@PackageName			SYSNAME
	,@PackageID				UNIQUEIDENTIFIER
	,@PackageNameID			INT OUTPUT
)
AS

-- Find the PackageNameID
SELECT 
	@PackageNameID = id.[PackageNameID]
FROM 
	[hist].[DTSStore_PackageNames] id
WHERE id.[PackageName] = @PackageName
AND id.[PackageID] = @PackageID

IF @PackageNameID IS NULL
BEGIN
	INSERT INTO [hist].[DTSStore_PackageNames] ([PackageName],[PackageID])
	VALUES (@PackageName,@PackageID)
	
	SET @PackageNameID = SCOPE_IDENTITY()
END


GO
/****** Object:  StoredProcedure [hist].[DTSStore_StorePackageData]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[DTSStore_StorePackageData]
**  Desc:			Procedure to save DTS packages into the repository
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			(unknown)
**  Debug:			
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090728	Matt Stanford	Emergency change because of broken HistServerID
********************************************************************************************************/
CREATE PROCEDURE [hist].[DTSStore_StorePackageData] (
	@SourceServer			SYSNAME
	,@name					SYSNAME
	,@id					UNIQUEIDENTIFIER
	,@versionid				UNIQUEIDENTIFIER
	,@description			NVARCHAR(1024)
	,@categoryid			UNIQUEIDENTIFIER
	,@createdate			DATETIME
	,@owner					SYSNAME
	,@packagedata			IMAGE
	,@owner_sid				VARBINARY(85)
	,@packagetype			INT
)
AS

DECLARE
	@CategoryID_INT			INT
	,@DescriptionID			INT
	,@OwnerID				INT
	,@PackageNameID			INT
	,@ServerID				INT
	
EXECUTE [hist].[DTSStore_GetCategoryID] @CategoryID, @CategoryID_INT OUTPUT
EXECUTE [hist].[DTSStore_GetDescriptionID] @Description, @DescriptionID OUTPUT
EXECUTE [hist].[DTSStore_GetOwnerID] @owner, @owner_sid, @OwnerID OUTPUT
EXECUTE [hist].[DTSStore_GetPackageNameID] @name, @id, @PackageNameID OUTPUT
EXECUTE [hist].[ServerInventory_GetServerID] @SourceServer, @ServerID OUTPUT

-- Now that all of the IDs have been collected, lets insert the data into 
-- the main table

IF NOT EXISTS (
	SELECT 
		[HistServerID]
		,[PackageNameID]
		,[VersionID] 
	FROM [hist].[DTSStore_PackageStore] 
	WHERE [HistServerID] = @ServerID
	AND [PackageNameID] = @PackageNameID
	AND [VersionID] = @versionid
	)
BEGIN
	INSERT INTO [hist].[DTSStore_PackageStore] ([HistServerID], PackageNameID, VersionID, DescriptionID, CategoryID, CreateDate, OwnerID, PackageData, PackageType)
	VALUES (@ServerID, @PackageNameID, @versionid, @DescriptionID, @CategoryID_INT, @createdate, @OwnerID, @packagedata, @packagetype)
END


GO
/****** Object:  StoredProcedure [hist].[General_GetFileNameID]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [hist].[General_GetFileNameID] (
	@FileName				VARCHAR(900)
	,@HistFileNameID		INT OUTPUT
)
AS

SET NOCOUNT ON

SELECT
	@HistFileNameID = [HistFileNameID]
FROM [hist].[General_FileNames] f
WHERE f.[FileName] = @FileName

IF @HistFileNameID IS NULL AND @FileName IS NOT NULL
BEGIN
	INSERT INTO [hist].[General_FileNames] ([FileName])
	VALUES (@FileName)
	
	SET @HistFileNameID = SCOPE_IDENTITY()
END
	

GO
/****** Object:  StoredProcedure [hist].[General_GetFullFileNameID]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [hist].[General_GetFullFileNameID] (
	@Path					VARCHAR(900)
	,@FileName				VARCHAR(900)
	,@FullFileNameID		INT OUTPUT
)
AS

SET NOCOUNT ON

DECLARE
	@HistPathID				INT
	,@HistFileNameID		INT
	
EXEC [hist].[General_GetPathID] @Path, @HistPathID OUTPUT
EXEC [hist].[General_GetFileNameID] @FileName, @HistFileNameID OUTPUT

SELECT
	@FullFileNameID = ffn.[HistPathFileNameID]
FROM [hist].[General_FullFileName] ffn
WHERE ffn.[HistPathID] = @HistPathID
AND ffn.[HistFileNameID] = @HistFileNameID

IF @FullFileNameID IS NULL
BEGIN
	INSERT INTO [hist].[General_FullFileName] ([HistPathID],[HistFileNameID])
	VALUES (@HistPathID,@HistFileNameID)
	
	SET @FullFileNameID = SCOPE_IDENTITY()
END
	

GO
/****** Object:  StoredProcedure [hist].[General_GetPathID]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [hist].[General_GetPathID] (
	@Path					VARCHAR(900)
	,@HistPathID			INT OUTPUT
)
AS

SET NOCOUNT ON

SELECT
	@HistPathID = [HistPathID]
FROM [hist].[General_Paths] p
WHERE p.[Path] = @Path

IF @HistPathID IS NULL AND @Path IS NOT NULL
BEGIN
	INSERT INTO [hist].[General_Paths] ([Path])
	VALUES (@Path)
	
	SET @HistPathID = SCOPE_IDENTITY()
END
	

GO
/****** Object:  StoredProcedure [hist].[Jobs_RunTimes]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [hist].[Jobs_RunTimes]
(
	@ServerName						VARCHAR(MAX)	= NULL
	,@JobNameWildCard				VARCHAR(100)	= NULL
	,@DateStart						DATETIME		= NULL
	,@DateEnd						DATETIME		= NULL
)

AS

SET NOCOUNT ON

SELECT [ServerName]
      ,[JobName]
	  ,[run_duration]/60 AS [RunTimeMinutes]
	  ,MIN([run_datetime]) AS [DateTime]
FROM [hist].[Jobs_SQL_JobHistory_vw]
WHERE ServerName = @ServerName 
	AND [JobName] LIKE @JobNameWildCard
	AND [run_datetime] BETWEEN @DateStart AND @DateEnd
GROUP BY [ServerName],[JobName],[run_duration]/60,CAST([run_datetime] AS DATE)
ORDER BY CAST([run_datetime] AS DATE)

SET NOCOUNT OFF


GO
/****** Object:  StoredProcedure [hist].[Jobs_SQL_GetDisabledJobs]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [hist].[Jobs_SQL_GetDisabledJobs]
@EnvironmentIDs NVARCHAR(20)
,@ServerIDs		 NVARCHAR(MAX)
AS
SELECT s.[ServerName]
      ,j.[name] AS [JobName]
      ,j.[description]
      ,MAX(jh.[run_datetime]) AS [LastRun]
  FROM [hist].[Jobs_SQL_Jobs] j
  JOIN [hist].[Jobs_SQL_JobHistory] jh
  ON j.[histjobid] = jh.[histjobid]
  JOIN [hist].[ServerInventory_ServerIDs] s
	ON j.[HistServerID] = s.[HistServerID]
  JOIN [dbo].[ServerInventory_SQL_AllServers_vw] a
	ON s.[ServerName] = a.[FullName]
  JOIN [dbo].[ServerInventory_Environments] e
	ON a.[Environment] = e.[EnvironmentName]
  LEFT JOIN [dbo].[Split_fn](@EnvironmentIDs,',') eids
	ON e.[EnvironmentID] = eids.[item]
  LEFT JOIN [dbo].[Split_fn](@ServerIDs,',') sids
	ON s.[HistServerID] = sids.[item]
  WHERE 
  jh.[step_id] = 0
  AND jh.[run_status] = 0
  AND j.[enabled] = 0
  AND j.[LastseenOn] > DATEADD(day,-1,GETDATE())
  AND ((eids.[item] IS NOT NULL AND @EnvironmentIDs IS NOT NULL) OR @EnvironmentIDs IS NULL)
  AND ((sids.[item] IS NOT NULL AND @ServerIDs IS NOT NULL) OR @ServerIDs IS NULL)
  GROUP BY s.[ServerName]
      ,j.[name]
      ,j.[enabled]
      ,j.[description]
      ,j.[LastSeenOn]
  ORDER BY [ServerName], [JobName]

GO
/****** Object:  StoredProcedure [hist].[Jobs_SQL_GetFailedMaintenanceJobs]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [hist].[Jobs_SQL_GetFailedMaintenanceJobs]
@StartDate		 DATETIME
,@EndDate		 DATETIME
,@EnvironmentIDs NVARCHAR(20)
,@ServerIDs		 NVARCHAR(MAX)
,@Wildcard		 NVARCHAR(20) = 'DBA%'
,@ITOwners		 NVARCHAR(MAX)
AS
SELECT s.[ServerName]
      ,j.[name] AS [JobName]
	  ,j.[enabled]
	  ,jh.[Message]
      ,MAX(jh.[run_datetime]) AS [Run Date]
	  ,sc.[ITOwner]
FROM [hist].[Jobs_SQL_Jobs] j
  JOIN [hist].[Jobs_SQL_JobHistory] jh
	ON j.[histjobid] = jh.[histjobid]
  JOIN [hist].[ServerInventory_ServerIDs] s
	ON j.[HistServerID] = s.[HistServerID]
  JOIN [dbo].[ServerInventory_SQL_AllServers_vw] a
	ON s.[ServerName] = a.[FullName]
  JOIN [dbo].[ServerInventory_Environments] e
	ON a.[Environment] = e.[EnvironmentName]
  JOIN  [DBAStage].[dbo].[ServerInventory_SQL_VersionCompliance_vw] sc
	ON sc.[ServerName] =  a.[FullName]
  LEFT JOIN [dbo].[Split_fn](@EnvironmentIDs,',') eids
	ON e.[EnvironmentID] = eids.[item]
  LEFT JOIN [dbo].[Split_fn](@ServerIDs,',') sids
	ON s.[HistServerID] = sids.[item]
  LEFT JOIN [dbo].[Split_fn](@ITOwners,',') itos
    ON sc.[ITOwner] = itos.[item]
WHERE 
	jh.[step_id] = 0
	AND jh.[run_status] = 0
	AND (j.[name] LIKE '%maintenance%' 
			OR j.[name] LIKE '%index%'
			OR j.[name] LIKE '%stats%'
			OR j.[name] LIKE '%check%'
			OR j.[name] LIKE '%dbcc%'
			OR j.[name] LIKE @Wildcard)
	AND jh.[run_datetime] BETWEEN @StartDate AND @EndDate 
	AND ((eids.[item] IS NOT NULL AND @EnvironmentIDs IS NOT NULL) OR @EnvironmentIDs IS NULL)
	AND ((sids.[item] IS NOT NULL AND @ServerIDs IS NOT NULL) OR @ServerIDs IS NULL)
	AND ((itos.[item] IS NOT NULL AND @ITOwners IS NOT NULL) OR @ITOwners IS NULL)
GROUP BY s.[ServerName]
    ,j.[name]
    ,j.[enabled]
	,jh.[Message]
    ,j.[LastSeenOn]
	,sc.[ITOwner]
ORDER BY [ServerName],[JobName]


GO
/****** Object:  StoredProcedure [hist].[Jobs_SQL_GetJobFailures]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [hist].[Jobs_SQL_GetJobFailures]
@StartDate			DATETIME
,@EndDate			DATETIME
,@EnvironmentIDs	NVARCHAR(MAX)
,@ServerIDs			NVARCHAR(MAX)
AS
SELECT s.[ServerName]
      ,j.[name] AS [JobName]
	  ,YEAR(jh.[run_datetime]) AS [Year]
	  ,MONTH(jh.[run_datetime]) AS [Month]
	  ,COUNT(0) AS [Failures]
       FROM [hist].[Jobs_SQL_Jobs] j
       RIGHT JOIN [hist].[Jobs_SQL_JobHistory] jh
            ON j.[histjobid] = jh.[histjobid]
       RIGHT JOIN [hist].[ServerInventory_ServerIDs] s
            ON j.[HistServerID] = s.[HistServerID]
	   JOIN [dbo].[ServerInventory_SQL_AllServers_vw] a
			ON s.[ServerName] = a.[FullName]
	   JOIN [dbo].[ServerInventory_Environments] e
			ON a.[Environment] = e.[EnvironmentName]
	   LEFT JOIN [dbo].[Split_fn](@EnvironmentIDs,',') eids
			ON e.[EnvironmentID] = eids.[item]
	   LEFT JOIN [dbo].[Split_fn](@ServerIDs,',') sids
			ON s.[HistServerID] = sids.[item]
  WHERE 
       jh.[step_id] = 0
       AND jh.[run_status] = 0
	   AND jh.run_datetime BETWEEN @StartDate AND @EndDate
	   AND ((eids.[item] IS NOT NULL AND @EnvironmentIDs IS NOT NULL) OR @EnvironmentIDs IS NULL)
	   AND ((sids.[item] IS NOT NULL AND @ServerIDs IS NOT NULL) OR @ServerIDs IS NULL)
GROUP BY YEAR(jh.[run_datetime])
	  ,MONTH(jh.[run_datetime])
	  ,s.[ServerName]
      ,j.[name]
ORDER BY [ServerName], [JobName], YEAR(jh.[run_datetime])
	  ,MONTH(jh.[run_datetime])



GO
/****** Object:  StoredProcedure [hist].[Jobs_SQL_GetJobIDByName]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [hist].[Jobs_SQL_GetJobIDByName]
(
	@ServerName						VARCHAR(200)
	,@JobName						SYSNAME
	,@HistJobID						INT OUTPUT
)
AS

SET NOCOUNT ON

DECLARE 
	@HistServerID					INT

EXEC [hist].[ServerInventory_GetServerID] @ServerName, @HistServerID OUTPUT

SELECT
	@HistJobID = MAX(j.[HistJobID])
FROM [hist].Jobs_SQL_Jobs j
WHERE j.[HistServerID] = @HistServerID
AND j.[name] = @JobName


GO
/****** Object:  StoredProcedure [hist].[Jobs_SQL_GetPercentJobFailures]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [hist].[Jobs_SQL_GetPercentJobFailures]
@StartDate			DATETIME
,@EndDate			DATETIME
,@Percent			TINYINT
,@EnvironmentIDs	NVARCHAR(256)
,@ServerIDs			NVARCHAR(MAX)
,@ITOwners			NVARCHAR(MAX)
AS
SELECT A.[ServerName]
	   ,A.[JobName]
	   ,A.[Enabled]
	   ,A.[NumFailures]
	   ,A.[TotalRuns]
	   ,A.[PercentFailure]
	   ,A.[ITOwner]
FROM 
		(
			SELECT 
				s.[ServerName]		
				,j.[name]																					AS [JobName]
				,j.[enabled]																				AS [Enabled]
				,COUNT(CASE WHEN jh.[run_status] = 0 then 1 else NULL end)									AS [NumFailures]
				,COUNT(jh.[run_status])																		AS [TotalRuns]
				,COUNT(CASE WHEN jh.[run_status] = 0 then 1 else NULL end)*100  / COUNT(jh.[run_status])	AS [PercentFailure]
				,sc.[ITOwner]
			FROM [hist].[Jobs_SQL_Jobs] j
				JOIN [hist].[Jobs_SQL_JobHistory] jh
					ON j.[histjobid] = jh.[histjobid]
				RIGHT JOIN [hist].[ServerInventory_ServerIDs] s
					ON j.[HistServerID] = s.[HistServerID]
				JOIN [dbo].[ServerInventory_SQL_AllServers_vw] a
					ON s.[ServerName] = a.[FullName]
				JOIN [dbo].[ServerInventory_Environments] e
					ON a.[Environment] = e.[EnvironmentName]
				JOIN  [DBAStage].[dbo].[ServerInventory_SQL_VersionCompliance_vw] sc
					ON sc.[ServerName] =  a.[FullName]
				LEFT JOIN [dbo].[Split_fn](@EnvironmentIDs,',') eids
					ON e.[EnvironmentID] = eids.[item]
				LEFT JOIN [dbo].[Split_fn](@ServerIDs,',') sids
					ON s.[HistServerID] = sids.[item]
				LEFT JOIN [dbo].[Split_fn](@ITOwners,',') itos
					ON sc.[ITOwner] = itos.[item]
			WHERE jh.[run_datetime] BETWEEN @StartDate AND @EndDate
				AND jh.[step_id] = 0
				AND ((eids.[item] IS NOT NULL AND @EnvironmentIDs IS NOT NULL) OR @EnvironmentIDs IS NULL)
				AND ((sids.[item] IS NOT NULL AND @ServerIDs IS NOT NULL) OR @ServerIDs IS NULL)
				AND ((itos.[item] IS NOT NULL AND @ITOwners IS NOT NULL) OR @ITOwners IS NULL)
			GROUP BY 
				s.[ServerName]
				,j.[name]
				,j.[enabled]
				,sc.[ITOwner]
		) A
WHERE A.[PercentFailure] >= @Percent
ORDER BY [ServerName],[JobName],[PercentFailure] DESC


GO
/****** Object:  StoredProcedure [hist].[Jobs_SQL_InsertJob]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [hist].[Jobs_SQL_InsertJob]
(
	@ServerName						VARCHAR(200)
	,@job_id						UNIQUEIDENTIFIER
	,@name							SYSNAME
	,@enabled						TINYINT
	,@description					NVARCHAR(512)
	,@start_step_id					INT
	,@category_id					INT
	,@owner_sid						VARBINARY(85)
	,@delete_level					INT
	,@date_created					DATETIME
	,@date_modified					DATETIME
	,@version_number				INT
)
AS

SET NOCOUNT ON

DECLARE @HistServerID INT

EXEC [hist].[ServerInventory_GetServerID] @ServerName, @HistServerID OUTPUT

-- See if we've already got a record
IF NOT EXISTS(
	SELECT
		*
	FROM [hist].[Jobs_SQL_Jobs] j
	WHERE j.[HistServerID] = @HistServerID
	AND j.[job_id] = @job_id
	AND j.[date_modified] = @date_modified
)
-- No record found, do an insert
BEGIN

	INSERT INTO [hist].[Jobs_SQL_Jobs] ([HistServerID], [job_id], [name], [enabled], [description], [start_step_id], [category_id], [owner_sid], [delete_level], [date_created], [date_modified], [version_number])
	VALUES (@HistServerID, @job_id, @name, @enabled, @description, @start_step_id, @category_id, @owner_sid, @delete_level, @date_created, @date_modified, @version_number)

	SELECT 1
END
ELSE
BEGIN
	UPDATE j
		SET j.[LastSeenOn] = GETDATE()
	FROM [hist].[Jobs_SQL_Jobs] j
	WHERE j.[HistServerID] = @HistServerID
	AND j.[job_id] = @job_id
	AND j.[date_modified] = @date_modified

	SELECT 0
END


GO
/****** Object:  StoredProcedure [hist].[Jobs_SQL_InsertJobHistory]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [hist].[Jobs_SQL_InsertJobHistory]
(
	@ServerName						VARCHAR(200)
	,@JobName						SYSNAME
	,@instance_id					INT
	,@step_id						INT
	,@step_name						SYSNAME
	,@sql_message_id				INT
	,@sql_severity					INT
	,@message						NVARCHAR(1024)
	,@run_status					INT
	,@run_date						INT
	,@run_time						INT
	,@run_duration					INT
	,@operator_id_emailed			INT
	,@operator_id_netsent			INT
	,@operator_id_paged				INT
	,@retries_attempted				INT
	,@server						SYSNAME
)

AS

SET NOCOUNT ON

DECLARE 
	@HistJobID						INT
	,@run_datetime					DATETIME
	,@HistServerID					INT
	,@dt							CHAR(8)
	,@tm							CHAR(6)
	
EXEC [hist].[Jobs_SQL_GetJobIDByName] @ServerName, @JobName, @HistJobID OUTPUT
EXEC [hist].[ServerInventory_GetServerID] @server, @HistServerID OUTPUT

SET @dt = RIGHT('00000000' + CAST(@run_date AS VARCHAR(8)),8)
SET @tm = RIGHT('000000' + CAST(@run_time AS VARCHAR(6)),6)

SET @run_datetime = CAST(LEFT(@dt,4) + '-' + SUBSTRING(@dt,5,2) + '-' + RIGHT(@dt,2) + ' ' + LEFT(@tm,2) + ':' + SUBSTRING(@tm,3,2) + ':' + RIGHT(@tm,2) AS DATETIME)

IF @HistJobID IS NOT NULL
BEGIN

	IF NOT EXISTS (SELECT * FROM [hist].[Jobs_SQL_JobHistory] WHERE [HistJobID] = @HistJobID AND [instance_id] = @instance_id)
	BEGIN
		-- INSERT The record
		INSERT INTO [hist].[Jobs_SQL_JobHistory] ([HistJobID], [instance_id], [step_id], [step_name], [sql_message_id], [sql_severity], [message], [run_status], [run_datetime], [run_duration], [operator_id_emailed], [operator_id_netsent], [operator_id_paged], [retries_attempted], [HistServerID])
		VALUES(@HistJobID,@instance_id,@step_id,@step_name,@sql_message_id,@sql_severity,@message,@run_status,@run_datetime,@run_duration,@operator_id_emailed,@operator_id_netsent,@operator_id_paged,@retries_attempted,@HistServerID)
	
	END

END


GO
/****** Object:  StoredProcedure [hist].[Jobs_SQL_InsertJobStep]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [hist].[Jobs_SQL_InsertJobStep]
(
	@ServerName						VARCHAR(200)
	,@JobName						SYSNAME
	,@step_id						INT
	,@step_name						SYSNAME
	,@subsystem						NVARCHAR(40)
	,@command						NVARCHAR(max)
	,@flags							INT
	,@additional_parameters			NTEXT
	,@cmdexec_success_code			INT
	,@on_success_action				TINYINT
	,@on_success_step_id			INT
	,@on_fail_action				TINYINT
	,@on_fail_step_id				INT
	,@server						SYSNAME
	,@database_name					SYSNAME
	,@database_user_name			SYSNAME
	,@retry_attempts				INT
	,@retry_interval				INT
	,@os_run_priority				INT
	,@output_file_name				NVARCHAR(200)
)
AS

SET NOCOUNT ON

DECLARE
	@HistDatabaseID		INT
	,@HistServerID		INT
	,@HistJobID			INT
	
-- Lookup some IDs
EXEC [hist].[ServerInventory_GetServerID] @server, @HistServerID OUTPUT
EXEC [hist].[ServerInventory_SQL_GetDatabaseID] @database_name, @HistDatabaseID OUTPUT
EXEC [hist].[Jobs_SQL_GetJobIDByName] @ServerName, @JobName, @HistJobID OUTPUT

-- If the step does not already exist then add it
IF NOT EXISTS(SELECT * FROM [hist].[Jobs_SQL_JobSteps] WHERE [HistJobID] = @HistJobID AND [step_id] = @step_id)
BEGIN
	INSERT INTO [hist].[Jobs_SQL_JobSteps] ([HistJobID], [step_id], [step_name], [subsystem], [command], [flags], [additional_parameters], [cmdexec_success_code], [on_success_action], [on_success_step_id], [on_fail_action], [on_fail_step_id], [HistServerIDForServerCol], [HistDatabaseID], [database_user_name], [retry_attempts], [retry_interval], [os_run_priority], [output_file_name])
	VALUES (@HistJobID, @step_id, @step_name, @subsystem, @command, @flags, @additional_parameters, @cmdexec_success_code, @on_success_action, @on_success_step_id, @on_fail_action, @on_fail_step_id, @HistServerID, @HistDatabaseID, @database_user_name, @retry_attempts, @retry_interval, @os_run_priority, @output_file_name)
END


GO
/****** Object:  StoredProcedure [hist].[Logging_who_Collector]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************************************************************************
**	Name:			[audit].[Logging_who_Collector]
**	Desc:			Procedure to manage inserting who data from a remote instance
**	Auth:			Adam Bean (SQLSlayer.com)
**	Parameters:		All parameters are passed in from who_Collector powershell script
**	Date:			2009.04.01
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:		Description:
**	--------	--------	---------------------------------------
**	20090706	Adam Bean	Added lock support for modifications to who
********************************************************************************************************/

CREATE PROCEDURE [hist].[Logging_who_Collector]
(
	@ServerID				INT
	,@SPID					INT
	,@KPID					INT		
	,@DBName				VARCHAR(128)	
	,@Query					VARCHAR(4000)	
	,@Login					VARCHAR(128)		
	,@HostName				VARCHAR(128)		
	,@Status				VARCHAR(24)		
	,@Command				VARCHAR(32)		
	,@BlkBy					INT		
	,@TranCount				INT				
	,@ReadLockCount			INT				
	,@WriteLockCount		INT
	,@SchemaLockCount		INT
	,@WaitType				VARCHAR(64)	
	,@PercentComplete		REAL			
	,@EstCompTime			BIGINT			
	,@CPU					INT				
	,@IO					BIGINT			
	,@Reads					INT				
	,@Writes				INT				
	,@LastRead				DATETIME		
	,@LastWrite				DATETIME		
	,@StartTime				DATETIME		
	,@LastBatch				DATETIME		
	,@ProgramName			VARCHAR(256)		
)

AS

DECLARE
	@DatabaseID				INT
	,@QueryID				INT
	,@LoginID				INT
	,@HostID				INT
	,@StatusID				INT	
	,@CommandID				INT
	,@LockID				INT
	,@WaitID				INT
	,@ProgramID				INT

SET NOCOUNT ON

-- Setup table to hold captured data	
IF OBJECT_ID('audit.who_Logging') IS NULL
BEGIN
	CREATE TABLE [audit].[who_Logging]
		(
			[ID]					BIGINT IDENTITY(1,1)
			,[ServerID]				INT
			,[SPID]					INT
			,[KPID]					INT
			,[DatabaseID]			INT
			,[QueryID]				INT
			,[LoginID]				INT
			,[HostID]				INT
			,[StatusID]				INT
			,[CommandID]			INT
			,[BlkBy]				INT
			,[TranCount]			INT
			,[ReadLockCount]		INT
			,[WriteLockCount]		INT
			,[SchemaLockCount]		INT
			,[LockID]				INT
			,[WaitID]				INT
			,[PercentComplete]		DECIMAL(5,2)
			,[EstCompTime]			INT
			,[CPU]					INT
			,[IO]					INT
			,[Reads]				INT
			,[Writes]				INT
			,[LastRead]				SMALLDATETIME
			,[LastWrite]			SMALLDATETIME
			,[StartTime]			SMALLDATETIME
			,[LastBatch]			SMALLDATETIME
			,[ProgramID]			INT
			,[DateCreated]			SMALLDATETIME
		)
	ALTER TABLE [audit].[who_Logging] ADD CONSTRAINT [who_Logging_DateCreated]  DEFAULT (GETDATE()) FOR [DateCreated]
END

EXEC [audit].[Logging_who_GetDBName] @DBName, @DatabaseID OUTPUT
EXEC [audit].[Logging_who_GetQuery] @Query, @QueryID OUTPUT
EXEC [audit].[Logging_who_GetLoginName] @Login, @LoginID OUTPUT
EXEC [audit].[Logging_who_GetHostName] @HostName, @HostID OUTPUT
EXEC [audit].[Logging_who_GetStatus] @Status, @StatusID OUTPUT
EXEC [audit].[Logging_who_GetCommand] @Command, @CommandID OUTPUT
EXEC [audit].[Logging_who_GetWaitType] @WaitType, @WaitID OUTPUT
EXEC [audit].[Logging_who_GetProgramName] @ProgramName, @ProgramID OUTPUT

INSERT INTO [audit].[who_Logging]
([ServerID], [SPID], [KPID], [DatabaseID], [QueryID], [LoginID], [HostID], [StatusID], [CommandID], [BlkBy], [TranCount], [ReadLockCount], [WriteLockCount], [SchemaLockCount], [WaitID], [PercentComplete], [EstCompTime], [CPU], [IO], [Reads], [Writes], [LastRead], [LastWrite], [StartTime], [LastBatch], [ProgramID])
VALUES
(@ServerID, @SPID, @KPID, @DatabaseID, @QueryID, @LoginID, @HostID, @StatusID, @CommandID, @BlkBy, @TranCount, @ReadLockCount, @WriteLockCount, @SchemaLockCount, @WaitID, @PercentComplete, @EstCompTime, @CPU, @IO, @Reads, @Writes, @LastRead, @LastWrite, @StartTime, @LastBatch, @ProgramID)

SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [hist].[Logging_who_GetCommand]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************************************************************************
**	Name:			[audit].[Logging_who_GetCommand]
**	Desc:			Procedure to retrieve Command ID for [audit].[Logging_who_Collector]
**	Auth:			Adam Bean (SQLSlayer.com)
**	Parameters:		All parameters are passed in from [Logging_who_Collector]
**	Date:			2009.04.17
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:		Description:
**	--------	--------	---------------------------------------
** 
********************************************************************************************************/

CREATE PROCEDURE [hist].[Logging_who_GetCommand]
(
	@Command	VARCHAR(128)
	,@CommandID	INT OUTPUT
)
AS

SET NOCOUNT ON

-- Create table
IF OBJECT_ID('audit.who_CommandIDs') IS NULL
BEGIN
	CREATE TABLE [audit].[who_CommandIDs]
	(
		[CommandID]	INT IDENTITY(1,1) NOT NULL
		,[Command]	VARCHAR (128) NULL
		,PRIMARY KEY NONCLUSTERED ([CommandID] ASC)
	) ON [PRIMARY]
END

-- Find the Command ID
SELECT 
	@CommandID = [CommandID]
FROM [audit].[who_CommandIDs] id
WHERE (id.[Command] = @Command AND @Command IS NOT NULL)
OR (id.[Command] IS NULL AND @Command IS NULL)

IF @CommandID IS NULL
BEGIN
	INSERT INTO [audit].[who_CommandIDs] ([Command]) 
	VALUES (@Command)
	
	SET @CommandID = SCOPE_IDENTITY()
END

SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [hist].[Logging_who_GetDatabaseName]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************************************************************************
**	Name:			[audit].[Logging_who_GetDatabaseName]
**	Desc:			Procedure to retrieve Database ID for [audit].[Logging_who_Collector]
**	Auth:			Adam Bean (SQLSlayer.com)
**	Parameters:		All parameters are passed in from [Logging_who_Collector]
**	Date:			2009.04.17
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:		Description:
**	--------	--------	---------------------------------------
** 
********************************************************************************************************/

CREATE PROCEDURE [hist].[Logging_who_GetDatabaseName]
(
	@DatabaseName		VARCHAR(128)
	,@DatabaseID		INT OUTPUT
)
AS

SET NOCOUNT ON

-- Create table
IF OBJECT_ID('audit.who_DatabaseIDs') IS NULL
BEGIN
	CREATE TABLE [audit].[who_DatabaseIDs]
	(
		[DatabaseID]	INT IDENTITY(1,1) NOT NULL
		,[DatabaseName]	VARCHAR (128) NULL
		,PRIMARY KEY NONCLUSTERED ([DatabaseID] ASC)
	) ON [PRIMARY]
END

-- Find the Database ID
SELECT 
	@DatabaseID = [DatabaseID]
FROM [audit].[who_DatabaseIDs] id
WHERE (id.[DatabaseName] = @DatabaseName AND @DatabaseName IS NOT NULL)
OR (id.[DatabaseName] IS NULL AND @DatabaseName IS NULL)

IF @DatabaseID IS NULL
BEGIN
	INSERT INTO [audit].[who_DatabaseIDs] (DatabaseName) 
	VALUES (@DatabaseName)
	
	SET @DatabaseID = SCOPE_IDENTITY()
END

SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [hist].[Logging_who_GetDBName]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************************************************************************
**	Name:			[audit].[Logging_who_GetDBName]
**	Desc:			Procedure to retrieve database ID for [audit].[Logging_who_Collector]
**	Auth:			Adam Bean (SQLSlayer.com)
**	Parameters:		All parameters are passed in from [Logging_who_Collector]
**	Date:			2009.04.17
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:		Description:
**	--------	--------	---------------------------------------
** 
********************************************************************************************************/

CREATE PROCEDURE [hist].[Logging_who_GetDBName]
(
	@DBName		VARCHAR(128)
	,@DBID		INT OUTPUT
)
AS

SET NOCOUNT ON

-- Create table
IF OBJECT_ID('audit.who_DatabaseIDs') IS NULL
BEGIN
	CREATE TABLE [audit].[who_DatabaseIDs]
	(
		[DatabaseID]	INT IDENTITY(1,1) NOT NULL
		,[DBName]		VARCHAR (128) NULL
		,PRIMARY KEY NONCLUSTERED ([DatabaseID] ASC)
	) ON [PRIMARY]
END

-- Find the DatabaseID
SELECT 
	@DBID = [DatabaseID]
FROM [audit].[who_DatabaseIDs] id
WHERE id.[DBName] = @DBName

IF @DBID IS NULL
BEGIN
	INSERT INTO [audit].[who_DatabaseIDs] (DBName) 
	VALUES (@DBName)
	
	SET @DBID = SCOPE_IDENTITY()
END

SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [hist].[Logging_who_GetHostName]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************************************************************************
**	Name:			[audit].[Logging_who_GetHostName]
**	Desc:			Procedure to retrieve Host ID for [audit].[Logging_who_Collector]
**	Auth:			Adam Bean (SQLSlayer.com)
**	Parameters:		All parameters are passed in from [Logging_who_Collector]
**	Date:			2009.04.17
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:		Description:
**	--------	--------	---------------------------------------
** 
********************************************************************************************************/

CREATE PROCEDURE [hist].[Logging_who_GetHostName]
(
	@HostName	VARCHAR(128)
	,@HostID	INT OUTPUT
)
AS

SET NOCOUNT ON

-- Create table
IF OBJECT_ID('audit.who_HostIDs') IS NULL
BEGIN
	CREATE TABLE [audit].[who_HostIDs]
	(
		[HostID]	INT IDENTITY(1,1) NOT NULL
		,[HostName]	VARCHAR (128) NULL
		,PRIMARY KEY NONCLUSTERED ([HostID] ASC)
	) ON [PRIMARY]
END

-- Find the Host ID
SELECT 
	@HostID = [HostID]
FROM [audit].[who_HostIDs] id
WHERE (id.[HostName] = @HostName AND @HostName IS NOT NULL)
OR (id.[HostName] IS NULL AND @HostName IS NULL)

IF @HostID IS NULL
BEGIN
	INSERT INTO [audit].[who_HostIDs] ([HostName]) 
	VALUES (@HostName)
	
	SET @HostID = SCOPE_IDENTITY()
END

SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [hist].[Logging_who_GetLock]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************************************************************************
**	Name:			[audit].[Logging_who_GetLock]
**	Desc:			Procedure to retrieve Lock ID for [audit].[Logging_who_Collector]
**	Auth:			Adam Bean (SQLSlayer.com)
**	Parameters:		All parameters are passed in from [Logging_who_Collector]
**	Date:			2009.04.17
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:		Description:
**	--------	--------	---------------------------------------
** 
********************************************************************************************************/

CREATE PROCEDURE [hist].[Logging_who_GetLock]
(
	@LockMode	VARCHAR(4000)
	,@LockID	INT OUTPUT
)
AS

SET NOCOUNT ON

-- Create table
IF OBJECT_ID('audit.who_LockIDs') IS NULL
BEGIN
	CREATE TABLE [audit].[who_LockIDs]
	(
		[LockID]	INT IDENTITY(1,1) NOT NULL
		,[LockMode]	VARCHAR (128) NULL
		,PRIMARY KEY NONCLUSTERED ([LockID] ASC)
	) ON [PRIMARY]
END

-- Find the LoginID
SELECT 
	@LockID = [LockID]
FROM [audit].[who_LockIDs] id
WHERE id.[LockID] = @LockMode

IF @LockID IS NULL
BEGIN
	INSERT INTO [audit].[who_LockIDs] ([LockMode]) 
	VALUES (@LockMode)
	
	SET @LockID = SCOPE_IDENTITY()
END

SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [hist].[Logging_who_GetLockMode]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************************************************************************
**	Name:			[audit].[Logging_who_GetLockMode]
**	Desc:			Procedure to retrieve Lock ID for [audit].[Logging_who_Collector]
**	Auth:			Adam Bean (SQLSlayer.com)
**	Parameters:		All parameters are passed in from [Logging_who_Collector]
**	Date:			2009.04.17
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:		Description:
**	--------	--------	---------------------------------------
** 
********************************************************************************************************/

CREATE PROCEDURE [hist].[Logging_who_GetLockMode]
(
	@LockMode	VARCHAR(4000)
	,@LockID	INT OUTPUT
)
AS

SET NOCOUNT ON

-- Create table
IF OBJECT_ID('audit.who_LockIDs') IS NULL
BEGIN
	CREATE TABLE [audit].[who_LockIDs]
	(
		[LockID]	INT IDENTITY(1,1) NOT NULL
		,[LockMode]	VARCHAR (128) NULL
		,PRIMARY KEY NONCLUSTERED ([LockID] ASC)
	) ON [PRIMARY]
END

-- Find the Lock ID
SELECT 
	@LockID = [LockID]
FROM [audit].[who_LockIDs] id
WHERE (id.[LockMode] = @LockMode AND @LockMode IS NOT NULL)
OR (id.[LockMode] IS NULL AND @LockMode IS NULL)

IF @LockID IS NULL
BEGIN
	INSERT INTO [audit].[who_LockIDs] ([LockMode]) 
	VALUES (@LockMode)
	
	SET @LockID = SCOPE_IDENTITY()
END

SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [hist].[Logging_who_GetLoginName]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************************************************************************
**	Name:			[audit].[Logging_who_GetLoginName]
**	Desc:			Procedure to retrieve Login ID for [audit].[Logging_who_Collector]
**	Auth:			Adam Bean (SQLSlayer.com)
**	Parameters:		All parameters are passed in from [Logging_who_Collector]
**	Date:			2009.04.17
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:		Description:
**	--------	--------	---------------------------------------
** 
********************************************************************************************************/

CREATE PROCEDURE [hist].[Logging_who_GetLoginName]
(
	@LoginName	VARCHAR(128)
	,@LoginID	INT OUTPUT
)
AS

SET NOCOUNT ON

-- Create table
IF OBJECT_ID('audit.who_LoginIDs') IS NULL
BEGIN
	CREATE TABLE [audit].[who_LoginIDs]
	(
		[LoginID]		INT IDENTITY(1,1) NOT NULL
		,[Login]		VARCHAR (128) NULL
		,PRIMARY KEY NONCLUSTERED ([LoginID] ASC)
	) ON [PRIMARY]
END

-- Find the Login ID
SELECT 
	@LoginID = [LoginID]
FROM [audit].[who_LoginIDs] id
WHERE (id.[Login] = @LoginName AND @LoginName IS NOT NULL)
OR (id.[Login] IS NULL AND @LoginName IS NULL)

IF @LoginID IS NULL
BEGIN
	INSERT INTO [audit].[who_LoginIDs] ([Login]) 
	VALUES (@LoginName)
	
	SET @LoginID = SCOPE_IDENTITY()
END

SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [hist].[Logging_who_GetProgramName]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************************************************************************
**	Name:			[audit].[Logging_who_GetProgramName]
**	Desc:			Procedure to retrieve Program ID for [audit].[Logging_who_Collector]
**	Auth:			Adam Bean (SQLSlayer.com)
**	Parameters:		All parameters are passed in from [Logging_who_Collector]
**	Date:			2009.04.17
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:		Description:
**	--------	--------	---------------------------------------
** 
********************************************************************************************************/

CREATE PROCEDURE [hist].[Logging_who_GetProgramName]
(
	@ProgramName	VARCHAR(128)
	,@ProgramID		INT OUTPUT
)
AS

SET NOCOUNT ON

-- Create table
IF OBJECT_ID('audit.who_ProgramIDs') IS NULL
BEGIN
	CREATE TABLE [audit].[who_ProgramIDs]
	(
		[ProgramID]	INT IDENTITY(1,1) NOT NULL
		,[ProgramName]	VARCHAR (128) NULL
		,PRIMARY KEY NONCLUSTERED ([ProgramID] ASC)
	) ON [PRIMARY]
END

-- Find the Program ID
SELECT 
	@ProgramID = [ProgramID]
FROM [audit].[who_ProgramIDs] id
WHERE (id.[ProgramName] = @ProgramName AND @ProgramName IS NOT NULL)
OR (id.[ProgramName] IS NULL AND @ProgramName IS NULL)

IF @ProgramID IS NULL
BEGIN
	INSERT INTO [audit].[who_ProgramIDs] (ProgramName) 
	VALUES (@ProgramName)
	
	SET @ProgramID = SCOPE_IDENTITY()
END

SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [hist].[Logging_who_GetQuery]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************************************************************************
**	Name:			[audit].[Logging_who_GetQuery]
**	Desc:			Procedure to retrieve Query ID for [audit].[Logging_who_Collector]
**	Auth:			Adam Bean (SQLSlayer.com)
**	Parameters:		All parameters are passed in from [Logging_who_Collector]
**	Date:			2009.04.17
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:		Description:
**	--------	--------	---------------------------------------
** 
********************************************************************************************************/

CREATE PROCEDURE [hist].[Logging_who_GetQuery]
(
	@Query		VARCHAR(4000)
	,@QueryID	INT OUTPUT
)
AS

SET NOCOUNT ON

-- Create table 
IF OBJECT_ID('audit.who_QueryIDs') IS NULL
BEGIN
	CREATE TABLE [audit].[who_QueryIDs]
	(
		[QueryID]	INT IDENTITY(1,1) NOT NULL
		,[Query]	VARCHAR (4000) NULL
		,PRIMARY KEY NONCLUSTERED ([QueryID] ASC)
	) ON [PRIMARY]
END

-- Find the Query ID
SELECT 
      @QueryID = [QueryID]
FROM [audit].[who_QueryIDs] id
WHERE (id.[Query] = @Query AND @Query IS NOT NULL)
OR (id.[Query] IS NULL AND @Query IS NULL)


IF @QueryID IS NULL
BEGIN
	INSERT INTO [audit].[who_QueryIDs] ([Query]) 
	VALUES (@Query)
	
	SET @QueryID = SCOPE_IDENTITY()
END

SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [hist].[Logging_who_GetStatus]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************************************************************************
**	Name:			[audit].[Logging_who_GetStatus]
**	Desc:			Procedure to retrieve Status ID for [audit].[Logging_who_Collector]
**	Auth:			Adam Bean (SQLSlayer.com)
**	Parameters:		All parameters are passed in from [Logging_who_Collector]
**	Date:			2009.04.17
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:		Description:
**	--------	--------	---------------------------------------
** 
********************************************************************************************************/

CREATE PROCEDURE [hist].[Logging_who_GetStatus]
(
	@Status		VARCHAR(128)
	,@StatusID	INT OUTPUT
)
AS

SET NOCOUNT ON

-- Create table
IF OBJECT_ID('audit.who_StatusIDs') IS NULL
BEGIN
	CREATE TABLE [audit].[who_StatusIDs]
	(
		[StatusID]	INT IDENTITY(1,1) NOT NULL
		,[Status]	VARCHAR (128) NULL
		,PRIMARY KEY NONCLUSTERED ([StatusID] ASC)
	) ON [PRIMARY]
END

-- Find the Status ID
SELECT 
	@StatusID = [StatusID]
FROM [audit].[who_StatusIDs] id
WHERE (id.[Status] = @Status AND @Status IS NOT NULL)
OR (id.[Status] IS NULL AND @Status IS NULL)

IF @StatusID IS NULL
BEGIN
	INSERT INTO [audit].[who_StatusIDs] ([Status]) 
	VALUES (@Status)
	
	SET @StatusID = SCOPE_IDENTITY()
END

SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [hist].[Logging_who_GetWaitType]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************************************************************************
**	Name:			[audit].[Logging_who_GetWaitType]
**	Desc:			Procedure to retrieve Wait ID for [audit].[Logging_who_Collector]
**	Auth:			Adam Bean (SQLSlayer.com)
**	Parameters:		All parameters are passed in from [Logging_who_Collector]
**	Date:			2009.04.17
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:		Description:
**	--------	--------	---------------------------------------
** 
********************************************************************************************************/

CREATE PROCEDURE [hist].[Logging_who_GetWaitType]
(
	@WaitType	VARCHAR(4000)
	,@WaitID	INT OUTPUT
)
AS

SET NOCOUNT ON

-- Create table
IF OBJECT_ID('audit.who_WaitIDs') IS NULL
BEGIN
	CREATE TABLE [audit].[who_WaitIDs]
	(
		[WaitID]	INT IDENTITY(1,1) NOT NULL
		,[WaitType]	VARCHAR (128) NULL
		,PRIMARY KEY NONCLUSTERED ([WaitID] ASC)
	) ON [PRIMARY]
END

-- Find the Wait ID
SELECT 
	@WaitID = [WaitID]
FROM [audit].[who_WaitIDs] id
WHERE (id.[WaitType] = @WaitType AND @WaitType IS NOT NULL)
OR (id.[WaitType] IS NULL AND @WaitType IS NULL)

IF @WaitID IS NULL
BEGIN
	INSERT INTO [audit].[who_WaitIDs] ([WaitType]) 
	VALUES (@WaitType)
	
	SET @WaitID = SCOPE_IDENTITY()
END

SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [hist].[Metrics_QueryStats_Insert]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************************************************************************
**  Name:			[hist].[Metrics_QueryStats_Insert]
**  Desc:			Procedure to insert history values
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			2009-12-11
**  Debug:			
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20100218	Matt Stanford	Removed the average columns
********************************************************************************************************/
CREATE PROCEDURE [hist].[Metrics_QueryStats_Insert] (
	@ServerName					VARCHAR(200)
	,@DBName					NVARCHAR(128)
	,@Schema					NVARCHAR(128)
	,@Object					NVARCHAR(128)
	,@Count						BIGINT 
	,@Total_CPU_Time			BIGINT 
	,@Last_CPU					BIGINT 
	,@Min_CPU					BIGINT 
	,@Max_CPU					BIGINT 
	,@Total_Run_Time			BIGINT 
	,@Last_Run_Time				BIGINT 
	,@Min_Run_Time				BIGINT 
	,@Max_Run_Time				BIGINT 
	,@Total_Logical_Writes		BIGINT 
	,@Last_Logical_Writes		BIGINT 
	,@Min_Logical_Writes		BIGINT 
	,@Max_Logical_Writes		BIGINT 
	,@Total_Physical_Reads		BIGINT 
	,@Last_Physical_Reads		BIGINT 
	,@Min_Physical_Reads		BIGINT 
	,@Max_Physical_Reads		BIGINT 
	,@Total_Logical_Reads		BIGINT 
	,@Last_Logical_Reads		BIGINT 
	,@Min_Logical_Reads			BIGINT 
	,@Max_Logical_Reads			BIGINT 
)
AS

-- Lookup the server id
DECLARE
	@HistServerID			INT
	,@HistObjectID			INT
	,@HistDatabaseID		INT
	
EXEC [hist].[ServerInventory_GetServerID] @ServerName, @HistServerID OUTPUT

-- Get the database id
EXEC [hist].[ServerInventory_SQL_GetDatabaseID] @DBName, @HistDatabaseID OUTPUT

-- Get the object id
EXEC [hist].[ServerInventory_SQL_GetObjectID] @Object, @Schema, NULL, @HistObjectID OUTPUT

-- insert the data
INSERT INTO [hist].[Metrics_QueryStats] ([HistServerID], [HistDatabaseID], [HistObjectID], [Count], [Total_CPU_Time], [Last_CPU], [Min_CPU], [Max_CPU], [Total_Run_Time], [Last_Run_Time], [Min_Run_Time], [Max_Run_Time], [Total_Logical_Writes], [Last_Logical_Writes], [Min_Logical_Writes], [Max_Logical_Writes], [Total_Physical_Reads], [Last_Physical_Reads], [Min_Physical_Reads], [Max_Physical_Reads], [Total_Logical_Reads], [Last_Logical_Reads], [Min_Logical_Reads], [Max_Logical_Reads])
VALUES (@HistServerID,@HistDatabaseID, @HistObjectID,@Count, @Total_CPU_Time, @Last_CPU, @Min_CPU, @Max_CPU, @Total_Run_Time, @Last_Run_Time, @Min_Run_Time, @Max_Run_Time, @Total_Logical_Writes, @Last_Logical_Writes, @Min_Logical_Writes, @Max_Logical_Writes, @Total_Physical_Reads, @Last_Physical_Reads, @Min_Physical_Reads, @Max_Physical_Reads, @Total_Logical_Reads, @Last_Logical_Reads, @Min_Logical_Reads, @Max_Logical_Reads)



GO
/****** Object:  StoredProcedure [hist].[ServerInventory_GetServerID]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[ServerInventory_GetServerID]
**  Desc:			Gets/Creates a HistServerID based on the Server Name
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			(unknown)
**  Debug:			
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090716	Matt Stanford	Reduced the data type from VARCHAR(1000) to VARCHAR(200)
**								Changed because of HistServerID change
********************************************************************************************************/
CREATE PROCEDURE [hist].[ServerInventory_GetServerID] (
	@ServerName			VARCHAR(200)
	,@ServerID			INT OUTPUT
)
AS

-- Find the ServerID
SELECT 
	@ServerID = [HistServerID]
FROM 
	[hist].[ServerInventory_ServerIDs] id
WHERE (id.[ServerName] = @ServerName AND @ServerName IS NOT NULL)
OR (id.[ServerName] IS NULL AND @ServerName IS NULL)

IF @ServerID IS NULL
BEGIN
	INSERT INTO [hist].[ServerInventory_ServerIDs] ([ServerName]) 
	VALUES (@ServerName)
	
	SET @ServerID = SCOPE_IDENTITY()
END


GO
/****** Object:  StoredProcedure [hist].[ServerInventory_SQL_Configurations_InsertValue]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[ServerInventory_SQL_Configurations_InsertValue]
**  Desc:			Adds all of the configuration values historically for all servers
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			(unknown)
**  Debug:			
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
********************************************************************************************************/
CREATE PROCEDURE [hist].[ServerInventory_SQL_Configurations_InsertValue] (
	@ServerName					VARCHAR(200)
	,@ConfigName				NVARCHAR(35)
	,@ConfigValue				SQL_VARIANT
	,@ConfigValueInUse			SQL_VARIANT
)
AS

DECLARE 
	@RefConfigOptionID			INT
	,@HistServerID				INT
	,@HistConfigValueID			INT

-- Get/Create the HistServerID
EXEC [hist].[ServerInventory_GetServerID] @ServerName, @HistServerID OUTPUT

-- Lookup the ConfigOptionID
SELECT
	@RefConfigOptionID = co.[RefConfigOptionID]
FROM [ref].[ServerInventory_SQL_ConfigurationOptions] co
WHERE co.[name] = @ConfigName

IF @RefConfigOptionID IS NOT NULL
BEGIN

	-- Check to see if the value already exists
	SELECT 
		@HistConfigValueID = MAX([HistConfigValueID])
	FROM [hist].[ServerInventory_SQL_ConfigurationValues]
	WHERE [RefConfigOptionID]	= @RefConfigOptionID
	AND [HistServerID]			= @HistServerID
	AND [value]					= @ConfigValue
	AND [value_in_use]			= @ConfigValueInUse
	
	IF @HistConfigValueID IS NOT NULL
	BEGIN
		-- Configuration Exists, mark it as "seen"
		-- Do an update
		UPDATE [hist].[ServerInventory_SQL_ConfigurationValues]
		SET [DateLastSeenOn] = GETDATE()
		WHERE [HistConfigValueID] = @HistConfigValueID
	END
	ELSE
	BEGIN
		-- Doesn't exist, new configuration!!!
		-- Do an insert
		INSERT INTO [hist].[ServerInventory_SQL_ConfigurationValues] ([RefConfigOptionID], [HistServerID], [value], [value_in_use])
		VALUES (@RefConfigOptionID, @HistServerID, @ConfigValue, @ConfigValueInUse)
	
	END

END

GO
/****** Object:  StoredProcedure [hist].[ServerInventory_SQL_GetDatabaseID]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [hist].[ServerInventory_SQL_GetDatabaseID] (
	@DBName				SYSNAME
	,@DatabaseID		INT OUTPUT
)
AS

SELECT 
	@DatabaseID = [DatabaseID]
FROM 
	[hist].[ServerInventory_SQL_DatabaseIDs] id
WHERE (id.[DBName] = @DBName AND @DBName IS NOT NULL)
OR (id.[DBName] IS NULL AND @DBName IS NULL)

IF @DatabaseID IS NULL
BEGIN
	INSERT INTO [hist].[ServerInventory_SQL_DatabaseIDs] (DBName) 
	VALUES (@DBName)
	
	SET @DatabaseID = SCOPE_IDENTITY()
END

GO
/****** Object:  StoredProcedure [hist].[ServerInventory_SQL_GetHistIndexID]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[ServerInventory_SQL_GetHistIndexID]
**  Desc:			Adds/Looks up the HistIndexID for each Index
**  Auth:			Matt Stanford
**  Date:			2009-10-13
**  Debug:			
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
********************************************************************************************************/
CREATE PROCEDURE [hist].[ServerInventory_SQL_GetHistIndexID] (
	@ServerName				VARCHAR(200)
	,@DatabaseName			NVARCHAR(128)
	,@SchemaName			NVARCHAR(128)
	,@TableName				NVARCHAR(128)
	,@IndexName				NVARCHAR(128)
	,@IndexType				TINYINT
	,@is_unique				BIT
	,@ignore_dup_key		BIT
	,@is_primary_key		BIT
	,@fillfactor			TINYINT
	,@is_padded				BIT
	,@is_disabled			BIT
	,@allow_row_locks		BIT
	,@allow_page_locks		BIT
	,@has_filter			BIT
	,@HistIndexID			INT OUTPUT
)

AS
SET NOCOUNT ON

DECLARE
	@HistServerDBTableID	INT
	
-- Get the Server - DB - Schema - Table ID
EXEC [hist].[ServerInventory_SQL_GetServerDBTableID] 
	@ServerName			= @ServerName
	,@DatabaseName		= @DatabaseName
	,@SchemaName		= @SchemaName
	,@TableName			= @TableName
	,@ServerDBTableID	= @HistServerDBTableID OUTPUT
	
-- Lookup the index
SELECT 
	@HistIndexID = [HistIndexID]
FROM [hist].[ServerInventory_SQL_IndexMaster] im
WHERE [HistServerDBTableID] = @HistServerDBTableID
AND [IndexName] = @IndexName
AND [is_unique] = @is_unique
AND [ignore_dup_key] = @ignore_dup_key
AND [is_primary_key] = @is_primary_key
AND [fillfactor] = @fillfactor
AND [is_padded] = @is_padded
AND [is_disabled] = @is_disabled
AND [allow_row_locks] = @allow_row_locks
AND [allow_page_locks] = @allow_page_locks
AND [has_filter] = @has_filter

IF @HistIndexID IS NOT NULL
BEGIN -- Update the last seen date

	UPDATE [hist].[ServerInventory_SQL_IndexMaster]
		SET [DateLastSeenOn] = GETDATE()
	WHERE [HistIndexID] = @HistIndexID

END
ELSE -- Gotta do an insert
BEGIN
	INSERT INTO [hist].[ServerInventory_SQL_IndexMaster]
	VALUES (@HistServerDBTableID,@IndexName,@IndexType,@is_unique,@ignore_dup_key,@is_primary_key,@fillfactor,@is_padded
	,@is_disabled,@allow_row_locks,@allow_page_locks,@has_filter,GETDATE(),GETDATE())
	
	SET @HistIndexID = SCOPE_IDENTITY()
END


GO
/****** Object:  StoredProcedure [hist].[ServerInventory_SQL_GetObjectID]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[ServerInventory_SQL_GetObjectID]
**  Desc:			Procedure to get/insert object IDs
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			2009-12-11
**  Debug:			
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20091215	Matt Stanford	Typo - fixed
********************************************************************************************************/
CREATE PROCEDURE [hist].[ServerInventory_SQL_GetObjectID] (
	@ObjectName			NVARCHAR(128)
	,@SchemaName		NVARCHAR(128)
	,@SQLType			NVARCHAR(128) = NULL
	,@ObjectID			INT OUTPUT
)
AS

SELECT 
	@ObjectID = ObjectID
FROM 
	[hist].[ServerInventory_SQL_ObjectIDs] id
WHERE id.[ObjectName] = @ObjectName
AND id.[SchemaName] = @SchemaName
AND (id.[SQLType] = @SQLType OR @SQLType IS NULL)


IF @ObjectID IS NULL
BEGIN
	INSERT INTO [hist].[ServerInventory_SQL_ObjectIDs] ([ObjectName], [SchemaName],[SQLType])
	VALUES (@ObjectName,@SchemaName,@SQLType)
	
	SET @ObjectID = SCOPE_IDENTITY()
END


GO
/****** Object:  StoredProcedure [hist].[ServerInventory_SQL_GetServerDBTableID]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[ServerInventory_SQL_GetServerDBTableID]
**  Desc:			Procedure to save table/server pairings into the repository
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			(unknown)
**  Debug:			
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090728	Matt Stanford	Emergency change because of broken HistServerID
********************************************************************************************************/
CREATE PROCEDURE [hist].[ServerInventory_SQL_GetServerDBTableID] (
	@ServerName			VARCHAR(1000)
	,@DatabaseName		SYSNAME
	,@SchemaName		SYSNAME
	,@TableName			SYSNAME
	,@ServerDBTableID	INT OUTPUT
)
AS

DECLARE 
	@ServerID			INT
	,@DatabaseID		INT
	,@TableID			INT

-- Find the ServerID
EXEC [hist].[ServerInventory_GetServerID] @ServerName, @ServerID OUTPUT

-- Find the DatabaseID
EXEC [hist].[ServerInventory_SQL_GetDatabaseID] @DatabaseName, @DatabaseID OUTPUT

-- Find the TableID
EXEC [hist].[ServerInventory_SQL_GetTableID] @TableName, @SchemaName, @TableID OUTPUT

-- Find the ServerDBTableID	
SELECT 
	@ServerDBTableID = ServerDBTableID
FROM
	[hist].[ServerInventory_SQL_ServerDBTableIDs] id
WHERE id.[HistServerID]	= @ServerID
AND	id.[DatabaseID]	= @DatabaseID
AND id.[TableID]	= @TableID

-- Add the server/db/schema/table combo if necessary
IF @ServerDBTableID IS NULL
BEGIN
	INSERT INTO [hist].[ServerInventory_SQL_ServerDBTableIDs] ([HistServerID], [DatabaseID], [TableID], [LastUpdated]) 
	VALUES (@ServerID, @DatabaseID, @TableID, GETDATE())
	
	SET @ServerDBTableID = SCOPE_IDENTITY()
END


GO
/****** Object:  StoredProcedure [hist].[ServerInventory_SQL_GetTableID]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [hist].[ServerInventory_SQL_GetTableID] (
	@TableName			SYSNAME
	,@SchemaName		SYSNAME
	,@TableID			INT OUTPUT
)
AS

SELECT 
	@TableID = TableID
FROM 
	[hist].[ServerInventory_SQL_TableIDs] id
WHERE id.[TableName] = @TableName
AND id.[SchemaName] = @SchemaName

IF @TableID IS NULL
BEGIN
	INSERT INTO [hist].[ServerInventory_SQL_TableIDs] ([TableName], [SchemaName]) 
	VALUES (@TableName,@SchemaName)
	
	SET @TableID = SCOPE_IDENTITY()
END

GO
/****** Object:  StoredProcedure [hist].[ServerInventory_SQL_IndexUsage_InsertValue]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[ServerInventory_SQL_IndexUsage_InsertValue]
**  Desc:			Inserts Usage information for each index
**  Auth:			Matt Stanford
**  Date:			2009-10-13
**  Debug:			
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
********************************************************************************************************/
CREATE PROCEDURE [hist].[ServerInventory_SQL_IndexUsage_InsertValue] (
	@ServerName				VARCHAR(200)
	,@DatabaseName			NVARCHAR(128)
	,@SchemaName			NVARCHAR(128)
	,@TableName				NVARCHAR(128)
	,@IndexName				NVARCHAR(128)
	,@IndexType				TINYINT
	,@is_unique				BIT
	,@ignore_dup_key		BIT
	,@is_primary_key		BIT
	,@fillfactor			TINYINT
	,@is_padded				BIT
	,@is_disabled			BIT
	,@allow_row_locks		BIT
	,@allow_page_locks		BIT
	,@has_filter			BIT
	,@ReadCount				BIGINT
	,@WriteCount			BIGINT
	,@SampleMSTicks			BIGINT
)
AS

SET NOCOUNT ON

DECLARE
	@HistIndexID			INT

EXEC [hist].[ServerInventory_SQL_GetHistIndexID] 
	@ServerName = @ServerName
	,@DatabaseName = @DatabaseName
	,@SchemaName = @SchemaName
	,@TableName = @TableName
	,@IndexName = @IndexName
	,@IndexType = @IndexType
	,@is_unique = @is_unique
	,@ignore_dup_key = @ignore_dup_key
	,@is_primary_key = @is_primary_key
	,@fillfactor = @fillfactor
	,@is_padded = @is_padded
	,@is_disabled = @is_disabled
	,@allow_row_locks = @allow_row_locks
	,@allow_page_locks = @allow_page_locks
	,@has_filter = @has_filter
	,@HistIndexID = @HistIndexID OUTPUT

INSERT INTO [hist].[ServerInventory_SQL_IndexUsage] ([HistIndexID],[ReadCount],[WriteCount],[SampleMSTicks],[SampleDate])
VALUES (@HistIndexID,@ReadCount,@WriteCount,@SampleMSTicks,GETDATE())


GO
/****** Object:  StoredProcedure [hist].[ServerInventory_SQL_SaveDatabaseAttribute]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[ServerInventory_SQL_SaveDatabaseAttribute]
**  Desc:			Stores database attribute values
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			2011.04.06
**  Debug:			
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
********************************************************************************************************/
CREATE PROCEDURE [hist].[ServerInventory_SQL_SaveDatabaseAttribute] (
	@ServerName				VARCHAR(200)
	,@DatabaseName			VARCHAR(128)
	,@file_id				INT
	,@AttributeName			VARCHAR(500)
	,@AttributeValue		SQL_VARIANT
)
AS
SET NOCOUNT ON

DECLARE 
	@HistServerID				INT
	,@AttributeID				INT
	,@DatabaseID				INT
	,@DatabaseAttributeValueID	INT
	,@CurrentValue				SQL_VARIANT

-- Get the server ID	
EXEC [hist].[ServerInventory_GetServerID] @ServerName = @ServerName, @ServerID = @HistServerID OUTPUT

-- Get the attribute ID
SELECT
	@AttributeID = [DatabaseAttributeID]
FROM [hist].[ServerInventory_SQL_DatabaseAttributeMaster]
WHERE [AttributeName] = @AttributeName

IF @AttributeID IS NULL
BEGIN
	INSERT INTO [hist].[ServerInventory_SQL_DatabaseAttributeMaster] ([AttributeName])
	VALUES (@AttributeName)
	
	SET @AttributeID = SCOPE_IDENTITY()
END

-- Get the database ID
EXEC [hist].[ServerInventory_SQL_GetDatabaseID] @DBName = @DatabaseName, @DatabaseID = @DatabaseID OUTPUT

-- If the most recent entry is not equal to the current value (or null) then insert a new record.
;WITH CTE 
AS (
	SELECT 
		MAX([DatabaseAttributeValueID]) AS [AttributeID]
	FROM [hist].[ServerInventory_SQL_DatabaseAttributeValues]
	WHERE [HistServerID]			= @HistServerID
	AND [HistDatabaseID]			= @DatabaseID
	AND ISNULL([file_id],'')		= ISNULL(@file_id,'')
	AND [DatabaseAttributeID]		= @AttributeID
)
SELECT 
	@DatabaseAttributeValueID = c.[AttributeID]
	,@CurrentValue = v.[AttributeValue]
FROM CTE c
INNER JOIN [hist].[ServerInventory_SQL_DatabaseAttributeValues] v
	ON c.[AttributeID] = v.[DatabaseAttributeValueID]

-- The one condition to do an update
IF (@DatabaseAttributeValueID IS NOT NULL AND @CurrentValue = @AttributeValue)
BEGIN
	UPDATE a SET [DateLastSeenOn] = GETDATE()
	FROM [hist].[ServerInventory_SQL_DatabaseAttributeValues] a
	WHERE a.[DatabaseAttributeValueID] = @DatabaseAttributeValueID
END
-- Do the insert
ELSE
BEGIN
	INSERT INTO [hist].[ServerInventory_SQL_DatabaseAttributeValues] ([HistServerID], [HistDatabaseID], [file_id], [DatabaseAttributeID], [AttributeValue], [DateCreated], [DateLastSeenOn])
	VALUES (@HistServerID, @DatabaseID, @file_id, @AttributeID, @AttributeValue, GETDATE(), GETDATE())
END
GO
/****** Object:  StoredProcedure [hist].[SpaceUsed_DatabaseSizes_InsertValue]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[hist].[SpaceUsed_DatabaseSizes_InsertValue]
**  Desc:			Adds a database size sampling into the repository
**  Auth:			Matt Stanford (SQLSlayer)
**  Date:			(unknown)
**  Debug:			
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090706	Matt Stanford	Fully backwards-compatible change to add DataSizeUnusedMB and LogSizeUnusedMB
********************************************************************************************************/
CREATE PROCEDURE [hist].[SpaceUsed_DatabaseSizes_InsertValue] (
	@ServerName			VARCHAR(1000)
	,@DBName			SYSNAME
	,@DataSizeMB		BIGINT
	,@LogSizeMB			BIGINT
	,@DataSizeUnusedMB	BIGINT = NULL
	,@LogSizeUnusedMB	BIGINT = NULL
)
AS

DECLARE 
	@ServerID			INT
	,@DatabaseID		INT
	
-- Find the ServerID
EXEC [hist].[ServerInventory_GetServerID] @ServerName, @ServerID OUTPUT

-- Find the DatabaseID
EXEC [hist].[ServerInventory_SQL_GetDatabaseID] @DBName, @DatabaseID OUTPUT

-- Now that we've got that, just insert into our detail table
INSERT INTO [hist].[SpaceUsed_DatabaseSizes] ([HistServerID], [DatabaseID], [DataSizeMB], [LogSizeMB], [DataSizeUnusedMB], [LogSizeUnusedMB], [SampleDate])
VALUES (@ServerID, @DatabaseID, @DataSizeMB, @LogSizeMB, @DataSizeUnusedMB, @LogSizeUnusedMB, GETDATE())


GO
/****** Object:  StoredProcedure [hist].[SpaceUsed_DriveSizes_GetDriveLabelIDs]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
* Name
	[hist].[SpaceUsed_DriveSizes_GetDriveLabelIDs]

* Author
	Kathy J Toth
	
* Date
	2011.19.07
	
* Synopsis
	This procedure will insert the drive label if it doesn't already exist and then return the drive label ID.
	
* Description
	This procedure will insert the drive label if it doesn't already exist and then return the drive label ID.

* Examples
	EXEC [hist].[SpaceUsed_DriveSizes_GetDriveLabelIDs] @DriveLabel = 'SQL Data'

* Dependencies
	[hist].[SpaceUsed_DriveSizes_DriveLabel]

* Parameters
	@DriveLable		- This is the drive label of said drive
	
*******************************************************************************
* License
*******************************************************************************
	Copyright © SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/
CREATE PROCEDURE [hist].[SpaceUsed_DriveSizes_GetDriveLabelIDs] (
	@DriveLabel NVARCHAR (100)
	,@DriveLabelID INT OUTPUT
)

AS 

IF NOT EXISTS

(
	SELECT [DriveLabel]
	FROM [hist].[SpaceUsed_DriveSizes_DriveLabel]
	WHERE [DriveLabel] = ISNULL (@DriveLabel,'')
	
)
BEGIN
	INSERT INTO [hist].[SpaceUsed_DriveSizes_DriveLabel] ([DriveLabel])
	VALUES (ISNULL(@DriveLabel,''))
	SET @DriveLabelID = SCOPE_IDENTITY()
	
END
ELSE
BEGIN	
	SET @DriveLabelID = 
	(
		SELECT DriveLabelID
		FROM [hist].[SpaceUsed_DriveSizes_DriveLabel]
		WHERE [DriveLabel] = ISNULL (@DriveLabel,'')
	)
END	

--------------------------------------------------------------------------------------------------------------


GO
/****** Object:  StoredProcedure [hist].[SpaceUsed_DriveSizes_GetDriveLetterIDs]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
* Name
	[hist].[SpaceUsed_DriveSizes_GetDriveLetterIDs]

* Author
	Kathy J Toth
	
* Date
	2011.19.07
	
* Synopsis
	This procedure will insert the drive letter if it doesn't already exist and then return the drive letter ID.
	
* Description
	This procedure will insert the drive letter if it doesn't already exist and then return the drive letter ID.

* Examples
	EXEC [hist].[SpaceUsed_DriveSizes_GetDriveLetterIDs] @DriveLetter = 'A'

* Dependencies
	[hist].[SpaceUsed_DriveSizes_DriveLetter]

* Parameters
	@DriveLetter		- This is the drive letter of said drive
	
*******************************************************************************
* License
*******************************************************************************
	Copyright © SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/
CREATE PROCEDURE [hist].[SpaceUsed_DriveSizes_GetDriveLetterIDs] (
	@DriveLetter	NVARCHAR (2)
	,@DriveLetterID	INT OUTPUT
)

AS

IF NOT EXISTS
	(
		SELECT [DriveLetter]
		FROM [hist].[SpaceUsed_DriveSizes_DriveLetter]
		WHERE [DriveLetter] = REPLACE(ISNULL(NULLIF(LEFT(@DriveLetter,1),''),'MP'),':','')
	)

BEGIN
	INSERT INTO [hist].[SpaceUsed_DriveSizes_DriveLetter]  ([DriveLetter]) 
	VALUES (REPLACE(ISNULL(NULLIF(LEFT(@DriveLetter,1),''),'MP'),':',''))
	SET @DriveLetterID = SCOPE_IDENTITY()
END
ELSE
BEGIN	
	SET @DriveLetterID = 
	(
		SELECT [DriveLetterID]
		FROM [hist].[SpaceUsed_DriveSizes_DriveLetter]
		WHERE [DriveLetter] = REPLACE(ISNULL(NULLIF(LEFT(@DriveLetter,1),''),'MP'),':','')
	)
END	




GO
/****** Object:  StoredProcedure [hist].[SpaceUsed_DriveSizes_InsertDriveMaster]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
* Name
	[hist].[SpaceUsed_DriveSizes_InsertDriveMaster]

* Author
	Kathy J Toth
	
* Date
	2011.19.07
	
* Synopsis
	This procedure gathers all the supporting disk size and detailed information including the server name.
	
* Description
	This procedure gathers all the supporting disk size and detailed information including the server name.

* Examples
	EXEC [hist].[SpaceUsed_DriveSizes_InsertDriveMaster] @ServerName = 'UHAEMRSQLTEST01', @DriveLetter = 'E', @DriveLabel = 'SQL Data', @FreeSpaceMB = '34', @CapacityMB = '250'

* Dependencies
	[hist].[SpaceUsed_DriveSizes_DriveMaster]
	[hist].[SpaceUsed_DriveSizes_DriveLabel]
	[hist].[SpaceUsed_DriveSizes_DriveLetter]
	[hist].[SpaceUsed_DriveSizes_RunIDs]
	[hist].[ServerInventory_ServerIDs]
	
*******************************************************************************
* License
*******************************************************************************
	Copyright © SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/
CREATE PROCEDURE [hist].[SpaceUsed_DriveSizes_InsertDriveMaster] (
	@ServerName		VARCHAR (256)
	,@DriveLetter	VARCHAR (2)
	,@DriveLabel	VARCHAR (30)
	,@DrivePath		VARCHAR (100)
	,@FreeSpaceMB	INT
	,@CapacityMB	INT
	,@MountPoint	BIT
	,@RunID			INT
)

AS

SET NOCOUNT ON

DECLARE 
	 @HistServerID	INT
	,@DriveLetterID INT
	,@DriveLabelID INT

-- Insert New RunID

IF NOT EXISTS
	(
		SELECT [RunID]
		FROM [DBACentral].[hist].[SpaceUsed_DriveSizes_RunIDs] AS SISRID
		WHERE [RunID] = @RunID
	)

BEGIN
	INSERT INTO [DBACentral].[hist].[SpaceUsed_DriveSizes_RunIDs] ( [RunID], [DateCreated] )
	VALUES (@RunID,GETDATE())
END

-- Get the server ID	
EXEC [hist].[ServerInventory_GetServerID] @ServerName = @ServerName, @ServerID = @HistServerID OUTPUT

-- Get the drive letter ID
EXEC [hist].[SpaceUsed_DriveSizes_GetDriveLetterIDs] @DriveLetter = @DriveLetter, @DriveLetterID = @DriveLetterID OUTPUT

-- Get the drive label ID
EXEC [hist].[SpaceUsed_DriveSizes_GetDriveLabelIDs] @DriveLabel = @DriveLabel, @DriveLabelID = @DriveLabelID OUTPUT

-- Insert all the gathered data plus the drive sizes
BEGIN
	INSERT INTO [hist].[SpaceUsed_DriveSizes_DriveMaster] ([RunID], [HistServerID],[DriveLetterID], [DriveLabelID], [DrivePath], [FreeSpaceMB], [CapacityMB], [MountPoint])
	VALUES (@RunID, @HistServerID,@DriveLetterID, @DriveLabelID, @DrivePath, @FreeSpaceMB, @CapacityMB, @MountPoint)		   
END



GO
/****** Object:  StoredProcedure [hist].[SpaceUsed_FileSizes_InsertValue]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [hist].[SpaceUsed_FileSizes_InsertValue] (
	@ServerName				VARCHAR(200)
	,@Path					VARCHAR(900)
	,@FileName				VARCHAR(900)
	,@FileAttributes		VARCHAR(5)
	,@FileSizeKB			BIGINT
)
AS

SET NOCOUNT ON

DECLARE
	@HistServerID			INT
	,@HistFullNameID		INT

EXEC [hist].[ServerInventory_GetServerID] @ServerName, @HistServerID OUTPUT
EXEC [hist].[General_GetFullFileNameID] @Path, @FileName, @HistFullNameID OUTPUT

IF @HistServerID IS NOT NULL AND @HistFullNameID IS NOT NULL
BEGIN
	INSERT INTO [hist].[SpaceUsed_FileSizes] ([HistServerID], [HistPathFileNameID], [FileSizeKB], [FileAttribute])
	VALUES (@HistServerID, @HistFullNameID, @FileSizeKB, @FileAttributes)

END


GO
/****** Object:  StoredProcedure [hist].[SpaceUsed_TableSizes_InsertValue]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [hist].[SpaceUsed_TableSizes_InsertValue] (
	@ServerName			VARCHAR(1000)
	,@DBName			SYSNAME
	,@SchemaName		SYSNAME
	,@TableName			SYSNAME
	,@RowCount			BIGINT
	,@ReservedSpaceKB	BIGINT
	,@DataSpaceKB		BIGINT
	,@IndexSizeKB		BIGINT
	,@UnusedKB			BIGINT
)
AS

SET NOCOUNT ON

DECLARE 
	@ServerDBTableID	INT
	
-- Get the ServerDBTableID
EXEC [hist].[ServerInventory_SQL_GetServerDBTableID] @ServerName, @DBName, @SchemaName, @TableName, @ServerDBTableID OUTPUT

-- Now that we've got that, just insert into our detail table
INSERT INTO [hist].[SpaceUsed_TableSizes] ([ServerDBTableID], [RowCount], [ReservedSpaceKB], [DataSpaceKB], [IndexSizeKB], [UnusedKB], [SampleDate])
VALUES (@ServerDBTableID, @RowCount, @ReservedSpaceKB, @DataSpaceKB, @IndexSizeKB, @UnusedKB, GETDATE())

GO
/****** Object:  StoredProcedure [hist].[SQLRestarts_Collector_InsertValue]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/******************************************************************************
* Name
	[hist].[SQLRestarts_Collector_InsertValue]

* Author
	Adam Bean
	
* Date
	2011.04.20
	
* Synopsis
	Support procedure to insert data from powershell SQL restart collector
	
* Examples
	Only to be called from powershell collector - Collect-SQLRestarts

*******************************************************************************
* License
*******************************************************************************
	Copyright © SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/

CREATE PROCEDURE [hist].[SQLRestarts_Collector_InsertValue]
(
	@RunID				INT
	,@ServerName		VARCHAR(256)
	,@TimeOfRestart		DATETIME
	,@OutageInSeconds	INT
)

AS

SET NOCOUNT ON

DECLARE 
	@HistServerID	INT

-- Find the RunID
IF NOT EXISTS
(
	SELECT [RunID]
	FROM [hist].[SQLRestarts_RunIDs]
	WHERE [RunID] = @RunID
)
BEGIN
	INSERT INTO [hist].[SQLRestarts_RunIDs]
	([RunID])
	SELECT @RunID
END

-- Get the server ID	
EXEC [hist].[ServerInventory_GetServerID] @ServerName = @ServerName, @ServerID = @HistServerID OUTPUT

-- Insert the data
IF NOT EXISTS 
(
	SELECT 
		[HistServerID]
		,[RunID]
	FROM [hist].[SQLRestarts_History]
	WHERE [HistServerID] = @HistServerID
	AND [TimeOfRestart] = @TimeOfRestart
)
BEGIN	
	INSERT INTO [hist].[SQLRestarts_History]
	([HistServerID], [RunID], [TimeOfRestart], [OutageInSeconds])
	VALUES
	(@HistServerID, @RunID, @TimeOfRestart, @OutageInSeconds)
END

SET NOCOUNT OFF


GO
/****** Object:  StoredProcedure [hist].[SQLRestarts_GetOutageDuration]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [hist].[SQLRestarts_GetOutageDuration]
@StartDate		 DATETIME
,@EndDate		 DATETIME
,@EnvironmentIDs	 NVARCHAR(20)
,@ServerIDs		 NVARCHAR(MAX)
,@RestartType    NVARCHAR(100)
AS
SELECT r.[ServerName]
	   ,Count(*) AS [Restarts]
      ,CONVERT(DECIMAL(10,2),ROUND(CAST(SUM([OutageInSeconds]) AS DECIMAL(18,0))/60,2)) AS [OutageMinutes]
  FROM [hist].[SQLRestarts_vw] r
  JOIN [dbo].[ServerInventory_Environments] e
	ON r.[Environment] = e.[EnvironmentName]
  LEFT JOIN [dbo].[Split_fn](@EnvironmentIDs,',') eids
	ON e.[EnvironmentID] = eids.[item]
  LEFT JOIN [dbo].[Split_fn](@ServerIDs,',') sids
	ON r.[HistServerID] = sids.[item]
  LEFT JOIN [dbo].[Split_fn](@RestartType,',') rest
	ON r.[Type] = rest.[item]
  WHERE [TimeOfRestart] BETWEEN @StartDate AND @EndDate
    AND ((eids.[item] IS NOT NULL AND @EnvironmentIDs IS NOT NULL) OR @EnvironmentIDs IS NULL)
	AND ((sids.[item] IS NOT NULL AND @ServerIDs IS NOT NULL) OR @ServerIDs IS NULL)
	AND ((rest.[item] IS NOT NULL AND @RestartType IS NOT NULL) OR @RestartType IS NULL)
  GROUP BY r.[ServerName] 
  ORDER BY r.[ServerName]



GO
/****** Object:  StoredProcedure [hist].[SQLRestarts_UpdateValue]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/******************************************************************************
* Name
	[hist].[SQLRestarts_UpdateValue]

* Author
	Adam Bean
	
* Date
	2011.04.20
	
* Synopsis
	Support procedure to update SQL restart data

*******************************************************************************
* License
*******************************************************************************
	Copyright © SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/

CREATE PROCEDURE [hist].[SQLRestarts_UpdateValue]
(
	@SQLRestartID	INT
	,@DBAComments	VARCHAR(MAX)
	,@Type			BIT
)

AS

SET NOCOUNT ON

UPDATE [hist].[SQLRestarts_History]
	SET [DBAComments] = @DBAComments
	,[Type] = @Type
WHERE [SQLRestartHistoryID] = @SQLRestartID

SET NOCOUNT OFF


GO
/****** Object:  StoredProcedure [hist].[TestConnections_Collector]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************************************************************************
**	Name:			[audit].[TestConnections_Collector]
**	Desc:			Procedure to test connections to available servers
**	Auth:			Adam Bean (SQLSlayer.com)
**	Parameters:		All parameters are passed in from TestConnections_Collector powershell script
**	Date:			06.04.09
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:		Description:
**	--------	--------	---------------------------------------
**  20090708	Adam Bean	Added support for both SERVERNAME properties
********************************************************************************************************/

CREATE PROCEDURE [hist].[TestConnections_Collector]
(
	@ServerID					INT
	,@ServerName				VARCHAR(256)
	,@InstanceName				VARCHAR(256) = NULL
	,@LocalServerName			VARCHAR(512)
	,@ServerPropertyServerName	VARCHAR(512)
	,@CouldConnect				BIT
	,@Error						VARCHAR(MAX) = NULL	
)

AS

SET NOCOUNT ON

-- Setup table to hold captured data	
IF OBJECT_ID('audit.TestConnections') IS NULL
BEGIN 
	CREATE TABLE [audit].[TestConnections]
		(
			[ServerID]						INT
			,[ServerName]					VARCHAR(256)
			,[InstanceName]					VARCHAR(256)
			,[LocalServerName]				VARCHAR(512)
			,[ServerPropertyServerName]		VARCHAR(512)
			,[Connected]					BIT
			,[Error]						VARCHAR(MAX)
			,[DateCreated]					SMALLDATETIME
		)
	ALTER TABLE [audit].[TestConnections] ADD CONSTRAINT [TestConnections_DateCreated]  DEFAULT (GETDATE()) FOR [DateCreated]
END

INSERT INTO [audit].[TestConnections]
([ServerID], [ServerName], [InstanceName], [LocalServerName], [ServerPropertyServerName], [Connected], [Error])
VALUES
(@ServerID, @ServerName, @InstanceName, @LocalServerName, @ServerPropertyServerName, @CouldConnect, @Error)

SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [hist].[Users_GetUserID]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [hist].[Users_GetUserID] (
	@UserName				SYSNAME
	,@UserID				INT OUTPUT
)
AS

SET NOCOUNT ON

SELECT
	@UserID = id.[UserID]
FROM [hist].[Users_UserNames] id
WHERE id.[UserName] = @UserName

IF (@UserID IS NULL)
BEGIN
	INSERT INTO [hist].[Users_UserNames] ([UserName])
	VALUES (@UserName)
	
	SET @UserID = SCOPE_IDENTITY()
END


GO
/****** Object:  StoredProcedure [mon].[SQLRestarts_Notification]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
* Name
	[mon].[SQLRestarts_Notification]

* Author
	Adam Bean
	
* Date
	2011.04.20
	
* Synopsis
	Procedure to scan SQL restart data and send notification of data that needs to be updated

*******************************************************************************
* License
*******************************************************************************
	Copyright © SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/

CREATE PROCEDURE [mon].[SQLRestarts_Notification]
(
	@Recipients			VARCHAR(1024)	= NULL
	,@SendEmail			TINYINT			= 1
	,@Debug				BIT				= 0
)

AS

SET NOCOUNT ON 

DECLARE
	@Body				VARCHAR(MAX)
	,@EmailHeader		VARCHAR(2048)
	,@EmailFooter		VARCHAR(2048)
	,@EmailBody			VARCHAR(2048)
	,@Subject			VARCHAR(256)
	,@SPID				INT
	,@SQLID				INT
	,@SQL				VARCHAR(128)
	,@Count				INT
	,@Connections		INT

IF @Debug = 1
	PRINT '/*** DEBUG ENABLED ****/'
	
-- Verify valid @Recipients
IF @Recipients IS NULL AND @Debug = 0
BEGIN
	RAISERROR ('@Recipients must be passed in.',16,1) WITH NOWAIT
	RETURN
END

-- Replace commas with semi-colons to work properly for database mail
SET @Recipients = REPLACE(@Recipients, ',', ';')

-- Build email
-- Setup table to build body
DECLARE @BodyContents TABLE 
(
	[html]			VARCHAR(MAX)
)

-- Setup table to build email
DECLARE @Email TABLE 
(
	[ID]			INT IDENTITY
	,[html]			VARCHAR(MAX)
)

-- Set subject
SET @Subject = 'Recent SQL restarts requiring information'

-- Build the email header
SELECT @EmailHeader = 
'
<HTML>
<DIV ALIGN="CENTER">
<TABLE BORDER="5" CELLSPACING="5" CELLPADDING="5" ALIGN="CENTER">
<CAPTION>
	<H2>Production SQL instances restarted in the last 30 days that have not yet been commented on.</H2>
	<p>Fill in @DBAComments appropriately with any notes about the incident.
	<br>
	Set @Type = 1 if expected, 0 (default) if unexpected.</p>
	<EM>The data can be queried through the [' + DB_NAME() +'].[hist].[SQLRestarts_vw] view.</EM>
</CAPTION>
<TR BGCOLOR="#C0C0C0">
	<TH>Server Name</TH>
	<TH>Time Of Restart</TH>
	<TH>Outage Duration (S)</TH>
	<TH>TSQL command</TH>
</TR>
'

-- Build the email footer
SELECT @EmailFooter =
'
</DIV>
</TABLE>
<P ALIGN="CENTER"><FONT SIZE="1">
Message sent from ' + @@SERVERNAME + ' via SQL Server Agent @ ' + CAST(GETDATE() AS VARCHAR(24)) + '.<br>
</P>
</FONT>
</HTML>
'

-- Build the email body
INSERT INTO @BodyContents
SELECT '
		<TR>
			<TD>' + [ServerName] + '</TD>
			<TD>' + CAST([TimeOfRestart] AS VARCHAR) + '</TD>
			<TD>' + CAST([OutageInSeconds] AS VARCHAR) + '</TD>
			<TD>' + 'EXEC [' + DB_NAME() +'].[hist].[SQLRestarts_UpdateValue] @SQLRestartID = ' + CAST([SQLRestartHistoryID] AS VARCHAR) + ', @DBAComments = '''', @Type = 0</TD>
		</TR>' AS [html]
FROM [hist].[SQLRestarts_vw]
WHERE DATEADD(DAY,-30,GETDATE()) < [TimeOfRestart]
AND [Environment] = 'PROD'
AND [DBAComments] IS NULL

-- Build the email
INSERT INTO @Email
SELECT @EmailHeader
UNION ALL
SELECT * FROM @BodyContents
UNION ALL
SELECT @EmailFooter

-- Put it all together
SET @Count = 1
SET @Body = ''
WHILE @Count <= (SELECT COUNT(*) FROM @Email)
BEGIN
	SET @Body = @Body + (SELECT [html] FROM @Email WHERE [ID] = @Count)
	SET @Count = @Count + 1
END

-- Only send email if there is data
IF @Count > 3
BEGIN
	IF @Debug = 0
	BEGIN
		-- Send mail using sp_send_dbmail
		EXEC [msdb].[dbo].[sp_send_dbmail]
			@Recipients		= @Recipients
			,@Subject		= @Subject
			,@Body			= @Body
			,@Body_format	= 'HTML'
	END
	ELSE
	BEGIN
		PRINT 'Debug enabled, no email will be sent'
		SELECT 
			*
		FROM [hist].[SQLRestarts_vw]
		WHERE DATEADD(DAY,-30,GETDATE()) < [TimeOfRestart]
		AND [Environment] = 'PROD'
		AND [DBAComments] IS NULL
	END
END

SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [rpt].[Backup_Summary]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**	Name:			[rpt].[Backup_Summary]
**	Desc:			Reporting procedure for backup history SSRS report
**	Auth:			Adam Bean (SQLSlayer.com)
**	Date:			2009.04.02
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:		Description:
**	--------	--------	---------------------------------------
**  
********************************************************************************************************/

CREATE PROCEDURE [rpt].[Backup_Summary]
(
	@Environment	VARCHAR(MAX)	= NULL
	,@ServerName	VARCHAR(MAX)	= NULL
	,@DBName		NVARCHAR(MAX)	= NULL
	,@DateStart		DATETIME		= NULL
	,@DateEnd		DATETIME		= NULL
)

AS

SET NOCOUNT ON

SELECT 
	[Environment]
	,[ServerName]
	,[DBName]
	,[StartDate]
	,[EndDate]
	,[BUTime_Seconds]
	,[BUTime_Minutes]
	,[Size_MB]
	,[Size_GB]
	,[BackupType]
	,[UserName]
	,[PhysicalDeviceName]
	,[BackupPath]
	,[FileName]
FROM [rpt].[Backup_Summary_vw]
WHERE (@Environment IS NULL OR [Environment] IN (SELECT [item] AS [Environment] FROM [admin].[dbo].[Split_fn](@Environment,',')))
AND (@ServerName IS NULL OR [ServerName] IN (SELECT [item] AS [ServerName] FROM [admin].[dbo].[Split_fn](@ServerName,',')))
AND (@DBName IS NULL OR [DBName] IN (SELECT [item] AS [DatabaseName] FROM [admin].[dbo].[Split_fn](@DBName,',')))
AND (@DateStart IS NULL AND @DateEnd IS NULL OR [StartDate] BETWEEN @DateStart AND @DateEnd)

SET NOCOUNT OFF


GO
/****** Object:  StoredProcedure [rpt].[BackupAudit]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
* Name
	[rpt].[BackupAudit]

* Author
	Adam Bean
	
* Date
	2012.06.18
	
* Synopsis
	Raw data for all SQL Server backups
	
* Description
	Summary data displaying actual, expected and missed backups by server, database, day

* Examples
	EXEC [rpt].[BackupAudit] @ServerName = 'X,Y,Z'
	EXEC [rpt].[BackupAudit] @ServerName = 'X,Y,Z', @DBName = 'master,model'
	EXEC [rpt].[BackupAudit] @ServerName = 'X', @HalfHourTlogBackupServers = 'X'
	EXEC [rpt].[BackupAudit] @ServerName = 'X', @DBNameExclude = 'admin', @DateStart = '2012.01.01', @DateEnd = '2012.05.01'

* Dependencies
	[dbo].[Split_fn]

* Parameters
	@ServerName = Server(s) to be reindexed (CSV supported, NULL for all, accepts % for searching)
	@ServerNameExclude = Server(s) to excluded (CSV supported, NULL for all, accepts % for searching)
	@DBName = Database(s) to be reindexed (CSV supported, NULL for all, accepts % for searching)
	@DBNameExclude = Database(s) to excluded (CSV supported, NULL for all, accepts % for searching)
	@QuarterHourTlogBackupServers = Server(s) that run tlog backups every quarter hour
	@HalfHourTlogBackupServers = Server(s) that run tlog backups every half hour
	@PhysicalDeviceName = Wild card match on the backup device name (for example "TDP" for searching for all TSM (Tivoli) backups)
	@DeviceType = Query on the device type from backupmediafamily (for example 2 = disk, 5 = tape)
	@DateStart = Start of date range to query on
	@DateEnd = End of date range to query on
	
* Notes
	Data is reliant upon DBAC data collector "Collect-BackupHistory"
	@PhysicalDeviceName should be replaced by device_type in backupmediafamily after data collectors have been updated
	@QuarterHourTlogBackupServers is in place to avoid having hard coded server names for servers that have tlog backups every half an hour
	@HalfHourTlogBackupServers is in place to avoid having hard coded server names for servers that have tlog backups every half an hour

*******************************************************************************
* License
*******************************************************************************
	Copyright © SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20120807	Meghan Brooks	Changed logic on only retrieving active databases. Needed to be >= @EndDate, not a week prior. Was giving inaccurate numbers
	20120627	Adam Bean		Updated for new schema for the backup repo and added @DeviceType
	20120702	Adam Bean		Set [DBActual] and [TlogActual] to have a DEFAULT of 0 because the join wasn't setting it to 0 when the data didn't exist
	20120710	Adam Bean		Fixed "TOP N can not be negative" error by adding a check on @DateEnd (line 151)
******************************************************************************/

CREATE PROCEDURE [rpt].[BackupAudit]
(
	@ServerName						VARCHAR(MAX)	= NULL
	,@ServerNameExclude				VARCHAR(MAX)	= NULL
	,@DBName						VARCHAR(MAX)	= NULL
	,@DBNameExclude					VARCHAR(MAX)	= NULL
	,@HalfHourTlogBackupServers		VARCHAR(512)	= NULL
	,@QuarterHourTlogBackupServers	VARCHAR(512)	= NULL
	,@PhysicalDeviceName			VARCHAR(16)		= NULL
	,@DeviceType					VARCHAR(8)		= NULL
	,@DateStart						DATETIME		= NULL
	,@DateEnd						DATETIME		= NULL
)

AS

SET NOCOUNT ON 

DECLARE
	@FirstBackupDate				DATE
	,@RecoveryModel					VARCHAR(16)
	,@IsTlogShipped					BIT

-- If date range variables were not set, set to first day of month (start) to yesterday (end)
IF @DateStart IS NULL SET @DateStart = CONVERT(VARCHAR,DATEADD(MONTH, DATEDIFF(MONTH,0,GETDATE()), 0),102)
IF @DateEnd IS NULL SET @DateEnd = CONVERT(VARCHAR,DATEADD(DAY,DATEDIFF(DAY,1,GETDATE()),0),102)

-- temp table cleanup
IF OBJECT_ID('tempdb.dbo.#MinBackupDate') IS NOT NULL
   DROP TABLE #MinBackupDate
IF OBJECT_ID('tempdb.dbo.#Backups') IS NOT NULL
   DROP TABLE #Backups

-- temp table to house the first time a backup took place
CREATE TABLE #MinBackupDate
(
	[ServerName]					VARCHAR(256)
	,[DBName]						VARCHAR(256)
	,[FirstBackupDate]				DATETIME
)

-- Main temp table housing our backup data
CREATE TABLE #Backups
(
	[ServerName]					VARCHAR(256)	NULL
	,[DBName]						VARCHAR(128)	NULL
	,[RecoveryModel]				VARCHAR(16)		NULL
	,[IsTlogShipped]				BIT				NULL
	,[DBExpected]					INT	DEFAULT(1)
	,[DBActual]						INT DEFAULT(0)
	,[DBMissed]						INT				NULL
	,[TlogExpected]					INT				NULL
	,[TlogActual]					INT	DEFAULT(0)
	,[TlogMissed]					INT				NULL
	,[CalendarDate]					SMALLDATETIME		
)

-- Get the first time a given server/database had a backup
INSERT INTO #MinBackupDate
SELECT 
	bh.[ServerName]
	,bh.[DBName]								AS [DBName]
	,MIN(CONVERT(VARCHAR,bh.[StartDate],102))	AS [FirstBackupDate]
FROM [hist].[Backups_History_vw] bh
JOIN [dbo].[ServerInventory_SQL_AllServers_vw] s									-- Only retrieve active servers
	ON s.[FullName] = bh.[ServerName]
LEFT JOIN [dbo].[Split_fn](@ServerName,',') sn
	ON bh.[ServerName] = sn.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@ServerNameExclude,',') sne
	ON bh.[ServerName] = sne.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@DBName,',') db
	ON bh.[DBName] = db.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
	ON bh.[DBName] = de.[item] COLLATE DATABASE_DEFAULT
WHERE (@DateStart IS NULL OR CONVERT(VARCHAR,bh.[StartDate],102) >= @DateStart)		-- Start at or greater than @DateStart
AND (@DateEnd IS NULL OR CONVERT(VARCHAR,bh.[StartDate],102) <= @DateEnd)			-- Only retrieve backups prior @DateEnd
AND ((sn.[item] IS NOT NULL AND @ServerName IS NOT NULL) OR @ServerName IS NULL)	-- Specified servers, or all
AND ((db.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL)			-- Specified databases, or all
AND sne.[item] IS NULL																-- All but excluded servers
AND de.[item] IS NULL																-- All but excluded databases
GROUP BY
	bh.[ServerName]
	,bh.[DBName]

-- Add indexing to our temp table
CREATE NONCLUSTERED INDEX [#IX__MinBackups__ServerName__DBName] ON #MinBackupDate
(
	[ServerName]
	,[DBName]
)

-- Now we need to populate a record for every server, database, day
-- This is to handle where a day passed and no #Backups occured on a given date
DECLARE #dbs CURSOR LOCAL STATIC FOR
SELECT
	mbd.[ServerName]
	,mbd.[DBName]
	,mbd.[FirstBackupDate]
	,pvt.[recovery_model_desc]
	,pvt.[is_tlog_shipped]
FROM
(
	SELECT 
		da.[ServerName]
		,da.[DBName]
		,da.[AttributeName]
		,CAST(da.[AttributeValue] AS VARCHAR(160)) AS [AttributeValue]
		,ROW_NUMBER () OVER (PARTITION BY da.[ServerName], da.[DBName], da.[AttributeName] ORDER BY da.[DateLastSeenOn] DESC) AS [RowNo]
	FROM [hist].[ServerInventory_SQL_DatabaseAttributes_vw] da
	LEFT JOIN [dbo].[Split_fn](@ServerName,',') sn
		ON da.[ServerName] = sn.[item] COLLATE DATABASE_DEFAULT
	LEFT JOIN [dbo].[Split_fn](@ServerNameExclude,',') sne
		ON da.[ServerName] = sne.[item] COLLATE DATABASE_DEFAULT
	LEFT JOIN [dbo].[Split_fn](@DBName,',') db
		ON da.[DBName] = db.[item] COLLATE DATABASE_DEFAULT
	LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
		ON da.[DBName] = de.[item] COLLATE DATABASE_DEFAULT
	WHERE [AttributeName] IN ('is_tlog_shipped', 'recovery_model_desc','state_desc') 
	AND da.[DateLastSeenOn] >= DATEADD(WEEK,-1,GETDATE())--@DateEnd --DATEADD(WEEK,-1,GETDATE())								-- Only retrieve active databases
	AND da.[file_id] IS NULL															-- Only want the database level attributes, not per file
	AND ((sn.[item] IS NOT NULL AND @ServerName IS NOT NULL) OR @ServerName IS NULL)	-- Specified servers, or all
	AND ((db.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL)			-- Specified databases, or all
	AND sne.[item] IS NULL																-- All but excluded servers
	AND de.[item] IS NULL																-- All but excluded databases
) src
PIVOT
(
	MAX([AttributeValue])
	FOR [AttributeName] IN ([state_desc], [recovery_model_desc], [is_tlog_shipped], [source_database_id])
) pvt
JOIN #MinBackupDate mbd
	ON pvt.[ServerName] = mbd.[ServerName]
	AND pvt.[DBName] = mbd.[DBName]
--WHERE @DateStart >= mbd.[FirstBackupDate]	-- Can't report on backups that haven't taken place before the @DateStart [[keeping this here for now as this was the root of the TOP N negative issue]]
WHERE pvt.[state_desc] = 'ONLINE'			-- Only retrieve online databases
AND pvt.[RowNo] = 1							-- Only retrieve the most active record
ORDER BY 1, 2

OPEN #dbs
FETCH NEXT FROM #dbs INTO @ServerName, @DBName, @FirstBackupDate, @RecoveryModel, @IsTlogShipped
WHILE @@FETCH_STATUS = 0
BEGIN

	-- Populate our staging table with all possible dates for every database/server
	INSERT INTO #Backups
	([CalendarDate], [ServerName], [DBName], [RecoveryModel], [IsTlogShipped])
	SELECT TOP 
		(DATEDIFF(DAY,CONVERT(VARCHAR,@FirstBackupDate,102),@DateEnd) + 1) DATEADD(DAY,ROW_NUMBER() OVER (ORDER BY [CalendarDate]) -1, @FirstBackupDate) AS [CalendarDate]
		,@ServerName
		,@DBName
		,@RecoveryModel
		,@IsTlogShipped
	FROM [dbo].[Calendar]

FETCH NEXT FROM #dbs INTO @ServerName, @DBName, @FirstBackupDate, @RecoveryModel, @IsTlogShipped
END
CLOSE #dbs
DEALLOCATE #dbs

-- Add indexing to our temp table
CREATE NONCLUSTERED INDEX [#IX__Backups__RecoveryModel] ON #Backups
(
	[RecoveryModel]
)

CREATE NONCLUSTERED INDEX [#IX__Backups__ServerName__DBName__CalendarDate] ON #Backups
(
	[ServerName]
	,[DBName]
	,[CalendarDate]
)

-- Now we need to update the data with the expected amount of transaction log backups
-- This was broken off seperately to allow for ease of updates for specific environment configurations
UPDATE #Backups
	SET [TlogExpected] = 
		CASE
			WHEN [RecoveryModel] != 'SIMPLE' AND [IsTlogShipped] = 1								THEN 0	-- Transaction log shipping (where we don't expect the file system #Backups to count as the DR #Backups)
			WHEN [RecoveryModel] != 'SIMPLE' AND [ServerName] IN (@HalfHourTlogBackupServers)		THEN 48	-- Environment specific (30 minute tlog backups)
			WHEN [RecoveryModel] != 'SIMPLE' AND [ServerName] IN (@QuarterHourTlogBackupServers)	THEN 96	-- Environment specific (15 minute tlog backups)
			WHEN [RecoveryModel] != 'SIMPLE'														THEN 24	-- May not fit all environments; however we should expect 24 tlogs a day (1 per hour)
			ELSE 0
		END

-- Now find the actual amount of database backups that took place per server/database/day
UPDATE bu
	SET 
		bu.[DBActual] = ISNULL(act.[DBActual],0)
		-- If more backups took place than expected, return a 0 vs. a negative
		,bu.[DBMissed] = 
		(
			CASE
				WHEN (bu.[DBExpected] - ISNULL(act.[DBActual],0)) < 0 THEN 0
				ELSE (bu.[DBExpected] - ISNULL(act.[DBActual],0))
			END
		)
FROM #Backups bu
LEFT JOIN
(
	SELECT 
		COUNT(bh.[BackupType])					AS [DBActual]
		,bu.[ServerName]
		,bu.[DBName]
		,CONVERT(VARCHAR,bu.[CalendarDate],102) AS [CalendarDate]
	FROM #Backups bu
	LEFT JOIN [hist].[Backups_History_vw] bh
		ON bu.[ServerName] = bh.[ServerName]
		AND bu.[DBName] = bh.[DBName]
		AND CONVERT(VARCHAR,bu.[CalendarDate],102) = CONVERT(VARCHAR,bh.[StartDate],102)
		AND bh.[BackupType] != 'L'
		AND (bh.[PhysicalDeviceName] LIKE '%' + @PhysicalDeviceName + '%' OR @PhysicalDeviceName IS NULL)
	LEFT JOIN [dbo].[Split_fn](@DeviceType,',') dt
		ON bh.[DeviceType] = dt.[item] COLLATE DATABASE_DEFAULT
	WHERE ((dt.[item] IS NOT NULL AND @DeviceType IS NOT NULL) OR @DeviceType IS NULL)	-- Specified device types, or all
	GROUP BY
		bu.[ServerName]
		,bu.[DBName]
		,CONVERT(VARCHAR,bu.[CalendarDate],102)
) act
	ON bu.[ServerName] = act.[ServerName]
	AND bu.[DBName] = act.[DBName]
	AND CONVERT(VARCHAR,bu.[CalendarDate],102) = CONVERT(VARCHAR,act.[CalendarDate],102)

-- Now find the actual amount of transaction log backups that took place per server/database/day
UPDATE bu
	SET 
		bu.[TlogActual] = ISNULL(act.[TlogActual],0)
		-- If more backups took place than expected, return a 0 vs. a negative
		,bu.[TlogMissed] = 
		(
			CASE
				WHEN (bu.[TlogExpected] - ISNULL(act.[TlogActual],0)) < 0 THEN 0
				ELSE (bu.[TlogExpected] - ISNULL(act.[TlogActual],0))
			END
		)
FROM #Backups bu
LEFT JOIN
(
	SELECT 
		COUNT(bh.[BackupType])					AS [TlogActual]
		,bu.[ServerName]
		,bu.[DBName]
		,CONVERT(VARCHAR,bu.[CalendarDate],102)	AS [CalendarDate]
	FROM #Backups bu
	LEFT JOIN [hist].[Backups_History_vw] bh
		ON bu.[ServerName] = bh.[ServerName]
		AND bu.[DBName] = bh.[DBName]
		AND CONVERT(VARCHAR,bu.[CalendarDate],102) = CONVERT(VARCHAR,bh.[StartDate],102)
		AND bh.[BackupType] = 'L'
		AND (bh.[PhysicalDeviceName] LIKE '%' + @PhysicalDeviceName + '%' OR @PhysicalDeviceName IS NULL)
	GROUP BY
		bu.[ServerName]
		,bu.[DBName]
		,CONVERT(VARCHAR,bu.[CalendarDate],102)
) act
	ON bu.[ServerName] = act.[ServerName]
	AND bu.[DBName] = act.[DBName]
	AND CONVERT(VARCHAR,bu.[CalendarDate],102) = CONVERT(VARCHAR,act.[CalendarDate],102)

-- Return results
SELECT
	[ServerName]
	,[DBName]
	,[RecoveryModel]
	,[DBExpected]
	,[DBActual]
	,[DBMissed]
	,[TlogExpected]
	,[TlogActual]
	,[TlogMissed]
	,[CalendarDate]
FROM #Backups
ORDER BY [ServerName], [DBName], [CalendarDate]

SET NOCOUNT OFF


GO
/****** Object:  StoredProcedure [rpt].[DBSizes_CurrentSizes_ByDatabase]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			[rpt].[DBSizes_CurrentSizes_ByDatabase]
**	Desc:			Reporting procedure to display database sizes per server
**	Auth:			Adam Bean (SQLSlayer.com)
**	Notes:			Date range is to ensure only recent databases are counted
**	Date:			2010.08.13
*******************************************************************************
**  License
*******************************************************************************
**  Copyright © SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommonf.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposef. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  YYYYDDMM	Full Name	
********************************************************************************************************/

CREATE PROCEDURE [rpt].[DBSizes_CurrentSizes_ByDatabase]

AS

SELECT
	[Environment]
	,[ServerName]
	,[DBName]
	,[LastDataSizeMB]
	,[LastLogSizeMB]
	,[TotalSizeMB]
FROM [rpt].[DBSizes_CurrentSizes_ByDatabase_vw]
OPTION (MAXDOP 1)

GO
/****** Object:  StoredProcedure [rpt].[DBSizes_FastChanging_Data]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			[rpt].[DBSizes_FastChanging_Data]
**	Desc:			Reporting procedure to display fast growing data files
**	Auth:			Adam Bean (SQLSlayer.com)
**  Dependancies:	dbo.Split_fn
**  Parameters:		@Environment = Environment to query based on dbo.ServerInventory_Environments, CSV supported
					@ServerName = Server (+\Instance) to query based on dbo.ServerInventory_SQL_Master, CSV supported
					@DBName = Database to query based on dbo.ServerInventory_SQL_DatabaseIDs, CSV supported
					@AvgFileSizeMin = Minimum average data file size
					@ChangePct = Range (+/-) for % of change on data files to exclude from results
**	Notes:			Date range is to ensure only recent databases are counted
**	Date:			2010.08.11
*******************************************************************************
**  License
*******************************************************************************
**  Copyright © SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommonf.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposef. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  YYYYDDMM	Full Name	
********************************************************************************************************/

CREATE PROCEDURE [rpt].[DBSizes_FastChanging_Data]
(
	@Environment				VARCHAR(128)	= NULL
	,@ServerName				VARCHAR(MAX)	= NULL
	,@DBName					VARCHAR(MAX)	= NULL
	,@AvgFileSizeMin			INT				= 512
	,@ChangePct					INT				= 20
	,@DateStart					DATETIME		= NULL
	,@DateEnd					DATETIME		= NULL
)

AS

DECLARE @DBSizes TABLE 
	(
		[Environment]			VARCHAR(128)
		,[ServerName]			VARCHAR(MAX)
		,[DBName]				VARCHAR(MAX)
		,[Samples]				INT
		,[AvgSizeMB]			BIGINT
		,[FirstSizeMB]			BIGINT NULL		
		,[LastSizeMB]			BIGINT NULL
		,[PercentChange]		AS ROUND((CAST(NULLIF([LastSizeMB],0) AS DECIMAL(10,2))/CAST(NULLIF([AvgSizeMB],0) AS DECIMAL(10,2)) * 100) - 100,1)
		,[EstDaysOfSize]		INT NULL
		,[FirstSampleDate]		DATETIME
		,[LastSampleDate]		DATETIME
	)

-- Check parameters
-- If no date range was passed, set to current minus one month
IF @DateStart IS NULL AND @DateEnd IS NULL
BEGIN	
	SET @DateEnd = GETDATE()
	SET @DateStart = DATEADD(mm,-1,@DateEnd)
END
-- If @DateStart was passed and @DateEnd was not, set @DateEnd to current 
IF @DateStart IS NOT NULL AND @DateEnd IS NULL
	SET @DateEnd = GETDATE()
-- If @DateEnd was passed and @DateStart was not, set @DateStart to @DateEnd minus one month
IF @DateEnd IS NOT NULL AND @DateStart IS NULL
	SET @DateStart = DATEADD(mm,-1,@DateEnd)

-- Populate our working table
-- Get averages and counts based on @Environment, @ServerName, @DBName, @DateStart, @DateEnd
INSERT INTO @DBSizes ([Environment],[ServerName],[DBName],[AvgSizeMB],[Samples],[FirstSampleDate],[LastSampleDate])
SELECT 
	[Environment]
	,[ServerName]
	,[DBName]
	,AVG([DataSizeMB])	AS [AvgSizeMB]
	,COUNT(*)			AS [Samples]
	,MIN([SampleDate])	AS [LastSampleDate]
	,MAX([SampleDate])	AS [LastSampleDate]
FROM [rpt].[DBSizes_Summary_vw] s
LEFT JOIN [dbo].[Split_fn](@Environment,',') e
	ON s.[Environment] = e.[item]
LEFT JOIN [dbo].[Split_fn](@ServerName,',') sn
	ON s.[ServerName] = sn.[item]
LEFT JOIN [dbo].[Split_fn](@DBName,',') db
	ON s.[DBName] = db.[item]
WHERE (@DateStart IS NULL AND @DateEnd IS NULL OR [SampleDate] BETWEEN @DateStart AND @DateEnd)
AND ((e.[item] IS NOT NULL AND @Environment IS NOT NULL) OR @Environment IS NULL) -- Specified environment(s), or all
AND ((sn.[item] IS NOT NULL AND @ServerName IS NOT NULL) OR @ServerName IS NULL) -- Specified server name(s), or all
AND ((db.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified database name(s), or all
GROUP BY [Environment], [ServerName], [DBName]

-- Retrieve first and last data sizes
UPDATE d 
SET 
	d.[FirstSizeMB] = s2.[DataSizeMB]
	,d.[LastSizeMB] = s.[DataSizeMB]
FROM [rpt].[DBSizes_Summary_vw] s
JOIN @DBSizes d
	ON s.[ServerName] = d.[ServerName]
	AND s.[DBName] = d.[DBName]
	AND s.[SampleDate] = d.[LastSampleDate]
JOIN [rpt].[DBSizes_Summary_vw] s2
	ON s2.[ServerName] = d.[ServerName]
	AND s2.[DBName] = d.[DBName]
	AND s2.[SampleDate] = d.[FirstSampleDate]
	
-- Find out the amount of days the data file has been this size, or within 10% of it
UPDATE d
SET d.[EstDaysOfSize] = t.[Total]
FROM @DBSizes d
JOIN 
	(
		SELECT 
			s.[ServerName]
			,s.[DBName]
			,COUNT(*) AS [Total]
		FROM [rpt].[DBSizes_Summary_vw] s
		JOIN @DBSizes d
			ON s.[ServerName] = d.[ServerName]
			AND s.[DBName] = d.[DBName]
		WHERE s.[DataSizeMB] BETWEEN (d.[LastSizeMB] - (d.[LastSizeMB] * .10)) AND (d.[LastSizeMB] + (d.[LastSizeMB] * .10))
		GROUP BY s.[ServerName], s.[DBName]
	) t
ON t.[ServerName] = d.[ServerName]
AND t.[DBName] = d.[DBName]

-- Return results greater than @AvgFileSizeMin and less than or greater than @ChangePct
SELECT
	*
FROM
(
	SELECT 
		[ServerName]
		,[DBName]
		,[Samples]
		,[FirstSizeMB]
		,[AvgSizeMB]
		,[LastSizeMB]
		,CAST([PercentChange] AS DECIMAL(10,2))		AS [PercentChange]
		,[EstDaysOfSize]
		,CONVERT(VARCHAR(10),[FirstSampleDate],102)	AS [FirstSampleDate]
		,CONVERT(VARCHAR(10),[LastSampleDate],102)	AS [LastSampleDate]
	FROM @DBSizes 
	WHERE ([AvgSizeMB] > @AvgFileSizeMin 
		AND [PercentChange] NOT BETWEEN -@ChangePct AND @ChangePct
		AND [LastSampleDate] > DATEADD(dd,-2,GETDATE()))
) t
ORDER BY [PercentChange] DESC


GO
/****** Object:  StoredProcedure [rpt].[DBSizes_FastChanging_Log]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			[rpt].[DBSizes_FastChanging_Log]
**	Desc:			Reporting procedure to display fast growing log files
**	Auth:			Adam Bean (SQLSlayer.com)
**  Dependancies:	dbo.Split_fn
**  Parameters:		@Environment = Environment to query based on dbo.ServerInventory_Environments, CSV supported
					@ServerName = Server (+\Instance) to query based on dbo.ServerInventory_SQL_Master, CSV supported
					@DBName = Database to query based on dbo.ServerInventory_SQL_DatabaseIDs, CSV supported
					@AvgFileSizeMin = Minimum average log file size
					@ChangePct = Range (+/-) for % of change on log files to exclude from results
**	Notes:			Date range is to ensure only recent databases are counted
**	Date:			2010.08.11
*******************************************************************************
**  License
*******************************************************************************
**  Copyright © SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommonf.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposef. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  YYYYDDMM	Full Name	
********************************************************************************************************/

CREATE PROCEDURE [rpt].[DBSizes_FastChanging_Log]
(
	@Environment				VARCHAR(128)	= NULL
	,@ServerName				VARCHAR(MAX)	= NULL
	,@DBName					VARCHAR(MAX)	= NULL
	,@AvgFileSizeMin			INT				= 512
	,@ChangePct					INT				= 20
	,@DateStart					DATETIME		= NULL
	,@DateEnd					DATETIME		= NULL
)

AS

DECLARE @DBSizes TABLE 
	(
		[Environment]			VARCHAR(128)
		,[ServerName]			VARCHAR(MAX)
		,[DBName]				VARCHAR(MAX)
		,[Samples]				INT
		,[EstDaysOfSizeData]	INT NULL
		,[AvgSizeMB]			BIGINT
		,[FirstSizeMB]			BIGINT NULL
		,[LastSizeMB]			BIGINT NULL
		,[PercentChange]		AS ROUND((CAST(NULLIF([LastSizeMB],0) AS DECIMAL(10,2))/CAST(NULLIF([AvgSizeMB],0) AS DECIMAL(10,2)) * 100) - 100,1)
		,[EstDaysOfSize]		INT NULL
		,[FirstSampleDate]		DATETIME
		,[LastSampleDate]		DATETIME
	)

-- Check parameters
-- If no date range was passed, set to current minus one month
IF @DateStart IS NULL AND @DateEnd IS NULL
BEGIN	
	SET @DateEnd = GETDATE()
	SET @DateStart = DATEADD(mm,-1,@DateEnd)
END
-- If @DateStart was passed and @DateEnd was not, set @DateEnd to current 
IF @DateStart IS NOT NULL AND @DateEnd IS NULL
	SET @DateEnd = GETDATE()
-- If @DateEnd was passed and @DateStart was not, set @DateStart to @DateEnd minus one month
IF @DateEnd IS NOT NULL AND @DateStart IS NULL
	SET @DateStart = DATEADD(mm,-1,@DateEnd)

-- Populate our working table
-- Get averages and counts based on @Environment, @ServerName, @DBName, @DateStart, @DateEnd
INSERT INTO @DBSizes ([Environment],[ServerName],[DBName],[AvgSizeMB],[Samples],[FirstSampleDate],[LastSampleDate])
SELECT 
	[Environment]
	,[ServerName]
	,[DBName]
	,AVG([LogSizeMB])	AS [AvgSizeMB]
	,COUNT(*)			AS [Samples]
	,MIN([SampleDate])	AS [FirstSampleDate]
	,MAX([SampleDate])	AS [LastSampleDate]
FROM [rpt].[DBSizes_Summary_vw] s
LEFT JOIN [dbo].[Split_fn](@Environment,',') e
	ON s.[Environment] = e.[item]
LEFT JOIN [dbo].[Split_fn](@ServerName,',') sn
	ON s.[ServerName] = sn.[item]
LEFT JOIN [dbo].[Split_fn](@DBName,',') db
	ON s.[DBName] = db.[item]
WHERE (@DateStart IS NULL AND @DateEnd IS NULL OR [SampleDate] BETWEEN @DateStart AND @DateEnd)
AND ((e.[item] IS NOT NULL AND @Environment IS NOT NULL) OR @Environment IS NULL) -- Specified environment(s), or all
AND ((sn.[item] IS NOT NULL AND @ServerName IS NOT NULL) OR @ServerName IS NULL) -- Specified server name(s), or all
AND ((db.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified database name(s), or all
GROUP BY [Environment], [ServerName], [DBName]

-- Retrieve first and last log sizes
UPDATE d 
SET 
	d.[FirstSizeMB] = s2.[LogSizeMB]
	,d.[LastSizeMB] = s.[LogSizeMB]
FROM [rpt].[DBSizes_Summary_vw] s
JOIN @DBSizes d
	ON s.[ServerName] = d.[ServerName]
	AND s.[DBName] = d.[DBName]
	AND s.[SampleDate] = d.[LastSampleDate]
JOIN [rpt].[DBSizes_Summary_vw] s2
	ON s2.[ServerName] = d.[ServerName]
	AND s2.[DBName] = d.[DBName]
	AND s2.[SampleDate] = d.[FirstSampleDate]
	
-- Find out the amount of days the log file has been this size, or within 10% of it
UPDATE d
SET d.[EstDaysOfSize] = t.[Total]
FROM @DBSizes d
JOIN 
	(
		SELECT 
			s.[ServerName]
			,s.[DBName]
			,COUNT(*) AS [Total]
		FROM [rpt].[DBSizes_Summary_vw] s
		JOIN @DBSizes d
			ON s.[ServerName] = d.[ServerName]
			AND s.[DBName] = d.[DBName]
		WHERE s.[LogSizeMB] BETWEEN (d.[LastSizeMB] - (d.[LastSizeMB] * .10)) AND (d.[LastSizeMB] + (d.[LastSizeMB] * .10))
		GROUP BY s.[ServerName], s.[DBName]
	) t
ON t.[ServerName] = d.[ServerName]
AND t.[DBName] = d.[DBName]

-- Return results greater than @AvgFileSizeMin and less than or greater than @ChangePct
SELECT
	*
FROM
(
	SELECT 
		[ServerName]
		,[DBName]
		,[Samples]
		,[FirstSizeMB]
		,[AvgSizeMB]
		,[LastSizeMB]
		,CAST([PercentChange] AS DECIMAL(10,2))		AS [PercentChange]
		,[EstDaysOfSize]
		,CONVERT(VARCHAR(10),[FirstSampleDate],102)	AS [FirstSampleDate]
		,CONVERT(VARCHAR(10),[LastSampleDate],102)	AS [LastSampleDate]
	FROM @DBSizes 
	WHERE ([AvgSizeMB] > @AvgFileSizeMin 
			AND [PercentChange] NOT BETWEEN -@ChangePct AND @ChangePct
			AND [LastSampleDate] > DATEADD(dd,-2,GETDATE()))
) t
ORDER BY [PercentChange] DESC

GO
/****** Object:  StoredProcedure [rpt].[DBSizes_FileGrowth_DateRange]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			[rpt].[DBSizes_FileGrowth_DateRange]
**	Desc:			Reporting procedure to display database growth between two dates
**	Auth:			Adam Bean (SQLSlayer.com)
**	Notes:			Date range is to ensure only recent databases are counted
**	Date:			2010.08.17
*******************************************************************************
**  License
*******************************************************************************
**  Copyright © SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommonf.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposef. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  YYYYDDMM	Full Name	
********************************************************************************************************/

CREATE PROCEDURE [rpt].[DBSizes_FileGrowth_DateRange]
(
	@Environment		VARCHAR(128)	= NULL
	,@ServerName		VARCHAR(MAX)	= NULL
	,@DBName			VARCHAR(MAX)	= NULL
	,@DateStart			DATETIME		= NULL
	,@DateEnd			DATETIME		= NULL
	,@ExcludeSystem		BIT				= 0
)

AS

SET NOCOUNT ON

---- Check parameters
-- If @DateStart was passed and @DateEnd was not, set @DateEnd to current 
IF @DateStart IS NOT NULL AND @DateEnd IS NULL
	SET @DateEnd = GETDATE()
-- If @DateEnd was passed and @DateStart was not, set @DateStart to @DateEnd minus one month
IF @DateEnd IS NOT NULL AND @DateStart IS NULL
	SET @DateStart = DATEADD(mm,-1,@DateEnd)

SELECT
	t1.[Environment]
	,t1.[ServerName]
	,t.[DBName]
	,t.[FirstSampleDate]
	,t1.[DataSizeMB]													AS [FirstDataSizeMB]
	,t1.[LogSizeMB]														AS [FirstLogSizeMB]
	,ISNULL(t1.[DataSizeUnusedMB],0)									AS [FirstDataSizeUnusedMB]
	,ISNULL(t1.[LogSizeUnusedMB],0)										AS [FirstLogSizeUnusedMB]
	,t.[LastSampleDate]
	,t2.[DataSizeMB]													AS [LastDataSizeMB]
	,t2.[LogSizeMB]														AS [LastLogSizeMB]
	,ISNULL(t2.[DataSizeUnusedMB],0)									AS [LastDataSizeUnusedMB]
	,ISNULL(t2.[LogSizeUnusedMB],0)										AS [LastLogSizeUnusedMB]
	,ISNULL(t2.[DataSizeMB],0) - ISNULL(t1.[DataSizeMB],0)				AS [DataSizeChangeMB]
	,ISNULL(t2.[LogSizeMB],0) - ISNULL(t1.[LogSizeMB],0)				AS [LogSizeChangeMB]
	,ISNULL(t2.[DataSizeUnusedMB],0) - ISNULL(t1.[DataSizeUnusedMB],0)	AS [DataSizeUnusedChangeMB]
	,ISNULL(t2.[LogSizeUnusedMB],0) - ISNULL(t1.[LogSizeUnusedMB],0)	AS [LogSizeUnusedChangeMB]
FROM 
(		
	SELECT
		[ServerName]
		,[DBName]
		,MIN([SampleDate])	AS [FirstSampleDate]
		,MAX([SampleDate])	AS [LastSampleDate]		
	FROM [rpt].[DBSizes_Summary_vw] s
	LEFT JOIN [dbo].[Split_fn](@Environment,',') e
		ON s.[Environment] = e.[item]
	LEFT JOIN [dbo].[Split_fn](@ServerName,',') sn
		ON s.[ServerName] = sn.[item]
	LEFT JOIN [dbo].[Split_fn](@DBName,',') db
		ON s.[DBName] = db.[item]
	WHERE (@DateStart IS NULL AND @DateEnd IS NULL OR [SampleDate] BETWEEN @DateStart AND @DateEnd)
	AND ((e.[item] IS NOT NULL AND @Environment IS NOT NULL) OR @Environment IS NULL) -- Specified environment(s), or all
	AND ((sn.[item] IS NOT NULL AND @ServerName IS NOT NULL) OR @ServerName IS NULL) -- Specified server name(s), or all
	AND ((db.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified database name(s), or all
	AND (([DBName] NOT IN ('admin','master','model','msdb','tempdb') 
			OR [DBName] LIKE '%adventureworks%' 
			OR [DBName] LIKE '%distribution%')
			OR @ExcludeSystem = 0) -- Exclude system databases if specified
	GROUP BY 
		[ServerName]
		,[DBName]
) t
JOIN [rpt].[DBSizes_Summary_vw] t1
	ON t.[ServerName] = t1.[ServerName]
	AND t.[DBName] = t1.[DBName]
	AND t.[FirstSampleDate] = t1.[SampleDate]
JOIN [rpt].[DBSizes_Summary_vw] t2
	ON t.[ServerName] = t2.[ServerName]
	AND t.[DBName] = t2.[DBName]
	AND t.[LastSampleDate] = t2.[SampleDate]

SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [rpt].[DBSizes_Summary]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			[rpt].[DBSizes_Summary]
**	Desc:			Reporting procedure for database sizes report
**	Auth:			Adam Bean (SQLSlayer.com)
**  Dependancies:	dbo.Split_fn
**	Date:			2010.08.11
*******************************************************************************
**  License
*******************************************************************************
**  Copyright © SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  YYYYDDMM	Full Name	
********************************************************************************************************/

CREATE PROCEDURE [rpt].[DBSizes_Summary]
(
	@Environment	VARCHAR(128)	= NULL
	,@ServerName	VARCHAR(MAX)	= NULL
	,@DBName		VARCHAR(MAX)	= NULL
	,@DateStart		DATETIME		= NULL
	,@DateEnd		DATETIME		= NULL
)

AS

SET NOCOUNT ON

SELECT 
	[Environment]
	,[ServerName]
	,[DBName]             
	,[DataSizeMB]
	,[DataSizeMBIncrease]
	,[DataPercentChange]
	,[LogSizeMB]
	,[LogSizeMBIncrease]
	,[LogPercentChange]
	,[DataSizeUnusedMB]
	,[DataSizeUnusedMBIncrease]
	,[LogSizeUnusedMB]
	,[LogSizeUnusedMBIncrease]
	,[SampleDate]
FROM [rpt].[DBSizes_Summary_vw] s
LEFT JOIN [dbo].[Split_fn](@Environment,',') e
	ON s.[Environment] = e.[item]
LEFT JOIN [dbo].[Split_fn](@ServerName,',') sn
	ON s.[ServerName] = sn.[item]
LEFT JOIN [dbo].[Split_fn](@DBName,',') db
	ON s.[DBName] = db.[item]
WHERE (@DateStart IS NULL AND @DateEnd IS NULL OR [SampleDate] BETWEEN @DateStart AND @DateEnd)
AND ((e.[item] IS NOT NULL AND @Environment IS NOT NULL) OR @Environment IS NULL) -- Specified environment(s), or all
AND ((sn.[item] IS NOT NULL AND @ServerName IS NOT NULL) OR @ServerName IS NULL) -- Specified server name(s), or all
AND ((db.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified database name(s), or all

SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [rpt].[DBSizes_Summary_Daily]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			[rpt].[DBSizes_Summary_Daily]
**	Desc:			Reporting procedure for database sizes report to show daily sizes
**	Auth:			Adam Bean (SQLSlayer.com)
**  Dependancies:	dbo.Split_fn
**	Date:			2010.08.16
*******************************************************************************
**  License
*******************************************************************************
**  Copyright © SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  YYYYDDMM	Full Name	
********************************************************************************************************/

CREATE PROCEDURE [rpt].[DBSizes_Summary_Daily]
(
	@Environment	VARCHAR(128)	= NULL
	,@ServerName	VARCHAR(MAX)	= NULL
	,@DBName		VARCHAR(MAX)	= NULL
	,@DateStart		DATETIME		= NULL
	,@DateEnd		DATETIME		= NULL
	,@ExcludeSystem	BIT				= 0
)

AS

SET NOCOUNT ON

SELECT 
	[Environment]
	,[ServerName]
	,[DBName]             
	,[DataSizeMB]
	,[LogSizeMB]
	,[DataSizeUnusedMB]
	,[LogSizeUnusedMB]
	,CONVERT(VARCHAR(10), [SampleDate], 102)				AS [SampleDate]
FROM [rpt].[DBSizes_Summary_vw] s
LEFT JOIN [dbo].[Split_fn](@Environment,',') e
	ON s.[Environment] = e.[item]
LEFT JOIN [dbo].[Split_fn](@ServerName,',') sn
	ON s.[ServerName] = sn.[item]
LEFT JOIN [dbo].[Split_fn](@DBName,',') db
	ON s.[DBName] = db.[item]
	AND s.[ServerName] = sn.[item]
WHERE (@DateStart IS NULL AND @DateEnd IS NULL OR [SampleDate] BETWEEN @DateStart AND @DateEnd)
AND ((e.[item] IS NOT NULL AND @Environment IS NOT NULL) OR @Environment IS NULL) -- Specified environment(s), or all
AND ((sn.[item] IS NOT NULL AND @ServerName IS NOT NULL) OR @ServerName IS NULL) -- Specified server name(s), or all
AND ((db.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified database name(s), or all
AND (([DBName] NOT IN ('admin','pubs','northwind','master','model','msdb','tempdb') 
		OR [DBName] LIKE '%adventureworks%' 
		OR [DBName] LIKE '%distribution%')
		OR @ExcludeSystem = 0) -- Exclude system databases if specified

SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [rpt].[DBSizes_Summary_Monthly]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			[rpt].[DBSizes_Summary_Monthly]
**	Desc:			Reporting procedure for database sizes report to show monthly sizes
**	Auth:			Adam Bean (SQLSlayer.com)
**  Dependancies:	dbo.Split_fn
**	Date:			2010.08.16
*******************************************************************************
**  License
*******************************************************************************
**  Copyright © SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  YYYYDDMM	Full Name	
********************************************************************************************************/

CREATE PROCEDURE [rpt].[DBSizes_Summary_Monthly]
(
	@Environment	VARCHAR(128)	= NULL
	,@ServerName	VARCHAR(MAX)	= NULL
	,@DBName		VARCHAR(MAX)	= NULL
	,@DateStart		DATETIME		= NULL
	,@DateEnd		DATETIME		= NULL
	,@ExcludeSystem	BIT				= 0
)

AS

SET NOCOUNT ON

SELECT 
	[Environment]
	,[ServerName]
	,[DBName]             
	,[DataSizeMB]
	,[LogSizeMB]
	,[DataSizeUnusedMB]
	,[LogSizeUnusedMB]
	,CONVERT(VARCHAR(10), [SampleDate], 102)				AS [SampleDate]
FROM [rpt].[DBSizes_Summary_vw] s
LEFT JOIN [dbo].[Split_fn](@Environment,',') e
	ON s.[Environment] = e.[item]
LEFT JOIN [dbo].[Split_fn](@ServerName,',') sn
	ON s.[ServerName] = sn.[item]
LEFT JOIN [dbo].[Split_fn](@DBName,',') db
	ON s.[DBName] = db.[item]
WHERE DATEPART(dd, [SampleDate]) = 1
AND (@DateStart IS NULL AND @DateEnd IS NULL OR [SampleDate] BETWEEN @DateStart AND DATEADD(dd,+1,@DateEnd))
AND ((e.[item] IS NOT NULL AND @Environment IS NOT NULL) OR @Environment IS NULL) -- Specified environment(s), or all
AND ((sn.[item] IS NOT NULL AND @ServerName IS NOT NULL) OR @ServerName IS NULL) -- Specified server name(s), or all
AND ((db.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified database name(s), or all
AND (([DBName] NOT IN ('admin','master','model','msdb','tempdb') 
		OR [DBName] LIKE '%adventureworks%' 
		OR [DBName] LIKE '%distribution%')
		OR @ExcludeSystem = 0) -- Exclude system databases if specified

SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [rpt].[JobHistory]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			[rpt].[JobHistory]
**	Desc:			Description
**	Auth:			Adam Bean (SQLSlayer.com)
**  Dependancies:	If available
**  Parameters:		Describe each parameter
**  Usage:			Sample usage
**  Notes:			If required
**	Date:			YYYY.MM.DD
*******************************************************************************
**  License
*******************************************************************************
**  Copyright © SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  YYYYDDMM	Full Name	
********************************************************************************************************/

CREATE PROCEDURE [rpt].[JobHistory]
(
	@ServerName			NVARCHAR(1024)	= NULL
	,@ServerNameexclude	NVARCHAR(1024)	= NULL
	,@JobName			NVARCHAR(1024)	= NULL
	,@JobNameExclude	NVARCHAR(1024)	= NULL
)

AS

SET NOCOUNT ON

SELECT
	si.[ServerName]
	,sj.[Name]
	,sj.[Enabled]
	,CASE jh.[run_status]
		WHEN 0 THEN 'Failed'
		WHEN 1 THEN 'Succeeded' 
		WHEN 2 THEN 'Retry' 
		WHEN 3 THEN 'Canceled'
		WHEN 4 THEN 'In progress'
	END AS [Last_Run_Outcome]
	,MAX(jh.[run_datetime])									AS [Last_Run_Date]
	,jh.[run_duration]										AS [Last_Run_Time]
	,MIN(jh.[run_duration])									AS [Min_Run_Time]
	,AVG(jh.[run_duration])									AS [Avg_Run_Time]
	,MAX(jh.[run_duration])									AS [Max_Run_Time]
	,SUM(CASE WHEN jh.[run_status] = 1 THEN 1 ELSE 0 END)	AS [Successful]
	,SUM(CASE WHEN jh.[run_status] = 0 THEN 1 ELSE 0 END)	AS [Failed]
	,SUM(CASE WHEN jh.[run_status] = 3 THEN 1 ELSE 0 END)	AS [Cancelled]
FROM [hist].[Jobs_SQL_JobHistory] jh
INNER JOIN [hist].[Jobs_SQL_Jobs] sj
	ON sj.[HistJobID] = jh.[HistJobID]
INNER JOIN [hist].[ServerInventory_ServerIDs] si
	ON sj.[HistServerID] = si.[HistServerID]
LEFT JOIN [dbo].[Split_fn](@ServerName,',') s
	ON si.[ServerName] = s.[item]
LEFT JOIN [dbo].[Split_fn](@ServerNameExclude,',') se
	ON si.[ServerName] = se.[item]
LEFT JOIN [dbo].[Split_fn](@JobName,',') j
	ON sj.[name] = j.[item]
LEFT JOIN [dbo].[Split_fn](@JobNameExclude,',') je
	ON sj.[name] = je.[item]
WHERE [step_id] != 0
AND ((s.[item] IS NOT NULL AND @ServerName IS NOT NULL) OR @ServerName IS NULL) -- Specified server(s), or all
AND ((j.[item] IS NOT NULL AND @JobName IS NOT NULL) OR @JobName IS NULL) -- Specified job(s), or all
AND se.[item] IS NULL -- All but excluded server(s)
AND je.[item] IS NULL -- All but excluded job(s)
GROUP BY si.[ServerName], sj.[Name], sj.[Enabled], jh.[run_status], jh.[run_duration], jh.[run_datetime]
ORDER BY [ServerName], [name], [run_datetime]

SET NOCOUNT OFF


GO
/****** Object:  StoredProcedure [rpt].[ServerInventory_SQL_Configurations_CompareServers]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**  Name:			[rpt].[ServerInventory_SQL_Configurations_CompareServers]
**  Desc:			Compares the current system configuration of two different servers
**  Auth:			Matt Stanford
**  Date:			2009-08-12
**  Debug:			
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
********************************************************************************************************/
CREATE PROCEDURE [rpt].[ServerInventory_SQL_Configurations_CompareServers]
(
	@ServerA			VARCHAR(200)
	,@ServerB			VARCHAR(200)
	,@ShowDifferentOnly	BIT = 0
)
AS

--DECLARE 
--	@ServerA VARCHAR(200)
--	,@ServerB VARCHAR(200)

IF OBJECT_ID('tempdb.dbo.#ServerAVals') IS NOT NULL
	DROP TABLE #ServerAVals

IF OBJECT_ID('tempdb.dbo.#ServerBVals') IS NOT NULL
	DROP TABLE #ServerBVals

IF OBJECT_ID('tempdb.dbo.#ConfigOpts') IS NOT NULL
	DROP TABLE #ConfigOpts
	
-- Collect the union of the options between the two servers
SELECT DISTINCT
	[configuration_id]
	,[name]
	,[description]
	,[minimum]
	,[maximum]
	,[is_dynamic]
	,[is_advanced]
INTO #ConfigOpts
FROM [hist].[ServerInventory_SQL_ConfigurationValues_Current_vw]
WHERE [ServerName] IN (@ServerA,@ServerB)

-- Get all of Server A's current values
SELECT
	[ServerName]
	,[configuration_id]
	,[value]
	,[value_in_use]
	,[DateCreated]
	,[DateLastSeenOn]
INTO #ServerAVals
FROM [hist].[ServerInventory_SQL_ConfigurationValues_Current_vw]
WHERE [ServerName] = @ServerA

-- Get all of Server B's current values
SELECT
	[ServerName]
	,[configuration_id]
	,[value]
	,[value_in_use]
	,[DateCreated]
	,[DateLastSeenOn]
INTO #ServerBVals
FROM [hist].[ServerInventory_SQL_ConfigurationValues_Current_vw]
WHERE [ServerName] = @ServerB

SELECT
	@ServerA												AS [ServerA]
	,@ServerB												AS [ServerB]
	,co.[configuration_id]
	,co.[name]
	,co.[description]
	,co.[minimum]
	,co.[maximum]
	,co.[is_dynamic]
	,co.[is_advanced]
	,a.[value]												AS [ServerA_value]
	,b.[value]												AS [ServerB_value]
	,a.[value_in_use]										AS [ServerA_value_in_use]
	,b.[value_in_use]										AS [ServerB_value_in_use]
	,CASE 
		WHEN a.[value] = b.[value] 
			AND a.[value_in_use] = b.[value_in_use]
			THEN 0
		ELSE 1
	END														AS [is_different]
FROM #ConfigOpts co
LEFT OUTER JOIN #ServerAVals a
	ON a.[configuration_id] = co.[configuration_id]
LEFT OUTER JOIN #ServerBVals b
	ON b.[configuration_id] = co.[configuration_id]
WHERE (@ShowDifferentOnly = 0)
OR (@ShowDifferentOnly = 1 AND 
CASE 
	WHEN a.[value] = b.[value] 
		AND a.[value_in_use] = b.[value_in_use]
		THEN 0
	ELSE 1
END = 1
)

GO
/****** Object:  StoredProcedure [rpt].[SpaceUsed_AvgDBGrowthPerDay]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************************
**	Name:			[rpt].[SpaceUsed_AvgDBGrowthPerDay]
**	Desc:			Reporting procedure to get average database growth per day
**	Auth:			Matt Stanford
**	Debug:			
DECLARE
	@Exclusions	[DatabaseListType]
	
INSERT INTO @Exclusions
VALUES('OECArchive'),('OECImports'),('admin'),('master'),('model'),('OECFaxManager'),('tempdb'),('OECConquest')

EXEC [report].[SpaceUsed_AvgDBGrowthPerDay] '2009-01-17', '2009-04-01', 'S227938HZ1SQL1\Legacy', @Exclusions

**	Date:			2009-04-28
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090716	Matt Stanford	Moved from "report" schema to "rpt"
********************************************************************************************************/
CREATE PROCEDURE [rpt].[SpaceUsed_AvgDBGrowthPerDay] (
	@StartDate		DATETIME
	,@EndDate		DATETIME
	,@ServerName	VARCHAR(200)
	,@Exclusions	[DatabaseListType] READONLY
)
AS 
SET NOCOUNT ON 
/*
-- DEBUG!!!
DECLARE
	@StartDate		DATETIME
	,@EndDate		DATETIME
	,@ServerName	VARCHAR(200)
	,@Exclusions	[DatabaseListType]
	
INSERT INTO @Exclusions
VALUES('OECArchive'),('OECImports'),('admin'),('master'),('model'),('OECFaxManager'),('tempdb'),('OECConquest')
	
SET @StartDate = '2009-01-17'
SET @EndDate = '2009-04-01'
SET @ServerName = 's227938hz1sql1\legacy'
*/
----
DECLARE 
	@InitValue		BIGINT
	,@FinalValue	BIGINT
	,@Diff			BIGINT
	,@Days			INT
	
SET @Days = DATEDIFF(day,@StartDate,@EndDate)
	
SELECT
	@InitValue = SUM(ds.[DataSizeMB]) + SUM(ds.[LogSizeMB])
FROM [hist].[SpaceUsed_DatabaseSizes_vw] ds
LEFT OUTER JOIN @Exclusions ex
	ON ds.[DBName] = ex.[DBName]
WHERE ex.[DBName] IS NULL
AND ds.[SampleDate] BETWEEN @StartDate AND DATEADD(day,1,@StartDate)
AND ds.[ServerName] = @ServerName

SELECT
	@FinalValue = SUM(ds.[DataSizeMB]) + SUM(ds.[LogSizeMB])
FROM [hist].[SpaceUsed_DatabaseSizes_vw] ds
LEFT OUTER JOIN @Exclusions ex
	ON ds.[DBName] = ex.[DBName]
WHERE ex.[DBName] IS NULL
AND ds.[SampleDate] BETWEEN DATEADD(day,-1,@EndDate) AND @EndDate
AND ds.[ServerName] = @ServerName

SET @Diff = @FinalValue - @InitValue

SELECT 
	@InitValue AS [Initial Size (MB)]
	,@FinalValue AS [Final Size (MB)]
	,CAST(@Diff AS DECIMAL(12,2))/@Days AS [Size (MB) Per Day Growth]


GO
/****** Object:  StoredProcedure [rpt].[TestBackupAudit]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
* Name
	[rpt].[TestBackupAudit]

* Author
	Adam Bean
	
* Date
	2012.06.18
	
* Synopsis
	Raw data for all SQL Server backups
	
* Description
	Summary data displaying actual, expected and missed backups by server, database, day

* Examples
	EXEC [rpt].[BackupAudit] @ServerName = 'X,Y,Z'
	EXEC [rpt].[BackupAudit] @ServerName = 'X,Y,Z', @DBName = 'master,model'
	EXEC [rpt].[BackupAudit] @ServerName = 'X', @HalfHourTlogBackupServers = 'X'
	EXEC [rpt].[BackupAudit] @ServerName = 'X', @DBNameExclude = 'admin', @DateStart = '2012.01.01', @DateEnd = '2012.05.01'

* Dependencies
	[dbo].[Split_fn]

* Parameters
	@ServerName = Server(s) to be reindexed (CSV supported, NULL for all, accepts % for searching)
	@ServerNameExclude = Server(s) to excluded (CSV supported, NULL for all, accepts % for searching)
	@DBName = Database(s) to be reindexed (CSV supported, NULL for all, accepts % for searching)
	@DBNameExclude = Database(s) to excluded (CSV supported, NULL for all, accepts % for searching)
	@QuarterHourTlogBackupServers = Server(s) that run tlog backups every quarter hour
	@HalfHourTlogBackupServers = Server(s) that run tlog backups every half hour
	@PhysicalDeviceName = Wild card match on the backup device name (for example "TDP" for searching for all TSM (Tivoli) backups)
	@DeviceType = Query on the device type from backupmediafamily (for example 2 = disk, 5 = tape)
	@DateStart = Start of date range to query on
	@DateEnd = End of date range to query on
	
* Notes
	Data is reliant upon DBAC data collector "Collect-BackupHistory"
	@PhysicalDeviceName should be replaced by device_type in backupmediafamily after data collectors have been updated
	@QuarterHourTlogBackupServers is in place to avoid having hard coded server names for servers that have tlog backups every half an hour
	@HalfHourTlogBackupServers is in place to avoid having hard coded server names for servers that have tlog backups every half an hour

*******************************************************************************
* License
*******************************************************************************
	Copyright © SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20120807	Meghan Brooks	Changed logic on only retrieving active databases. Needed to be >= @EndDate, not a week prior. Was giving inaccurate numbers
	20120627	Adam Bean		Updated for new schema for the backup repo and added @DeviceType
	20120702	Adam Bean		Set [DBActual] and [TlogActual] to have a DEFAULT of 0 because the join wasn't setting it to 0 when the data didn't exist
	20120710	Adam Bean		Fixed "TOP N can not be negative" error by adding a check on @DateEnd (line 151)
******************************************************************************/

CREATE PROCEDURE [rpt].[TestBackupAudit]
(
	@ServerName						VARCHAR(MAX)	= NULL
	,@ServerNameExclude				VARCHAR(MAX)	= NULL
	,@DBName						VARCHAR(MAX)	= NULL
	,@DBNameExclude					VARCHAR(MAX)	= NULL
	,@HalfHourTlogBackupServers		VARCHAR(512)	= NULL
	,@QuarterHourTlogBackupServers	VARCHAR(512)	= NULL
	,@PhysicalDeviceName			VARCHAR(16)		= NULL
	,@DeviceType					VARCHAR(8)		= NULL
	,@DateStart						DATETIME		= NULL
	,@DateEnd						DATETIME		= NULL
)

AS

SET NOCOUNT ON 

DECLARE
	@FirstBackupDate				DATE
	,@RecoveryModel					VARCHAR(16)
	,@IsTlogShipped					BIT

-- If date range variables were not set, set to first day of month (start) to yesterday (end)
IF @DateStart IS NULL SET @DateStart = CONVERT(VARCHAR,DATEADD(MONTH, DATEDIFF(MONTH,0,GETDATE()), 0),102)
IF @DateEnd IS NULL SET @DateEnd = CONVERT(VARCHAR,DATEADD(DAY,DATEDIFF(DAY,1,GETDATE()),0),102)

-- temp table cleanup
IF OBJECT_ID('tempdb.dbo.#MinBackupDate') IS NOT NULL
   DROP TABLE #MinBackupDate
IF OBJECT_ID('tempdb.dbo.#Backups') IS NOT NULL
   DROP TABLE #Backups

-- temp table to house the first time a backup took place
CREATE TABLE #MinBackupDate
(
	[ServerName]					VARCHAR(256)
	,[DBName]						VARCHAR(256)
	,[FirstBackupDate]				DATETIME
)

-- Main temp table housing our backup data
CREATE TABLE #Backups
(
	[ServerName]					VARCHAR(256)	NULL
	,[DBName]						VARCHAR(128)	NULL
	,[RecoveryModel]				VARCHAR(16)		NULL
	,[IsTlogShipped]				BIT				NULL
	,[DBExpected]					INT	DEFAULT(1)
	,[DBActual]						INT DEFAULT(0)
	,[DBMissed]						INT				NULL
	,[TlogExpected]					INT				NULL
	,[TlogActual]					INT	DEFAULT(0)
	,[TlogMissed]					INT				NULL
	,[CalendarDate]					SMALLDATETIME		
)

-- Get the first time a given server/database had a backup
INSERT INTO #MinBackupDate
SELECT 
	bh.[ServerName]
	,bh.[DBName]								AS [DBName]
	,MIN(CONVERT(VARCHAR,bh.[StartDate],102))	AS [FirstBackupDate]
FROM [hist].[Backups_History_vw] bh
JOIN [dbo].[ServerInventory_SQL_AllServers_vw] s									-- Only retrieve active servers
	ON s.[FullName] = bh.[ServerName]
LEFT JOIN [dbo].[Split_fn](@ServerName,',') sn
	ON bh.[ServerName] = sn.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@ServerNameExclude,',') sne
	ON bh.[ServerName] = sne.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@DBName,',') db
	ON bh.[DBName] = db.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
	ON bh.[DBName] = de.[item] COLLATE DATABASE_DEFAULT
WHERE (@DateStart IS NULL OR CONVERT(VARCHAR,bh.[StartDate],102) = @DateStart)		-- Start at or greater than @DateStart
AND (@DateEnd IS NULL OR CONVERT(VARCHAR,bh.[StartDate],102) <= @DateEnd)			-- Only retrieve backups prior @DateEnd
AND ((sn.[item] IS NOT NULL AND @ServerName IS NOT NULL) OR @ServerName IS NULL)	-- Specified servers, or all
AND ((db.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL)			-- Specified databases, or all
AND sne.[item] IS NULL																-- All but excluded servers
AND de.[item] IS NULL																-- All but excluded databases
GROUP BY
	bh.[ServerName]
	,bh.[DBName]

-- Add indexing to our temp table
CREATE NONCLUSTERED INDEX [#IX__MinBackups__ServerName__DBName] ON #MinBackupDate
(
	[ServerName]
	,[DBName]
)

-- Now we need to populate a record for every server, database, day
-- This is to handle where a day passed and no #Backups occured on a given date
DECLARE #dbs CURSOR LOCAL STATIC FOR
SELECT
	mbd.[ServerName]
	,mbd.[DBName]
	,mbd.[FirstBackupDate]
	,pvt.[recovery_model_desc]
	,pvt.[is_tlog_shipped]
FROM
(
	SELECT 
		da.[ServerName]
		,da.[DBName]
		,da.[AttributeName]
		,CAST(da.[AttributeValue] AS VARCHAR(160)) AS [AttributeValue]
		,ROW_NUMBER () OVER (PARTITION BY da.[ServerName], da.[DBName], da.[AttributeName] ORDER BY da.[DateLastSeenOn] DESC) AS [RowNo]
	FROM [hist].[ServerInventory_SQL_DatabaseAttributes_vw] da
	LEFT JOIN [dbo].[Split_fn](@ServerName,',') sn
		ON da.[ServerName] = sn.[item] COLLATE DATABASE_DEFAULT
	LEFT JOIN [dbo].[Split_fn](@ServerNameExclude,',') sne
		ON da.[ServerName] = sne.[item] COLLATE DATABASE_DEFAULT
	LEFT JOIN [dbo].[Split_fn](@DBName,',') db
		ON da.[DBName] = db.[item] COLLATE DATABASE_DEFAULT
	LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
		ON da.[DBName] = de.[item] COLLATE DATABASE_DEFAULT
	WHERE [AttributeName] IN ('is_tlog_shipped', 'recovery_model_desc','state_desc') 
	AND da.[DateLastSeenOn] >= DATEADD(WEEK,-1,GETDATE())--@DateEnd --DATEADD(WEEK,-1,GETDATE())								-- Only retrieve active databases
	AND da.[file_id] IS NULL															-- Only want the database level attributes, not per file
	AND ((sn.[item] IS NOT NULL AND @ServerName IS NOT NULL) OR @ServerName IS NULL)	-- Specified servers, or all
	AND ((db.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL)			-- Specified databases, or all
	AND sne.[item] IS NULL																-- All but excluded servers
	AND de.[item] IS NULL																-- All but excluded databases
) src
PIVOT
(
	MAX([AttributeValue])
	FOR [AttributeName] IN ([state_desc], [recovery_model_desc], [is_tlog_shipped], [source_database_id])
) pvt
JOIN #MinBackupDate mbd
	ON pvt.[ServerName] = mbd.[ServerName]
	AND pvt.[DBName] = mbd.[DBName]
--WHERE @DateStart >= mbd.[FirstBackupDate]	-- Can't report on backups that haven't taken place before the @DateStart [[keeping this here for now as this was the root of the TOP N negative issue]]
WHERE pvt.[state_desc] = 'ONLINE'			-- Only retrieve online databases
AND pvt.[RowNo] = 1							-- Only retrieve the most active record
ORDER BY 1, 2

OPEN #dbs
FETCH NEXT FROM #dbs INTO @ServerName, @DBName, @FirstBackupDate, @RecoveryModel, @IsTlogShipped
WHILE @@FETCH_STATUS = 0
BEGIN

	-- Populate our staging table with all possible dates for every database/server
	INSERT INTO #Backups
	([CalendarDate], [ServerName], [DBName], [RecoveryModel], [IsTlogShipped])
	SELECT TOP 
		(DATEDIFF(DAY,CONVERT(VARCHAR,@FirstBackupDate,102),@DateEnd) + 1) DATEADD(DAY,ROW_NUMBER() OVER (ORDER BY [CalendarDate]) -1, @FirstBackupDate) AS [CalendarDate]
		,@ServerName
		,@DBName
		,@RecoveryModel
		,@IsTlogShipped
	FROM [dbo].[Calendar]

FETCH NEXT FROM #dbs INTO @ServerName, @DBName, @FirstBackupDate, @RecoveryModel, @IsTlogShipped
END
CLOSE #dbs
DEALLOCATE #dbs

-- Add indexing to our temp table
CREATE NONCLUSTERED INDEX [#IX__Backups__RecoveryModel] ON #Backups
(
	[RecoveryModel]
)

CREATE NONCLUSTERED INDEX [#IX__Backups__ServerName__DBName__CalendarDate] ON #Backups
(
	[ServerName]
	,[DBName]
	,[CalendarDate]
)

-- Now we need to update the data with the expected amount of transaction log backups
-- This was broken off seperately to allow for ease of updates for specific environment configurations
UPDATE #Backups
	SET [TlogExpected] = 
		CASE
			WHEN [RecoveryModel] != 'SIMPLE' AND [IsTlogShipped] = 1								THEN 0	-- Transaction log shipping (where we don't expect the file system #Backups to count as the DR #Backups)
			WHEN [RecoveryModel] != 'SIMPLE' AND [ServerName] IN (@HalfHourTlogBackupServers)		THEN 48	-- Environment specific (30 minute tlog backups)
			WHEN [RecoveryModel] != 'SIMPLE' AND [ServerName] IN (@QuarterHourTlogBackupServers)	THEN 96	-- Environment specific (15 minute tlog backups)
			WHEN [RecoveryModel] != 'SIMPLE'														THEN 24	-- May not fit all environments; however we should expect 24 tlogs a day (1 per hour)
			ELSE 0
		END

-- Now find the actual amount of database backups that took place per server/database/day
UPDATE bu
	SET 
		bu.[DBActual] = ISNULL(act.[DBActual],0)
		-- If more backups took place than expected, return a 0 vs. a negative
		,bu.[DBMissed] = 
		(
			CASE
				WHEN (bu.[DBExpected] - ISNULL(act.[DBActual],0)) < 0 THEN 0
				ELSE (bu.[DBExpected] - ISNULL(act.[DBActual],0))
			END
		)
FROM #Backups bu
LEFT JOIN
(
	SELECT 
		COUNT(bh.[BackupType])					AS [DBActual]
		,bu.[ServerName]
		,bu.[DBName]
		,CONVERT(VARCHAR,bu.[CalendarDate],102) AS [CalendarDate]
	FROM #Backups bu
	LEFT JOIN [hist].[Backups_History_vw] bh
		ON bu.[ServerName] = bh.[ServerName]
		AND bu.[DBName] = bh.[DBName]
		AND CONVERT(VARCHAR,bu.[CalendarDate],102) = CONVERT(VARCHAR,bh.[StartDate],102)
		AND bh.[BackupType] != 'L'
		AND (bh.[PhysicalDeviceName] LIKE '%' + @PhysicalDeviceName + '%' OR @PhysicalDeviceName IS NULL)
	LEFT JOIN [dbo].[Split_fn](@DeviceType,',') dt
		ON bh.[DeviceType] = dt.[item] COLLATE DATABASE_DEFAULT
	WHERE ((dt.[item] IS NOT NULL AND @DeviceType IS NOT NULL) OR @DeviceType IS NULL)	-- Specified device types, or all
	GROUP BY
		bu.[ServerName]
		,bu.[DBName]
		,CONVERT(VARCHAR,bu.[CalendarDate],102)
) act
	ON bu.[ServerName] = act.[ServerName]
	AND bu.[DBName] = act.[DBName]
	AND CONVERT(VARCHAR,bu.[CalendarDate],102) = CONVERT(VARCHAR,act.[CalendarDate],102)

-- Now find the actual amount of transaction log backups that took place per server/database/day
UPDATE bu
	SET 
		bu.[TlogActual] = ISNULL(act.[TlogActual],0)
		-- If more backups took place than expected, return a 0 vs. a negative
		,bu.[TlogMissed] = 
		(
			CASE
				WHEN (bu.[TlogExpected] - ISNULL(act.[TlogActual],0)) < 0 THEN 0
				ELSE (bu.[TlogExpected] - ISNULL(act.[TlogActual],0))
			END
		)
FROM #Backups bu
LEFT JOIN
(
	SELECT 
		COUNT(bh.[BackupType])					AS [TlogActual]
		,bu.[ServerName]
		,bu.[DBName]
		,CONVERT(VARCHAR,bu.[CalendarDate],102)	AS [CalendarDate]
	FROM #Backups bu
	LEFT JOIN [hist].[Backups_History_vw] bh
		ON bu.[ServerName] = bh.[ServerName]
		AND bu.[DBName] = bh.[DBName]
		AND CONVERT(VARCHAR,bu.[CalendarDate],102) = CONVERT(VARCHAR,bh.[StartDate],102)
		AND bh.[BackupType] = 'L'
		AND (bh.[PhysicalDeviceName] LIKE '%' + @PhysicalDeviceName + '%' OR @PhysicalDeviceName IS NULL)
	GROUP BY
		bu.[ServerName]
		,bu.[DBName]
		,CONVERT(VARCHAR,bu.[CalendarDate],102)
) act
	ON bu.[ServerName] = act.[ServerName]
	AND bu.[DBName] = act.[DBName]
	AND CONVERT(VARCHAR,bu.[CalendarDate],102) = CONVERT(VARCHAR,act.[CalendarDate],102)

-- Return results
SELECT
	[ServerName]
	,[DBName]
	,[RecoveryModel]
	,[DBExpected]
	,[DBActual]
	,[DBMissed]
	,[TlogExpected]
	,[TlogActual]
	,[TlogMissed]
	,[CalendarDate]
FROM #Backups
ORDER BY [ServerName], [DBName], [CalendarDate]

SET NOCOUNT OFF


GO
/****** Object:  StoredProcedure [rpt].[who_Detail]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************************************************************************
**	Name:			[rpt].[who_Detail]
**	Desc:			Procedure to display detail in sub report of Connections Summary SSRS report
**	Auth:			Adam Bean (SQLSlayer.com)
**	Date:			2009.04.03
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:		Description:
**	--------	--------	---------------------------------------
**  
********************************************************************************************************/

CREATE PROCEDURE [rpt].[who_Detail]
(
	@ID INT
)

AS

SET NOCOUNT ON

SELECT 
	*
FROM [rpt].[who_Summary_vw]
WHERE [ID] = @ID

SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [rpt].[who_Summary]    Script Date: 5/22/2017 1:13:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************************************************************************
**	Name:			[rpt].[who_Summary]
**	Desc:			Procedure for who SSRS report
**	Auth:			Adam Bean (SQLSlayer.com)
**	Date:			2009.04.01
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:		Description:
**	--------	--------	---------------------------------------
**  
********************************************************************************************************/

CREATE PROCEDURE [rpt].[who_Summary]
(
	@ServerName		VARCHAR(MAX)	= NULL
	,@DBName		VARCHAR(MAX)	= NULL
	,@Login			VARCHAR(MAX)	= NULL
	,@HostName		VARCHAR(MAX)	= NULL	
	,@ProgramName	VARCHAR(MAX)	= NULL	
	,@DateStart		DATETIME		= NULL
	,@DateEnd		DATETIME		= NULL
)

AS

SET NOCOUNT ON

SELECT 
	[ID]
	,[ServerName]
	,[DBName]
	,[Query]
	,[Login]
	,[HostName]
	,[StartTime]
	,[ProgramName]
	,[DateCreated]
FROM [rpt].[who_Summary_vw]
WHERE (@ServerName IS NULL OR [ServerName] IN (SELECT [item] AS [ServerName] FROM [admin].[dbo].[Split_fn](@ServerName,',')))
AND (@DBName IS NULL OR [DBName] IN (SELECT [item] AS [ServerName] FROM [admin].[dbo].[Split_fn](@DBName,',')))
AND (@Login IS NULL OR [Login] IN (SELECT [item] AS [ServerName] FROM [admin].[dbo].[Split_fn](@Login,',')))
AND (@HostName IS NULL OR [HostName] IN (SELECT [item] AS [ServerName] FROM [admin].[dbo].[Split_fn](@HostName,',')))
AND (@ProgramName IS NULL OR [ProgramName] IN (SELECT [item] AS [ServerName] FROM [admin].[dbo].[Split_fn](@ProgramName,',')))
AND (@DateStart IS NULL AND @DateEnd IS NULL OR [DateCreated] BETWEEN @DateStart AND @DateEnd)

SET NOCOUNT OFF

GO
EXEC [DBACentral].sys.sp_addextendedproperty @name=N'Version', @value=N'1.4.10' 
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "det"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 229
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 6
               Left = 267
               Bottom = 101
               Right = 437
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 102
               Left = 267
               Bottom = 197
               Right = 437
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'hist', @level1type=N'VIEW',@level1name=N'SpaceUsed_DatabaseSizes_vw'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'hist', @level1type=N'VIEW',@level1name=N'SpaceUsed_DatabaseSizes_vw'
GO
USE [master]
GO
ALTER DATABASE [DBACentral] SET  READ_WRITE 
GO
